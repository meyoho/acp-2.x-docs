# 概述

## 读者对象

适用于具备基本的 linux、容器、Kubernetes 知识，想要安装和配置平台的用户。包括：

* 平台管理员

* 平台使用者


##  文档目的

本文档针对 TKE for Alauda Container Platform v2.6 容器平台，提供部署、配置、验证、使用、巡检、排障等日常维护操作的指导。

## 修订记录

| 版本 | 修订日期   | 修订人  | 修订内容 | 审核人 | 文档生效日期 |
| ---- | ---------- | ------ | -------- | ------ | ------------ |
| v0.1    | 2019-12-30 | 王洪涛 | 初稿。  |        | 2020-02-05   |
| v0.2    | 2020-02-10 | 王洪涛 | 修复错误。 |        | 2020-02-10   |
| v1.0    | 2020-03-05 | 王洪涛 | 增添内容 |        | |



<div STYLE="page-break-after: always;"></div>

# 平台部署、配置及验证

## 平台部署

请参考 《TKE for Alauda Container Platform v2.6 部署文档》说明部署平台。



## 平台功能验证

请参考 《TKE for Alauda Container Platform v2.6 部署文档》中 “平台配置及测试” 章节中的内容验证平台功能。

## 平台配置管理

### 用 ovn (版本 v0.10.2) 替换 TKE 集群 flannel/galaxy 网络

#### 备份及删除现有网络的 ds 

**注意**：需要在集群的 master 上操作。

1. 先备份 flannel/galaxy 的 ds，由于 flannel 的网段数据存在 node 的 annotation 中，模式存在 configmap 中，因此只需要备份 ds 的 yaml 即可：

	```
	mkdir /cpaas/backup ; cd /cpaas/backup
	kubectl -n kube-system get ds flannel -o yaml > flannel.bak.yaml
	kubectl -n kube-system get ds galaxy-daemonset -o yaml >  galaxy.bak.yaml
	```

2. 删除 flannel 的 ds，删除各节点上的 cni0 和 flannel.1 及残余路由。

	```
	kubectl -n kube-system delete ds flannel
	kubectl -n kube-system delete ds galaxy-daemonset
	 
	ip link del cni0
	ip link del flannel.1
	
	# 如果有cni0或flannel.1的残留的话
	ip route del xxxx
	
	# 备份并删除cni配置
	cp -r /etc/cni/net.d/ .
	rm -f /etc/cni/net.d/*
	
	#重启 kubelet
	systemctl restart kubelet
	```


#### 安装 ovn 

**注意**：需要在集群的 master 上操作。

1. 给运行 ovs db 的节点添加 label，并查看标签。

	```
	# 打标签，请将 node1 node2 替换成要运行 ovs db 的节点，一般选择在 master 上运行 ovs db
	kubectl label no node1,node2 kube-ovn/role=master
	# 查看标签
	kubectl get no --show-labels
	```

2. 创建 ovn 相关资源。

	**说明**：Kube-OVN Controller 和 CNIServer 中有大量可配参数，这里为了快速上手，我们不做更改。默认配置下 Kube-OVN 会使用 10.16.0.0/16 作为默认子网，100.64.0.1/16 作为主机和 Pod 通信子网，使用 Kubernetes 中的 Node 主网卡作为 Pod 流量通信使用网卡，并开启流量镜像功能。

	```
	cat <<EOF > kube-ovn-crd.yaml
	apiVersion: apiextensions.k8s.io/v1beta1
	kind: CustomResourceDefinition
	metadata:
	  name: ips.kubeovn.io
	spec:
	  group: kubeovn.io
	  version: v1
	  scope: Cluster
	  names:
	    plural: ips
	    singular: ip
	    kind: IP
	    shortNames:
	      - ip
	  additionalPrinterColumns:
	    - name: IP
	      type: string
	      JSONPath: .spec.ipAddress
	    - name: Mac
	      type: string
	      JSONPath: .spec.macAddress
	    - name: Node
	      type: string
	      JSONPath: .spec.nodeName
	    - name: Subnet
	      type: string
	      JSONPath: .spec.subnet
	---
	apiVersion: apiextensions.k8s.io/v1beta1
	kind: CustomResourceDefinition
	metadata:
	  name: subnets.kubeovn.io
	spec:
	  group: kubeovn.io
	  version: v1
	  scope: Cluster
	  names:
	    plural: subnets
	    singular: subnet
	    kind: Subnet
	    shortNames:
	      - subnet
	  subresources:
	    status: {}
	  additionalPrinterColumns:
	    - name: Protocol
	      type: string
	      JSONPath: .spec.protocol
	    - name: CIDR
	      type: string
	      JSONPath: .spec.cidrBlock
	    - name: Private
	      type: boolean
	      JSONPath: .spec.private
	    - name: NAT
	      type: boolean
	      JSONPath: .spec.natOutgoing
	    - name: Default
	      type: boolean
	      JSONPath: .spec.default
	    - name: GatewayType
	      type: string
	      JSONPath: .spec.gatewayType
	    - name: Used
	      type: integer
	      JSONPath: .status.usingIPs
	    - name: Available
	      type: integer
	      JSONPath: .status.availableIPs
	  validation:
	    openAPIV3Schema:
	      properties:
	        spec:
	          required: ["cidrBlock"]
	          properties:
	            cidrBlock:
	              type: "string"
	            gateway:
	              type: "string"
	EOF
	
	cat <<EOF >kube-ovn-ovn.yaml
	apiVersion: v1
	kind: Namespace
	metadata:
	  name: kube-ovn
	
	---
	apiVersion: v1
	kind: ConfigMap
	metadata:
	  name: ovn-config
	  namespace: kube-ovn
	
	---
	apiVersion: v1
	kind: ServiceAccount
	metadata:
	  name: ovn
	  namespace: kube-ovn
	
	---
	apiVersion: rbac.authorization.k8s.io/v1
	kind: ClusterRole
	metadata:
	  annotations:
	    rbac.authorization.k8s.io/system-only: "true"
	  name: system:ovn
	rules:
	  - apiGroups:
	      - "kubeovn.io"
	    resources:
	      - subnets
	      - subnets/status
	      - ips
	    verbs:
	      - "*"
	  - apiGroups:
	      - ""
	    resources:
	      - pods
	      - namespaces
	      - nodes
	      - configmaps
	    verbs:
	      - create
	      - get
	      - list
	      - watch
	      - patch
	      - update
	  - apiGroups:
	      - ""
	      - networking.k8s.io
	      - apps
	    resources:
	      - networkpolicies
	      - services
	      - endpoints
	      - statefulsets
	      - daemonsets
	    verbs:
	      - get
	      - list
	      - watch
	  - apiGroups:
	      - ""
	    resources:
	      - events
	    verbs:
	      - create
	      - patch
	      - update
	
	---
	apiVersion: rbac.authorization.k8s.io/v1
	kind: ClusterRoleBinding
	metadata:
	  name: ovn
	roleRef:
	  name: system:ovn
	  kind: ClusterRole
	  apiGroup: rbac.authorization.k8s.io
	subjects:
	  - kind: ServiceAccount
	    name: ovn
	    namespace: kube-ovn
	
	---
	kind: Service
	apiVersion: v1
	metadata:
	  name: ovn-nb
	  namespace: kube-ovn
	spec:
	  ports:
	    - name: ovn-nb
	      protocol: TCP
	      port: 6641
	      targetPort: 6641
	  type: ClusterIP
	  selector:
	    app: ovn-central
	    ovn-nb-leader: "true"
	  sessionAffinity: None
	
	---
	kind: Service
	apiVersion: v1
	metadata:
	  name: ovn-sb
	  namespace: kube-ovn
	spec:
	  ports:
	    - name: ovn-sb
	      protocol: TCP
	      port: 6642
	      targetPort: 6642
	  type: ClusterIP
	  selector:
	    app: ovn-central
	    ovn-sb-leader: "true"
	  sessionAffinity: None
	
	---
	kind: Deployment
	apiVersion: apps/v1
	metadata:
	  name: ovn-central
	  namespace: kube-ovn
	  annotations:
	    kubernetes.io/description: |
	      OVN components: northd, nb and sb.
	spec:
	  replicas: 1
	  strategy:
	    rollingUpdate:
	      maxSurge: 0%
	      maxUnavailable: 100%
	    type: RollingUpdate
	  selector:
	    matchLabels:
	      app: ovn-central
	  template:
	    metadata:
	      labels:
	        app: ovn-central
	        component: network
	        type: infra
	    spec:
	      tolerations:
	      - operator: Exists
	        effect: NoSchedule
	      affinity:
	        podAntiAffinity:
	          requiredDuringSchedulingIgnoredDuringExecution:
	            - labelSelector:
	                matchLabels:
	                  app: ovn-central
	              topologyKey: kubernetes.io/hostname
	      serviceAccountName: ovn
	      hostNetwork: true
	      containers:
	        - name: ovn-central
	          image: "index.alauda.cn/alaudak8s/kube-ovn-db:v0.10.0"
	          imagePullPolicy: IfNotPresent
	          securityContext:
	            capabilities:
	              add: ["SYS_NICE"]
	          env:
	            - name: POD_IP
	              valueFrom:
	                fieldRef:
	                  fieldPath: status.podIP
	            - name: POD_NAME
	              valueFrom:
	                fieldRef:
	                  fieldPath: metadata.name
	            - name: POD_NAMESPACE
	              valueFrom:
	                fieldRef:
	                  fieldPath: metadata.namespace
	          resources:
	            requests:
	              cpu: 500m
	              memory: 300Mi
	          volumeMounts:
	            - mountPath: /run/openvswitch
	              name: host-run-ovs
	            - mountPath: /var/run/openvswitch
	              name: host-run-ovs
	            - mountPath: /sys
	              name: host-sys
	              readOnly: true
	            - mountPath: /etc/openvswitch
	              name: host-config-openvswitch
	            - mountPath: /var/log/openvswitch
	              name: host-log
	          readinessProbe:
	            exec:
	              command:
	                - sh
	                - /root/ovn-is-leader.sh
	            periodSeconds: 3
	          livenessProbe:
	            exec:
	              command:
	              - sh
	              - /root/ovn-healthcheck.sh
	            initialDelaySeconds: 30
	            periodSeconds: 7
	            failureThreshold: 5
	      nodeSelector:
	        beta.kubernetes.io/os: "linux"
	        kube-ovn/role: "master"
	      volumes:
	        - name: host-run-ovs
	          hostPath:
	            path: /run/openvswitch
	        - name: host-sys
	          hostPath:
	            path: /sys
	        - name: host-config-openvswitch
	          hostPath:
	            path: /etc/origin/openvswitch
	        - name: host-log
	          hostPath:
	            path: /var/log/openvswitch
	
	---
	kind: DaemonSet
	apiVersion: apps/v1
	metadata:
	  name: ovs-ovn
	  namespace: kube-ovn
	  annotations:
	    kubernetes.io/description: |
	      This daemon set launches the openvswitch daemon.
	spec:
	  selector:
	    matchLabels:
	      app: ovs
	  updateStrategy:
	    type: OnDelete
	  template:
	    metadata:
	      labels:
	        app: ovs
	        component: network
	        type: infra
	    spec:
	      tolerations:
	      - operator: Exists
	        effect: NoSchedule
	      serviceAccountName: ovn
	      hostNetwork: true
	      hostPID: true
	      containers:
	        - name: openvswitch
	          image: "index.alauda.cn/alaudak8s/kube-ovn-node:v0.10.0"
	          imagePullPolicy: IfNotPresent
	          securityContext:
	            runAsUser: 0
	            privileged: true
	          env:
	            - name: POD_IP
	              valueFrom:
	                fieldRef:
	                  fieldPath: status.podIP
	          volumeMounts:
	            - mountPath: /lib/modules
	              name: host-modules
	              readOnly: true
	            - mountPath: /run/openvswitch
	              name: host-run-ovs
	            - mountPath: /var/run/openvswitch
	              name: host-run-ovs
	            - mountPath: /sys
	              name: host-sys
	              readOnly: true
	            - mountPath: /etc/openvswitch
	              name: host-config-openvswitch
	            - mountPath: /var/log/openvswitch
	              name: host-log
	          readinessProbe:
	            exec:
	              command:
	              - sh
	              - /root/ovs-healthcheck.sh
	            periodSeconds: 5
	          livenessProbe:
	            exec:
	              command:
	              - sh
	              - /root/ovs-healthcheck.sh
	            initialDelaySeconds: 10
	            periodSeconds: 5
	            failureThreshold: 5
	          resources:
	            requests:
	              cpu: 200m
	              memory: 300Mi
	            limits:
	              cpu: 1000m
	              memory: 800Mi
	      nodeSelector:
	        beta.kubernetes.io/os: "linux"
	      volumes:
	        - name: host-modules
	          hostPath:
	            path: /lib/modules
	        - name: host-run-ovs
	          hostPath:
	            path: /run/openvswitch
	        - name: host-sys
	          hostPath:
	            path: /sys
	        - name: host-config-openvswitch
	          hostPath:
	            path: /etc/origin/openvswitch
	        - name: host-log
	          hostPath:
	            path: /var/log/openvswitch
	EOF
	REGISTRY_ENDPOINT=$(docker info |grep 60080  |tr -d ' ')
	sed -i -e "s/index.alauda.cn/${REGISTRY_ENDPOINT}/g" kube-ovn-ovn.yaml
	sed -i -e "s/index.alauda.cn/${REGISTRY_ENDPOINT}/g" kube-ovn-crd.yaml
	kubectl apply -f kube-ovn-crd.yaml
	kubectl apply -f kube-ovn-ovn.yaml
	```

3. 检查 crd 是否存在 `kubectl get crd `，应该包含 ips.kubeovn.io 和 subnets.kubeovn.io。

4. 安装 Kube-OVN Controller 及 CNIServer，执行如下命令：

	```
	cat <<EOF > kube-ovn.yaml
	---
	kind: Deployment
	apiVersion: apps/v1
	metadata:
	  name: kube-ovn-controller
	  namespace: kube-ovn
	  annotations:
	    kubernetes.io/description: |
	      kube-ovn controller
	spec:
	  replicas: 2
	  selector:
	    matchLabels:
	      app: kube-ovn-controller
	  strategy:
	    rollingUpdate:
	      maxSurge: 0%
	      maxUnavailable: 100%
	    type: RollingUpdate
	  template:
	    metadata:
	      labels:
	        app: kube-ovn-controller
	        component: network
	        type: infra
	    spec:
	      tolerations:
	      - operator: Exists
	        effect: NoSchedule
	      affinity:
	        podAntiAffinity:
	          requiredDuringSchedulingIgnoredDuringExecution:
	            - labelSelector:
	                matchLabels:
	                  app: kube-ovn-controller
	              topologyKey: kubernetes.io/hostname
	      serviceAccountName: ovn
	      hostNetwork: true
	      containers:
	        - name: kube-ovn-controller
	          image: "index.alauda.cn/alaudak8s/kube-ovn-controller:v0.10.0"
	          imagePullPolicy: IfNotPresent
	          command:
	          - /kube-ovn/start-controller.sh
	          args:
	          - --default-cidr=10.16.0.0/16
	          - --default-gateway=10.16.0.1
	          - --node-switch-cidr=100.64.0.0/16
	          env:
	            - name: POD_NAME
	              valueFrom:
	                fieldRef:
	                  fieldPath: metadata.name
	            - name: KUBE_NAMESPACE
	              valueFrom:
	                fieldRef:
	                  fieldPath: metadata.namespace
	            - name: KUBE_NODE_NAME
	              valueFrom:
	                fieldRef:
	                  fieldPath: spec.nodeName
	          readinessProbe:
	            exec:
	              command:
	                - sh
	                - /kube-ovn/kube-ovn-controller-healthcheck.sh
	            periodSeconds: 3
	          livenessProbe:
	            exec:
	              command:
	                - sh
	                - /kube-ovn/kube-ovn-controller-healthcheck.sh
	            initialDelaySeconds: 30
	            periodSeconds: 7
	            failureThreshold: 5
	      nodeSelector:
	        beta.kubernetes.io/os: "linux"
	
	---
	kind: DaemonSet
	apiVersion: apps/v1
	metadata:
	  name: kube-ovn-cni
	  namespace: kube-ovn
	  annotations:
	    kubernetes.io/description: |
	      This daemon set launches the kube-ovn cni daemon.
	spec:
	  selector:
	    matchLabels:
	      app: kube-ovn-cni
	  updateStrategy:
	    type: OnDelete
	  template:
	    metadata:
	      labels:
	        app: kube-ovn-cni
	        component: network
	        type: infra
	    spec:
	      tolerations:
	      - operator: Exists
	        effect: NoSchedule
	      serviceAccountName: ovn
	      hostNetwork: true
	      hostPID: true
	      initContainers:
	      - name: install-cni
	        image: "index.alauda.cn/alaudak8s/kube-ovn-cni:v0.10.0"
	        imagePullPolicy: IfNotPresent
	        command: ["/kube-ovn/install-cni.sh"]
	        securityContext:
	          runAsUser: 0
	          privileged: true
	        volumeMounts:
	          - mountPath: /etc/cni/net.d
	            name: cni-conf
	          - mountPath: /opt/cni/bin
	            name: cni-bin
	      containers:
	      - name: cni-server
	        image: "index.alauda.cn/alaudak8s/kube-ovn-cni:v0.10.0"
	        imagePullPolicy: IfNotPresent
	        command:
	          - sh
	          - /kube-ovn/start-cniserver.sh
	        args:
	          - --enable-mirror=true
	        securityContext:
	          capabilities:
	            add: ["NET_ADMIN", "SYS_ADMIN", "SYS_PTRACE"]
	        env:
	          - name: POD_IP
	            valueFrom:
	              fieldRef:
	                fieldPath: status.podIP
	          - name: KUBE_NODE_NAME
	            valueFrom:
	              fieldRef:
	                fieldPath: spec.nodeName
	        volumeMounts:
	          - mountPath: /run/openvswitch
	            name: host-run-ovs
	          - mountPath: /var/run/netns
	            name: host-ns
	            mountPropagation: HostToContainer
	        readinessProbe:
	          exec:
	            command:
	              - nc
	              - -z
	              - -w3
	              - 127.0.0.1
	              - "10665"
	          periodSeconds: 3
	        livenessProbe:
	          exec:
	            command:
	              - nc
	              - -z
	              - -w3
	              - 127.0.0.1
	              - "10665"
	          initialDelaySeconds: 30
	          periodSeconds: 7
	          failureThreshold: 5
	      nodeSelector:
	        beta.kubernetes.io/os: "linux"
	      volumes:
	        - name: host-run-ovs
	          hostPath:
	            path: /run/openvswitch
	        - name: cni-conf
	          hostPath:
	            path: /etc/cni/net.d
	        - name: cni-bin
	          hostPath:
	            path: /opt/cni/bin
	        - name: host-ns
	          hostPath:
	            path: /var/run/netns
	
	--
	kind: DaemonSet
	apiVersion: apps/v1
	metadata:
	  name: kube-ovn-pinger
	  namespace: kube-ovn
	  annotations:
	    kubernetes.io/description: |
	      This daemon set launches the openvswitch daemon.
	spec:
	  selector:
	    matchLabels:
	      app: kube-ovn-pinger
	  updateStrategy:
	    type: RollingUpdate
	  template:
	    metadata:
	      labels:
	        app: kube-ovn-pinger
	        component: network
	        type: infra
	    spec:
	      tolerations:
	        - operator: Exists
	          effect: NoSchedule
	      serviceAccountName: ovn
	      hostPID: true
	      containers:
	        - name: pinger
	          image: "index.alauda.cn/alaudak8s/kube-ovn-pinger:v0.10.0"
	          imagePullPolicy: IfNotPresent
	          securityContext:
	            runAsUser: 0
	            privileged: false
	          env:
	            - name: POD_IP
	              valueFrom:
	                fieldRef:
	                  fieldPath: status.podIP
	            - name: HOST_IP
	              valueFrom:
	                fieldRef:
	                  fieldPath: status.hostIP
	            - name: POD_NAME
	              valueFrom:
	                fieldRef:
	                  fieldPath: metadata.name
	            - name: NODE_NAME
	              valueFrom:
	                fieldRef:
	                  fieldPath: spec.nodeName
	          volumeMounts:
	            - mountPath: /lib/modules
	              name: host-modules
	              readOnly: true
	            - mountPath: /run/openvswitch
	              name: host-run-ovs
	            - mountPath: /var/run/openvswitch
	              name: host-run-ovs
	            - mountPath: /sys
	              name: host-sys
	              readOnly: true
	            - mountPath: /etc/openvswitch
	              name: host-config-openvswitch
	            - mountPath: /var/log/openvswitch
	              name: host-log
	          resources:
	            requests:
	              cpu: 100m
	              memory: 300Mi
	            limits:
	              cpu: 200m
	              memory: 400Mi
	      nodeSelector:
	        beta.kubernetes.io/os: "linux"
	      volumes:
	        - name: host-modules
	          hostPath:
	            path: /lib/modules
	        - name: host-run-ovs
	          hostPath:
	            path: /run/openvswitch
	        - name: host-sys
	          hostPath:
	            path: /sys
	        - name: host-config-openvswitch
	          hostPath:
	            path: /etc/origin/openvswitch
	        - name: host-log
	          hostPath:
	            path: /var/log/openvswitch
	---
	kind: Service
	apiVersion: v1
	metadata:
	  name: kube-ovn-pinger
	  namespace: kube-ovn
	  labels:
	    app: kube-ovn-pinger
	spec:
	  selector:
	    app: kube-ovn-pinger
	  ports:
	    - port: 8080
	      name: metrics
	--
	kind: Service
	apiVersion: v1
	metadata:
	  name: kube-ovn-controller
	  namespace: kube-ovn
	  labels:
	    app: kube-ovn-controller
	spec:
	  selector:
	    app: kube-ovn-controller
	  ports:
	    - port: 10660
	      name: metrics
	EOF
	REGISTRY_ENDPOINT=$(docker info |grep 60080  |tr -d ' ')
	sed -i -e "s/index.alauda.cn/${REGISTRY_ENDPOINT}/g" kube-ovn.yaml
	kubectl apply -f kube-ovn.yaml
	```

5. 检查 pod  `kubectl get pod -n kube-ovn `。

6.  检查 subnet `kubectl get subnet `。

7. (可选）安装 Kubectl 插件 。

	```
	## 下载 kubectl-ko 文件
	wget https://raw.githubusercontent.com/alauda/kube-ovn/v0.10.0/dist/images/kubectl-ko
	
	## 将文件复制到 $PATH 下的某个目录
	mv kubectl-ko /usr/local/bin/kubectl-ko
	
	## 给 kubectl-ko增加可执行权限
	chmod +x /usr/local/bin/kubectl-ko
	
	## 检查插件状态
	kubectl plugin list
	The following compatible plugins are available:
	
	/usr/local/bin/kubectl-ko
	
	##对网络质量进行检查
	kubectl ko diagnose all
	```


8. 重建非 host 模式的 pod。

	```
	kubectl -n xxx delete po --all
	```

9. 如果该集群还需要对接 UI（可选），那么需要在 kube-public 下创建 ovn 的 configmap。

	```
	kubectl -n kube-public create cm cni-kube-ovn
	```

10. 检查对接 UI 是否成功，进入平台管理视图，侧边栏选 **网络 > 子网**，切换到对应的 ovn 集群，可以看到有子网菜单说明安装 ovn 成功。



#### 回滚 

**注意**：需要在集群的 master 上操作。

1. 删除 kube-ovn 资源。

	```
	cat <<EOF > cleanup.sh
	#!/bin/bash
	set -eu
	
	for subnet in $(kubectl get subnet -o name); do
	  kubectl patch "$subnet" --type='json' -p '[{"op": "remove", "path": "/metadata/finalizers"}]'
	done
	
	# Delete Kube-OVN components
	kubectl delete -f https://raw.githubusercontent.com/alauda/kube-ovn/v0.10.0/yamls/kube-ovn.yaml --ignore-not-found=true
	kubectl delete -f https://raw.githubusercontent.com/alauda/kube-ovn/v0.10.0/yamls/ovn.yaml --ignore-not-found=true
	kubectl delete -f https://raw.githubusercontent.com/alauda/kube-ovn/v0.10.0/yamls/crd.yaml --ignore-not-found=true
	
	# Remove annotations in all pods of all namespaces
	for ns in $(kubectl get ns -o name |cut -c 11-); do
	  echo "annotating pods in  ns:$ns"
	  kubectl annotate pod  --all ovn.kubernetes.io/cidr- -n "$ns"
	  kubectl annotate pod  --all ovn.kubernetes.io/gateway- -n "$ns"
	  kubectl annotate pod  --all ovn.kubernetes.io/ip_address- -n "$ns"
	  kubectl annotate pod  --all ovn.kubernetes.io/logical_switch- -n "$ns"
	  kubectl annotate pod  --all ovn.kubernetes.io/mac_address- -n "$ns"
	  kubectl annotate pod  --all ovn.kubernetes.io/port_name- -n "$ns"
	  kubectl annotate pod  --all ovn.kubernetes.io/allocated- -n "$ns"
	done
	
	# Remove annotations in namespaces and nodes
	kubectl annotate no --all ovn.kubernetes.io/cidr-
	kubectl annotate no --all ovn.kubernetes.io/gateway-
	kubectl annotate no --all ovn.kubernetes.io/ip_address-
	kubectl annotate no --all ovn.kubernetes.io/logical_switch-
	kubectl annotate no --all ovn.kubernetes.io/mac_address-
	kubectl annotate no --all ovn.kubernetes.io/port_name-
	kubectl annotate ns --all ovn.kubernetes.io/cidr-
	kubectl annotate ns --all ovn.kubernetes.io/exclude_ips-
	kubectl annotate ns --all ovn.kubernetes.io/gateway-
	kubectl annotate ns --all ovn.kubernetes.io/logical_switch-
	kubectl annotate ns --all ovn.kubernetes.io/private-
	kubectl annotate ns --all ovn.kubernetes.io/allow-
	EOF
	bash cleanup.sh
	```

2. 删除各节点上的残留文件并重启节点。

	```
	rm -rf /var/run/openvswitch
	rm -rf /etc/origin/openvswitch/
	rm -rf /etc/openvswitch
	```

3. 安装回 flannel。

	```
	kubectl apply -f flannel.bak.yaml
	```

4. 重建非 host 模式的 pod。

	```
	kubectl -n xxx delete po --all
	```


### 用 calico 替换 flannel/galaxy 网络

**注意**：删除及备份，可以参考上面一节，**用 ovn 替换 flannel/galaxy 网络** 中，备份及删除的内容。

#### 安装 alauda-calico  


**注意**：在集群的 master 上操作。

1. 创建 default-subnet.yml。

	```
	CIDR_SUBNET='10.160.0.1/16'       ## calico 使用的 ip 范围
	IPIPMODE=Always                   ## 有Always，CrossSubnet, Never 三种模式，默认Always
	cat <<EOF > default-subnet.yml 
	apiVersion: kubeovn.io/v1
	kind: Subnet
	metadata:
	  name: default-ipv4-ippool
	spec:
	  blockSize: 26
	  cidrBlock: ${CIDR_SUBNET}
	  default: true
	  ipipMode: ${IPIPMODE}
	  namespaces: []
	  natOutgoing: true
	  private: false
	  protocol: IPv4
	  vxlanMode: Never
	EOF
	```

2. 创建 calico.yml 并将其中 `CALICO_IPV4POOL_CIDR` 改为你的 `cidr`，将 `CALICO_IPV4POOL_IPIP` 改成你需要的(Always、 CrossSubnet、 Never)。

	```
	CIDR_SUBNET='10.160.0.1/16'       ## calico 使用的 ip 范围
	IPIPMODE=Always                   ## 有Always，CrossSubnet, Never 三种模式，默认Always
	## 上面两个变量的值，和创建 default-subnet.yaml 时的变量一致
	cat <<EOF > calico.yml
	---
	# Source: calico/templates/calico-config.yaml
	# This ConfigMap is used to configure a self-hosted Calico installation.
	kind: ConfigMap
	apiVersion: v1
	metadata:
	  name: calico-config
	  namespace: kube-system
	data:
	  # Typha is disabled.
	  typha_service_name: "none"
	  # Configure the backend to use.
	  calico_backend: "bird"
	
	  # Configure the MTU to use
	  veth_mtu: "1440"
	
	  # The CNI network configuration to install on each node.  The special
	  # values in this config will be automatically populated.
	  cni_network_config: |-
	    {
	      "name": "k8s-pod-network",
	      "cniVersion": "0.3.0",
	      "plugins": [
	        {
	          "type": "calico",
	          "log_level": "info",
	          "datastore_type": "kubernetes",
	          "nodename": "__KUBERNETES_NODE_NAME__",
	          "mtu": __CNI_MTU__,
	          "ipam": {
	              "type": "calico-ipam"
	          },
	          "policy": {
	              "type": "k8s"
	          },
	          "kubernetes": {
	              "kubeconfig": "__KUBECONFIG_FILEPATH__"
	          }
	        },
	        {
	          "type": "portmap",
	          "snat": true,
	          "capabilities": {"portMappings": true}
	        }
	      ]
	    }
	
	---
	# Source: calico/templates/kdd-crds.yaml
	apiVersion: apiextensions.k8s.io/v1beta1
	kind: CustomResourceDefinition
	metadata:
	   name: felixconfigurations.crd.projectcalico.org
	spec:
	  scope: Cluster
	  group: crd.projectcalico.org
	  version: v1
	  names:
	    kind: FelixConfiguration
	    plural: felixconfigurations
	    singular: felixconfiguration
	---
	
	apiVersion: apiextensions.k8s.io/v1beta1
	kind: CustomResourceDefinition
	metadata:
	  name: ipamblocks.crd.projectcalico.org
	spec:
	  scope: Cluster
	  group: crd.projectcalico.org
	  version: v1
	  names:
	    kind: IPAMBlock
	    plural: ipamblocks
	    singular: ipamblock
	
	---
	
	apiVersion: apiextensions.k8s.io/v1beta1
	kind: CustomResourceDefinition
	metadata:
	  name: blockaffinities.crd.projectcalico.org
	spec:
	  scope: Cluster
	  group: crd.projectcalico.org
	  version: v1
	  names:
	    kind: BlockAffinity
	    plural: blockaffinities
	    singular: blockaffinity
	
	---
	
	apiVersion: apiextensions.k8s.io/v1beta1
	kind: CustomResourceDefinition
	metadata:
	  name: ipamhandles.crd.projectcalico.org
	spec:
	  scope: Cluster
	  group: crd.projectcalico.org
	  version: v1
	  names:
	    kind: IPAMHandle
	    plural: ipamhandles
	    singular: ipamhandle
	
	---
	
	apiVersion: apiextensions.k8s.io/v1beta1
	kind: CustomResourceDefinition
	metadata:
	  name: ipamconfigs.crd.projectcalico.org
	spec:
	  scope: Cluster
	  group: crd.projectcalico.org
	  version: v1
	  names:
	    kind: IPAMConfig
	    plural: ipamconfigs
	    singular: ipamconfig
	
	---
	
	apiVersion: apiextensions.k8s.io/v1beta1
	kind: CustomResourceDefinition
	metadata:
	  name: bgppeers.crd.projectcalico.org
	spec:
	  scope: Cluster
	  group: crd.projectcalico.org
	  version: v1
	  names:
	    kind: BGPPeer
	    plural: bgppeers
	    singular: bgppeer
	
	---
	
	apiVersion: apiextensions.k8s.io/v1beta1
	kind: CustomResourceDefinition
	metadata:
	  name: bgpconfigurations.crd.projectcalico.org
	spec:
	  scope: Cluster
	  group: crd.projectcalico.org
	  version: v1
	  names:
	    kind: BGPConfiguration
	    plural: bgpconfigurations
	    singular: bgpconfiguration
	
	---
	
	apiVersion: apiextensions.k8s.io/v1beta1
	kind: CustomResourceDefinition
	metadata:
	  name: ippools.crd.projectcalico.org
	spec:
	  scope: Cluster
	  group: crd.projectcalico.org
	  version: v1
	  names:
	    kind: IPPool
	    plural: ippools
	    singular: ippool
	
	---
	
	apiVersion: apiextensions.k8s.io/v1beta1
	kind: CustomResourceDefinition
	metadata:
	  name: hostendpoints.crd.projectcalico.org
	spec:
	  scope: Cluster
	  group: crd.projectcalico.org
	  version: v1
	  names:
	    kind: HostEndpoint
	    plural: hostendpoints
	    singular: hostendpoint
	
	---
	
	apiVersion: apiextensions.k8s.io/v1beta1
	kind: CustomResourceDefinition
	metadata:
	  name: clusterinformations.crd.projectcalico.org
	spec:
	  scope: Cluster
	  group: crd.projectcalico.org
	  version: v1
	  names:
	    kind: ClusterInformation
	    plural: clusterinformations
	    singular: clusterinformation
	
	---
	
	apiVersion: apiextensions.k8s.io/v1beta1
	kind: CustomResourceDefinition
	metadata:
	  name: globalnetworkpolicies.crd.projectcalico.org
	spec:
	  scope: Cluster
	  group: crd.projectcalico.org
	  version: v1
	  names:
	    kind: GlobalNetworkPolicy
	    plural: globalnetworkpolicies
	    singular: globalnetworkpolicy
	
	---
	
	apiVersion: apiextensions.k8s.io/v1beta1
	kind: CustomResourceDefinition
	metadata:
	  name: globalnetworksets.crd.projectcalico.org
	spec:
	  scope: Cluster
	  group: crd.projectcalico.org
	  version: v1
	  names:
	    kind: GlobalNetworkSet
	    plural: globalnetworksets
	    singular: globalnetworkset
	
	---
	
	apiVersion: apiextensions.k8s.io/v1beta1
	kind: CustomResourceDefinition
	metadata:
	  name: networkpolicies.crd.projectcalico.org
	spec:
	  scope: Namespaced
	  group: crd.projectcalico.org
	  version: v1
	  names:
	    kind: NetworkPolicy
	    plural: networkpolicies
	    singular: networkpolicy
	
	---
	
	apiVersion: apiextensions.k8s.io/v1beta1
	kind: CustomResourceDefinition
	metadata:
	  name: networksets.crd.projectcalico.org
	spec:
	  scope: Namespaced
	  group: crd.projectcalico.org
	  version: v1
	  names:
	    kind: NetworkSet
	    plural: networksets
	    singular: networkset
	---
	# Source: calico/templates/rbac.yaml
	
	# Include a clusterrole for the kube-controllers component,
	# and bind it to the calico-kube-controllers serviceaccount.
	kind: ClusterRole
	apiVersion: rbac.authorization.k8s.io/v1
	metadata:
	  name: calico-kube-controllers
	rules:
	  # Nodes are watched to monitor for deletions.
	  - apiGroups: [""]
	    resources:
	      - nodes
	    verbs:
	      - watch
	      - list
	      - get
	  # Pods are queried to check for existence.
	  - apiGroups: [""]
	    resources:
	      - pods
	    verbs:
	      - get
	  # IPAM resources are manipulated when nodes are deleted.
	  - apiGroups: ["crd.projectcalico.org"]
	    resources:
	      - ippools
	    verbs:
	      - list
	  - apiGroups: ["crd.projectcalico.org"]
	    resources:
	      - blockaffinities
	      - ipamblocks
	      - ipamhandles
	    verbs:
	      - get
	      - list
	      - create
	      - update
	      - delete
	  # Needs access to update clusterinformations.
	  - apiGroups: ["crd.projectcalico.org"]
	    resources:
	      - clusterinformations
	    verbs:
	      - get
	      - create
	      - update
	---
	kind: ClusterRoleBinding
	apiVersion: rbac.authorization.k8s.io/v1
	metadata:
	  name: calico-kube-controllers
	roleRef:
	  apiGroup: rbac.authorization.k8s.io
	  kind: ClusterRole
	  name: calico-kube-controllers
	subjects:
	- kind: ServiceAccount
	  name: calico-kube-controllers
	  namespace: kube-system
	---
	# Include a clusterrole for the calico-node DaemonSet,
	# and bind it to the calico-node serviceaccount.
	kind: ClusterRole
	apiVersion: rbac.authorization.k8s.io/v1
	metadata:
	  name: calico-node
	rules:
	  # The CNI plugin needs to get pods, nodes, and namespaces.
	  - apiGroups: [""]
	    resources:
	      - pods
	      - nodes
	      - namespaces
	    verbs:
	      - get
	  - apiGroups: [""]
	    resources:
	      - endpoints
	      - services
	    verbs:
	      # Used to discover service IPs for advertisement.
	      - watch
	      - list
	      # Used to discover Typhas.
	      - get
	  - apiGroups: [""]
	    resources:
	      - nodes/status
	    verbs:
	      # Needed for clearing NodeNetworkUnavailable flag.
	      - patch
	      # Calico stores some configuration information in node annotations.
	      - update
	  # Watch for changes to Kubernetes NetworkPolicies.
	  - apiGroups: ["networking.k8s.io"]
	    resources:
	      - networkpolicies
	    verbs:
	      - watch
	      - list
	  # Used by Calico for policy information.
	  - apiGroups: [""]
	    resources:
	      - pods
	      - namespaces
	      - serviceaccounts
	    verbs:
	      - list
	      - watch
	  # The CNI plugin patches pods/status.
	  - apiGroups: [""]
	    resources:
	      - pods/status
	    verbs:
	      - patch
	  # Calico monitors various CRDs for config.
	  - apiGroups: ["crd.projectcalico.org"]
	    resources:
	      - globalfelixconfigs
	      - felixconfigurations
	      - bgppeers
	      - globalbgpconfigs
	      - bgpconfigurations
	      - ippools
	      - ipamblocks
	      - globalnetworkpolicies
	      - globalnetworksets
	      - networkpolicies
	      - networksets
	      - clusterinformations
	      - hostendpoints
	      - blockaffinities
	    verbs:
	      - get
	      - list
	      - watch
	  # Calico must create and update some CRDs on startup.
	  - apiGroups: ["crd.projectcalico.org"]
	    resources:
	      - ippools
	      - felixconfigurations
	      - clusterinformations
	    verbs:
	      - create
	      - update
	  # Calico stores some configuration information on the node.
	  - apiGroups: [""]
	    resources:
	      - nodes
	    verbs:
	      - get
	      - list
	      - watch
	  # These permissions are only requried for upgrade from v2.6, and can
	  # be removed after upgrade or on fresh installations.
	  - apiGroups: ["crd.projectcalico.org"]
	    resources:
	      - bgpconfigurations
	      - bgppeers
	    verbs:
	      - create
	      - update
	  # These permissions are required for Calico CNI to perform IPAM allocations.
	  - apiGroups: ["crd.projectcalico.org"]
	    resources:
	      - blockaffinities
	      - ipamblocks
	      - ipamhandles
	    verbs:
	      - get
	      - list
	      - create
	      - update
	      - delete
	  - apiGroups: ["crd.projectcalico.org"]
	    resources:
	      - ipamconfigs
	    verbs:
	      - get
	  # Block affinities must also be watchable by confd for route aggregation.
	  - apiGroups: ["crd.projectcalico.org"]
	    resources:
	      - blockaffinities
	    verbs:
	      - watch
	  # The Calico IPAM migration needs to get daemonsets. These permissions can be
	  # removed if not upgrading from an installation using host-local IPAM.
	  - apiGroups: ["apps"]
	    resources:
	      - daemonsets
	    verbs:
	      - get
	  - apiGroups: ["kubeovn.io"]
	    resources:
	      - ips
	    verbs:
	      - get
	      - list
	---
	apiVersion: rbac.authorization.k8s.io/v1
	kind: ClusterRoleBinding
	metadata:
	  name: calico-node
	roleRef:
	  apiGroup: rbac.authorization.k8s.io
	  kind: ClusterRole
	  name: calico-node
	subjects:
	- kind: ServiceAccount
	  name: calico-node
	  namespace: kube-system
	
	---
	# Source: calico/templates/calico-node.yaml
	# This manifest installs the calico-node container, as well
	# as the CNI plugins and network config on
	# each master and worker node in a Kubernetes cluster.
	kind: DaemonSet
	apiVersion: apps/v1
	metadata:
	  name: calico-node
	  namespace: kube-system
	  labels:
	    k8s-app: calico-node
	spec:
	  selector:
	    matchLabels:
	      k8s-app: calico-node
	  updateStrategy:
	    type: RollingUpdate
	    rollingUpdate:
	      maxUnavailable: 1
	  template:
	    metadata:
	      labels:
	        k8s-app: calico-node
	      annotations:
	        # This, along with the CriticalAddonsOnly toleration below,
	        # marks the pod as a critical add-on, ensuring it gets
	        # priority scheduling and that its resources are reserved
	        # if it ever gets evicted.
	        scheduler.alpha.kubernetes.io/critical-pod: ''
	    spec:
	      nodeSelector:
	        beta.kubernetes.io/os: linux
	      hostNetwork: true
	      tolerations:
	        # Make sure calico-node gets scheduled on all nodes.
	        - effect: NoSchedule
	          operator: Exists
	        # Mark the pod as a critical add-on for rescheduling.
	        - key: CriticalAddonsOnly
	          operator: Exists
	        - effect: NoExecute
	          operator: Exists
	      serviceAccountName: calico-node
	      # Minimize downtime during a rolling upgrade or deletion; tell Kubernetes to do a "force
	      # deletion": https://kubernetes.io/docs/concepts/workloads/pods/pod/#termination-of-pods.
	      terminationGracePeriodSeconds: 0
	      priorityClassName: system-node-critical
	      initContainers:
	        # This container performs upgrade from host-local IPAM to calico-ipam.
	        # It can be deleted if this is a fresh installation, or if you have already
	        # upgraded to use calico-ipam.
	        - name: upgrade-ipam
	          image: index.alauda.cn/claas/calico-cni:alauda-v3.9.0-patch-4
	          command: ["/opt/cni/bin/calico-ipam", "-upgrade"]
	          env:
	            - name: KUBERNETES_NODE_NAME
	              valueFrom:
	                fieldRef:
	                  fieldPath: spec.nodeName
	            - name: CALICO_NETWORKING_BACKEND
	              valueFrom:
	                configMapKeyRef:
	                  name: calico-config
	                  key: calico_backend
	          volumeMounts:
	            - mountPath: /var/lib/cni/networks
	              name: host-local-net-dir
	            - mountPath: /host/opt/cni/bin
	              name: cni-bin-dir
	        # This container installs the CNI binaries
	        # and CNI network config file on each node.
	        - name: install-cni
	          image: index.alauda.cn/claas/calico-cni:alauda-v3.9.0-patch-4
	          command: ["/install-cni.sh"]
	          env:
	            # Name of the CNI config file to create.
	            - name: CNI_CONF_NAME
	              value: "10-calico.conflist"
	            # The CNI network config to install on each node.
	            - name: CNI_NETWORK_CONFIG
	              valueFrom:
	                configMapKeyRef:
	                  name: calico-config
	                  key: cni_network_config
	            # Set the hostname based on the k8s node name.
	            - name: KUBERNETES_NODE_NAME
	              valueFrom:
	                fieldRef:
	                  fieldPath: spec.nodeName
	            # CNI MTU Config variable
	            - name: CNI_MTU
	              valueFrom:
	                configMapKeyRef:
	                  name: calico-config
	                  key: veth_mtu
	            # Prevents the container from sleeping forever.
	            - name: SLEEP
	              value: "false"
	          volumeMounts:
	            - mountPath: /host/opt/cni/bin
	              name: cni-bin-dir
	            - mountPath: /host/etc/cni/net.d
	              name: cni-net-dir
	        # Adds a Flex Volume Driver that creates a per-pod Unix Domain Socket to allow Dikastes
	        # to communicate with Felix over the Policy Sync API.
	        - name: flexvol-driver
	          image: index.alauda.cn/claas/calico-pod2daemon-flexvol:v3.9.0
	          volumeMounts:
	          - name: flexvol-driver-host
	            mountPath: /host/driver
	      containers:
	        # Runs calico-node container on each Kubernetes node.  This
	        # container programs network policy and routes on each
	        # host.
	        - name: calico-node
	          image: index.alauda.cn/claas/calico-node:v3.9.0
	          env:
	            # Use Kubernetes API as the backing datastore.
	            - name: DATASTORE_TYPE
	              value: "kubernetes"
	            # Wait for the datastore.
	            - name: WAIT_FOR_DATASTORE
	              value: "true"
	            # Set based on the k8s node name.
	            - name: NODENAME
	              valueFrom:
	                fieldRef:
	                  fieldPath: spec.nodeName
	            # Choose the backend to use.
	            - name: CALICO_NETWORKING_BACKEND
	              valueFrom:
	                configMapKeyRef:
	                  name: calico-config
	                  key: calico_backend
	            # Cluster type to identify the deployment type
	            - name: CLUSTER_TYPE
	              value: "k8s,bgp"
	            # Auto-detect the BGP IP address.
	            - name: IP
	              value: "autodetect"
	            # Enable IPIP
	            - name: CALICO_IPV4POOL_IPIP
	              value: "${IPIPMODE}"
	            # Set MTU for tunnel device used if ipip is enabled
	            - name: FELIX_IPINIPMTU
	              valueFrom:
	                configMapKeyRef:
	                  name: calico-config
	                  key: veth_mtu
	            # The default IPv4 pool to create on startup if none exists. Pod IPs will be
	            # chosen from this range. Changing this value after installation will have
	            # no effect. This should fall within `--cluster-cidr`.
	            - name: CALICO_IPV4POOL_CIDR
	              value: "${CIDR_SUBNET}"
	            # Disable file logging so `kubectl logs` works.
	            - name: CALICO_DISABLE_FILE_LOGGING
	              value: "true"
	            # Set Felix endpoint to host default action to ACCEPT.
	            - name: FELIX_DEFAULTENDPOINTTOHOSTACTION
	              value: "ACCEPT"
	            # Disable IPv6 on Kubernetes.
	            - name: FELIX_IPV6SUPPORT
	              value: "false"
	            # Set Felix logging to "info"
	            - name: FELIX_LOGSEVERITYSCREEN
	              value: "info"
	            - name: FELIX_HEALTHENABLED
	              value: "true"
	          securityContext:
	            privileged: true
	          resources:
	            requests:
	              cpu: 250m
	          livenessProbe:
	            exec:
	              command:
	              - /bin/calico-node
	              - -felix-live
	            periodSeconds: 10
	            initialDelaySeconds: 10
	            failureThreshold: 6
	          readinessProbe:
	            exec:
	              command:
	              - /bin/calico-node
	              - -felix-ready
	              - -bird-ready
	            periodSeconds: 10
	          volumeMounts:
	            - mountPath: /lib/modules
	              name: lib-modules
	              readOnly: true
	            - mountPath: /run/xtables.lock
	              name: xtables-lock
	              readOnly: false
	            - mountPath: /var/run/calico
	              name: var-run-calico
	              readOnly: false
	            - mountPath: /var/lib/calico
	              name: var-lib-calico
	              readOnly: false
	            - name: policysync
	              mountPath: /var/run/nodeagent
	      volumes:
	        # Used by calico-node.
	        - name: lib-modules
	          hostPath:
	            path: /lib/modules
	        - name: var-run-calico
	          hostPath:
	            path: /var/run/calico
	        - name: var-lib-calico
	          hostPath:
	            path: /var/lib/calico
	        - name: xtables-lock
	          hostPath:
	            path: /run/xtables.lock
	            type: FileOrCreate
	        # Used to install CNI.
	        - name: cni-bin-dir
	          hostPath:
	            path: /opt/cni/bin
	        - name: cni-net-dir
	          hostPath:
	            path: /etc/cni/net.d
	        # Mount in the directory for host-local IPAM allocations. This is
	        # used when upgrading from host-local to calico-ipam, and can be removed
	        # if not using the upgrade-ipam init container.
	        - name: host-local-net-dir
	          hostPath:
	            path: /var/lib/cni/networks
	        # Used to create per-pod Unix Domain Sockets
	        - name: policysync
	          hostPath:
	            type: DirectoryOrCreate
	            path: /var/run/nodeagent
	        # Used to install Flex Volume Driver
	        - name: flexvol-driver-host
	          hostPath:
	            type: DirectoryOrCreate
	            path: /usr/libexec/kubernetes/kubelet-plugins/volume/exec/nodeagent~uds
	---
	
	apiVersion: v1
	kind: ServiceAccount
	metadata:
	  name: calico-node
	  namespace: kube-system
	
	---
	# Source: calico/templates/calico-kube-controllers.yaml
	
	# See https://github.com/projectcalico/kube-controllers
	apiVersion: apps/v1
	kind: Deployment
	metadata:
	  name: calico-kube-controllers
	  namespace: kube-system
	  labels:
	    k8s-app: calico-kube-controllers
	spec:
	  # The controllers can only have a single active instance.
	  replicas: 1
	  selector:
	    matchLabels:
	      k8s-app: calico-kube-controllers
	  strategy:
	    type: Recreate
	  template:
	    metadata:
	      name: calico-kube-controllers
	      namespace: kube-system
	      labels:
	        k8s-app: calico-kube-controllers
	      annotations:
	        scheduler.alpha.kubernetes.io/critical-pod: ''
	    spec:
	      nodeSelector:
	        beta.kubernetes.io/os: linux
	      tolerations:
	        # Mark the pod as a critical add-on for rescheduling.
	        - key: CriticalAddonsOnly
	          operator: Exists
	        - key: node-role.kubernetes.io/master
	          effect: NoSchedule
	      serviceAccountName: calico-kube-controllers
	      priorityClassName: system-cluster-critical
	      containers:
	        - name: calico-kube-controllers
	          image: index.alauda.cn/claas/calico-kube-controllers:v3.9.0
	          env:
	            # Choose which controllers to run.
	            - name: ENABLED_CONTROLLERS
	              value: node
	            - name: DATASTORE_TYPE
	              value: kubernetes
	          readinessProbe:
	            exec:
	              command:
	              - /usr/bin/check-status
	              - -r
	
	--
	
	apiVersion: v1
	kind: ServiceAccount
	metadata:
	  name: calico-kube-controllers
	  namespace: kube-system
	--
	# Source: calico/templates/calico-etcd-secrets.yaml
	
	--
	# Source: calico/templates/calico-typha.yaml
	
	--
	# Source: calico/templates/configure-canal.yaml
	
	
	EOF
	```

3. 创建 cni-configmap.yml crd.yml。 

	```
	cat <<EOF > cni-configmap.yml
	apiVersion: v1
	kind: ConfigMap
	metadata:
	  name: cni-calico
	  namespace: kube-public
	EOF
	
	cat <<EOF > crd.yml
	apiVersion: apiextensions.k8s.io/v1beta1
	kind: CustomResourceDefinition
	metadata:
	  name: ips.kubeovn.io
	spec:
	  group: kubeovn.io
	  version: v1
	  scope: Cluster
	  names:
	    plural: ips
	    singular: ip
	    kind: IP
	    shortNames:
	      - ip
	  additionalPrinterColumns:
	    - name: IP
	      type: string
	      JSONPath: .spec.ipAddress
	    - name: Mac
	      type: string
	      JSONPath: .spec.macAddress
	    - name: Node
	      type: string
	      JSONPath: .spec.nodeName
	    - name: Subnet
	      type: string
	      JSONPath: .spec.subnet
	---
	apiVersion: apiextensions.k8s.io/v1beta1
	kind: CustomResourceDefinition
	metadata:
	  name: subnets.kubeovn.io
	spec:
	  group: kubeovn.io
	  version: v1
	  scope: Cluster
	  names:
	    plural: subnets
	    singular: subnet
	    kind: Subnet
	    shortNames:
	      - subnet
	  subresources:
	    status: {}
	  additionalPrinterColumns:
	    - name: Protocol
	      type: string
	      JSONPath: .spec.protocol
	    - name: CIDR
	      type: string
	      JSONPath: .spec.cidrBlock
	    - name: Private
	      type: boolean
	      JSONPath: .spec.private
	    - name: NAT
	      type: boolean
	      JSONPath: .spec.natOutgoing
	    - name: Default
	      type: boolean
	      JSONPath: .spec.default
	    - name: GatewayType
	      type: string
	      JSONPath: .spec.gatewayType
	    - name: Used
	      type: integer
	      JSONPath: .status.usingIPs
	    - name: Available
	      type: integer
	      JSONPath: .status.availableIPs
	  validation:
	    openAPIV3Schema:
	      properties:
	        spec:
	          required: ["cidrBlock"]
	          properties:
	            cidrBlock:
	              type: "string"
	            gateway:
	              type: "string"
	EOF
	```

4. 执行如下命令，创建资源。

	```
	kubectl apply -f cni-configmap.yml
	kubectl apply -f crd.yml
	kubectl apply -f calico
	```

5. 执行 ```kubectl get ippool``` 直到看到了 `default-ipv4-ippool` 然后再创建 raven-agent.yml。

	```
	cat <<EOF > raven-agent.yml
	---
	apiVersion: rbac.authorization.k8s.io/v1
	kind: ClusterRole
	metadata:
	  annotations:
	    rbac.authorization.k8s.io/system-only: "true"
	  name: system:raven
	rules:
	  - apiGroups:
	      - "kubeovn.io"
	    resources:
	      - subnets
	      - subnets/status
	      - ips
	    verbs:
	      - "*"
	  - apiGroups:
	      - ""
	    resources:
	      - pods
	      - namespaces
	      - configmaps
	    verbs:
	      - create
	      - get
	      - list
	      - watch
	      - patch
	      - update
	  - apiGroups:
	      - apps
	    resources:
	      - statefulsets
	      - daemonsets
	      - deployments
	      - replicasets
	    verbs:
	      - get
	      - list
	      - watch
	  - apiGroups:
	      - ""
	    resources:
	      - events
	    verbs:
	      - create
	      - patch
	      - update
	  - apiGroups:
	      - "crd.projectcalico.org"
	    resources:
	      - ippools
	    verbs:
	      - "*"
	
	---
	apiVersion: rbac.authorization.k8s.io/v1
	kind: ClusterRoleBinding
	metadata:
	  name: raven
	roleRef:
	  name: system:raven
	  kind: ClusterRole
	  apiGroup: rbac.authorization.k8s.io
	subjects:
	  - kind: ServiceAccount
	    name: raven
	    namespace: kube-system
	
	---
	apiVersion: v1
	kind: ServiceAccount
	metadata:
	  name: raven
	  namespace: kube-system
	
	
	---
	apiVersion: apps/v1beta1
	kind: Deployment
	metadata:
	  name: raven
	  namespace: kube-system
	  labels:
	    app: raven
	spec:
	  replicas: 2
	  selector:
	    matchLabels:
	      app: raven
	  template:
	    metadata:
	      labels:
	        app: raven
	      annotations:
	        scheduler.alpha.kubernetes.io/critical-pod: ''
	    spec:
	      serviceAccountName: raven
	      hostNetwork: true
	      tolerations:
	        - effect: NoSchedule
	          operator: Exists
	        - key: CriticalAddonsOnly
	          operator: Exists
	        - effect: NoExecute
	          operator: Exists
	      containers:
	      - name: raven
	        image: index.alauda.cn/claas/raven:b0b6142fca3113e3cacb981ae2565e92ba2233d6
	        env:
	          - name: POD_NAME
	            valueFrom:
	              fieldRef:
	                fieldPath: metadata.name
	          - name: POD_NAMESPACE
	            valueFrom:
	              fieldRef:
	                fieldPath: metadata.namespace
	          - name: RAVEN_ARGS
	            value: "-v=4"
	
	EOF
	REGISTRY_ENDPOINT=$(docker info |grep 60080  |tr -d ' ')
	sed -i -e "s/index.alauda.cn/${REGISTRY_ENDPOINT}/g" raven-agent.yml 
	```

6. 执行下面的命令创建资源。

	```
	kubectl apply -f default-subnet.yml
	kubectl apply -f raven-agent.yml
	```

#### 安装开源 calico  

**注意**：在集群的 master 上操作。

```
POD_CIDR="<your-pod-cidr>"   ## cidr
curl https://docs.projectcalico.org/v3.9/manifests/calico.yaml -O
sed -i -e "s?192.168.0.0/16?$POD_CIDR?g" calico.yaml
kubectl apply -f calico.yaml
```

#### 删除 calico 回滚

```
cat <<EOF > delete_calico.sh
#!/bin/bash
kubectl delete -n kube-system configmap/cni-calico
kubectl delete customresourcedefinition.apiextensions.k8s.io/ips.kubeovn.io
kubectl delete customresourcedefinition.apiextensions.k8s.io/subnets.kubeovn.io
kubectl delete -n kube-system configmap/calico-config
kubectl delete customresourcedefinition.apiextensions.k8s.io/felixconfigurations.crd.projectcalico.org
kubectl delete customresourcedefinition.apiextensions.k8s.io/ipamblocks.crd.projectcalico.org
kubectl delete customresourcedefinition.apiextensions.k8s.io/blockaffinities.crd.projectcalico.org
kubectl delete customresourcedefinition.apiextensions.k8s.io/ipamhandles.crd.projectcalico.org
kubectl delete customresourcedefinition.apiextensions.k8s.io/ipamconfigs.crd.projectcalico.org
kubectl delete customresourcedefinition.apiextensions.k8s.io/bgppeers.crd.projectcalico.org
kubectl delete customresourcedefinition.apiextensions.k8s.io/bgpconfigurations.crd.projectcalico.org
kubectl delete customresourcedefinition.apiextensions.k8s.io/ippools.crd.projectcalico.org
kubectl delete customresourcedefinition.apiextensions.k8s.io/hostendpoints.crd.projectcalico.org
kubectl delete customresourcedefinition.apiextensions.k8s.io/clusterinformations.crd.projectcalico.org
kubectl delete customresourcedefinition.apiextensions.k8s.io/globalnetworkpolicies.crd.projectcalico.org
kubectl delete customresourcedefinition.apiextensions.k8s.io/globalnetworksets.crd.projectcalico.org
kubectl delete customresourcedefinition.apiextensions.k8s.io/networkpolicies.crd.projectcalico.org
kubectl delete customresourcedefinition.apiextensions.k8s.io/networksets.crd.projectcalico.org
kubectl delete clusterrole.rbac.authorization.k8s.io/calico-kube-controllers
kubectl delete clusterrolebinding.rbac.authorization.k8s.io/calico-kube-controllers
kubectl delete clusterrole.rbac.authorization.k8s.io/calico-node
kubectl delete clusterrolebinding.rbac.authorization.k8s.io/calico-node
kubectl delete -n kube-system daemonset.apps/calico-node
kubectl delete -n kube-system serviceaccount/calico-node
kubectl delete -n kube-system deployment.apps/calico-kube-controllers
kubectl delete -n kube-system serviceaccount/calico-kube-controllers
EOF
bash delete_calico.sh
```

然后，按照上一节，回滚 ovn 的方法，重新安装 flannel 即可。

### chart 安装、删除、升级、查看等运维工作

1. #### Helm

   1. 基本概念

      1. helm

         Helm 是一个命令行下的客户端工具。主要用于 Kubernetes 应用程序 Chart 的创建、打包、发布以及创建和管理本地和远程的 Chart 仓库。

      2. Tiller

         helm的服务端，部署于kubernetes内，Tiller接受helm的请求，并根据chart生成kubernetes部署文件（helm称为release），然后提交给 Kubernetes 创建应用。Tiller 还提供了 Release 的升级、删除、回滚等一系列功能。

      3. Charts

         Helm 的软件包，采用 TAR 格式。类似于 APT 的 DEB 包或者 YUM 的 RPM 包，其包含了一组定义 Kubernetes 资源相关的 YAML 文件。

      4. Repoistory

         用于发布和存储Chart的仓库。

   2. 工作原理

      ![1582867766929](images//1582867766929.png)

      1. Chart install

         - helm从制定目录或tar文件解析chart结构信息

         - helm将制定的chart结构和value信息通过gRPC协议传递给tiller

         - tiller根据chart和values生成一个release

         - tiller通过json将release发送给kubernetes，生成release

      2. Chart update

         - helm从制定的目录或tar文件解析chart结构信息

         - helm将制定的chart结构和value信息通过gRPC协议传给tiller

         - tiller生成release并更新制定名称的release的history

         - tiller将release信息发送给kubernetes用于更新release

      3. Chart Rollback

         - helm将会滚的release名称传递给tiller

         - tiller根据release名称查找history

         - tiller从history中获取到上一个release

         - tiller将上一个release发送给kubernetes用于替换当前release

      4. Chart 处理依赖

         - Tiller 在处理 Chart 时，直接将 Chart 以及其依赖的所有 Charts 合并为一个 Release，同时传递给 Kubernetes。因此 Tiller 并不负责管理依赖之间的启动顺序。Chart 中的应用需要能够自行处理依赖关系。

   3. helm v2与v3对比

      ![1582620459403](images//1582620459403.png)

      - Release 不再是全局资源，而是存储在各自命名空间内

      - Helm 2默认情况下使用ConfigMaps存储版本信息。在Helm 3中，将Secrets用作默认存储驱动程序

      - 把requirements.yaml合并成Chart.yaml

      - helm install需要提供名称，如果实在不想提供名称，指定参数--generate-name，在v2时可以不提供，不提供名称时将自动生成一个名称，这功能比较令人讨厌

      - 去除用于本地临时搭建Chart Repository的helm serve命令

      - Values支持JSON Schema校验器，自动检查所有输入的变量格式

      - helm cli命令重命名

   4. 常用命令

      ```
      通过completion命令来设置 helm 命令自动补全：
      
      在 bash 中设置当前 shell 的自动补全，要先安装 bash-completion 包
      # source <(helm completion bash)
      在bash shell中永久的添加自动补全
      # echo "source <(helm completion bash)" >> ~/.bashrc
      
      查看当前安装的charts
      # helm list
      
      chart仓库更新命令：
      # helm repo update
       
      强制删除已经容器化的release ：
      # helm delete --purge harbor
      
      查找一个charts
      # helm search harbor
      
      安装一个charts
      # helm install stable/harbor
      
      查看release的历史版本
      # helm history harbor
      
      回滚到某一个版本，如回滚到第一个版本
      # helm rollback harbor 1
      
      更多参考链接：
      http://confluence.alauda.cn/pages/viewpage.action?pageId=27174027
      ```

   5. chart目录结构

      ```
      chart_demo
      ├── charts     #该目录中放置当前Chart依赖的其它Chart
      ├── Chart.yaml #用于描述Chart的相关信息，包括名字、描述信息以及等。
      ├── templates  #部署文件模版目录，模版使用的值来自values.yaml和由Tiller提供的值
      │   ├── deployment.yaml #kubernetes Deployment object
      │   ├── _helpers.tpl    #用于修改kubernetes objcet配置的模板
      │   ├── ingress.yaml
      │   ├── NOTES.txt    #用于介绍 Chart 部署后的一些信息，例如：如何使用这个 Chart、列出缺省的设置等。
      │   ├── service.yaml #kubernetes Serivce
      │   └── tests
      │       └── test-connection.yaml
      └── values.yaml #用于存储 templates 目录中模板文件中用到变量的值。
      ```

   6. 常见问题

      1. helm upgrade报错Error: UPGRADE FAILED: no Deployment with the name found

         - 说明

           首先只是一个helm官方的bug，不要怀疑自己，确实是helm官方的bug。

         - 报错版本

           2.9.1必然有这个错误，2.13.0之后不知道有没有修复，不确定

         - 错误原因

           helm的源码，没有加入事务机制，当我们第一次upgrade失败后，就helm就失去了报错的组件的定位。找不到了，所以无法更新。

         - 解决方法

           升级helm到2.13.0之后的版本

           同时加上–atomic的注释

         - atomic注释的说明

           如果upgrade更新报错，那么将会回滚至之前的版本

         - 在2.14.0之后的版本加入了cleanfailup的注释，就是为了修复这个错误，失败后重新定位到deployment或者cm等等组件

   7. Helm 最佳实践

      1. history控制

         默认情况下，helm release 的历史版本是无限制的，时间久了，会导致 helm list 等操作极其缓慢。默认情况下，helm 的 release 信息以 ConfigMap 的形式存储于 kube-system 这个 ns 下，我们可以通过如下命令查看到一个集群里所有的 release 的 history 统计:

         ```
         kubectl -n kube-system get cm --no-headers -l OWNER=TILLER | wc -l
         ```

         当输出结果为成百上千时，一般就会导致 helm list 等操作变慢了。应对方法如下：

         helm init 命令 提供了 --history-max 参数来控制历史版本的长度。示例如下:

         ```
         # 干净的环境
         helm init --history-max=10
          
         # helm 已经部署的情况下
         helm init --upgrade --history-max=10
         ```

2. #### captain

   1. 基本信息

      1. 介绍

         Captain 是一个标准的 kubernetes controller, 以 helm v3 library 为基础，参考 helm v3 design proposal 而实现。用户可以通过 HelmRequest CRD 来描述对 helm charts 的部署需求，captain 负责同步 HelmRequest 的状态并部署 helm charts。

      2. 通用操作

         helm 中的很多概念在 captain 都通过 crd 实现了，可以用 kubectl 来进行查看及排错，常见的操作

         如下: 

         - get: 查看
         - describe: 查看事件及可能的错误信息
         - edit: 更新

         资源如果不是处于 Synced 状态即表示有问题，可以通过 describe 操作查看事件

         如果想创建 ChartRepo 或者 HelmRequest， 直接用 kubectl + yaml 即可

      3. 仓库

         将helm 的仓库以配置文件的方式存储于本地目录中，在helm中可以通过 helm repo list 来查看. captain 有类似的仓库概念，可以通过如下命令查看: 

         ```
         kubectl get ctr -n <system-namespace>
         ```

         正常情况下，各个 ChartRepo 都应该处于 Synced 状态。

      4. HelmRequest (hr)

         HelmRequest (hr)  对应于一个 charts 的部署，其内容与 `helm install/upgrade` 等的参数类似，

         详细介绍可参考 [HelmRequest CRD](https://github.com/alauda/captain/blob/master/docs/helmrequest.md), 常用操作如下:

         ```
         kubectl get hr -n <ns>  ##查看
          
         kubectl edit hr -n <ns> ##更新
         
         kubectl describe hr -n <ns> ##描述一个helmrequest
         
         kubectl captain create ## 创建一个helmrequest
         
         kubectl captain import ## 从一个release 导入一个helmrequest
         
         kubectl captain rollback ## 回滚一个helmrequest
         
         kubectl captain upgrade ##升级一个helmrequest
         
         kubectl captain trigger-update ## 触发更新一个helmrequest
         ```

   2. captain处理流程及功能特性

      1. 流程图

         ![1582876945889](images//1582876945889.png)

      2. 功能与特性

      - 支持多集群：Helm v3的设计中并没有支持多集群的功能。Captain组件基于 cluster-registry项目实现了对多集群的支持。用户可以将HelmRequest安装到当前集群，指定集群，甚至是所有集群。这在企业场景下是非常有用的。

      - 依赖检查：Helm本身具备依赖概念，功能也比较复杂。Captain在Helm v3的基础上加了新的依赖检查。如果HelmRequest A 依赖于 HelmRequest B, 在声明好依赖之后。Captain 在同步HelmRequest A 时，会首先检查 HelmRequest B是否已经同步。只有在所有依赖都同步成功之后，当前的HelmRequest才会开始同步。

      - 集中式配置valuesFrom：在Helm v2中部署Chart 时，可以通过在命令行添加参数或者使用 values.yaml 文件来对 chart 部署添加一些自定义变量。在Captain中，除了对这种传统的方式进行进行支持外，也充分利用了kubernetes 中 ConfigMap 以及 Secret 作为独立配置的功能。用户可以在HelmRequest 的定义中通过 valuesFrom的来自外部的ConfigMap 或者Secret 作为 values 来源。在微服务场景下，用户通常需要用多个 HelmRequest 来描述不同chart 的安装需求，而这些 chart 之间通常又经常有很多需要共同的配置， valuesFrom 能很好地解决这个问题。
      - 资源冲突的处理： 用户在使用 helm v2 的时候，经常会碰到很多使用上的不便之处。因为资源已存在导致的安装失败，因为卸载不彻底进一步导致下一步安装失败，这其中就包含了经典的CRD管理的问题(因为要等带crd的api生效，引入了crd-install hook, 加了hook的CRD在删除 release时不会删除)。Captain 为了解决了这个问题，引入了类似于 `kubectl apply`的逻辑。当发现资源已存在的时候，会执行更新操作，一些无法更新的资源会进行删除重建操作。这样的策略能够极大地解决用户使用时遇到的各种不便之处

   3. 运维手册
      1. 安装

         1.1. 安装包

         ```
         helm install --version <captain chart version> --debug --namespace=<ns> --set global.registry.address=<init registry> --set alaudaChartRepoURL=<init 节点上的 chart repo> --set namespace=<ns> --name=captain stable/captain --wait --timeout 3000
         ```

         kubectl-captain  在安装目录下的 other 目录里

         1.2. 安装要求

         1.2.1. 软件依赖

         captain 依赖 cert-manager ，必须在 cert-manager 部署成功后，安装 captain

      2. 替换helm

         2.1.  已经通过 helm 部署的 chart 怎样迁移到 helm 上

         详见升级说明文档，大致流程如下：

         - 通过 kubectl captain 命令或者直接创建 helmrequest 资源，hr 资源存在 global 的集群内
         - captain 侦测到新的 helmrequest 资源，会自动部署 chart ，成功后，会生成 release 资源。chart 安装到那个集群，release 资源就存在那个集群内

         2.2. 迁移到 captain 的 chart 如何在 helm 里删掉

         详见升级说明文档，大致流程如下：

         - helm 每成功安装一次 chart ，就会在 kube-system 的 ns 下创建一个 cm，cm 的名字是 chart 的 release 名字+ v[0-9]，比如 cert-manager.v1，是 cert-manager 这个 chart 第一次安装成功的版本，以后每更新一次这个 chart ，后面的数字就加1
         - 找到这个 cm，删掉之后执行 helm list -a 命令就看不到 chart 了，但是 chart 创建的资源还存在

      3. 使用方式

         3.1. captain 安装 chart 

         - kubectl captain create ，比如kubectl captain create --version 2.1 --namespace=cpaas-system --chart=stable/amp-minio --set global.registry.address=10.0.128.172:60080  amp-minio  即创建了一个名为 amp-minio 的 HelmRequest 资源
         - 写 yaml ，然后 kubectl create -f

         3.2. captain 更新、升级 chart

         ```
         kubectl edit <hr name> -n <ns> 修改 chart 的变量和版本
         ```

         3.3. captain 删除 chart

         ```
         kubectl delete <hr name> -n <ns> 修改 chart 的变量和版本
         ```

         3.4. captain 查看 chart 状态

         ```
         kubectl get <hr name> -n <ns>                 ## 查看 hr 的状态
         
         kubectl describe <hr name> -n <ns>       ## 查看 hr 详细情况
         
         kubectl get release -n <ns>                       ## 查看通过 captain 部署的 chart
         ```

         3.5. captain 列出 chart 资源

         ```
         kubectl get <hr name> -n <ns> 
         ```

      4. Helm安装的Captain迁移至非Helm安装的Captain

         - 简介

         Captain 的最新版本去除了 cert-manager 的依赖，改用直接用 yaml 部署。已经用 helm 部署过的 Captain , 如果直接删除的话，会导致相关CRD及CR被删。所以本文描述的是手工清理掉相关资源并且用最新的方式安装 Captain 的文档。

         - 清理

         ```
         kubectl delete mutatingwebhookconfigurations.admissionregistration.k8s.io captain
         kubectl delete validatingwebhookconfigurations.admissionregistration.k8s.io captain
         kubectl delete svc -n cpaas-system captain
         kubectl delete issuer captain-selfsigned-issuer -n cpaas-system
         kubectl delete cert captain-serving-cert -n cpaas-system
         kubectl delete deploy captain -n cpaas-system
         kubectl delete secret captain-webhook-cert -n cpaas-system
         # (可能没有，出错可以忽略)
         kubectl get cm -n kube-system |grep ^captain | awk '{print $1}' | xargs kubectl delete cm -n kube-system
         ```

         - 安装

         ```
         wget -c https://raw.githubusercontent.com/alauda/captain/master/artifacts/all/release.yaml
         # linux 环境 "" 去掉
         sed -i "" 's/captain-system/cpaas-system/g' deploy.yaml
         kubectl apply --validate=false -n cpaas-system -f deploy.yaml
         ```

   4. 常见问题排查

      1. k8s 1.14.6版本暂不支持 captain(可能出现问题,  未进行测试)

      2. cannot re-use a name that is still in use

         - 问题解释:

           Captain 会不断地尝试同步一个 HelmRequest, 在同步的过程中，会不断地创建 Release 资源(更新或重试都会产生新的), 如果在这个过程中，产生了pending或类似进行中状态的 Release, 下一次重试时就会碰到这个错误

         - 处理方式:

           ```
           kubectl get rel -n <.spec.namespace>
            
           # 删除所有的 release 资源
           kubectl get rel -n <.spec.namespace>  | grep <.spec.releaseName>\. | awk '{print $1}' | xargs kubectl delete -n <.spec.namespace> rel
            
           # 触发更新。或者直接用kubectl 编辑hr 增加一个任意的values即可
           kubectl captain trigger-update <hr-name> -n <hr-namespace>
           ```

      3. could not get apiVersions from Kubernetes: unable to retrieve the complete list of server APIs: xxx: the server is currently unable to handle the request

         - 问题解释

           此问题的原因是因为CRD, 有的crd安装之后， apiserver的discovery信息并未就绪，导致captain里面访问k8s出错

         - 处理方式:

           在目标集群，执行```kubectl get apiservice```  

           有的 apiservice 会显示为 false. 定位到具体的 group 后，可找相应的此功能的负责人去处理此事。创建的如下:

           1. metrics: 一般是 metrics server没有部署
           2. devops： devops组件没有正常运行
           3. cert-manager: cert-manager没有正常运行。

           等到都 True 之后，重试或者触发更新即可

3. #### sentry

   1. 介绍

      1. 基本信息

         Chart Operator (sentry)是一个基于k8s的operator， 以 helm v3 library 为基础, 通过声明的方式来管理charts的安装和更新。

         通过名为AppRelease的k8s自定义资源来描述所期望安装/更新chart的状态。基于集群中AppRelease的资源创建、更新事件，来执行对chart内资源执行apply操作。

      2. 特性

         - 通过声明方式安装、升级Chart版本
         - 从chart仓库拉取chart源码
         - 在AppRleases spec.values来指定chart values值
         - 通过过滤chart内CRD类型资源，提前安装并等待状态Ready
         - 对多个AppRelease资源可多任务并行处理
         - 支持多个AppRelease之间安装依赖
         - 支持某个资源更新后对指定Pod重启
         - 支持apprelease handler开发，实现对chart values hook和 chart安装前后的hook

      3. AppRelease配置定义

         | Spec              | Default | Description                                                  | Required |
         | ----------------- | ------- | ------------------------------------------------------------ | -------- |
         | version           | v0.0.9  | 支持的sentry最低版本                                         | true     |
         | type              |         | AppRelease资源类型，用于定制chart部署安装的handler           | false    |
         | upgradeFailPolicy | ignore  | 部署升级过程资源失败的处理策略，类型ignore，recreate         | false    |
         | timeout           | 300     | 部署升级资源状态监听超时时间，单位秒                         | false    |
         | wait              | true    | 是否等待Pods,PVCs,Services,Deployment,StatefulSet,ReplicaSet Ready | false    |
         | autoRecycle       | false   | AppRelease资源被删除时，安装相关的部署资源是否移除（未实现） | false    |
         | chart.repository  |         | 所安装chart的仓库地址                                        | true     |
         | chart.name        |         | chart名称                                                    | true     |
         | chart.version     |         | chart版本，留空不填，默认使用最新版本                        | true     |
         | values            |         | 安装chart所需values值，与values.yaml对应                     | false    |
         | dependents        |         | 依赖组件                                                     | false    |
         | restartPolicy     |         | 重启策略，当指定的配置ConfigMap或Secret更新，相关pod自动重启 | false    |
         | clientOnly        | false   | 用于开关是否从集群获取apiservices                            | false    |

         | Label                    | Default | Description                                                  | Required |
         | ------------------------ | ------- | ------------------------------------------------------------ | -------- |
         | apprelease/interval-sync | true    | 是否定时循环覆盖更新，默认2小时。chart内资源也可使用该label控制是否覆盖 | false    |

      4. AppRelease自定义资源

         每个chart release均由`AppRelease`负责声明。该自定义资源的结构示例如下: 

         ```yaml
         apiVersion: operator.alauda.io/v1alpha1
         kind: AppRelease
         metadata:
           labels:
             apprelease/interval-sync: "true"
           name: dex
           namespace: system
         spec:
           version: v0.0.8
           type: dex
           upgradeFailPolicy: ignore
           timeout: 300
           wait: true
           autoRecycle: false
           chart:
             repository: http://chart.com
             name: dex
             version: v2.5-b.1
           values:
             global:
               namespace: cpaas-system
               replicas: 1
           dependents:
           - cert-manager   >=v2.0.0
           restartPolicy:
           - name: dex-configmap
             kind: ConfigMap
             podSelectors:
             - matchLabels:
                 service_name: dex
         ```

         释义: 

         - 该资源可以指定ns或者与sentry在同一ns下。

         - `type`项用于定制开发时，自定义资源类型声明。如chart安装前需要做migrate，或依赖存量数据，都可以声明`type`后实现相应的`handler`。`handler`开放程度，webhook注入和验证，部署/升级前定制。

         - `chart`项需要提供一个集群能访问的chart仓库。

         - `timeout`用于设置部署和升级时，等待deployment等资源running的超时时间。

         - `upgradeFailPolicy`如果设置为`recreate`，在资源的apply失败时将尝试将该资源先删除后创建的操作。

         - `values`项需要提供给chart的values值，该values最终将于覆盖values.yaml文件中定义的默认值。

         - `dependents`项填写当前chart安装所依赖的apprelease name及chart version。版本检查基于[go-version](https://github.com/hashicorp/go-version) 版本比较操作符支持："", "=", "!=", ">", "<", ">=", "<="， 不填默认为“等于”。 `restartPolicy` pod重启策略，当指定的配置ConfigMap或Secret更新时，根据指定的pod selector对相关pod重启。

      5. Apprelease状态

         通过`AppRlease`的`status`可以查看指定chart部署/升级状态

         ```shell
         ➜  sentry git:(master) ✗ kubectl get ar -n namespace
         NAME           CHART          REVISION   STATUS     MESSAGE                   AGE
         cert-manager   cert-manager   v2.6-b.1   deployed   chart install succeeded   17m
         ```

         5.1Apprelease状态字段说明

         | Name     | Description        |
         | :------- | :----------------- |
         | NAME     | apprelease 名称    |
         | CHART    | chart 名称         |
         | REVISION | 已安装的chart版本  |
         | STATUS   | 安装状态           |
         | MESSAGE  | 安装过程的状态信息 |
         | AGE      | 已安装时间         |

         5.2Apprelease各阶段状态说明

         | STATUS          | Description   |
         | :-------------- | :------------ |
         | STATUS          | Description   |
         | unknown         | 安装还未开始  |
         | pending-install | 正在安装      |
         | pending-upgrade | 正在更新      |
         | failed          | 安装/更新失败 |
         | deployed        | 安装/更新完成 |

         ```shell
         status:
           chartName: dex
           conditions:
           - lastTransitionTime: "2019-12-11T06:46:29Z"
             lastUpdateTime: "2019-12-11T09:24:07Z"
             message: 'chart fetched: /tmp/download/target/dex'
             reason: RepoChartInCache
             status: "True"
             type: ChartFetched
           - lastTransitionTime: "2019-12-11T06:46:33Z"
             lastUpdateTime: "2019-12-11T09:24:14Z"
             message: handler finished
             reason: PerformHandleState
             status: "True"
             type: Released
           observedGeneration: 0
           releaseStatus: deployed
           revision: v2.5-b.1
         ```

      6. 支持范围

         1. 支持部署global集群。包含 acp，devops，asm
         2. 支持部署业务集群。包含 日志，监控

   2. 运维手册

      1. sentry工作流程图

         ![1582875201881](images/1582875201881.png)

      2. global集群部署流程

         1. 部署一个k8s集群。

         2. 安装helm和tiller，确保chart仓库配置正确。

         3. 创建要部署的namespace。比如:demo-system

         4. 在demo-system中，创建docker-registry secret (例如名字为 alaudak8s)用于后续下载部署所需镜像。

            kubectl create secret docker-registry alaudak8s --docker-server=index.alauda.cn --docker-username=xxxx --docker-password=xxxx --docker-email=abc@local.com -n demo-system

         5. 将创建好的secret更新到demo-system的default sa的imagePullSecrets 中。

         6. 执行命令: 略，详情请看2.8 部署文档

         7. 确认组件状态

            ```
            kubectl get apprelease -n demo-system 确认所有组件部署成功。
            ```

         8. 访问页面

      3. 业务集群部署流程

            1-5 前5步同global集群部署的1-5

         6. 将集群接入的global中，并获取当前集群所在名字(比如 demo-cluster)

         7. 执行命令: 略，详情请看2.8 部署文档

   3. 常见问题排查

      (1) sentry 部署 prometheus:  localVolumePath留空 ,   storageClassName 填 <sc name>

      (2)sentry部署增加kubefedVersion参数， --set platform.kubefedVersion=v2.8.0

      (3)假设一个环境，部署的时候没安装 devops ，之后想要安装 devops，用 helm upgrade 修改 sentry 这个 chart 的变量， --set platform.devops=true 

      (4)如何查看平台chart的安装状态

      A：拟定平台安装的ns为cpaas-system，通过查看apprelease资源信息即可。

      ```
      kubectl get apprelease -n cpaas-system
       
      ----
       
      [root@aq-zy-v2-8-1-m-1 charts]# kubectl get apprelease -n cpaas-system
      NAME                        CHART                       REVISION   STATUS     MESSAGE                   AGE
      alauda-base                 alauda-base                 v2.8.1     deployed   chart install succeeded   3h
      alauda-cluster-base         alauda-cluster-base         v2.8.1     deployed   chart install succeeded   3h
      ```

      

      (5) 安装chart过程中，apprelease的STAUS长时间处于pending-install，MESSAGE显示waiting resource ready

      A：查看该chart所安装的pod是否出现了拉取镜像失败，或者crash原因，处理好pod问题使得能够running，该apprelease状态可以正常通过。

      

      (6)安装chart过程中，apprelease的MESSAGE提示类似于check dependents failed: waiting charts "alauda-base->=v2.5-b.51" deployed的消息

      A：该消息提示当前安装的chart依赖于alauda-base chart的安装，且版本要求高于v2.5-b.51。

      解决办法，该问题原因是受安装依赖所致，将alauda-base apprelease等待机制跳过即可。编辑alauda-base apprelease，在spec里增加**wait: false**配置即可。

      例子：

      ```
      spec:
        wait: false
        chart:
          name: alauda-base
          repository: http://chartmuseum:chartmuseum@10.0.129.96:8088/
          version: v2.8.1
      ....
      ```

      

      (7)如何更新参数

      A：平台chart相关的apprelease资源目前在chart cpaas-appreleases里管理。

      sentry 通过 platform apprelease资源又将平台所有的经常变的参数聚合到该资源，理论小改动，如chart版本只需更新platform apprelease资源的values。

      例子：

      ```
      spec:
        chart:
          name: cpaas-appreleases
          repository: http://chartmuseum:chartmuseum@10.0.129.96:8088/
          version: v2.8.1
        type: platform
        values:
          acp:
            install: true
            version: v2.8.1
          asm:
            install: true
            version: v2.8.1
          base:
            install: true
            version: v2.8.1
          captain:
            install: true
            version: v2.8.1
          certManager:
            install: true
            version: v2.8.1
      ```

      

      (8) 如何禁用apprelease同步

      A：默认2小时会将chart同步一次，如果手动修改deploy可能会因同步问题给撤销。

      在apprelease里增加label **apprelease/interval-sync: "false"** 可以避免该问题

      例子：

      ```
      generation: 8
      labels:
        apprelease/interval-sync: "false"
      name: xxxxx
      namespace: xxxxxx
      ```

      

      (9) 如何查看渲染后的chart资源

      A：在sentry pod目录/tmp/yaml可以查看到所安装的chart资源模板

      ```
      #进入sentry容器
      kubectl -n cpaas-system exec -it $(kubectl get pod -n cpaas-system | grep sentry | awk '{print $1}') sh
      
      #在sentry容器内,到安装chart的目录下
      /sentry # cd /tmp/yaml/
      ```






### 平台对接第三方组件

#### 对接 kafka

####对接 es

1. 执行以下命令行，删除 cpaas-elasticsearch。

	```
	kubectl delete hr -n cpaas-system elasticsearch # 删除 es
	kubectl edit cm -n cpaas-system acp-config # 修改变量 esHost 的值为要对接的 es 的值
	```

2. 参考以下命令创建 es 的 secret。

  ```
  cat <<EOF >/tmp/es_secret.yaml
  apiVersion: v1
  kind: Secret
  metadata:
    annotations:
      app.alauda.io/create-method: ""
      app.alauda.io/display-name: "Service es, created by chart"
      owners.alauda.io/info: '[]'
    labels:
      chart: "elasticsearch"
      app_name: cpaas-es
      service_name: cpaas-es
    name: cpaas-es
    namespace: cpaas-system
  type: Opaque
  data:
    password: <base64 加密过的 es 的密码>
    username: <base64 加密过的 es 的用户名>
  EOF
  kubectl create -f /tmp/es_secret.yaml
  ```

3. 参照本文档 captain 运维部分，刷新 es。执行相关命令行进行升级。


#### 对接存储

参考以下命令及备注说明，对接存储。

====================k8s对接ceph集群==================================

```
###1、在准备对接的集群所有节点安装ceph客户端
yum install  ceph-common ceph-fuse  -y
  
###2、腾讯的ceph集群默认为none模式，没有开启认证,通过配置文件获得monitor节点的配置（去腾讯的ceph集群的某个data节点操作）
conf=$(ps -fe | grep CLUSTERMON.*.conf | grep ceph-mon | awk '{print $(NF-2)}')
cat $conf | grep mon_addr           ##获得monitor节点的ip以及端口
    mon_addr = 10.0.129.175:6789
    mon_addr = 10.0.129.8:6791
    mon_addr = 10.0.129.73:6790
  
###3、准备好cephfs压缩包，将压缩包拷贝到某台master机器的/root下，并解压.
这个压缩包在init节点的安装包路径下的other目录。/root/alauda-k8s/other/ceph/cephfs.zip
scp cephfs.zip  root@1.1.1.1:/root/   ##需要将1.1.1.1换为业务集群的某台master机器的ip地址。
  
去刚传完文件的master的root下执行以下操作、
mv cephfs /tmp        ###为了防止当前目录下存在同名目录
unzip cephfs.zip
cd cephfs/
###将deployment.yaml中的image地址更换为自己环境的image地址（例如：10.0.128.173:60080/claas/cephfs-provisioner:latest）
kubectl create ns cephfs
NAMESPACE=cephfs
sed -r -i "s/namespace: [^ ]+/namespace: $NAMESPACE/g" /root/cephfs/*.yaml
sed -r -i "N;s/(name: PROVISIONER_SECRET_NAMESPACE.*\n[[:space:]]*)value:.*/\1value: $NAMESPACE/" /root/cephfs/deployment.yaml
kubectl apply -f /root/cephfs -n $NAMESPACE
  
  
###4、创建storageclass 
cat <<EOF > cephfs-storageclass.yaml
kind: StorageClass
apiVersion: storage.k8s.io/v1
metadata:
  name: cephfs
  selfLink: /apis/storage.k8s.io/v1/storageclasses/cephfs
  uid: 198bcd73-a1ff-11e8-aafa-5254004c757f
  resourceVersion: '11786274'
  annotations:
    ceph.sample: '{"kind":"PersistentVolumeClaim","apiVersion":"v1","metadata":{"name":"claim1","namespace":"sock-shop","selfLink":"/api/v1/namespaces/sock-shop/persistentvolumeclaims/claim1","uid":"7033b9fc-a213-11e8-aafa-5254004c757f","resourceVersion":"7437570","creationTimestamp":"2018-08-17T11:48:20Z","annotations":{"pv.kubernetes.io/bind-completed":"yes","pv.kubernetes.io/bound-by-controller":"yes","volume.beta.kubernetes.io/storage-class":"cephfs","volume.beta.kubernetes.io/storage-provisioner":"ceph.com/cephfs"},"finalizers":["kubernetes.io/pvc-protection"]},"spec":{"accessModes":["ReadWriteMany"],"resources":{"requests":{"storage":"1Gi"}},"volumeName":"pvc-7033b9fc-a213-11e8-aafa-5254004c757f","volumeMode":"Filesystem"},"status":{"phase":"Bound","accessModes":["ReadWriteMany"],"capacity":{"storage":"1Gi"}}}'
    ceph.type: cephfs
    kubectl.kubernetes.io/last-applied-configuration: |
      {"apiVersion":"storage.k8s.io/v1","kind":"StorageClass","metadata":{"annotations":{},"name":"cephfs","namespace":""},"parameters":{"adminId":"admin","adminSecretName":"ceph-admin-secret","adminSecretNamespace":"kube-system","claimRoot":"/volumes/kubernetes","monitors":"10.0.129.175:6789,10.0.129.8:6791,10.0.129.73:6790"},"provisioner":"ceph.com/cephfs","reclaimPolicy":"Delete","volumeBindingMode":"Immediate"}
provisioner: ceph.com/cephfs                             ## 上一行的ip+端口需要修改为第2步获得ceph集群的monitors的IP地址和端口
parameters:
  adminId: admin
  adminSecretName: cephfs-admin-secret
  adminSecretNamespace: kube-system
  claimRoot: /volumes/kubernetes
  monitors: '10.0.129.175:6789,10.0.129.8:6791,10.0.129.73:6790'         ## 需要修改为第2步获得ceph集群的monitors的IP地址和端口
reclaimPolicy: Delete
volumeBindingMode: Immediate
EOF
  
  
kubectl create -f cephfs-storageclass.yaml
  
  
  
###5、定义管理员密码
echo "AQCYfAxdbXTwCRAAnz9MvJgO3KselABH2OoKOA==" > /tmp/secret 
kubectl create secret generic cephfs-admin-secret --from-file=/tmp/secret --namespace=kube-system
kubectl create secret generic cephfs-secret-user --from-file=/tmp/secret
  
  
###6、创建pvc验证
  
  
cat <<EOF > cephfs.yaml
  
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: claim1
  namespace: default
  annotations:
    volume.beta.kubernetes.io/storage-class: cephfs
    volume.beta.kubernetes.io/storage-provisioner: ceph.com/cephfs
  finalizers:
    - kubernetes.io/pvc-protection
spec:
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 1Gi
  volumeMode: Filesystem
EOF
  
kubectl create -f cephfs.yaml
  
  
kubectl get pvc      ###查看刚创建的pvc为bound状态即成功
NAME     STATUS   VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS   AGE
claim1   Bound    pvc-b480aa29-a142-11e9-9d43-525400d63265   1Gi        RWX            cephfs         19m 
```

### es kafka 运维

#### 配置 kafka

#### 配置es ttl

1. 在 global 的 master 上，执行如下命令：

	```
	kubectl edit deployment cpaas-elasticsearch -n cpaas-system
	```

2. 在当前版本中，支持配置以下变量：

	```
	ALAUDA_AUDIT_TTL=30d
	ALAUDA_EVENT_TTL=30d
	ALAUDA_ES_TTL=7d
	```

	**说明**：上述配置表示，平台会保留 30 天的审计索引记录、30 天的事件索引记录、7 天的日志索引记录。请根据业务的实际需要修改对应的值即可。

#### 更改 es 数据存储位置

1. 在 global 的 master 上，执行如下命令：

	```
	kubectl edit hr -n cpaas-system elasticsearch 
	```

2. 增加如下变量：

	```
	elasticsearch:
	  hostpath: /cpaas/data/elasticsearch
	```
	**说明**：hostpath 的值，即是要改变的挂载的目录位置。


#### 增加 es 节点数量

**说明**：扩容后，主机数量尽量为奇数。同时不能通过chart 升级 es 了，否则升级之后，改变又变成了 chart 的默认值。

1. 查看 es pod 的 nodeSelector 。

   一般为：log: true。
   
   查看方法：
   
   `kubectl edit deployment elasticsearch -n cpaas-system`
   
   然后搜索：nodeSelector。

2. 给希望扩容的节点添加上述 Label。例如：给 node 添加如下标签：

	```
	kubectl label nodes <node-name> <label-key>=<label-value>
	比如：
	kubectl label nodes node1 log=true
	```

3. 修改 deployment。

   1. 获取新打 label 的 node 节点 IP。并将该 IP 拼接到 `env:ALAUDA_ES_CLUSTERS` 后面，以逗号分割。

   2. 修改 replicas 数量为 5 即可扩充 es 集群。



### 更改 global 组件资源限制

* 方式 1

	1. 执行下面的命令找到 global 组件的名字。
	
		```
		kubectl get deploy -n cpaas-system
		```
	
	2. 执行下面的命令，编辑组件资源。
	
		```
		kubectl edit deployment <组件名> -n cpaas-system
		```

* 方式 2

	参考用户手册，在界面上，选择 global 集群，找到要调整的组件，调整资源。

### 更改 global 组件实例数量

* 方式 1

	global 组件默认加了反亲和，所以 global 组件最大的实例数不能超过 global 集群内，打了 `global=true` 这个标签的 node 的数量，首先扩充节点数量，再更改实例数，方式如下：

	```
	kubectl label nodes node1 global=true
	```

	1. 给 node1 这个节点打上标签，然后执行下面的命令找到 global 组件的名字。

		```
		kubectl get deploy -n cpaas-system
		```

	2. 执行下面的命令扩容。

		```
		kubectl scale deployment -n cpaas-system <要扩容的 global 组件名> --replicas=<要扩容的实例的数量>
		```

* 方式 2

	参考用户手册，在 UI 界面上，选择 global 集群，找到要扩容的组件，扩容实例。
	
### 替换登录页 Logo 和背景图片

支持用户替换登录页的 Logo 和背景图片。


**准备工作**

* 请提前准备好需要替换的 Logo 和背景图片。

	![](images/faqlogo.png?classes=big)
	
	
	平台默认的  Logo 和背景图片的格式及大小请参见下表。
	
	| 类型              | 名称          | 格式 | 大小（像素） |
	| -------------- | ------------ | ---- | ------------ |
	| 登录页左上角 Logo | dex-logo      | svg  | 40\*60       |
	| 登录页背景图片    | login-bg-logo | svg  | 544*500      |

* 下载 themectl 文件。

	**提示**：在 global 集群的任意 Master 节点服务器上执行命令 `uname -s` 可查看服务器的操作系统，根据操作系统类型下载相应的 themectl 文件，单击以下链接可直接下载。
	
	* [Darwin](https://s3.cn-north-1.amazonaws.com.cn/get.alauda.cn/tool/themectl_darwin)
	
	* [Linux](https://s3.cn-north-1.amazonaws.com.cn/get.alauda.cn/tool/themectl_linux)
	
	**注意**：下载完成后，需要在服务器上执行 `chmod +x themectl` 命令，为该文件添加执行权限。

**操作步骤**

执行以下命令行单独更新/创建 Logo、背景图片配置字典。

```
./themectl update --kube-config ~/.kube/config --namespace cpaas-system --configmap-name <aks-logo> --from-file=./aks-logo.svg
```

**说明**：命令行中的 <aks-logo> 需要替换为配置字典名称。配置字典名称对应关系请参见下表。

| 配置字典名称  | 类型              |
| ------------- | ----------------- |
| dex-logo      | 登录页左上角 Logo |
| login-bg-logo | 登录页背景图片    |
| favicon-logo  | 浏览器 Logo       |


​	
<div STYLE="page-break-after: always;"></div>

# 平台使用

请参考 《TKE for Alauda Container Platform v2.6 用户手册》的内容了解平台的使用方法。

<div STYLE="page-break-after: always;"></div>

# 巡检


*** 推荐巡检流程 ***

* 巡检平台功能
* 巡检 k8s 集群状态
* 巡检服务器状态



## 常用巡检、定位及排障方法 -- linux 系统

### 1、cpu

```
一、CPU utilization 利用率
1.通过top命令的c参数来查看具体的进程对应的是哪个程序占用率最高
# top -c

二、CPU load负载
1.查看返回的（load averag）系统负载显示值
# top
2.查看多核CPU核心的当前运行状况信息， 每2秒更新一次
# mpstat -P ALL 2
3.查看等待在CPU资源的进程数，每1秒更新一次
# vmstat 1
4.查找占用大量IO的进程，间隔1秒输出3次
# iostat -x 1 3
```

### 2、内存

```
1.检查可用内存命令
free命令是最广泛使用的，用于检查有关系统RAM使用情况的信息
# free -mlth

top命令可用于打印系统的CPU和内存使用情况
# top

从/proc文件系统中提取与内存相关的信息。 这些文件包含有关系统和内核的动态信息，而不是真实的系统文件。
# cat /proc/meminfo       

2.释放内存
# sync && echo 1 > /proc/sys/vm/drop_caches
注意：在清空缓存之前使用sync命令同步数据到磁盘

说明：drop_caches的值可以是0-3之间的数字，代表不同的含义： 0：不释放（系统默认值） 1：释放页缓存pagecache 2：释放inode和目录树缓存，3清空所有的缓存

更多参考链接：
http://confluence.alauda.cn/pages/viewpage.action?pageId=61911643
```

### 3、磁盘

```
1.查看块设备
# lsblk

2.查看文件系统空间目录
# df -Th

3.查询当前目录下各文件夹大小
# du -alh --max-depth=1 /var/lib/docker
```

### 4、网络

```
1.链路联通测试
# ping <域名或IP>

2.链路探测分析
# mtr -rn <IP>

3.域名解析
# dig / nslookup <域名>

4.利用 URL 规则在命令行下工作的文件传输工具
# curl <clusterIP:端口>       //判断集群cluster IP是否可正常访问
# curl <podip:端口>           //判断pod IP是否可正常访问

5.显示网络连接，路由表，接口状态，伪装连接，网络链路信息和组播成员组
# netstat -tuanlp |grep 端口  //查看端口是否有监听

6.网络抓包分析
Tcpdump是一个用来捕获网络流量的工具，可以帮助我们解决一些常见的网络问题

举例1）监听本机指定网卡跟主机192.168.110.130之间往来的通信包
# tcpdump -i eth0 host 192.168.110.130

举例2）监听抓取特定主机的流量包，并将结果输出到pcap文件中
# tcpdump -i any -c 3 host 10.0.3.1 -w tmp/tcpdata.pcap


举例3）集群内两个node上的容器A和容器B(172.28.21.3:5000)不通，可在容器A所在的主机上捕获与容器B目标IP有关的流量
进入一个容器来尝试去和其他的容器进行通信
# tcpdump -i any host 172.28.21.3


理解输出结果：
不好的例子
15:15:43.323412 IP 10.0.3.246.56894 > 192.168.0.92.22: Flags [S], cksum 0xcf28 (incorrect -> 0x0388), seq 682725222, win 29200, options [mss 1460,sackOK,TS val 619989005 ecr 0,nop,wscale 7], length 0

15:15:44.321444 IP 10.0.3.246.56894 > 192.168.0.92.22: Flags [S], cksum 0xcf28 (incorrect -> 0x028e), seq 682725222, win 29200, options [mss 1460,sackOK,TS val 619989255 ecr 0,nop,wscale 7], length 0

15:15:46.321610 IP 10.0.3.246.56894 > 192.168.0.92.22: Flags [S], cksum 0xcf28 (incorrect -> 0x009a), seq 682725222, win 29200, options [mss 1460,sackOK,TS val 619989755 ecr 0,nop,wscale 7], length 0
上面显示了一个不好的通信例子，在这个例子中“不好”，代表通信没有建立起来。我们可以看到 10.0.3.246发出一个 SYN 数据包给 主机 192.168.0.92，但是主机并没有应答。

好的例子
15:18:25.716453 IP 10.0.3.246.34908 > 192.168.0.110.22: Flags [S], cksum 0xcf3a (incorrect -> 0xc838), seq 1943877315, win 29200, options [mss 1460,sackOK,TS val 620029603 ecr 0,nop,wscale 7], length 0

15:18:25.716777 IP 192.168.0.110.22 > 10.0.3.246.34908: Flags [S.], cksum 0x594a (correct), seq 4001145915, ack 1943877316, win 5792, options [mss 1460,sackOK,TS val 18495104 ecr 620029603,nop,wscale 2], length 0

15:18:25.716899 IP 10.0.3.246.34908 > 192.168.0.110.22: Flags [.], cksum 0xcf32 (incorrect -> 0x9dcc), ack 1, win 229, options [nop,nop,TS val 620029603 ecr 18495104], length 0
好的例子应该向上面这样，我们看到典型的 TCP 3次握手。第一数据包是 SYN 包，从主机 10.0.3.246 发送给 主机192.168.0.110，第二个包是 SYN-ACK 包，主机192.168.0.110 回应 SYN 包。最后一个包是一个 ACK 或者 SYN – ACK – ACK 包，是主机 10.0.3.246 回应收到了 SYN – ACK 包。从上面看到一个 TCP/IP 连接成功建立。

含义解析
基本上tcpdump的输出格式为：系统时间 来源地址.端口 > 目标地址.端口 数据包参数， > 符号代表数据的方向；
flags 标志由 tcp 协议的三次握手过程显示
•[S]: SYN（开始连接）
•[P]: PSH（推送数据）
•[F]: FIN（结束连接）
•[R]: RST（重置连接）
•[.]: 没有 Flag
•[S.]: SYN-ACK（SYN 报文的应答报文）

tcpdump  更多参考链接：
http://confluence.alauda.cn/pages/viewpage.action?pageId=61911666
```

### 5、进程

```
1.查看可执行路径
# echo $PATH

2.查看可执行文件的完整路径
# which kubectl

3.查看正在运行进程的详情
# ps -ef |grep <pid>

4.查看僵尸进程：
# ps -A -o stat,ppid,pid,cmd | grep -e '^[Zz]'

5.进程状态说明
R	表示进程在 CPU 的就绪队列中，正在运行或者正在等待运行
D	是不可中断状态睡眠（Uninterruptible Sleep），一般表示进程正在跟硬件交互，并且交互过程不允许被其他进程或中断打断
Z	表示僵尸进程，也就是进程实际上已经结束了，但是父进程还没有回收它的资源（比如进程的描述符、PID 等）
S	是可中断状态睡眠，表示进程因为等待某个事件而被系统挂起。
I	是空闲状态，用在不可中断睡眠的内核线程上。
X	表示进程已经消亡
s	表示这个进程是一个会话的领导进程
+	表示前台进程组
<	优先级较高的进程
l	以线程的方式运行

更多内容请参考 Linux进程状态参考手册：
http://confluence.alauda.cn/pages/viewpage.action?pageId=61911671
```

### 6、系统常见故障

#### 1.1 执行df -Th卡住

```
使用starce查看df -Th 执行状态,如果是卡在了 "/proc/sys/fs/binfmt_misc"这个位置
解决方式：
# systemctl restart proc-sys-fs-binfmt_misc.automount
```

#### 1.2 系统报错内存不分配

```
系统日志报错SLUB: Unable to allocate memory on node -1时

解决思路：
1.kubelet二进制文件中关闭kmem

2.或者释放内存来临时解决

更多参考链接：
http://confluence.alauda.cn/pages/viewpage.action?pageId=61911675
```

#### 1.3 系统IO导致高负载

```
节点高负载会导致进程无法获得足够的 cpu 时间片来运行，通常表现为网络 timeout，健康检查失败，服务不可用。

有时候即便 cpu ‘us’ (user) 不高但 cpu ‘id’ (idle) 很高的情况节点负载也很高，这通常是文件 IO 性能达到瓶颈导致 IO WAIT 过多，从而使得节点整体负载升高，影响其它进程的性能。

一、top 命令：
使用 top 命令看下当前负载：
top - 19:42:06 up 23:59,  2 users,  load average: 34.64, 35.80, 35.76
Tasks: 679 total,   1 running, 678 sleeping,   0 stopped,   0 zombie
Cpu(s): 15.6%us,  1.7%sy,  0.0%ni, 74.7%id,  7.9%wa,  0.0%hi,  0.1%si,  0.0%st
Mem:  32865032k total, 30989168k used,  1875864k free,   370748k buffers
Swap:  8388604k total,     5440k used,  8383164k free,  7982424k cached
  PID USER      PR  NI  VIRT  RES  SHR S %CPU %MEM    TIME+  COMMAND
 9783 mysql     20   0 17.3g  16g 8104 S 186.9 52.3   3752:33 mysqld
 5700 nginx     20   0 1330m  66m 9496 S  8.9  0.2   0:20.82 php-fpm
 6424 nginx     20   0 1330m  65m 8372 S  8.3  0.2   0:04.97 php-fpm
 6573 nginx     20   0 1330m  64m 7368 S  8.3  0.2   0:01.49 php-fpm
 
wa (wait) 表示 IO WAIT 的 cpu 占用，默认看到的是所有核的平均值，要看每个核的 wa 值需要按下 “1”:
top - 19:42:08 up 23:59,  2 users,  load average: 34.64, 35.80, 35.76
Tasks: 679 total,   1 running, 678 sleeping,   0 stopped,   0 zombie
Cpu0  : 29.5%us,  3.7%sy,  0.0%ni, 48.7%id, 17.9%wa,  0.0%hi,  0.1%si,  0.0%st
Cpu1  : 29.3%us,  3.7%sy,  0.0%ni, 48.9%id, 17.9%wa,  0.0%hi,  0.1%si,  0.0%st
Cpu2  : 26.1%us,  3.1%sy,  0.0%ni, 64.4%id,  6.0%wa,  0.0%hi,  0.3%si,  0.0%st
Cpu3  : 25.9%us,  3.1%sy,  0.0%ni, 65.5%id,  5.4%wa,  0.0%hi,  0.1%si,  0.0%st

wa 通常是 0%，如果经常在 1 之上，说明存储设备的速度已经太慢，无法跟上 cpu 的处理速度。

可以使用 iotop -oPa 查看哪些进程占用磁盘 IO:
Total DISK READ: 15.02 K/s | Total DISK WRITE: 3.82 M/s
  PID  PRIO  USER     DISK READ  DISK WRITE  SWAPIN     IO>    COMMAND
 1930 be/4 root          0.00 B   1956.00 K  0.00 % 83.34 % [flush-8:0]
 5914 be/4 nginx         0.00 B      0.00 B  0.00 % 36.56 % nginx:cache manager process
  880 be/3 root          0.00 B     21.27 M  0.00 % 35.03 % [jbd2/sda5-8]
 5913 be/2 nginx        36.00 K   1000.00 K  0.00 %  8.94 % nginx: worker process
 5910 be/2 nginx         0.00 B   1048.00 K  0.00 %  8.43 % nginx: worker process
 5896 be/2 nginx        56.00 K    452.00 K  0.00 %  6.91 % nginx: worker process
结果中可以看到flush-8:0和nginx占用了大量IO
```

#### 1.4 es日志占用大量空间

```
先查看当前所有索引
# curl -X GET -u <es-user>:<es-password> localhost:9200/_cat/indices
green open log-20200214        RTo0Sga9QDmZM5g_DpH2Ww 5 1  9873013   0  11.1gb   5.5gb
green open log-20200215        yDXUiN6GRT6GakhnvazXQw 5 1      172   0 430.4kb 215.2kb
green open event-20200215      Il3YJgQ7QZSMCK6o7zuvEg 5 1     5495 529  12.4mb   6.2mb

根据索引日期删除陈旧索引
# curl -XDELETE http://<es-user>:<es-password> localhost:9200/log-20200214

更多参考链接：
http://confluence.alauda.cn/pages/viewpage.action?pageId=48729648 （清理过期日志的操作）
http://confluence.alauda.cn/pages/viewpage.action?pageId=50828051 （ES配置及吞性能测试）
```


## 常用巡检、定位及排障方法 -- Docker运维

### 1、常用命令工具

#### 1.1 定位容器和运行状态

```
查看需要排查的容器或者状态非running的容器：
# docker ps -a

查看容器的配置信息和运行时状态：
docker inspect <container-id或contianer-name>

或Web控制台中选择指定应用，并查看配置信息
```

#### 1.2 查看容器应用日志

```
在命令行中输入下列命令，查看指定容器在stdout/stderr的日志。
注意使用 json-file 和 journald 之外的日志驱动程序时 docker logs 命令不可用。
# docker logs <container-id或contianer-name>

实时查看容器最后100行日志
# docker logs -f --tail=100 <container-id或contianer-name>

或Web控制台中也支持查看对应的日志信息
```

#### 1.3 查看容器进程信息

```
列出指定容器中运行的进程信息：
# docker top <container-id或contianer-name>

注意：这里的PID显示的是宿主机操作系统上的PID信息

进入容器内查看进程
# docker exec nginx-pod ps -ef

单独查看指定容器所属docker子进程的PID
# docker inspect -f '{{ .State.Pid }} {{ .Id }}' $(docker ps -aq)
```

#### 1.4 进入容器诊断问题

```
进入容器内部执行命令：
# docker exec -ti <container-id或contianer-name> sh
注意：如果容器支持bash，也可以在shell命令行中输入bash来执行命令。

或Web控制台中也支持exec的功能
```

#### 1.5 查看容器性能信息

```
查看容器的性能监控信息：
# docker stats <container-id或contianer-name>

或Web控制台中监控面板中也可查看容器的监控信息
```

#### 1.6 查看Docker日志

```
有时因为系统原因，Docker Engine无法正常创建、删除、启动、停止容器，我们需要查询Docker Engine日志来排查信息
# journalctl -xefu docker
```

### 2、Docker配置管理

#### 1.1 Docker与内核的版本

```
查看Client端和Server端的版本是否一致
# docker version

查看系统内核版本，高版本docker最好使用高版本内核（如CentOS的3.10.x版本）
# uname -r

docker版本升级
1.建议备份etcd数据
2.需要准备docker源
3.需要同步升级对应支持的内核版本
4.详情查阅专门的文档《docker升级文档》
```

#### 1.2 查看Docker配置

```
许多Linux发行版本使用systemd来启动docker daemon(即dockerd)

1.systemd下的docker配置
# systemctl status docker
● docker.service - Docker Application Container Engine
   Loaded: loaded (/usr/lib/systemd/system/docker.service; enabled; vendor preset: disabled)
  Drop-In: /etc/systemd/system/docker.service.d
           └─http-proxy.conf
   Active: active (running) since Tue 2020-02-11 16:31:12 CST; 17h ago
说明：
Loaded: 对应的是服务启动文件/usr/lib/systemd/system/docker.service
Drop-In:使用systemd的drop-in文件，即创建.conf文件放置在此处以添加新选项或覆盖服务启动文件的参数
Active: 描述服务当前的状态

2.dockerd默认配置文件
dockerd可以通过配置文件/etc/docker/daemon.json或者/etc/sysconfig/docker来修改来进行参数配置，默认无此文件创建即可；注意使用时两个文件有一个即可，目前常用的为daemon.json文件；

如：部分添加参数（insecure-registries #配置docker的私库地址、storage-driver #存储驱动）
2.1）修改或新增 /etc/docker/daemon.json
{
    "insecure-registries": [
       "11.11.11.11:8086"       
    ],    
    "storage-driver": "overlay2",

}
或者

2.2）修改或新增 /etc/sysconfig/docker
OPTIONS='--selinux-enabled --insecure-registry=11.11.11.11:8086'


设置后重启 docker 即可生效
# systemctl daemon-reload && systemctl restart docker

3.通过以下命令可查看docker在当前环境使用的配置
# systemctl show docker
```

#### 1.3 查看Docker驱动

```
查看docker的详细配置信息和状态
# docker info

1.查看存储驱动
# docker info |grep 'Storage Driver'
说明：常用的有overlay2和devicemapper等存储驱动，平台推荐使用overlay2

2.查看文件驱动
# docker info |grep 'Cgroup Driver'
说明：k8s的文件驱动默认使用的是cgroupfs，因此请注意docker和k8s的文件驱动保持一致。

3.查看日志驱动
# docker info |grep 'Logging Driver'
说明：docker默认的日志驱动为json-file，在使用 json-file 和 journald 之外的日志驱动程序时 docker logs 命令不可用
```

### 3、Docker常见故障

#### 1.1 Docker服务无法启动

```
通过下面的命令查看docker问题原因
# systemctl status docker -l
# journalctl -amu docker

问题可能性：
1./etc/docker/daemon.json等配置文件内容错误：存储驱动冲突、文件格式错误 如多了“,”号等原因
可通过以下命令进行检查：
# cat /etc/docker/daemon.json | jq  
# 或 cat /etc/docker/daemon.json | python -m tool.json
2.系统原因导致：磁盘内存等资源不足
```

#### 1.2 Docker相关命令卡死

```
docker ps等命令卡死（目前只出现在18.09以下的docker版本中）
问题可能性：
1.节点资源不足，需要释放或者添加资源
2.docker本身bug，可将低版本的docker升级至高版本来解决
3.升级内核至系统对应支持的高版本内核
```


## 常用巡检、定位及排障方法 -- kubernetes 运维

### 1、常用命令行工具

#### 1.1 Kubectl命令补全

```
通过completion命令来设置 kubectl 命令自动补全

在 bash 中设置当前 shell 的自动补全，要先安装 bash-completion 包
# source <(kubectl completion bash)

在bash shell中永久的添加自动补全
# echo "source <(kubectl completion bash)" >> ~/.bashrc
```

#### 1.2 获取资源

```
查看所有的资源信息,检查相关资源是否为“Running”正常状态
# kubectl get all

根据指定标签匹配到具体的pod
# kubectl get pods -l app=example

显示node节点的标签信息
# kubectl get node --show-labels

查看pod详细信息，也就是可以查看pod具体运行在哪个节点上（ip地址信息）
# kubectl get pod -o wide

查看Service的信息，假如Pod运行正常，但是又无法访问（集群内、外部）这时，我们需要检查Service是否正常
# kubectl get svc -n kube-system

查看命名空间
# kubectl get ns

查看所有pod所属的命名空间并且查看都在哪些节点上运行
# kubectl get pod --all-namespaces  -o wide

查看目前所有的replica set，显示了所有的pod的副本数，以及他们的可用数量以及状态等信息
# kubectl get rs

查看已经部署了的所有应用，可以看到容器，以及容器所用的镜像，标签等信息
# kubectl get deployments -o wide

查看创建的ingress资源
# kubectl get ingress 

查看集群内定义的 CRD 资源,CRD 允许我们基于已有的 Kube 资源，拓展集群能力,CRD 可以使我们自己定义一套成体系的规范，自造概念
# kubectl get crd 

查看集群健康情况
# kubectl get cs

查看集群节点状态
# kubectl get node -o wide
说明：“Ready”表示节点已就绪，为正常状态，反之则该节点出现异常。节点出现问题，则Pod无法调度到该节点。

使用journalctl查看服务日志,获取集群相关日志信息
# journalctl -xefu docker
# journalctl -f -u kubelet

使用“kubectl logs”查看容器日志,获取pod日志信息
# kubectl logs -f <pod-name> -n <namespaces>
```

#### 1.3 创建资源

```
# 根据文件或者输入来创建资源

创建资源，无法更新，必须先delete，不建议使用
# kubectl create -f xxx.yaml 

创建+更新，可以重复使用
# kubectl apply -f xxx.yaml

运行一个名称为nginx，副本数为3，标签为app=example，镜像为nginx:1.10，端口为80的容器实例
# kubectl run nginx --replicas=3 --labels="app=example" --image=nginx:1.10 --port=80

创建一个Deployment资源
# kubectl create deployment nginx --image=nginx

创建一个nginx服务并且暴露端口让外界可以访问
# kubectl expose deployment nginx --port=88 --target-port=80 --type=NodePort
```

#### 1.4 删除资源

```
通过具体的资源名称来删除deployment和service等资源
# kubectl delete <具体的资源名称>

也可以根据yaml文件删除对应的资源，但是yaml文件并不会被删除，这样更加高效
# kubectl delete -f xxx-deployment.yaml
# kubectl delete -f xxx-service.yaml

特殊情况，调用kube-api删除资源
如：
ns删不掉，直接修改 yaml 中的值，保存后，再调用kube-api来删除

可以在 master 上运行下面的命令删除所有 Terminating 状态的 ns，*** 注意：危险的命令，执行前一定要备份 etcd，并且审慎执行，错误删除后果很严重***  命令如下：

​```
for ns in $(kubectl get ns | grep Terminating | awk '{print $1}')
do
    kubectl get ns $ns -o json | jq 'del(.spec.finalizers)' | curl -v -H "Content-Type: application/json" -X PUT --data-binary @- http://127.0.0.1:8080/api/v1/namespaces/$ns/finalize
done
​```
```

#### 1.5 更新资源

```
编辑Deployment nginx的一些信息
# kubectl edit deployment nginx

编辑service类型的nginx的一些信息
# kubectl edit service/nginx       

将deployment中的nginx容器镜像更新为“nginx：1.9.1”，并记录当前操作的命令
# kubectl set image deployment/nginx-deployment nginx=nginx:1.9.1  --record
```

#### 1.6 动态伸缩pod

```
#根据<具体的资源名称>或者<文件>进行扩缩容

扩容：将名为nginx中的pod副本数设置为5
# kubectl scale deployment nginx --replicas 5 

缩容：将由“xxx.yaml”配置文件中指定的资源对象和名称标识的Pod资源副本设为3
# kubectl scale --replicas=3 -f xxx.yaml   
```

#### 1.7 交互命令

```
持续输出pod nginx中的容器web-1的日志
# kubectl logs -f nginx -c web-1

进入nginx容器
# kubectl exec -it nginx-deployment-58d6d6ccb8-lc5fp bash

从容器拷贝文件到物理机
# kubectl cp mysql-478535978-1dnm2:/tmp/message.log message.log

从物理机拷贝文件到容器
# kubectl cp message.log mysql-478535978-1dnm2:/tmp/message.log
```

#### 1.8 节点维护

```
使用top命令查看资源的cpu，内存磁盘等资源的使用率。前提集群已经有metrics-server
# kubectl top pod --all-namespaces

使用cordon命令将node1标记为不可调度
# kubectl cordon node1

查看节点状态，发现node1虽然还处于Ready状态，但是同时还被禁能了调度
# kubectl get node1
NAME        STATUS                     AGE
node1       Ready,SchedulingDisabled   1d

执行drain命令，将运行在node1上运行的pod平滑的赶到其他节点上
# kubectl drain node1

节点维护完后，使用uncordon命令解锁node1，使其重新变得可调度；
# kubectl uncordon node1
```

#### 1.9 设置标签

```
# label命令: 用于更新（增加、修改或删除）资源上的 label（标签）

给集群中node01节点添加node角色标签
# kubectl label nodes node01 node-role.kubernetes.io/node=

给名为nginx的Pod添加release=haha的标签,然后查看返回
# kubectl label pod nginx-pod  hehehe=haha

根据对应的label列出pod
# kubectl get pods -l hehehe  --show-labels
NAME        READY   STATUS    RESTARTS   AGE   LABELS
nginx-pod   1/1     Running   1          1h    hehehe=haha

删除一个Label，只需在命令行最后指定Label的key名并使用“ - ” 减号相连
# kubectl label pods nginx-pod hehehe-
```

#### 1.10 污点和容忍

```
使用taint命令给某个Node节点设置污点
# kubectl taint node k8s-node1 hehe-name=hahaha:NoExecute
删除污点命令
# kubectl taint node k8s-node1 hehe-name-

说明：给k8s-node1节点设置 key 为 hehe-name，value为 hahaha 的taint（污点），只要拥有和这个 taint相匹配的 toleration（容忍） 的 pod 才能够被分配到 k8s-node1 这个节点上。


pod添加容忍
pod 定义 toleration，匹配 key 为 hehe-name，value 为 hahaha 的 taint
cat > nginx-pod.yaml <<EOF
apiVersion: v1
kind: Pod
metadata:
  name: nginx-pod
  labels:
    app: nginx-pod
spec:
  containers:
  - name: nginx-pod
    image: nginx
    resources:
      limits:
        cpu: 30m
        memory: 20Mi
      requests:
        cpu: 20m
        memory: 10Mi
  tolerations:
  - key: hehe-name
    value: haha
    operator: Equal
    effect: NoExecute
EOF
```

#### 1.11 格式化输出

```
kubectl命令可用多种格式对结果进行显示，输出格式通过-o参数指定：如“json”、“yaml”、“jsonpath”等格式

查看指定Pod配置
# kubectl get pods mysql-58b6bff865-xdxx8 -o json

查看svc服务配置
# kubectl get svc mysql -o yaml

查看deployment配置
# kubectl get deployments mysql -o yaml

通过jsonpath统计分区kube-system下状态为Running的Pod中镜像名称
# kubectl get pods -o=jsonpath="{range .items[*]}{.status.phase=="Running"}{\"\t\"}{.spec.containers[*].image}{\"\n\"}{end}" -n kube-system

通过jsonpath获取到nginx01容器所在节点名称
# kubectl get po nginx01 -ojsonpath='{..nodeName}'
注意：
如果仅仅是查看结果也可通过grep来获取到同样的信息，但通过jsonpath是准确地获取到了一个属性的值,而grep则是截取的包含这个关键字的一行,如果我们要把获取的值作为下一个命令的的输入值时,通过grep获取的结果往往是需要处理的。例如通过grep获取到的结果如下：
# kubectl get po nginx01  -ojson|grep nodeName
  "nodeName": "k8s-node1"
```


#### 1.12 curl调用kube-api

```
说明：
k8s通过kube-apiserver这个进程提供服务，该进程运行在单个k8s-master节点上。默认有两个端口，http的8080（使用参数 --insecure-port）和 使用https的6443端口（--secure-port=6443）,两者功能相同，只https安全性更高。

常用的kubernetes API访问方式：
1.命令行工具kubectl
2.使用curl命令调用api

此处主要介绍curl命令使用token调用kube-api的使用

HTTP动词：（POST、DELETE、PUT、PUT）分别对应（增、删、查、改）

token的获取方法：
# TOKEN=$(kubectl describe secret $(kubectl get secrets | grep default | cut -f1 -d ' ') | grep -E '^token' | cut -f2 -d':' | tr -d '\t')

使用token查看支持的资源对象：
# curl -H "Authorization: Bearer $TOKEN" --insecure https://127.0.0.1:6443/api/v1
 
列出指定节点内所有Pod的信息
# curl -H "Authorization: Bearer $TOKEN" --insecure https://127.0.0.1:6443/api/v1/nodes/{name或ip}/pods/

列出指定节点内物理资源的统计信息
# curl -H "Authorization: Bearer $TOKEN" --insecure https://127.0.0.1:6443/api/v1/nodes/{name或ip}/stats/

创建一个namespaces
# curl -s -X  "POST" -H "Authorization: Bearer $TOKEN" --insecure https://127.0.0.1:6443/api/v1/namespaces
```

### 2、集群配置管理

#### 1.1 查看k8s文件

```
使用kubeadm安装的集群会有以下重要配置文件：

1.kubeadm初始化集群时为其他组件生成访问kube-ApiServer所需的配置文件xxx.conf（即kubeconfig文件）
比如：kubernetes-admin这个用户及相关公私钥和证书信息的admin.conf文件，同时kubectl config的配置信息中需要三个证书信息，也可以在admin.conf中找到
/etc/kubernetes/admin.conf
其他还有以下kubeconfig文件
/etc/kubernetes/controller-manager.conf
/etc/kubernetes/scheduler.conf
/etc/kubernetes/kubelet.conf
这些文件记录当前Master节点的{服务器地址、监听端口、证书目录等信息} 这样，对应的客户端（比如scheduler，kubelet等），可以直接加载相应的文件，使用里面的信息与kube-apiserver建立安全连接。

2.kubeadm会为Master组件生成Pod配置文件，这些组件会被master节点上的kubelet读取到，并且创建对应资源

## etcd保存了整个集群的状态
/etc/kubernetes/manifests/etcd.yaml

## apiserver提供了资源操作的唯一入口，并提供认证、授权、访问控制、API注册和发现等机制；
/etc/kubernetes/manifests/kube-apiserver.yaml

## 负责维护集群的状态，比如故障检测、自动扩展、滚动更新等；
/etc/kubernetes/manifests/kube-controller-manager.yaml   

## 负责资源的调度，按照预定的调度策略将Pod调度到相应的机器上；
/etc/kubernetes/manifests/kube-scheduler.yaml  

这些YAML只要出现在，被kubelet监视的/etc/kubernetes/manifests目录中，kubelet就会自动创建这些YAML中定义的Pod（Master组件的容器）Master容器启动后，kubeadm会通过检localhost:6443/healthz（Master组件的健康检查URL），等待Master组件完全运行

更多参考kubeadm集群相关配置手册：
http://confluence.alauda.cn/pages/viewpage.action?pageId=61911719
```


#### 1.2 查看及升级k8s版本

##### 1.2.1 查看k8s版本

```
在master节点执行以下命令，进行查看
$(which kubeadm) version
$(which kubectl) version
$(which kubelet) --version
```

##### 1.2.2 升级k8s版本

*** 升级会有专门的文档，这里就不展开讨论了 ***

```
1.建议先将 etcd数据、k8s配置文件的备份
2.k8s只能逐版本升级，举例：1.11.x —>1.12.x —>1.13.4
3.注意1.13版本之前，一定要在集群的master-init节点升级，查看master-init节点命令：kubectl get cm -n kube-system -o yaml |grep -i advertiseAddress
4.详情查阅专门的文档《kubernetes版本升级手册》
```

#### 1.3 查看及升级k8s证书

使用kubeadm创建完Kubernetes集群后, 默认会在/etc/kubernetes/pki目录下存放集群中需要用到的证书文件

##### 1.3.1 查看证书有效期

```
查看证书有效期，举例：
# openssl x509 -noout -text  -in /etc/kubernetes/pki/apiserver.crt  |grep Not

查看证书是否更新
# curl  https://`hostname -i`:6443-v --cacert
```

##### 1.3.2 相关证书说明

```
Kubernetes把证书放在了两个文件夹中
/etc/kubernetes/pki
/etc/kubernetes/pki/etcd

先从由Etcd集群根证书签发的证书算起：
1、etcd-server服务端证书，etcd对外提供服务
2、etcd-peer客户端证书，etcd集群内部各节点之间进行通信
3、healthcheck-client客户端证书，pod中定义有 Liveness 探针所使用的客户端证书
4、以及额外的配置在kube-apiserver中用来与etcd-server做双向认证的apiserver-etcd-client客户端证书

再算Kubernetes集群根证书签发的证书：
1、apiserver证书，kube-apiserver 组件持有的服务端证书,对外提供服务
2、kubelet证书，kubelet组件服务端证书，对外提供服务
3、controller-manager证书，仅用来签署service account的证书
4、scheduler证书，访问APIserver的证书
5、apiserver-kubelet-client证书，kubelet 组件持有的客户端证书, 用作 kube-apiserver 主动向 kubelet 发起请求时的客户端认证
6、front-proxy-client证书，代理端使用的客户端证书, 使用kubectl proxy来支持ssl代理用户与 kube-apiserver 认证
```

##### 1.3.3 升级证书

```
1.建议先etcd数据备份
2.kubeadm创建的CA证书默认的有效期是10年, 但根证书在master节点/etc/kubernetes/pki下面创建的其他若干证书默认只有1年的有效期，因此注意证书的有效期
3.详情查阅专门的文档《kubernetes证书更新手册》
```

### 3、k8s常见故障分析

#### 重点：Pod详解及故障分析图

Pod故障图解、Pod创建流程图、Pod状态说明 在线文档及附件
http://confluence.alauda.cn/pages/viewpage.action?pageId=61911678

##### Pod状态

| 状态      | 描述                                                         |
| --------- | ------------------------------------------------------------ |
| Running   | 该 Pod 已经绑定到了一个节点上，Pod 中所有的容器都已被创建。至少有一个容器正在运行，或者正处于启动或重启状态。 |
| Pending   | Pod 已被 Kubernetes 系统接受，但有一个或者多个容器镜像尚未创建。等待时间包括调度 Pod 的时间和通过网络下载镜像的时间，这可能需要花点时间。创建pod的请求已经被k8s接受，但是容器并没有启动成功，可能处在：写数据到etcd，调度，pull镜像，启动容器这四个阶段中的任何一个阶段，pending伴随的事件通常会有：ADDED, Modified这两个事件的产生 |
| Succeeded | Pod中的所有的容器已经正常的自行退出，并且k8s永远不会自动重启这些容器，一般会是在部署job的时候会出现。 |
| Failed    | Pod 中的所有容器都已终止了，并且至少有一个容器是因为失败终止。也就是说，容器以非0状态退出或者被系统终止。 |
| Unknown   | 出于某种原因，无法获得Pod的状态，通常是由于与Pod主机通信时出错。 |

#####  Pod详细状态说明

| 状态                                  | 描述                          |
| ------------------------------------- | ----------------------------- |
| CrashLoopBackOff                      | 容器退出，kubelet正在将它重启 |
| InvalidImageName                      | 无法解析镜像名称              |
| ImageInspectError                     | 无法校验镜像                  |
| ErrImageNeverPull                     | 策略禁止拉取镜像              |
| ImagePullBackOff                      | 正在重试拉取                  |
| RegistryUnavailable                   | 连接不到镜像中心              |
| ErrImagePull                          | 通用的拉取镜像出错            |
| CreateContainerConfigError            | 不能创建kubelet使用的容器配置 |
| CreateContainerError                  | 创建容器失败                  |
| m.internalLifecycle.PreStartContainer | 执行hook报错                  |
| RunContainerError                     | 启动容器失败                  |
| PostStartHookError                    | 执行hook报错                  |
| ContainersNotInitialized              | 容器没有初始化完毕            |
| ContainersNotRead                     | 容器没有准备完毕              |
| ContainerCreating                     | 容器创建中                    |
| PodInitializing                       | pod 初始化中                  |
| DockerDaemonNotReady                  | docker还没有完全启动          |
| NetworkPluginNotReady                 | 网络插件还没有完全启动        |

##### Pod（创建流程图）

![](images/pod 创建流程图.png)

Pod创建流程简析
1、客户端提交创建请求,可以使用kubectl命令行工具,也可以通过API Server的Restful API, 支持的数据类型包括JSON和YAML

2、API Server接收到pod创建请求后,不会直接创建pod, 而是生成一个包含创建信息的yaml, 然后将Pod数据存储到etcd, 创建deployment资源并初始化

3、controller监测发现新的deployment, 将该资源加入到内部工作队列, 发现该资源没有关联的pod和replicaset, 启用deployment controller创建replicaset资源,再启用replicaset controller创建pod

4、所有controller正常后,将deployment,replicaset,pod资源更新存储到etcd

5、scheduler监听 API Server上新的pod, 经过主机过滤主机打分规则, 尝试为Pod分配主机,将pod绑定(binding)到合适的主机

6、将绑定结果存储到etcd

7、kubelet根据调度结果执行Pod创建操作： 绑定成功后，scheduler会调用APIServer的API在etcd中创建一个boundpod对象，描述在自身Node上绑定运行的所有pod清单信息。运行在每个node节点上的kubelet也会定期与etcd同步boundpod信息，一旦发现应该在该node节点上运行的boundpod对象没有更新，则调用Docker API创建并启动pod内的容器

8.把本节点的容器信息pod信息同步到etcd

##### Pod（故障图解）

![](images/pod 故障图解.png)



#### 1.1 容器触发OOM

```
即Out-of-memory的缩写，对于内存来说，容器使用内存超过限制的limit时，这个容器就会被OOM kill掉；如果一个结束的容器允许重启，kubelet就会重启他，但是会出现其他类型的运行错误。

错误现象如：
1.dmesg -T |grep -A 3 'Task in'
Memory cgroup out of memory: Kill process 20234 

2.kubectl get po
NAME            READY     STATUS      RESTARTS   AGE
memory-test     0/1       OOMKilled   1          24s

解决思路：
1）排除pod占用内存的进程为不正常状态
2）系统内存充足的情况下，加大pod的limit内存限制
3）将占用内存高的pod，定点调度至资源充足的节点；或本节点加污点等方法
4）内存资源不足时，条件允许时可释放内存来临时解决
```

#### 1.2 POD被驱逐

```
当集群中不可压缩资源（内存，磁盘 IO）资源不足时，此时 Kubernetes 会从该节点上通过 kubelet 来驱逐一定数量的 Pod，以保证该节点上有充足的资源。

错误现象如：
1.web界面应用显示多条failed状态，或者 kubectl get po查看返回有Evicted的pod
NAME                       READY     STATUS      RESTARTS   AGE
memory-6b69d6467-2b9wp     0/1       Evicted     0          14s
memory-6b69d6467-3n8nz     0/1       Evicted     0          12s

2.kubectl describe node01 可以看到节点被打上污点了
Taints: node.kubernetes.io/disk-pressure:NoSchedule

3.流水线报以下错误
Cannot contact jenkins-slave-wlbxj-71wtb:hudson.remoting.ChanelClosedException:Chanel"unknow":Remote call on JNLP4-connect connection from 172-0-7-251.inghtspeed.brhmal.sbcglobal.net/172.0.7.251:59804 failed.The channel is closing down or has closed down
原因：运行jenkins-slave节点的var被message系统日志填满超过85%，导致slave被驱逐了

解决思路：
1）释放磁盘、内存等资源
2）将占用资源大的pod，定点部署在资源充足的节点
3）添加工作节点
```

#### 1.3 节点NotReady

```
如果Node状态为NotReady，一般通过查看节点事件或者kubelet服务，可以有助于我们排查问题

错误现象：
1.当前节点宕机

2.在 Conditions:段落可以获得初步的判断信息，例如: # kubectl describe node01
Conditions:
  Type             Status    Reason              Message
  ----             ------    ------              -------
  OutOfDisk        Unknown   NodeStatusUnknown   Kubelet stopped posting node status.
  MemoryPressure   Unknown   NodeStatusUnknown   Kubelet stopped posting node status.
  DiskPressure     Unknown   NodeStatusUnknown   Kubelet stopped posting node status.
  PIDPressure      Unknown   NodeStatusUnknown   Kubelet stopped posting node status.
  Ready            Unknown   NodeStatusUnknown   Kubelet stopped posting node status.


3.客户端 Kubelet 报错,如：# journalctl -f -u kubelet
-- Logs begin at 一 2019-10-28 16:09:21 CST. --
10月 28 16:50:48 k8s-node2 kubelet[14216]: W1028 16:50:48.984435   14216 cni.go:213] Unable to update cni config: No networks found in /etc/cni/net.d
10月 28 16:50:49 k8s-node2 kubelet[14216]: E1028 16:50:49.506102   14216 kubelet.go:2169] Container runtime network not ready: NetworkReady=false reason:NetworkPluginNotReady message:docker: network plugin is not ready: cni config uninitialized


解决思路：
1）节点宕机时恢复节点即可
2）节点资源不足，或者kubelet响应超时，需要重启kubelet
3）网络问题导致节点notReady，缺少网络插件
```

#### 1.4 集群内pod不通

```
错误现象：集群内部新添加两个工作节点后，这两个node节点上的pod互相访问不通，在自己节点上访问pod正常

解决思路：
1.检查node节点iptables

2.docker info 检查节点bridge是否放开，即主机内核参数中需要添加以下两个参数         
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
```

#### 1.5 Pod 内无法解析 DNS

```
集群中的DNS解析过程：pod的解析一般默认指向-->kube-dns（coredns）的Service IP，而集群内的dns又指向-->集群外部的DNS服务器地址，一般为所在node的/etc/resolv.conf文件中的地址

解决思路：
1.可能是 kube-dns 服务异常导致的，检查 kube-dns 是否处于正常运行状态
2.pod和所在node节点的dns解析地址不一致
3.Node 上面运行 iptables -P FORWARD ACCEPT。因为高版本Docker 会把默认的 iptables FORWARD 策略改为 DROP。这会引发 Pod 网络访问的问题
4.可能 kube-dns service 不存在，或者 endpoints 列表为空，则说明 kube-dns service 配置错误，可以重新创建 kube-dns service
5.可能kube-dns Pod 和 Service 都正常，那么就需要检查 kube-proxy 是否正确为 kube-dns 配置了负载均衡的 iptables 规则
```

#### 1.6 访问ClusterIP失败

```
错误现象：pod节点内无法访问ClusterIP

解决思路：
1.先确认是否有对应的 Endpoints，可通过以下命令来查看 # kubectl get endpoints <service-name>
2.Pod 的 containerPort 与 Service 的 containerPort 是否对应
3.直接访问 podIP:containerPort 是否正常
4.CNI 网络或主机路由异常也会导致类似的问题
5.kube-proxy服务异常，有可能未启动或者未正确配置相应的 iptables 规则
```


<div STYLE="page-break-after: always;"></div>


## 巡检平台功能

### 登录

![](images/00logoin1.png)

根据账号类型，选择合适的方式登录平台。

### UI 界面

打开浏览器，访问平台 UI。

![](images/view.png)

### 产品切换

管理员登录后，单击左上角，在各个产品间切换。


### 视图切换

分别在每个产品的 UI 页面上，单击右上角开关，进行管理视图和业务视图的切换。

#### 管理视图

在管理视图，当操作的资源和集群相关时，可灵活切换集群。

![](images/adminview1.png)

#### 业务视图

在业务视图，可灵活切换项目和命名空间。

![](images/userview.png)

### 集群状态

进入 Container Platform 产品的 UI，单击右上角，切换到管理视图。单击左侧导航栏中的集群，查看集群信息。

![](images/cluster.png)

单击集群右上角 ![](images/003point.png) 并选择集群管理，将跳转至集群管理页面。

![](images/clusterma.png)

单击集群详细页面，单击左上角基本信息、监控、项目。单击右上角操作下拉菜单，单击各个按钮检查是否正常。

![](images/clusteract.png)

### 日志

在管理视图，单击左侧导航栏中的日志，查看是否有日志信息。

在日志搜索框选择或输入查询条件，查询日志，例如：`cluster:global` 就是搜索 global 集群的日志。查询语句里，前面是条件，支持 cluster、node、application、path、source、project、instance ，后面是变量，根据集群实际情况填写。

![](images/log.png)



### 监控

在管理视图，单击左侧导航栏中的监控面板，查看不同集群监控情况。

可通过顶部导航栏中的集群切换入口，在不同集群的监控面板间进行切换。

![](images/monitor.png)

### 事件

在管理视图，单击左侧导航栏中的事件，查看不同集群的事件。

可通过顶部导航栏中的集群切换入口，在不同集群的监控面板间进行切换。

![](images/event.png)

### 告警

在管理视图，单击左侧导航栏中的告警，查看不同集群的告警配置。

可通过顶部导航栏中的集群切换入口，在不同集群的监控面板间进行切换。

![](images/alarm.png)

### 通知

在管理视图，单击左侧导航栏中的通知，单击各个菜单巡检功能是否正常。

![](images/notification.png)

### 存储

在管理视图，单击左侧导航栏中的存储，查看不同集群的存储配置。

可通过顶部导航栏中的集群切换入口，在不同集群的监控面板间进行切换。

![](images/pv.png)

### 网络

在管理视图，单击左侧导航栏中的网络，查看不同集群的负载均衡、子网、域名。

可通过顶部导航栏中的集群切换入口，在不同集群的监控面板间进行切换。

![](images/net.png)

### 项目

切换到业务视图，选择不同集群、选择不同项目。

![](images/project.png)

单击右侧进入按钮，进入项目的命名空间后，查看命名空间中的各项资源。

![](images/userview.png)


## 巡检 init 节点

### 检查容器

在 init 节点执行 `docker ps -a`

### pkg-registry

在 init 节点执行 `docker exec pkg-registry ls` 检查容器是否正常。

在 init 节点执行 `curl 127.0.0.1:60080/v2/_catalog | jq` 查看功能是否正常。

### chart repo

在 init 节点执行 `docker exec chart-repo ls` 检查容器是否正常。

在 init 节点执行 `curl $(cat /cpaas/install_info | awk -F '=' '/^CHART_ENDPOINT/{print $2}' | sed 's#/$##')/api/charts | jq` 查看功能是否正常。

### 软件源

在 init 节点执行 `docker exec yum ls` 检查容器是否正常。

在 init 节点执行 `curl 127.0.0.1:7000` 查看功能是否正常。



# 平台备份

## init 节点

备份 “/cpaas” 目录和安装目录。

##  Kubernetes 集群

### etcd 备份

请参考下面 etcd 运维部分

### global es 数据

默认不备份日志数据，如果客户有这样的需求，建议采用扩充 es 集群的节点数量，然后将副本数提高的方式备份，方式如下：

参考 **增加 es 节点数量**，然后修改 ***ALAUDA\_ES\_REPLICA*** 变量的值，默认是 1 副本，扩容节点数量后，可以扩到 2 或更多的副本数。

### 监控数据

默认不备份监控数据，如果客户有这样的需求，将每一个业务服务集群上，普罗米修斯运行的节点上的普罗米修斯挂卷的目录备份即可。


<div STYLE="page-break-after: always;"></div>

# 常见错误

## 基础设施

###  容器平台 2.x 认证及权限

#### 检查认证及权限组件状态是否正常的方法

#### 认证及权限相关故障排查

##### 401 问题

1. 查看 kube-apiserver log，搜索“oidc”关键词，看具体错误信息，如 x509 一般是证书用错了和 global 不一致。

2. 检查 apiserver 是否配置 oidc 项。

3. 确认网络是否通，curl dex 配置是否有返回。`curl -k https://xxxx.xxxx/dex/.well-known/openid-configuration`。

##### 403 问题

1. 检查所在集群是否安装 auth-controller2 或者 running。

2. 检查所在集群是否有 userbindings 资源, kubectl get userbindings。

	如果没有，重启 global 集群 auth-controller2 pod。如果有，则重启当前集群 auth-controller2 pod。
	
##### 登录报 500 问题

1. 确认 dex 是否 running。

2. 进去任意前端 pod 内（icarus ,diablo ,underlord ,mephisto），curl dex 配置是否有返回。

	```
	curl -k https://xxxx.xxxx/dex/.well-known/openid-configuration
	```

### 容器平台 2.x 网络相关问题

#### 检查网络组件状态是否正常的方法

##### dns

执行 `kubectl get svc -n kube-system kube-dns` 获取 kube-dns 的地址，然后执行 `nslookup kube-dns.kube-system.svc.cluster.local <上一步获取到的地址>` 验证解析是否成功。

##### 检查 calico

##### 检查 flannel

##### 检查 kube-ovn

##### 检查 alb

访问挂载到 alb 上的服务是否正常。

##### 检查 ingress

1. 执行`kubectl get ingress --all-namespaces` 获取 ingress。

2. 执行`kubectl get ingress -n <上一步获取到的 ns> <上一步获取到 ingress 名> -o yaml` 获取 ingress 详细信息。

3. 执行 `curl <ingress 的 host>/<ingress 的 path>` 来检查这个 ingress。

#### 网络相关故障排查

#### alb 故障排查

#### calico 网络故障排查

##### calico网络基础要求

需要开放 **每个node** 的 179 端口跑 BGP 协议，节点会通过 BGP 交换路由。如果子网启用 IPIP 的话，还需要额外开放 IPIP 协议。路由在 calico 网络中非常重要，直接影响容器网络通信。

**注意**：calico 子网有三种模式，ipipMode: Never/Always/CrossSubnet。含义为 pod 跨主机流量。  

* Always：总是启用 IPIP 隧道 。 

* CrossSubnet：当宿主机处于不同子网时，才启用IPIP隧道，宿主机同子网时，只用路由转发直连。 

* Never：只用路由转发直连。

##### calico 网络不通常规排查办法

先确定是谁和谁不通，由于是calico网络问题，至少其中有一方为非主机网络启动的 Pod，有属于 calico 子网的 Pod IP。

* 排查路由。 

	可以先判断下路由是否正常，这时在另一方所在的宿主机上执行 `ip route get 刚才获取的Pod IP`, 获取到这个 PodIP 的路由，一般情况下除非做了路由反射器的配置，这里合理的结果只有三种：
		
	1) `172.168.19.1 dev calibab86019bb9 src 192.168.16.86` ：Pod IP 在本机的情况。
	
	2) `172.168.19.67 via 192.168.16.87 dev tunl0 src 172.168.19.0` ：Pod IP 在其他机器，且开启了 IPIP 的情况。
	
	3) `172.168.19.67 via 192.168.16.87 dev eth0 src 192.168.16.86` ：Pod IP 在其他机器，走直连路由的情况。

典型的错误结果为 `8.8.8.8 via 192.168.16.1 dev eth0 src 192.168.16.86` ：这里走了默认网关，多半是 bgp 路由交换出问题失败了。

* 检查 BGP 连接状态。

	可以借助 calicoctl 这个官方的二进制工具来检查下 BGP 连接状态，该文件已经打包在了 raven 组件中。

	先执行 `kubectl -n kube-system cp ravenxxxxx:/raven/calicoctl /root/calicoctl`。在执行  `chmod +x /root/calicoctl` 后再执行 `./calicoctl node status`，即可查看 BGP 的连接情况。<br>**说明**：一般情况下这个表格里会显示与执行命令所在节点相连接的其他节点 IP，及连接情况，正常情况下连接的状态应该为 Established。如果不是则需要进一步排查，一般有如下几种情况可能导致这个状态异常。 
	
	* 环境 179 端口/BGP 协议被限制了。需要跟客户确认，或者自己进行 telnet/nc 检查。  
	
	* 宿主机多网卡，BGP 使用了错误的 IP。参考下面的 calico 多网卡环境调整来修改。  
	
	* as 号错误，这种一般出现与配置了路由反射器的情况，需要检查 BGP 配置里的 as 是否正确。如果是 calico 3.3.0 用默认 as 号可能出现问题，需要升级 calico-node 的镜像到 3.3.3 及以上，calico 3.9 不存在这个问题。  
	
	* calico-node 的 pod 没启动/状态不对。
	
	**说明**：如果这个表格里的节点数量不对也需要排查。可以检查 kube-system 命名空间下的 calico-node pod 的状态是否为运行中，如果不是，可以执行 `kubectl logs/describe` 看下这个 pod 的日志和事件。

* 排查网络限制

	如果 IPIP 协议没开放，也会导致路由正常，但是跨节点 ping podip 不通。
	
	这时可以取 2 个准备加入集群的机器手工构造下 ipip 隧道。
	
	```
	bash
	NOTE: 取两节点手动构造ipip隧道做通讯。给出命令和抓包
	
	节点A：ip eth0 192.168.16.226, 隧道ip用100.96.0.1
	
	ip tunnel add tunl0 mode ipip
	
	ifconfig tunl0 100.96.0.1 netmask 255.255.255.0
	
	ip route add 100.96.0.2/32 via 192.168.16.227 dev tunl0 onlink
	
	节点B：ip eth0 192.168.16.227，隧道ip用100.96.0.2
	
	ip tunnel add tunl0 mode ipip
	
	ifconfig tunl0 100.96.0.2 netmask 255.255.255.0
	
	ip route add 100.96.0.1/32 via 192.168.16.226 dev tunl0 onlink
	```

	从内节点 A，ping 100.96.0.2。
	
	在节点 B 上抓包，`tcpdump -i eth0 -n -e -vv 'ip[9]==4'` 可以看到包说明 ipip 可以连通。
	
	![ipip抓包](images/ipip-tcpdump.png)

* calico 多网卡环境调整

	默认情况下，calico node 的环境变量中有 `IP_AUTODETECTION_METHOD`。
	
	```
	bash
	在autodetect配置时，host主机的ip地址会使用first-found方式自动获取。即相当于默认配置如下环境变量，实际修改时，如果没有需要手动加上这个环境变量
	- name: IP_AUTODETECTION_METHOD
	  value: "first-found"
	 
	 
	执行如下命令修改calico-node的daemonset添加/修改环境变量
	kubectl -n kube-system edit ds calico-node
	```
	
	`IP_AUTODETECTION_METHOD` 值的详细说明可以参考 [详细说明](https://github.com/projectcalico/calico/blob/release-v3.1/v3.0/reference/node/configuration.md#ip-autodetection-methods)。
	
	这边简单介绍下几种方式及适合的场景。
	
	* first-found 多用于单网卡，这种模式会枚举本地网卡的有效ip地址，并使用获取到的第一个，因此多网卡时可能会有问题。例如：控制节点有时多网卡，业务网卡管集群，管理网卡与外部通讯。
	
	* can-reach=DESTINATION 用于多网卡，自动获取时，会使用可以访问 destination 的第一个 ip 地址。一般业务网段与管理网段隔离时，可以写只有业务网段能通的一个地址。
	
	* interface=INTERFACE-REGEX 用于多网卡，按参数的正则表达式匹配本地网卡，并使用其ip地址。多用于多网卡，且网卡名有规律/可配的情况。典型的配置为 `interface=eth0`。


**calico blocksize 说明**：calico 的块大小是一个子网默认情况下在每个节点上分配的 ip 数，默认为 26（即2^(32-26)=256个ip每个块）。对于银行客户这种 IP 严格控制的，默认的块大小可能有点太大了，需要调整小一些，比如：30（即2^(32-30)=4个ip每块），这样支持的节点数就会大大增加。除了默认子网外，新建的子网 blocksize 都支持创建时调整（创建后不可改）。因此要在网络规划的时候慎重考虑。

#### flannel 网络故障排查

#### kube-ovn 网络故障排查

#### macvlan 网络故障排查

#### kube-proxy 网络故障排查

##### 更新 nodeport 类型 svc 中的端口，新的端口可以访问，旧的端口也可以访问

kube-proxy 的已知问题，需要升级 kube-proxy 版本到 1.13.11 及以上版本。

![changelog](images/kube-proxy-port-bug.png)

#### k8s dns 故障排查

##### coredns

域名解析很慢，有差不多 5s 的延迟


配置 node local dns 可以缓解，[yaml](https://github.com/kubernetes/kubernetes/blob/master/cluster/addons/dns/nodelocaldns/nodelocaldns.yaml)。

根本原因是内核问题。详细讨论见 [仅内部人员可见](http://confluence.alauda.cn/pages/viewpage.action?pageId=39333034)。

##### kubernetes 配置自定义域名记录，通过 coredns


部分内网客户有增加自定义域名记录的需求。基本有 3 种方法：

* 客户存在内网dns，且该内网 dns 可以解析用户需要的域名。这种情况只需要确保 coredns 所在的宿主机的 `/etc/resolv.conf` 中配了该 dns 即可。

* 使用 kubernetes 原生的 hostalias 功能，对于每个 pod 额外配置 dns 记录，[参考信息](https://kubernetes.io/docs/concepts/services-networking/add-entries-to-pod-etc-hosts-with-host-aliases/)。

	``` 
	yaml
	apiVersion: v1
	kind: Pod
	metadata:
	  name: hostaliases-pod
	spec:
	  restartPolicy: Never
	  hostAliases:
	  - ip: "127.0.0.1"
	    hostnames:
	    - "foo.local"
	    - "bar.local"
	  - ip: "10.1.2.3"
	    hostnames:
	    - "foo.remote"
	    - "bar.remote"
	  containers:
	  - name: cat-hosts
	    image: busybox
	    command:
	    - cat
	    args:
	    - "/etc/hosts"
	```


* 使用 coredns 来做集群内全局的记录增加。

	* 用 file 插件

		执行 `kubectl -n kube-system edit cm coredns` 修改 coredns 的 configmap，默认情况下如下：
		
		```
		yaml
		apiVersion: v1
		data:
		  Corefile: |
		    .:53 {
		        errors
		        health
		        kubernetes cluster.local in-addr.arpa ip6.arpa {
		           pods insecure
		           upstream
		           fallthrough in-addr.arpa ip6.arpa
		        }
		        prometheus :9153
		        proxy . /etc/resolv.conf
		        cache 30
		        loop
		        reload
		        loadbalance
		    }
		kind: ConfigMap
		metadata:
		  creationTimestamp: "2019-06-18T08:10:05Z"
		  name: coredns
		  namespace: kube-system
		  resourceVersion: "181"
		  selfLink: /api/v1/namespaces/kube-system/configmaps/coredns
		  uid: 7abf504f-91a0-11e9-8570-52540089b1e0
		```
		
		将其修改为：
		
		```
		yaml
		apiVersion: v1
		data:
		  Corefile: |
		    .:53 {
		        errors
		        health
		        kubernetes cluster.local in-addr.arpa ip6.arpa {
		           pods insecure
		           upstream
		           fallthrough in-addr.arpa ip6.arpa
		        }
		        prometheus :9153
		        file /etc/coredns/example.db example.org //使用中将example.org换为根域名
		        proxy . /etc/resolv.conf
		        cache 30
		        loop
		        reload
		        loadbalance
		    }
		  example.db: |
		    example.org.            IN      SOA     sns.dns.icann.org. noc.dns.icann.org. 2019062541 7200 3600 1209600 3600 // 修改example.org.换为对应的，不要漏了.
		    xxx.example.org.            IN      A       1.1.1.1 // 对应a记录在这里添加,用户想加几个域名就仿照这行格式在下面增加
		kind: ConfigMap
		metadata:
		  creationTimestamp: "2019-06-18T08:10:05Z"
		  name: coredns
		  namespace: kube-system
		  resourceVersion: "181"
		  selfLink: /api/v1/namespaces/kube-system/configmaps/coredns
		  uid: 7abf504f-91a0-11e9-8570-52540089b1e0
		```
		
		修改后保存。 之后修改 coredns 的 deployment，将 example.db 挂载进去。并执行 `kubectl -n kube-system edit deploy coredns` 找到 volumes。
		
		```
		yaml
		      volumes:
		      - configMap:
		          defaultMode: 420
		          items:
		          - key: Corefile
		            path: Corefile
		          name: coredns
		        name: config-volume
		```
		
		将其改为：
		
		```
		yaml
		      volumes:
		        - name: config-volume
		          configMap:
		            name: coredns
		            items:
		            - key: Corefile
		              path: Corefile
		            - key: example.db
		              path: example.db
		```
		
		保存退出即可。

	* 用 hosts 插件，[参考信息](https://coredns.io/plugins/hosts/)。

		执行 `kubectl -n kube-system edit cm coredns` 修改 coredns 的 configmap， 默认情况如下：
		
		```
		yaml
		apiVersion: v1
		data:
		  Corefile: |
		    .:53 {
		        errors
		        health
		        kubernetes cluster.local in-addr.arpa ip6.arpa {
		           pods insecure
		           upstream
		           fallthrough in-addr.arpa ip6.arpa
		        }
		        prometheus :9153
		        proxy . /etc/resolv.conf
		        cache 30
		        loop
		        reload
		        loadbalance
		    }
		kind: ConfigMap
		metadata:
		  creationTimestamp: "2019-06-18T08:10:05Z"
		  name: coredns
		  namespace: kube-system
		  resourceVersion: "181"
		  selfLink: /api/v1/namespaces/kube-system/configmaps/coredns
		  uid: 7abf504f-91a0-11e9-8570-52540089b1e0
		```
	
		将其修改为：
	
		```
		yaml
		apiVersion: v1
		data:
		  Corefile: |
		    .:53 {
		        errors
		        health
		        kubernetes cluster.local in-addr.arpa ip6.arpa {
		           pods insecure
		           upstream
		           fallthrough in-addr.arpa ip6.arpa
		        }
		        prometheus :9153
		        hosts example.org { //修改这里，example.org为根域名
		            10.0.0.1 bbb.example.org //这里就跟本地配host格式一样
		            fallthrough
		        }
		        // 这里如果宿主机配了hosts，也可以直接写hosts，不用上面的写法
		        proxy . /etc/resolv.conf
		        cache 30
		        loop
		        reload
		        loadbalance
		    }
		kind: ConfigMap
		metadata:
		  creationTimestamp: "2019-06-18T08:10:05Z"
		  name: coredns
		  namespace: kube-system
		  resourceVersion: "181"
		  selfLink: /api/v1/namespaces/kube-system/configmaps/coredns
		  uid: 7abf504f-91a0-11e9-8570-52540089b1e0
		```
	
		修改后保存并重建 coredns 的 pod 即可。
	
	
	* 用 fowrad 插件，[参考信息](https://coredns.io/plugins/forward/)。

		执行 `kubectl -n kube-system edit cm coredns` 修改 coredns 的 configmap， 默认情况如下：
		
		```
		yaml
		apiVersion: v1
		data:
		  Corefile: |
		    .:53 {
		        errors
		        health
		        kubernetes cluster.local in-addr.arpa ip6.arpa {
		           pods insecure
		           upstream
		           fallthrough in-addr.arpa ip6.arpa
		        }
		        prometheus :9153
		        proxy . /etc/resolv.conf
		        cache 30
		        loop
		        reload
		        loadbalance
		    }
		kind: ConfigMap
		metadata:
		  creationTimestamp: "2019-06-18T08:10:05Z"
		  name: coredns
		  namespace: kube-system
		  resourceVersion: "181"
		  selfLink: /api/v1/namespaces/kube-system/configmaps/coredns
		  uid: 7abf504f-91a0-11e9-8570-52540089b1e0
		```
		
		将其修改为：
		
		```
		yaml
		apiVersion: v1
		data:
		  Corefile: |
		    .:53 {
		        errors
		        health
		        kubernetes cluster.local in-addr.arpa ip6.arpa {
		           pods insecure
		           upstream
		           fallthrough in-addr.arpa ip6.arpa
		        }
		        prometheus :9153
		        forward example.org. 127.0.0.1:9005 127.0.0.1:9006 // 这里example.org.是根域名，后面是客户的dns服务器地址，可以写多个
		        proxy . /etc/resolv.conf
		        cache 30
		        loop
		        reload
		        loadbalance
		    }
		kind: ConfigMap
		metadata:
		  creationTimestamp: "2019-06-18T08:10:05Z"
		  name: coredns
		  namespace: kube-system
		  resourceVersion: "181"
		  selfLink: /api/v1/namespaces/kube-system/configmaps/coredns
		  uid: 7abf504f-91a0-11e9-8570-52540089b1e0
		```
		
		修改后保存并重建 coredns 的 pod 即可。

##### kube-dns

#### 其他网络问题

##### Pod 配置了 HostPort 但是无法访问


**说明**：portmap 网络插件在低版本的时候有个 bug，写入的 iptables 规则顺序不可靠，导致有几率出现 HostPort 无法访问的情况，并且 portmap 一般为 CNI 网络插件释放到宿主机`/opt/cni/bin/`目录。

临时方案需要手动清除 iptables 规则，并替换 portmap 插件二进制文件。这个操作需要在集群所有主机上执行。 参见 [官方说明](https://github.com/containernetworking/plugins/pull/269)。 

长远的解决办法需要 CNI 组件更新自己内部打包的 portmap 插件版本。

### ETCD 运维

#### 升级
##### 说明
​        etcd一般和k8s集群一起升级，不会单独升级etcd组件；如果需要单独升级etcd组件，请参考待升级的版本与目前etcd版本的兼容性和升级路径，具体参阅etcd拟升级版本的官方文档并充分测试后在实施。
##### 操作示例 
```bash
kubeadm  upgrade apply v1.13.10   #使用新版本的kubeadm执行此命令，会自动升级etcd
```

#### 集群监控
##### 配置prometheus operator监控etcd

###### ACE，ACP原生部署prometheus监控etcd
​        参考[ACE2.9/ACP2.2部署prometheus](http://confluence.alauda.cn/pages/viewpage.action?pageId=50831306),注意,目前参照上述部署文档原生部署的prometheus监控etcd，只是完成对etcd的metrics的采集，缺少etcd相关的告警规则和grafana展示面板，产品从ACP2.8开始会加上这两项。如果要在现有版本的上面添加告警规则和展示面板，参考本文档后续章节。

​       因为etcd集群开启了证书认证，因此部署必须在prometheus部署的namespace里面，创建名为etcd-ca的secret，否则prometheus因为证书问题无法采集到etcd的metrics。

```
# 导出etcd-ca的secret为yaml文件  
    kubectl get secrets -n kube-system etcd-ca -o yaml > ~/etcd-ca.yaml
#将etcd-ca.yaml中的namespace替换为kube-prometheus将要安装的namespace，如alauda-system，并创建这个资源：
    kubectl apply -f ~/etcd-ca.yaml
```
###### 配置prometheus监控外部etcd集群
​       当etcd集群没有部署在kubernetes集群内场景，比如etcd集群为外部虚拟机环境时，可参考如下步骤配置对etcd监控数据的采集。
​       可参考[Prometheus Operator 监控 etcd 集群](http://confluence.alauda.cn/pages/viewpage.action?pageId=61912081)，操作步骤如下：

1. 创建prometheus采集etcd的metrics需要的secret，prometheus实例会将该secret挂载到pod里面。

```
kubectl -n alauda-system create secret generic etcd-certs  \
--from-file=/etc/kubernetes/pki/etcd/healthcheck-client.crt \
--from-file=/etc/kubernetes/pki/etcd/healthcheck-client.key \
--from-file=/etc/kubernetes/pki/etcd/ca.crt
```

2. 配置prometheuses.monitoring.coreos.com这个crd实例化的prometheus对象，引用上述配置的secret。

```
# 获取环境的prometheus的CRD实例名
prometheusCRD=$(kubectl get prometheuses -n cpaas-system |awk  'NR>1 {print $1}') 

# 对prometheus的CRD实例，追加创建的secrets
kubectl get   prometheuses -n cpaas-system ${prometheusCRD}  -o yaml|sed '/secrets\:/a\  - etcd-cert'|kubectl replace  -f  -
```

3. 创建prometheus监控etcd的ServiceMonitor资源对象，ServiceMonitor指明监控etcd需要的证书路径，也即第1步创建的secret在prometheus的pod里面挂载地址。

```
cat >prometheus-serviceMonitorEtcd.yaml <<EOF
apiVersion: monitoring.coreos.com/v1
kind: ServiceMonitor
metadata:
  name: etcd-k8s
  namespace: alauda-system
  labels:
    prometheus: kube-prometheus   
spec:
  jobLabel: k8s-app
  endpoints:
  - port: port
    interval: 30s
    scheme: https
    tlsConfig: 
      caFile: /etc/prometheus/secrets/etcd-certs/ca.crt
      certFile: /etc/prometheus/secrets/etcd-certs/healthcheck-client.crt
      keyFile: /etc/prometheus/secrets/etcd-certs/healthcheck-client.key
      insecureSkipVerify: true
  selector:
    matchLabels:
      k8s-app: etcd
  namespaceSelector:
    matchNames:
    - kube-system
EOF

kubectl create -f ./prometheus-serviceMonitorEtcd.yaml
```

4. 创建service资源对象和endpoint对象，service的label要符合第3步骤创建ServiceMonitor时的label selector，endpoint对象指明外部etcd集群的IP地址信息。

```
ETCD_IP1="1.x.x.x"      #etcd集群的第一个IP地址
ETCD_IP2="2.x.x.x"      #etcd集群的第二个IP地址
ETCD_IP3="3.x.x.x"      #etcd集群的第三个IP地址
cat >prometheus-etcdService.yaml <<EOF
apiVersion: v1
kind: Service
metadata:
  name: etcd-k8s
  namespace: kube-system
  labels:
    k8s-app: etcd
spec:
  type: ClusterIP
  clusterIP: None
  ports:
  - name: port
    port: 2379
    protocol: TCP

---
apiVersion: v1
kind: Endpoints
metadata:
  name: etcd-k8s
  namespace: kube-system
  labels:
    k8s-app: etcd
subsets:
- addresses:
  - ip: $(ETCD_IP1)
  - ip: $(ETCD_IP2)
  - ip: $(ETCD_IP3)
  ports:
  - name: port
    port: 2379
    protocol: TCP
EOF
kubectl create -f prometheus-etcdService.yaml
```

5. 登录prometheus实例查看prometheus是否已经通过自动发现机制开始采集etcd的metrics。

prometheus采集k8s集群外的应用的metrics，还可参考[使用prometheus operator监控部署在k8s集群外的mysql实例](https://blog.csdn.net/qqhappy8/article/details/102691817)


##### 常见etcd监控告警rule
etcd的metrics和含义见：https://github.com/etcd-io/etcd/blob/master/Documentation/metrics.md
    
常见告警rule
参考:https://github.com/etcd-io/etcd/blob/master/Documentation/op-guide/etcd3_alert.rules.yml，confluence链接如下[etcd监控告警规则](http://confluence.alauda.cn/pages/viewpage.action?pageId=61912734)。使用时下载该yaml文件，利用kubectl创建改资源对象即可

```
kubectl create -f  ./etcd3_alert.rules.yaml
```

部分告警规则释义如下：

1. 集群节点数是否足够(大于节点总数的50%)
    expr: sum(up{job=~".*etcd.*"} == bool 1) by (job) < ((count(up{job=~".*etcd.*"}) by (job) + 1) / 2)
    
2. 集群是否存活leader节点
    expr:etcd_server_has_leader{job=~".*etcd.*"} == 0
    
3. 磁盘监控指标wal_fsync_duration_seconds(etcd集群wal日志刷盘延时)和backend_commit_duration_seconds

| name | Description | type |
| ----- | ---- | ----|
| wal_fsync_duration_seconds | The latency distributions of fsync called by wal | Histogram |
| backend_commit_duration_seconds |The latency distributions of commit called by backend |Histogram |
     A wal_fsync is called when etcd persists its log entries to disk before applying them.
     A backend_commit is called when etcd commits an incremental snapshot of its most recent changes to disk.
****
    告警表达式:
    # 99%的wal_fsync延时超过500ms
    expr: |
      histogram_quantile(0.99, rate(etcd_disk_wal_fsync_duration_seconds_bucket{job=~".*etcd.*"}[5m]))>0.5
      
    # 99%的backend_commit延时超过250ms
    expr: |
      histogram_quantile(0.99, rate(etcd_disk_backend_commit_duration_seconds_bucket{job=~".*etcd.*"}[5m]))> 0.25
      

##### etcd监控的grafana展示面板 
 grafana官网使用比较多的面板(ID 3070):https://grafana.com/grafana/dashboards/3070。

 ID 3070的grafana展示面板json文件见confluence链接 [etcd的grafana展示面板](http://confluence.alauda.cn/pages/viewpage.action?pageId=61912600&flashId=914988870),使用时下载此json文件export到grafana即可，效果图如下：
  ![granfa-etcd面板](./images/grafana_etcd.png)


#### etcdctl常见运维操作
##### 拷贝etcdctl二进制到宿主机
```bash
ETCD_POD=`kubectl get pods -n kube-system |grep etcd|awk '{print $1}'|head  -n1`
ETCD_BIN=` kubectl exec -it -n kube-system $ETCD_POD -- which etcdctl`
kubectl cp -n kube-system ${ETCD_POD}:${ETCD_BIN} /usr/bin/etcdctl && chmod +x /usr/bin/etcdctl
   * 配置etcdctl别名 *
 ETCD_ENDPOINT=https://127.0.0.1:2379 #替换成环境实际地址
 alias etcdctl="ETCDCTL_API=3 etcdctl  --endpoints=${ETCD_ENDPOINT}  --cacert /etc/kubernetes/pki/etcd/ca.crt    --cert /etc/kubernetes/pki/etcd/peer.crt   --key /etc/kubernetes/pki/etcd/peer.key"
```
etcd集群证书说明：
etcd集群配置证书后，etcd节点之间，etcd节点与客户端之间都会进行双向SSL认证，SSL认证步骤如下：
![SSL认证](./images/SSL.png)

1.  通信双方的客户端向CA机构申请证书，CA机构下发根证书、客户端证书及私钥个申请者；
2.  客户端向服务器端发起请求，服务端下发服务端证书给客户端。客户端接收到证书后，通过私钥解密证书，并利用服务器端证书中的公钥认证证书信息比较证书里的消息，例如域名和公钥与服务器刚刚发送的相关消息是否一致，如果一致，则客户端认为这个服务器的合法身份；
3.  客户端发送客户端证书给服务器端，服务端接收到证书后，通过私钥解密证书，获得客户端的证书公钥，并用该公钥认证证书信息，确认客户端是否合法；
4.  客户端通过随机秘钥加密信息，并发送加密后的信息给服务端。服务器端和客户端协商好加密方案后，客户端会产生一个随机的秘钥，客户端通过协商好的加密方案，加密该随机秘钥，并发送该随机秘钥到服务器端。服务器端接收这个秘钥后，双方通信的所有内容都都通过该随机秘钥加密。
    
    etcd集群用到的证书如下：
    ca.crt   etcd集群用到的CA证书，集群唯一。
    peer.crt  每个etcd节点与其他节点ssl交互的证书，由CA签发。
    server.crt  etcd节点作为服务端的证书，由CA签发。
    healthcheck-client.crt   etcdctl作为客户端对集群健康检查用到的证书，由CA签发。

    
##### etcd增删改查

```
HOST1=<ETCD_IP1>
HOST2=<ETCD_IP2>
HOST3=<ETCD_IP2>
ENDPOINTS=https://$HOST1:2379,https://$HOST2:2379,https://$HOST3:2379
# 增
etcdctl --endpoints=$ENDPOINTS put foo "Hello World!"
# 查 
etcdctl --endpoints=$ENDPOINTS get foo
etcdctl --endpoints=$ENDPOINTS --write-out="json" get foo
# 删
etcdctl --endpoints=$ENDPOINTS del key 
etcdctl --endpoints=$ENDPOINTS del k --prefix
```
##### 集群状态
集群状态主要是etcdctl endpoint status 和etcdctl endpoint health两条命令

```
HOST1=<ETCD_IP1>
HOST2=<ETCD_IP2>
HOST3=<ETCD_IP2>
ENDPOINTS=https://$HOST1:2379,https://$HOST2:2379,https://$HOST3:2379
export ETCDCTL_API=3

etcdctl --write-out=table --endpoints=$ENDPOINTS  --cacert /etc/kubernetes/pki/etcd/ca.crt    --cert /etc/kubernetes/pki/etcd/peer.crt   --key /etc/kubernetes/pki/etcd/peer.key endpoint status
+--------------------------+------------------+---------+---------+-----------+-----------+------------+
|         ENDPOINT         |        ID        | VERSION | DB SIZE | IS LEADER | RAFT TERM | RAFT INDEX |
+--------------------------+------------------+---------+---------+-----------+-----------+------------+
| https://10.0.32.123:2379 | 34f656d5d59b4371 |  3.2.24 |   19 MB |     false |        46 |    1335548 |
| https://10.0.32.128:2379 | 6953f842be8e44e1 |  3.2.24 |   19 MB |      true |        46 |    1335548 |
|  https://10.0.32.35:2379 | 6971ff8090dd47c4 |  3.2.24 |   18 MB |     false |        46 |    1335548 |
+--------------------------+------------------+---------+---------+-----------+-----------+------------+

etcdctl --write-out=table --endpoints=$ENDPOINTS  --cacert /etc/kubernetes/pki/etcd/ca.crt    --cert /etc/kubernetes/pki/etcd/peer.crt   --key /etc/kubernetes/pki/etcd/peer.key endpoint health
https://10.0.32.35:2379 is healthy: successfully committed proposal: took = 2.129573ms
https://10.0.32.128:2379 is healthy: successfully committed proposal: took = 1.601473ms
https://10.0.32.123:2379 is healthy: successfully committed proposal: took = 2.067723ms
```

##### 集群成员
跟集群成员相关的命令如下：

```
    member add            Adds a member into the cluster
    member remove        Removes a member from the cluster
    member update        Updates a member in the cluster
    member list            Lists all members in the cluster
    
    etcdctl  --endpoints=$ETCD_ENDPOINT  --cacert /etc/kubernetes/pki/etcd/ca.crt    --cert /etc/kubernetes/pki/etcd/peer.crt   --key /etc/kubernetes/pki/etcd/peer.key member list  -w table
+------------------+---------+-------------+--------------------------+--------------------------+
|        ID        | STATUS  |    NAME     |        PEER ADDRS        |       CLIENT ADDRS       |
+------------------+---------+-------------+--------------------------+--------------------------+
| 34f656d5d59b4371 | started | 10.0.32.123 | https://10.0.32.123:2380 | https://10.0.32.123:2379 |
| 6953f842be8e44e1 | started | 10.0.32.128 | https://10.0.32.128:2380 | https://10.0.32.128:2379 |
| 6971ff8090dd47c4 | started |  10.0.32.35 |  https://10.0.32.35:2380 |  https://10.0.32.35:2379 |
+------------------+---------+-------------+--------------------------+--------------------------+
```

##### curl调用etcd的REST API

1. 查看etcd版本

```
ETCD_IP=<IP>  #etcd的ip地址，如10.0.32.123
 curl  --key /etc/kubernetes/pki/etcd/peer.key  --cert /etc/kubernetes/pki/etcd/peer.crt --cacert  /etc/kubernetes/pki/etcd/ca.crt https://${ETCD_IP}:2379/version
 
{"etcdserver":"3.2.24","etcdcluster":"3.2.0"}
```

2. 增加键和查看键值,删除键值

``` 
# 增加键
 curl  --key /etc/kubernetes/pki/etcd/peer.key  --cert /etc/kubernetes/pki/etcd/peer.crt --cacert  /etc/kubernetes/pki/etcd/ca.crt https://${ETCD_IP}:2379/v2/keys/hello -XPUT -d value="world"
{"action":"set","node":{"key":"/hello","value":"world","modifiedIndex":14,"createdIndex":14}}

# 查看键值
curl  --key /etc/kubernetes/pki/etcd/peer.key  --cert /etc/kubernetes/pki/etcd/peer.crt --cacert  /etc/kubernetes/pki/etcd/ca.crt https://${ETCD_IP}:2379/v2/keys/hello
{"action":"get","node":{"key":"/hello","value":"world","modifiedIndex":14,"createdIndex":14}}

# 删除键值
curl  --key /etc/kubernetes/pki/etcd/peer.key  --cert /etc/kubernetes/pki/etcd/peer.crt --cacert  /etc/kubernetes/pki/etcd/ca.crt  -XDELETE https://${ETCD_IP}:2379/v2/keys/for_delete
```

3. 查看集群member信息

```
curl  --key /etc/kubernetes/pki/etcd/peer.key  --cert /etc/kubernetes/pki/etcd/peer.crt --cacert  /etc/kubernetes/pki/etcd/ca.crt https://${ETCD_IP}:2379/v2/members | jq  .

  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   398  100   398    0     0   4617      0 --:--:-- --:--:-- --:--:--  4627
{
  "members": [
    {
      "id": "34f656d5d59b4371",
      "name": "10.0.32.123",
      "peerURLs": [
        "https://10.0.32.123:2380"
      ],
      "clientURLs": [
        "https://10.0.32.123:2379"
      ]
    },
    {
      "id": "6953f842be8e44e1",
      "name": "10.0.32.128",
      "peerURLs": [
        "https://10.0.32.128:2380"
      ],
      "clientURLs": [
        "https://10.0.32.128:2379"
      ]
    },
    {
      "id": "6971ff8090dd47c4",
      "name": "10.0.32.35",
      "peerURLs": [
        "https://10.0.32.35:2380"
      ],
      "clientURLs": [
        "https://10.0.32.35:2379"
      ]
    }
  ]
}

```

4. 查看集群leader和自身信息

```
# 查看leader信息
ETCD_IP=<10.0.32.123>  # 替换为环境自身IP
curl  --key /etc/kubernetes/pki/etcd/peer.key  --cert /etc/kubernetes/pki/etcd/peer.crt --cacert  /etc/kubernetes/pki/etcd/ca.crt https://${ETCD_IP}:2379/v2/stats/leader | jq .
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   435  100   435    0     0   3431      0 --:--:-- --:--:-- --:--:--  3452
{
  "leader": "34f656d5d59b4371",
  "followers": {
    "6953f842be8e44e1": {
      "latency": {
        "current": 0.00172,
        "average": 0.007778686201670091,
        "standardDeviation": 0.05529094633647756,
        "minimum": 0.00045,
        "maximum": 4.510362
      },
      "counts": {
        "fail": 1,
        "success": 33649
      }
    },
    "6971ff8090dd47c4": {
      "latency": {
        "current": 0.004509,
        "average": 0.006248231673911685,
        "standardDeviation": 0.015731215715418256,
        "minimum": 0.00046,
        "maximum": 0.676926
      },
      "counts": {
        "fail": 0,
        "success": 33586
      }
    }
  }
}

# 查看节点自身信息
ETCD_IP=<10.0.32.123>  # 替换为环境自身IP
curl  --key /etc/kubernetes/pki/etcd/peer.key  --cert /etc/kubernetes/pki/etcd/peer.crt --cacert  /etc/kubernetes/pki/etcd/ca.crt https://${ETCD_IP}:2379/v2/stats/self| jq .
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   355  100   355    0     0   3643      0 --:--:-- --:--:-- --:--:--  3659
{
  "name": "10.0.32.123",
  "id": "34f656d5d59b4371",
  "state": "StateLeader",
  "startTime": "2020-02-13T01:51:36.43251159Z",
  "leaderInfo": {
    "leader": "34f656d5d59b4371",
    "uptime": "1h28m20.883794832s",
    "startTime": "2020-02-13T01:51:37.732840716Z"
  },
  "recvAppendRequestCnt": 0,
  "sendAppendRequestCnt": 68777,
  "sendPkgRate": 12.016980897204034,
  "sendBandwidthRate": 9208.251952100534
}
相关字段含义如下
id: 节点ID
leaderInfo.leader: 集群leader节点ID
leaderInfo.uptime: 集群leader节点保持leader角色时长
name: 节点名字
recvAppendRequestCnt: 节点处理的附加请求总数
recvBandwidthRate: 节点每秒接收字节数（只适用follower节点）
recvPkgRate: 节点每秒接收到的请求数（只适用follower节点）
sendAppendRequestCnt: 节点发送请求总数
sendBandwidthRate: 节点每秒发送字节数(只适用leader节点)
sendPkgRate: 节点每秒发送请求数(只适用leader节点)
state: 节点角色，leader还是follower
startTime: 节点启动时间
```

5. 查看一个ETCD节点的metric

```
ETCD_IP=<10.0.32.123>  # 替换为环境自身IP
curl  --key /etc/kubernetes/pki/etcd/peer.key  --cert /etc/kubernetes/pki/etcd/peer.crt --cacert  /etc/kubernetes/pki/etcd/ca.crt https://${ETCD_IP}:2379/metrics
```
etcd的metrics和含义见：https://github.com/etcd-io/etcd/blob/master/Documentation/metrics.md



##### etcd数据压缩与碎片清理
        etcd存储的键值KV对中(K指key，V指value)，都有版本的控制，k，v的历史版本会持续存在，直至k，v被压缩，compaction开始之后，被压缩的版本之前的所有版本都被删除，但是kv占用的磁盘不会释放，形成碎片化。
        etcd集群存储的碎片化，长期积累会极大的占用磁盘的空间，且会影响etcd集群性能，建议定期进行碎片清理，操作命令如下
 
```
# 获取当前KV的版本
export ETCDCTL_API=3 
rev=$(etcdctl --cacert /etc/kubernetes/pki/etcd/ca.crt    --cert /etc/kubernetes/pki/etcd/peer.crt   --key /etc/kubernetes/pki/etcd/peer.key  --endpoints=https://127.0.0.1:2379 endpoint status --write-out="json" | egrep -o '"revision":[0-9]*' | egrep -o '[0-9].*')
# 压缩掉所有旧版本KV，注意压缩旧版本不会释放磁盘空间到操作系统，磁盘空间仍被etcd占用。
etcdctl --cacert /etc/kubernetes/pki/etcd/ca.crt    --cert /etc/kubernetes/pki/etcd/peer.crt   --key /etc/kubernetes/pki/etcd/peer.key  --endpoints=https://127.0.0.1:2379 compact $rev
# 整理多余的空间，释放磁盘空间到操作系统。
IP1="x.x.x.1"  IP2="x.x.x.2"  IP3="x.x.x.3"  #替换我集群实际IP
etcdctl  --cacert /etc/kubernetes/pki/etcd/ca.crt    --cert /etc/kubernetes/pki/etcd/peer.crt   --key /etc/kubernetes/pki/etcd/peer.key --endpoints=https://${IP1}:2379,https://${IP2}:2379,https://${IP3}:2379 defrag

## 注意，如果以"etcdctl --endpoints=http://127.0.0.1:2379 defrag"方式进行碎片清理，不会自动作用于集群全部节点，只会对本地节点起作用，如果要作用于集群全部节点，需要在命令的"--endpoints"传入节点的全部地址信息。##

# etcd 3.3版本以上支持，如果一个节点etcd进程未运行，可以直接作用于etcd数据目录进行碎片清理，命令为"etcdctl defrag --data-dir <path-to-etcd-data-dir>"   #
```
![defrag](./images/etcd_defrag.png)
如上图，一个集群，通过碎片整理，db文件磁盘占用，从1.2G下降到7.1M

##### 检查集群性能
**注意：压测会对集群造成比较大的负载压力，一定要在业务低谷期进行此项操作**

```
export ETCDCTL_API=3
ETCD_ENDPOINT=https://10.0.32.123:2379,https://10.0.32.128:2379,https://10.0.32.35:2379 # 替换为环境实际地址
etcdctl   --endpoints=${ETCD_ENDPOINT}  --cacert /etc/kubernetes/pki/etcd/ca.crt    --cert /etc/kubernetes/pki/etcd/peer.crt   --key /etc/kubernetes/pki/etcd/peer.key check perf --load=s
# --load 集群性能检查载荷，可选值有s(small), m(medium), l(large), xl(xLarge)。
 60 / 60 Booooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo! 100.00%1m0s
PASS: Throughput is 150 writes/s
PASS: Slowest request took 0.210423s
PASS: Stddev is 0.008842s
PASS

输出释义：
第一行输出集群吞吐是否PASS，为每秒集群write的QPS，失败会输出FAIL。
第二行输出最慢的请求花费时间，失败会输出FAIL。
第三行输出所有请求花费时间的标准方差，衡量etcd处理请求是否稳定，失败会输出FAIL。
第四行输出每次集群性能测试是否通过，未通过会显示FAIL。
```

##### etcd集群关联证书更新
etcd集群需要更新的证书有healthcheck-client.crt（etcd集群健康检查所需），apiserver_etcd_client.crt（kube-apiserver连接etcd集群所需），peer.crt（etcd节点之间互联所需）,server.crt（etcd集群server端证书）。查看证书有效期命令如

```
echo "etcd-healthcheck-client证书"
echo `openssl x509 -in /etc/kubernetes/pki/etcd/healthcheck-client.crt -noout -dates`
echo "--------------------------------"
echo "apiserver-etcd-client证书"
echo `openssl x509 -in /etc/kubernetes/pki/etcd/ apiserver-etcd-client.crt  -noout -dates`
echo "--------------------------------"
echo "etcd-peer证书"
echo `openssl x509 -in /etc/kubernetes/pki/etcd/peer.crt -noout -dates`
echo "--------------------------------"
echo "etcd-server证书"
echo `openssl x509 -in /etc/kubernetes/pki/etcd/server.crt -noout -dates`
```

证书更新脚本请参考 [集群证书更新](http://confluence.alauda.cn/pages/viewpage.action?pageId=39336674)进行 需要在每个master节点执行，脚本依次会更新healthcheck-client.crt，apiserver_etcd_client.crt，peer.crt，,server.crt四个证书。

在使用calico的网络模式时，calico-node也需要连接etcd集群，连接etcd集群的证书使用的secret挂载到pod的方式，因此此secret也是需要更新的，在一个master节点操作如下：

```
base64 /etc/kubernetes/pki/etcd/peer.crt | tr -d '\n' > calico-etcd-crt-data
base64 /etc/kubernetes/pki/etcd/peer.key | tr -d '\n' > calico-etcd-key-data
CALICO_ETCD_CRT_DATA=$(cat calico-etcd-crt-data)
CALICO_ETCD_KEY_DATA=$(cat calico-etcd-key-data)
kubectl get secrets -n kube-system etcd-peer -o yaml |sed -e "s/tls.crt:.*/tls.crt: "$CALICO_ETCD_CRT_DATA"/" -e "s/tls.key:.*/tls.key: "$CALICO_ETCD_KEY_DATA"/" | kubectl replace -f -
```

##### 访问etcd中kubernetes的元数

```
# 统计元数据的keys
#!/bin/bash
# Get kubernetes keys from etcd
export ETCDCTL_API=3
keys=`etcdctl get /registry --prefix -w json|python -m json.tool|grep key|cut -d ":" -f2|tr -d '"'|tr -d ","`
for x in $keys;do
  echo $x|base64 -d|sort
done

# 统计etcd存储的kubernetes元数据keys的个数 
etcdctl   --endpoints=${ETCD_ENDPOINT}  --cacert /etc/kubernetes/pki/etcd/ca.crt    --cert /etc/kubernetes/pki/etcd/peer.crt   --key /etc/kubernetes/pki/etcd/peer.key  get /regitry --prefix --keys-only |grep -v -E "^$" |awk -F'/' '{print $3}' |uniq  -c |sort  -n -r  -k 1
     
     126 services
     90 clusterroles
     83 pods
     82 secrets
     75 configmaps
 要关注数量最多的几个keys，如果一些keys数目明显异常，与其他keys相差很大，有可能暗示集群产生此key的组件异常，要联系研发确认是否正常行为，比如某环境遇到过平台产生的token打爆etcd的情况，secret数量异常。
```



#### etcd 日常巡检

##### 检查集群节点状态

```
export ETCDCTL_API=3
ETCD_ENDPOINT=https://{ETCD-IP1}:2379,https://{ETCD-IP2}:2379,https://{ETCD-IP3}:2379
etcdctl  --endpoints=$ETCD_ENDPOINT  \
--cacert /etc/kubernetes/pki/etcd/ca.crt    \
--cert /etc/kubernetes/pki/etcd/peer.crt   \
--key /etc/kubernetes/pki/etcd/peer.key \
member list 
执行上述命令，会返回集群每个节点的ID，状态，名字，peer-url等信息，集群每个节点的状态都为started，代表集群状态正常；同时注意member list出来的节点数目是否和etcd启动配置文件的"--initial-cluster"保持一致。
```


##### 检查某一个节点状态

```
export ETCDCTL_API=3
ETCD_ENDPOINT=https://{ETCD-IP1}:2379,https://{ETCD-IP2}:2379,https://{ETCD-IP3}:2379
etcdctl  -w table --endpoints=$ETCD_ENDPOINT  \
--cacert /etc/kubernetes/pki/etcd/ca.crt    \
--cert /etc/kubernetes/pki/etcd/peer.crt   \
--key /etc/kubernetes/pki/etcd/peer.key \
endpoint status
执行上述命令，终端会返回每个endpoint的状态信息，比如每个节点的ENDPOINT，ID，etcd版本，数据库大小，是否是leader等,要注意返回的etcd的数据库大小，如果集群规模不是很大，但是数据量很大，需对集群进行下碎片清理，具体操作参考文档上述章节。

etcdctl  -w table --endpoints=$ETCD_ENDPOINT  \
--cacert /etc/kubernetes/pki/etcd/ca.crt    \
--cert /etc/kubernetes/pki/etcd/peer.crt   \
--key /etc/kubernetes/pki/etcd/peer.key \
endpoint health
执行上述命令，如果全部endpoint返回 https://x.x.x.x:2379 is healthy: successfully committed proposal: took =xx ms ，代表集群正常。
```



##### 检查从 etcd 中读数据是否成功

```
export ETCDCTL_API=3
ETCD_ENDPOINT=https://{ETCD-IP1}:2379,https://{ETCD-IP2}:2379,https://{ETCD-IP3}:2379
etcdctl  -w table --endpoints=$ETCD_ENDPOINT  \
--cacert /etc/kubernetes/pki/etcd/ca.crt    \
--cert /etc/kubernetes/pki/etcd/peer.crt   \
--key /etc/kubernetes/pki/etcd/peer.key \
get foo
```
脚本执行成功，会返回foo的数据。


##### 检查向 etcd 写入数据是否成功

```
export ETCDCTL_API=3
ETCD_ENDPOINT=https://{ETCD-IP1}:2379,https://{ETCD-IP2}:2379,https://{ETCD-IP3}:2379
etcdctl  -w table --endpoints=$ETCD_ENDPOINT  \
--cacert /etc/kubernetes/pki/etcd/ca.crt    \
--cert /etc/kubernetes/pki/etcd/peer.crt   \
--key /etc/kubernetes/pki/etcd/peer.key \
put foo "abc"
```
 执行正常终端会返回OK。
 
 
##### 检查集群是否告警

```
export ETCDCTL_API=3
ETCD_ENDPOINT=https://{ETCD-IP1}:2379,https://{ETCD-IP2}:2379,https://{ETCD-IP3}:2379
etcdctl  --endpoints=$ETCD_ENDPOINT  \
--cacert /etc/kubernetes/pki/etcd/ca.crt    \
--cert /etc/kubernetes/pki/etcd/peer.crt   \
--key /etc/kubernetes/pki/etcd/peer.key \
alarm list
```
无告警终端会显示空，异常时会显示警告内容。

##### 巡检脚本示例
  利用curl命令，调用etcd的endpoint的health接口，逐个检查是否返回true
  
```
#!/bin/bash
ETCD_YAML="/etc/kubernetes/manifests/etcd.yaml"
ETCD_ENDPOINTS=`cat ${ETCD_YAML}|grep -i  "initial-cluster=" |awk -F'=|,' '{print $3,$5,$7}'|sed -e "s/2380/2379/g"`
ETCD_CA="/etc/kubernetes/pki/etcd/ca.crt"
ETCD_KEY="/etc/kubernetes/pki/etcd/peer.key"
ETCD_CRT="/etc/kubernetes/pki/etcd/peer.crt"
for i in $ETCD_ENDPOINTS
  do
    curl  --key ${ETCD_KEY} --cert ${ETCD_CRT} --cacert ${ETCD_CA}  $i/health |grep true 
    if [ $? -eq 0 ]; then
      echo -e "\033[32m etcd node $i is checked health\033[0m"
    else
      echo -e "\033[31m etcd node $i is checked failed\033[0m" 
    fi
  done
```



#### etcd 集群优化
##### 说明
    默认的etcd配置和部署方案满足大多数k8s集群的性能要求，如果一些特殊场景etcd无法满足，需要优化etcd集群时，可以参考如下。
##### 优化参考
###### 硬件层面
    参考 https://github.com/etcd-io/etcd/blob/master/Documentation/op-guide/hardware.md
    1.cpu 
        2~4 core即可保证etcd流畅运行，当每秒的请求成千上万时，CPU需要频繁地从内存加载数据，此时建议使用8 ~ 16个core。可以通过top等命令，检查etcd占用cpu的情况，来判断etcd进程是否cpu资源不足。
        
    2.内存
        平常情况下8G内存即可保证etcd流畅运行，当处理的qps上万的时候，建议16 ~ 64GB的内存量
        
    3.磁盘
        存储介质的质量是etcd运行performance和stability的关键，差劲的存储介质会导致延迟增加和系统不稳定。一般情况下顺序读写能达到50 IOPS(如7200RPM的磁盘)即可满足要求，当压力大的时候，要求能达到500 IOPS（SSD盘或者其他虚拟的block设备）。通常情况下建议使用SSD作为存储介质。磁盘异常，常会导致etcd性能急剧下降，进而引发各种故障。日常运维时，可通过iostat，iotop等工具，观察磁盘io是否稳定，io延时是否很大，必要时可考虑用单独的SSD挂载到etcd数据目录，避免与系统其它进程的IO争用。
        
    4.网络
        一般情况下1GbE（千兆）网卡可以保证稳定运行，对于大的集群则要求10GbE(万兆)网卡，同时尽量把etcd集群部署在同一个IDC以保证网络稳定。
        
###### etcd集群配置层面
etcd配置层面有"Heartbeat Interval"和"Election Timeout"。Heartbeat Interval指集群leader给follower发送心跳间隔的时间，Election Timeout指一个follower在多久没收到leader心跳信息后尝试竞争为leader的时间间隔。Heartbeat Interval默认值100ms， Election Timeout默认值1000ms，默认配置在集群瓶颈网络延时很低时工作良好，在集群网络延时大时，需要对这两个参数进行一定优化。

1. Heartbeat Interval
    Heartbeat Interval一般取值集群中两个peer之间RTT最大值，取值范围是[0.5 x RTT, 1.5 x RTT)。如果这个值过大，则会导致很晚才会发现leader失联，影响集群稳定性。
     RTT释义：Round-Trip Time，往返时延。在计算机网络中它是一个重要的性能指标，表示从发送端发送数据开始，到发送端收到来自接收端的确认（接收端收到数据后便立即发送确认），总共经历的时延，一定程度上反映了网络拥塞程度的变化。最简单的评估RTT的方法是使用ping工具。
     ![RTT](./images/RTT.png)

2. Election Timeout
    Election Timeout则依赖Heartbeat Interval和集群内所有RTT值的平均值，一般至少取值平均RTT的十倍，这个值的最大值是50,000ms（50s）。

整个集群内所有peer的这两个值都应该取同样的值，否则会引起混乱。命令行修改这两个值的方法如下

```
# Command line arguments:
$ etcd —heartbeat-interval=100 —election-timeout=500
# Environment variables:
$ ETCD_HEARTBEAT_INTERVAL=100 ETCD_ELECTION_TIMEOUT=500 etcd
```

3. 及时修复etcd集群中的不健康节点
   etcd集群为强一致性，会有木桶效应，集群中一个不健康的节点，会把整个集群的性能拖垮。如果集群某个节点异常，如磁盘io，网络不稳定等，需及时修复该节点对应问题，必要时可将该节点移出etcd集群，相关操作见下文节点扩缩容部分。

参考：https://etcd.io/docs/v3.2.17/tuning/

#### etcd 备份与恢复

##### 备份
###### 备份原理
     etcd集群的备份，是通过etcd集群的V3原生API，调用snapshot save命令，生成集群数据库的备份文件。

```
     etcdctl snapshot save  --help
  NAME:
	snapshot save - Stores an etcd node backend snapshot to a given file
  USAGE:
	etcdctl snapshot save <filename>
```


**注意**： 上述命令会将原来的 crontab 全部删掉，建议使用 `crontab -e` 命令添加。

###### 备份操作 

安装目录下的 “backup_recovery.sh” 脚本拷贝到每一台 “master 的/root” 目录下，然后执行`bash /root/backup_recovery.sh run_function=back_k8s_file`即可备份，也可以自行参考这个脚本或下面的例子，编写备份脚本
  
  ```
  #!/bin/bash
IP=<ETCD节点IP>  # 一般为master节点ip
BACKUP=/alauda/etcd_bak/
export ETCDCTL_API=3
mkdir -p $BACKUP
etcdctl   --cert=/etc/kubernetes/pki/etcd/healthcheck-client.crt --cacert=/etc/kubernetes/pki/etcd/ca.crt --key=/etc/kubernetes/pki/etcd/healthcheck-client.key --endpoints=https://$IP:2379 snapshot save $BACKUP/snap-$(date +%Y%m%d%H%M).db
  ```
  
  定期清理过期的etcd备份数据，脚本示例
  
  1. 按天数设置保留策略
  
  ```
 find  ${BACKUP} -mitime +7 -name "snap-*.db" |xargs rm -f
  ```
 
  2. 按备份个数设置备份策略
  
   ```
a=(`ls -ls $BACKUP | awk '{print $NF}' | sed 1d`)
if [ ${#a[@]} -gt 20 ];
then
for ((i=21; i<=${#a[@]}; i++));
do
rm -f /alauda/backup/etcd_backup/${a[i]}
done
fi
   ```

##### etcd 恢复
###### 集群一个节点恢复

    一个节点宕机，比如磁盘故障，并不会影响整个集群的正常工作。此时可通过以下几步恢复集群。
    
1. 在正常节点上查看集群状态并摘除异常节点 `etcdctl endpoint status`
2. 摘除异常节点 `etcdctl member remove $ID`
3. 重新部署服务后，将节点重新加入集群。
     * 由于ETCD集群证书依赖于服务器IP和主机名，为避免重新制作证书，需要保持节点IP和主机名不变；如需重新制作证书，可参照后续章节《节点扩缩容》节点扩容部分 *
     * 将节点重新加入集群`etcdctl member add $name --peer-urls=https://x.x.x.x:2380`此时查看集群状态，新加入的节点状态为unstarted
     * 删除新增成员的旧数据目录，更改相关配置,需将原etcd服务的旧数据目录删除，否则etcd会无法正常启动。新增节点是加入已有集群，所以需要修改配置`ETCD_INITIAL_CLUSTER_STATE="existing"`
     * 启动恢复节点etcd服务 检测集群是否正常`etcdctl endpoint status`

###### etcd整个集群恢复 （也可以参考平台升级文档中，最后一部分 etcd 回滚部分）
    etcd整个集群恢复，需要利用先前集群的备份文件进行恢复，操作步骤如下：
    
1. 停止全部etcd节点的etcd进程 `systemctl stop etcd` (适用systemd启动etcd) 或 `mv /etc/kubernetes/manifests/etcd.yaml /root` (适用于 kubelet pod启动etcd情况)
2. 使用scp，拷贝恢复所需备份文件到全部etcd节点。
3. 移走全部etcd节点的数据目录，也是一个备份`mv /var/lib/etcd/ /root`
4. 在每个etcd节点，使用snapshot restore命令从备份文件恢复etcd数据目录

```
ETCDCTL_API=3 etcdctl snapshot restore <etcd备份文件实际位置> \
--cert=/etc/kubernetes/pki/etcd/server.crt \
--key=/etc/kubernetes/pki/etcd/server.key \
--cacert=/etc/kubernetes/pki/etcd/ca.crt \
--data-dir=/var/lib/etcd \
--name ${i} \   # name需替换为每个etcd节点自身的name，集群唯一
--initial-cluster ${ETCD_1}=https://${ETCD_1}:2380,${ETCD_2}=https://${ETCD_2}:2380,${ETCD_3}=https://${ETCD_3}:2380 \  #替换为环境实际的etcd集群配置
--initial-advertise-peer-urls https://${i}:2380  # 需替换为每个etcd节点自身的URL
```

5. 全部节点恢复启动etcd进程`mv /root/etcd.yaml  /etc/kubernetes/manifests/`
6. 验证集群恢复成功。

```
ETCDCTL_API=3 etcdctl  member  list \
--cert=/etc/kubernetes/pki/etcd/server.crt \
--key=/etc/kubernetes/pki/etcd/server.key \
--cacert=/etc/kubernetes/pki/etcd/ca.crt
```

#### 节点扩缩容

##### 节点扩容

###### 适用场景
​       集群的数目一般为3或者5即可，成员不是越多越好，文档https://github.com/etcd-io/etcd/blob/master/Documentation/op-guide/runtime-configuration.md的“Change the cluster size”提到，扩容etcd集群节点数，可以提高集群容错性，也可以增加集群读取的吞吐，但同时etcd集群成员越多，leader的通信任务就越繁重，可能导致响应延迟上升。

###### 操作步骤

1. 在原有k8s集群一个master节点操作(示例为tstr501382)，执行etcdctl  member add将tstr501405a节点加入etcd集群。
​```bash
ETCDCTL_API=3  
etcdctl   --endpoints=<etcd集群的endpiont>   --cacert /etc/kubernetes/pki/etcd/ca.crt    --cert /etc/kubernetes/pki/etcd/peer.crt   --key /etc/kubernetes/pki/etcd/peer.key member add  <待添加节点主机名>   --peer-urls=<待添加节点的url>
```
![member add 操作](./images/20200205151207245.JPG.png)

2. 拷贝一个master节点的相关文件到待添加节点，主要为启动etcd静态pod的etcd.yaml和相关证书

​```
scp -rp  /etc/kubernetes/manifests/etcd.yaml  /etc/kubernetes/pki/  root@<待添加节点IP>:/home/cloudops/addetcd
```

3. 在待添加的etcd节点操作(示例为tstr501405a节点的/home/cloudops/addetcd目录)，利用拷贝过来的证书，修改相关信息，制作启动新etcd节点需要的证书(主要为peer.crt和server.crt)。

**以下为制作peer.crt证书操作**   

```
    **为待添加节点制作peer.crt证书，以下在待添加节点操作，注意修改dns和ip信息为对应节点信息**
cat <<EOF>peer-ssl.conf
   [req]
req_extensions = v3_req
distinguished_name = req_distinguished_name
[req_distinguished_name]
[v3_req]
keyUsage =critical, digitalSignature, keyEncipherment
extendedKeyUsage = TLS Web Server Authentication, TLS Web Client Authentication
subjectAltName = @alt_names
[alt_names]
DNS.1 = <tstr501405a>   #此处修改为待添加节点主机名
DNS.2 = localhost
IP.1 = <10.233.130.47>  #此处修改为待添加节点ip
IP.2 = 127.0.0.1
EOF

**利用拷贝过来的master节点证书信息，生成待添加节点的peer.crt证书**
openssl req -new -key ./pki/etcd/peer.key -out peer.csr -subj "/CN=$HOSTNAME" -config peer-ssl.conf
openssl x509 -req -in peer.csr -CA ./pki/etcd/ca.crt -CAkey ./pki/etcd/ca.key -CAcreateserial -out peer.crt -days 3650 -extensions v3_req -extfile peer-ssl.conf
```

**以下为制作server.crt证书操作**

   ```
   ** 制作待添加节点的server.crt证书，以下在待添加节点操作，注意修改dns和ip信息为对应节点信息 **
 
cat <<EOF>server-ssl.conf
[req]
req_extensions = v3_req
distinguished_name = req_distinguished_name
[req_distinguished_name]
[v3_req]
keyUsage =critical, digitalSignature, keyEncipherment
extendedKeyUsage = TLS Web Server Authentication, TLS Web Client Authentication
subjectAltName = @alt_names
[alt_names]
DNS.1 = tstr501405a   #此处修改为待添加节点主机名
DNS.2 = localhost
IP.1 = 10.233.130.47  #此处修改为待添加节点ip
IP.2 = 127.0.0.1
EOF
 
openssl req -new -key ./pki/etcd/server.key -out server.csr -subj "/CN=$HOSTNAME" -config server-ssl.conf
 
openssl x509 -req -in server.csr -CA ./pki/etcd/ca.crt -CAkey ./pki/etcd/ca.key -CAcreateserial -out server.crt -days 3650 -extensions v3_req -extfile server-ssl.conf
   ```
   
4. 参考第1步执行etcdctl member add操作时的返回结果，修改master节点拷贝过来的etcd.yaml文件。

```
[root@tstr501405a addetcd]# cat etcd.yaml
 apiVersion: v1
kind: Pod
metadata:
  annotations:
    scheduler.alpha.kubernetes.io/critical-pod: ""
  creationTimestamp: null
  labels:
    component: etcd
    tier: control-plane
  name: etcd
  namespace: kube-system
spec:
  containers:
  - command:
    - etcd
    - --advertise-client-urls=https://10.233.130.47:2379 #修改本节点信息
    - --cert-file=/etc/kubernetes/pki/etcd/server.crt
    - --client-cert-auth=true
    - --data-dir=/var/lib/etcd
    - --initial-advertise-peer-urls=https://10.233.130.47:2380 #修改本节点信息
    - --initial-cluster=tstr501383=https://10.233.130.16:2380,tstr501405a=https://10.233.130.47:2380,tstr501384=https://10.233.130.17:2380,tstr501382=https://10.233.130.15:2380 #增加待添加节点信息
    - --initial-cluster-state=existing  # 增加部分
    - --key-file=/etc/kubernetes/pki/etcd/server.key
    - --listen-client-urls=https://127.0.0.1:2379,https://10.233.130.47:2379 #修改为待添加节点信息
    - --listen-peer-urls=https://10.233.130.47:2380 #修改待添加节点信息 
    - --name=tstr501405a  #修改待添加节点主机名信息
    - --peer-cert-file=/etc/kubernetes/pki/etcd/peer.crt
    - --peer-client-cert-auth=true
    - --peer-key-file=/etc/kubernetes/pki/etcd/peer.key
    - --peer-trusted-ca-file=/etc/kubernetes/pki/etcd/ca.crt
    - --snapshot-count=10000
    - --trusted-ca-file=/etc/kubernetes/pki/etcd/ca.crt
    image: 10.233.71.70:60080/claas/etcd:3.2.24
    imagePullPolicy: IfNotPresent
    livenessProbe:
      exec:
        command:
        - /bin/sh
        - -ec
        - ETCDCTL_API=3 etcdctl --endpoints=https://[127.0.0.1]:2379 --cacert=/etc/kubernetes/pki/etcd/ca.crt
          --cert=/etc/kubernetes/pki/etcd/healthcheck-client.crt --key=/etc/kubernetes/pki/etcd/healthcheck-client.key
          get foo
```

![20200205153604461.JPG](./images/20200205153604461.JPG.png)

5. 待添加节点操作，拷贝相关证书到/etc/kubernetes/pki/etcd/目录（tstr501405a节点/home/cloudops/addetcd目录操作），拷贝修改好的etcd.yaml到/etc/kubernetes/manifests/，等待etcd静态pod启动.

```
cp peer.crt  server.crt  /etc/kubernetes/pki/etcd/
cp ./pki/etcd/healthcheck-client.crt ./pki/etcd/healthcheck-client.key  ./pki/etcd/server.key ./pki/etcd/peer.key  ./pki/etcd/ca.crt    /etc/kubernetes/pki/etcd/
cp etcd.yaml /etc/kubernetes/manifests/
```

6. 确认待添加的节点etcd启动成功,状态变成started.

![20200205154814963.JPG](./images/20200205154814963.JPG.png)

##### 节点缩容
###### 适用场景
​       节点故障，比如磁盘损坏或异常，操作系统彻底故障灯，导致该节点etcd进程始终无法启动,或者该节点etcd运行缓慢进而拖垮集群，需要将故障节点踢出集群
###### 操作步骤

1. 在一个master节点执行etcdctl member list，找出异常的etcd节点和对应ID

```
ETCDCTL_API=3  
etcdctl   --endpoints=<etcd集群的endpiont>   --cacert /etc/kubernetes/pki/etcd/ca.crt    --cert /etc/kubernetes/pki/etcd/peer.crt   --key /etc/kubernetes/pki/etcd/peer.key member list
```
![member_list](./images/member_list.png)

2. 在一个master节点执行etcdctl member remove 命令

```
ETCD_ID=<待删除节点的ID>
ETCDCTL_API=3  etcdctl   --endpoints=<etcd集群的endpiont>   --cacert /etc/kubernetes/pki/etcd/ca.crt    --cert /etc/kubernetes/pki/etcd/peer.crt   --key /etc/kubernetes/pki/etcd/peer.key member remove ${ETCD_ID}
```

3. 在删除的etcd节点上面，停止对应的etcd进程(如有)

`mv  /etc/kubernetes/manifests/etcd.yaml  /tmp` #适用kubelet静态pod启动etcd方式

`systemctl stop etcd` #适用systemd二进制启动etcd方式


#### etcd 常见故障排查
##### etcd节点无法启动
    1.etcd一般以静态pod方式运行，先通过“journalctl -u kubelet”检查该节点kubelet是否运行正常。
    2.如果kubelet正常，查看kubelet日志看是否有pod无法启动日志信息。
    3.通过kubectl logs -f -n kube-system etcd-$HOSTNAME,检查etcd启动日志是否有错误信息，如该节点与集群其他etcd节点网络是否正常互通，该节点etcd数据库目录(一般为/var/lib/etcd)是否正常。
    4.该节点etcd数据目录是否损坏，如日志显示数据目录损坏，可以参考前述的备份与恢复章节介绍的恢复方法，用备份文件恢复该节点数据目录。

##### kube-api无法连接etcd集群

1. 检查kube-apisever的yaml配置，确认kube-api与etcd连接的网络正常，kube-apiserver节点ping etcd的ip能通，且telnet也是通的。
2. kube-api连接etcd集群的证书是否过期，证书是否配置正确(证书需由 /etc/kubernetes/pki/etcd/ca.crt签发)。
3. 检查etcd集群本身是否正常，可参考文档前述日常巡检部分进行排查。
4. 
##### etcd timeout
    常见于磁盘或者网络原因，造成etcd性能异常，可参考前述章节的优化部分进行etcd性能调优后观测是否复现。
    
##### etcd选举leader失败，故障节点数量超过50%

1. 首先判断etcd集群是否有网络分区，造成集群分裂，如果网络原因造成一个节点与其他节点分割，网络恢复后，集群会自动恢复。
2. 如果消除网络分区集群仍然无法自动恢复，可以通过etcd集群的备份文件，重建etcd集群，具体恢复操作步骤见前述章节《备份恢复》恢复部分。
    
##### etcd 集群损坏，且无备份文件或备份损坏，集群恢复方法
    此种情况下，只能通过用etcd数据目录的/var/lib/etcd/member/snap/db作为备份文件，尝试重建集群恢复，可能会有数据丢失，或者无法恢复，仅为最终补救方案。
    
 1. 停掉全部节点的etcd进程。
      `systemctl  stop etcd`       # 适用systemd启动etcd进程情况
      `mv /etc/kubernetes/manifests/etcd.yaml /tmp`   # 适用kubelet静态pod启动etcd情况
2. 备份全部etcd节点的数据目录，一般为/var/lib/etcd目录 `cp -rp /var/lib/etcd  /root/etcd-back`
3. 选取一个节点的/var/lib/etcd/member/snap/db作为备份文件，参考前述章节《备份恢复》恢复部分步骤，重建etcd集群，操作流程基本一致，区别在于快照是从数据目录复制而来，没有完整性hash，因此在执行snapshot restore时需要加 --skip-hash-check参数。
    **注意：**
      操作时所有节点需要使用同一份db文件，而不是使用各节点自身目录下的db文件，也即要将其中一个节点上的/var/lib/etcd/member/snap/db文件scp到集群所有节点在进行恢复操作。
      使用etcdctl snapshot restore命令生成恢复数据目录时，须加--skip-hash-check参数，否则会报错
    
    
##### failed to send out heartbeat on time
    参考：https://github.com/etcd-io/etcd/blob/master/Documentation/faq.md
    
    通常情况下这个issue是disk运行过慢导致的，写磁盘过程可能要与其他应用竞争，或者因为磁盘是一个虚拟的或者是SATA类型的导致运行过慢，此时只有更好更快磁盘硬件才能解决问题。etcd暴露给Prometheus的metrics指标wal_fsync_duration_seconds就显示了wal日志的平均花费时间，通常这个指标应低于10ms。
    
    第二种原因就是CPU计算能力不足。如果是通过监控系统发现CPU利用率确实很高，就应该把etcd移到更好的机器上，然后通过cgroups保证etcd进程独享某些核的计算能力，或者提高etcd的priority。
    
    第三种原因就可能是网速过慢。如果Prometheus显示是网络服务质量不行，譬如延迟太高或者丢包率过高，那就把etcd移到网络不拥堵的情况下就能解决问题。但是如果etcd是跨机房部署的，长延迟就不可避免了，那就需要根据机房间的RTT调整heartbeat-interval，而参数election-timeout则至少是heartbeat-interval的5倍。
    
    
##### etcdserver：read-only range request "key：xxx" took too long to execute 
    参考 https://github.com/kubernetes/kubernetes/issues/70082
    常见原因多为磁盘性能不达标或者性能不稳定，IO抖动大，且集群任何一个节点的磁盘问题都可能导致该问题，日常可以通过iostat，iotop等工具，监测etcd大量出现上述报错时的IO情况，iowait是否明显增大；监控可以通过etcd两个metrics观测etcd_disk_wal_fsync_duration_seconds；etcd_disk_backend_commit_duration_seconds.
    
    metrics注释参考：https://github.com/etcd-io/etcd/blob/master/Documentation/metrics.md#disk
    
     A wal_fsync is called when etcd persists its log entries to disk before applying them.
     A backend_commit is called when etcd commits an incremental snapshot of its most recent changes to disk.
     High disk operation latencies (wal_fsync_duration_seconds or backend_commit_duration_seconds) often indicate disk issues. It may cause high request latency or make the cluster unstable.

    
    
 
### 日志、审计相关问题

#### 检查日志组件状态是否正常的方法

##### es

##### kafka

##### zookeeper

##### tiny

##### lanaya

##### nevermore

#### 检查审计组件状态是否正常的方法

##### 1

#### 日志、审计故障排查

##### 无日志

<img src="images/日志排查流程.jpg" style="zoom:50%;" />

**排查技巧**：

* 确定日志是写错误还是读错误；

* 确定日志故障在服务端还是客户端。

**范例**：

* kafka 未正常工作，tiny 无法传送日志。

	查看 nevermore 日志有 500 报错，500 错误多为 kafka 错误	
	重启 kafka 容器后 访问正常。

* nevermore 显示 204 状态。

	**现象**：nevermore 未正常工作，无法传送日志。

	**常见错误** ：有些容器有日志， 有些容器无日志。可判断服务端日志组件应该正常。客户端日志组件出问题可能性比较大。

	Nevermore 如果是没有数据或者丢数据，查看 nevermore 的日志。

	在 nevermore 容器内执行 `tail -f /var/log/td-agent/td-agent.log`，如果出现错误日志，表示服务端（tiny）有问题，需要检查服务端程序。如 tiny 写入 kafka 失败。

	如果是 ruby 的问题，可调试 nevermore：

	1）启动 nevermore 容器，注释掉，`run.sh` 的最后一行。
	
	2）然后执行 `run.sh`。
	
	3）执行 `/opt/td-agent/embedded/bin/ruby -Eascii-8bit:ascii-8bit /usr/sbin/td-agent --daemon /var/run/td-agent/td-agent.pid --under-supervisor`，手动启动。

##### 无审计

### 监控告警

#### 检查监控告警组件状态是否正常的方法

##### 1

#### 错误排查

##### 没有监控

监控信息为空，一般是 morgans 获取的数据为空，或者请求过程出错。

排查 Morgans 请求。 监控信息由 morgans 的 query 或者 query_range 请求获取，查看该请求的状态。用这个请求的 Request Id 从 Morgans pods 的日志(/var/lib/mathilde/morgans.log)去过滤该 id，下图是获取 id 的方法。

<img src="images/监控获取 id 方法.png" style="zoom:50%;" />

查看与 id 相关的日志，有无错误日志输出。可通过改变 morgans 日志级别（如：设置LOG_LEVEL=10），查看更详细日志。

如果日志有错，则根据错误日志分析；如果日志无错，请求正常，则可以根据该请求日志中解析出来的查询表达式“Query string”去相应集群的 Proemtheus 查询该表达式。可分解出该表达式中的基础 Metric，依次查询哪些 Metric 不存在，从而确定问题。

##### 告警实际值明显大于阈值，但告警状态显示正常

告警状态是从 Prometheus Server 评估的。可查看对应的 Prometheus Server 中该告警的状态。示例如下：

<img src="images/告警状态.png" style="zoom:50%;" />

上图即是查看位于 cpaas-system ns 的 proemtheusrule cpaas-agon-rules 同步过来的告警，每个告警的详情可以查看到。可查看对应告警规则的 expr 是否正确，expr 中 metric 的采集情况。

若对应 Prometheus Server 中没有该告警，则有可能是 operator 未能将 Prometheusrule 同步到 Prometheus Server。

* 可排查operator日志；

* 可排查 Prometheusrule 的 label 是否匹配 Prometheus 的 ruleNamespaceSelector 和ruleSelector。


##### 告警触发了，但没有收到通知

先确认是否基于该告警创建了通知，如果未创建通知，则告警触发后也不会收到通知。

告警是 prometheus server 是通过 alertmanager server 转发给 Morgans。

1. 查看 Promethues server/Alertmanager server 的日志，看是否有错。如果日志有错误排查错误。另外，很可能是 alertmanager 没有转发出去导致。

2. 如果 prometheus/alertmanager 没问题，再查看 Morgans 的日志（pod 的 `/var/lib/mathilde/morgans.log`)，过滤 "router"，依最新的 router 请求的 request_id 去过滤日志，查看整个的请求过程。如下图所示：

	<img src="images/通知.png" style="zoom:50%;" />

**补充说明：**

* 容器平台 v2.2+ 上 Courier 处理了 Morgans 产生的 NotificationMessage 发送通知，可以查看这个 NotificationMessagee 的 status 字段，可以看到发送状态。

* 容器平台 2.0/2.1 中，Morgans 直接发送通知，可查看日志。



### 其他 kubernets 或宿主机错误

#### kubectl 命令提示无法连接 kube-api

* 如果是高可用集群，首先检查 kube-api 的 lb。

* 如果是非高可用集群，去集群的 master 上执行 `docker ps -a | grep api` 查看 api 容器，然后执行 `docker logs -f <kubeapi 容器名字>` 查看容器日志。

执行 `journalctl -u kubelet` 查看 kubelet 创建容器的信息。

#### 磁盘空间不足

**故障现象**

创建服务或应用不成功，容器一直处于 createing 状态。

**可能的原因**

* 通过df 命令查看可用空间。如果磁盘空间还有很多，通过 `df –i` 命令查看 inode。

* 机器上是否还有别的更大的磁盘，部署的时候没有使用。

* 查找 docker 目录下是否哪个目录下有大的数据文件，有可能是某个容器内部产生的数据。

* “/var/log” 目录下是否有大的磁盘文。

* 查找是否机器上还有别的进程或容器产生了大量日志，这些进程通过其它方式运行在本机上，不受平台的管辖。

**解决方法**

* 如果是磁盘空间已满，需要先删除不用的数据，可以运行 `python /alauda/gc.py` 命令来清除日志、非运行状态的容器及不被使用的镜像。

* 如果用户使用的磁盘较小，需要更换磁盘。

* 如果有某个容器内产生了大量的数据或日志，可以根据业务的需求决定是否清除。

* 清理 “/var/log/” 下的系统的日志。

* 停掉所有非平台管辖的进程或容器。

**验证方法**

* `docker run` 命令创建容器，错误信息清除。

* 查看 df，磁盘使用率已经小于100%。


#### kubelet 日志报错：（journalctl -fu kubelet）

Kubernetes 出现故障，kubelet 无法启动。查看 kubelet 日志，日志报错显示 “Running with swap on is note supported ,please disable swap! or set --fail-swap-on flag to flase.”。

根本解决方法：

关闭 swap 分区：`swapoff -a`

**注意**：

kubelet 为什么关闭 swap？

* kubernetes 的想法是将实例紧密包装到尽可能接近 100％。 所有的部署应该与 CPU /内存限制固定在一起。 所以如果调度程序发送一个 pod 到一台机器，它不应该使用交换。 设计者不想交换，因为它会减慢速度。

* 关闭 swap 主要是为了性能考虑。

* 为了一些节省资源的场景，比如运行容器数量较多，可添加 kubelet 参数 `--fail-swap-on=false` 来解决。

* 所以需要关闭 swap 分区。



## 容器平台

### 首先参考本文档前面，k8s 运维部分，pod 详解


### 启动 pod 出错，提示无法 pull 镜像

**故障现象**

创建服务或应用不成功，POD 状态显示 pullimageserror。

**可能的原因**

部署服务或应用的主机 docker 配置文件 “/etc/docker/daemon.json” 中没有镜像仓库地址。

**解决方法**

在主机节点上编辑 “/etc/docker/daemon.json” 配置文件。

1. 在 "insecure-registries" 字段增加镜像仓库的地址，格式参考下面的示例：

	```
	"insecure-registries": [
	
	"10.1.0.23:5000",
	
	"172.18.0.4:60080"
	
	]，
	```


2. 执行 `systemctl daemon-reload && systemctl start` 命令。

**验证方法**

1. 执行 `docker info` 查看最后几行，在 Insecure Registries 这部分，确认镜像仓库地址存在其中。

2. 在集群内找到把 pod 调度到的那个 node ，在上面执行 `docker pull <镜像>` 查看是否能 pull 下来。

	如果可以，检查这个镜像仓库是否需要 login，如果需要 login，就需要给 Kubernetes 添加一个secrets。

kubernetes pull 镜像用的是自己存储的 user/password，这个在部署的时候一般都配置好了。默认只配置了 default 这个 namespace（kubernetes的）。如果服务是属于其他的 namespace，在创建服务的时候如果 namespace 不存在会提前创建，并从 default 同步 pull 镜像的认证信息。但偶尔会因为各种原因导致 pull 镜像信息同步或者配置出错，导致 pull 镜像失败。

服务所属 namespace 的规则是 `<app_name>--<space_name>`, 不存在的为 default，两者都不存在时 `namespace=default`。

正常情况下，每一个 registry 在 kubernets 的每一个 namespace 下都有一个 secret 记录，每一个 namespace 有一个叫 default 的 serviceaccount，里面列出了本 namespace 所有的可用的 imagePullSecrets，每个服务创建时都会这里找相应的登录认证信息。

**可能的原因及解决方法**：

搭建时配置的 user/password 不对或者权限不对: 在所有已经存在的 namespace 里删除原有 secret 并重新添加。

其他 namespace 同步 default 的信息没有同步成功：也就是说 default 里面的信息是正确的。简单的处理方式是把出问题的 namespace 删掉(里面没有服务或者服务很少或者没有重要的服务)，然后重试服务，它会自动重建 namespace，一般就成功了。

服务属于 app 或者某个 space 时，简单的解决方式是换个app名字或者 space，还是不行的话参考以下步骤。

**排查步骤**：

查看 default namespace下是否有相应的 secret。

```
kubectl get secrets  # 获取到对应registry的secret name
kubectl get sa default -o yaml |grep <secret-name>
```

如果没有，则说明部署配置有问题。

如果已经有其他服务用同样 registry 的镜像部署成功，则说明配置的 username/password 是正确的，但也有可能是部分镜像没权限。也有可能是 username/password 配置的不对导致登录不了。这两种情况出现较少，如果觉得没问题可以跳过。如果不确定可以检查下：

```
kubectl get secret <secret-name> -o yaml
echo "<base64_encoded_data>" | base64 -d
docker login
docker pull
```

获取出问题的服务所在的 namespace: <app_name>--<space-name>。没有相应值的用 default 填充，两者都没有的话 `namespace=default`。


查看服务所在 namespace 下的 iamge pull secrets 是否正常。

```
kubectl get secrets -n <namespace> # 看是否存在相应的secrets
kubectl get sa default -n <namespace> -o yaml |grep <secret_name> # 查看是否配置了secrets
```

如果上面两步有问题，则说明从 default 同步有问题。如果这个 namespace 下服务只有这一个或者很少不重要可以删除，那么可以删除这个 namespace，然后重试服务：


`kubectl delete namespace `

如果服务很多不能删除 namespace，则可以创建缺失的 secrets 并且确保其存在于 serviceaccount 中：

```
# if secrets not exist, recreate
kubectl create secret docker-registry <pull-image-secret-name> --docker-server=DOCKER_REGISTRY_SERVER --docker-username=DOCKER_USER --docker-password=DOCKER_PASSWORD --docker-email=DOCKER_EMAIL
 
kubectl get serviceaccounts default -o yaml > ./sa.yaml
 
vi sa.yaml
# 在文件添加以下内容
imagePullSecrets:
- name: <pull-image-secret-name> # 以后每次创建一个secret，在这个列表加一行类似的
  
kubectl replace serviceaccount default -f ./sa.yaml
```

以上操作知识针对 default namespace，可以确保以后新创建的 namespace 基本不出问题。对于已经存在的 namespace，也要做这些步骤。

```
kubectl create secret .... -n <namespace>
kubectl get sa default -n <namespace> -o yaml > ./sa.yaml
...
  
kubectl replace sa default -n <namespace> -f ./sa.yaml
```

### CPU 或内存资源不足

**故障现象**

创建服务或服务更新不成功，事件显示资源不足。

**可能的原因**

* 查看集群详情页面，看集群的cpu或者内存的使用率是否还有剩余。

* 集群中是否有打特殊tag的主机存在使得 cpu、mem 还有剩余，但是容器可运行的主机已不足。

* 查看服务的容器实例大小是否合适，判断运行容器内的进程是否需要更多的 cpu 或内存资源。

* 登录到 slave 每台虚拟机上，查看是否有通过其它方式运行的进程或容器占用了系统资源。

* 有之前未删除的容器在主机节点上运行。

**解决方法**

* 如果是机器资源不足，可以往集群中添加新的 slave。

* 如果因为特殊的 tag 存在使得，某个服务可运行的机器不足，但整体集群由富余的资源，则可以调整 tag 的分布。

* 如果服务需要更多的资源才能运行，则可以更改服务实例大小后，更新服务。

* 如果有其它进程或容器占用了主机资源，请删除它们。

**验证方法**

重新启动服务，如果可以运行表示问题已解决。



### 创建服务失败或服务无法访问

**故障现象**

创建服务或更新服务时，报错。服务的实例数为 0。

**可能的原因**

* 查看集群详情页，确认 cpu 和 mem 的使用率不高。有时即使还有一定的资源但是分散到每一台节点的资源其实已经不多了，这个时候，如果容器的大小较大，有可能造成任何一台机器都无法满足容器的需求。

* 无法下载指定的镜像，一般这种情况下是因为计算节点和 registry 的网络不通，或者权限不够。

* 容器会运行起来，但是过一会儿就自己退出，这个可能是因为容器内的应用程序有错误。

* 用户设置了健康检查，且健康检查的参数设置的不合理，导致服务时钟不能进入运行状态，最终因为超时被平台删掉。

* 镜像太大导致下载镜像非常慢。

* 服务已经创建成功，但是 Nginx 配置错误，导致容器无法访问。

**解决方法**

* 如果集群的计算节点不足，需要增加新的计算节点。或者将容器大小调小，使得可以跑在其中的一个计算节点上。

* 如果是镜像无法 pull 下来，需要检查网络、权限。

* 容器自己退出的解决办法是查找程序退出的原因，修改代码或者 Dockerfile。

* 因为健康检查设置导致的问题，需要调整参数，启动预估时间不应该太长，健康检查的 url 应该是正确的。

* 因为镜像太大，或者 pull 的较慢导致的问题，需要对镜像做优化，如：减小容器大小。

* 参考 **平台配置管理** 内容进行排查，确定 alb-nginx 的配置是否正确。

**验证方法**

再次创建服务成功。

### 删除 namespaces 但是长时间 ns 状态处在 Terminating 状态

k8s 的已知 bug，暂时没有找到原因，官方给出了解决办法：[参考](https://success.docker.com/article/kubernetes-namespace-stuck-in-terminating)。

可以在 master 上运行下面的命令删除所有 Terminating 状态的 ns。

**注意**：危险的命令，执行前一定要备份 etcd，并且审慎执行，错误删除后果很严重。  命令如下：

```
for ns in $(kubectl get ns | grep Terminating | awk '{print $1}')
do
    kubectl get ns $ns -o json | jq 'del(.spec.finalizers)' | curl -v -H "Content-Type: application/json" -X PUT --data-binary @- http://127.0.0.1:8080/api/v1/namespaces/$ns/finalize
done
```


​	
<div STYLE="page-break-after: always;"></div>	



#  应急预案

## 目的

本文档是在平台或平台管理的客户的业务服务遭遇突发事件，造成客户无法使用平台管理自己的业务服务，或者客户的业务服务无法完整的提供预订的功能，平台管理员紧急响应及恢复工作的指导文档。包括如下内容：

* 定义需要应急的突发事件

* 定义人员角色和职责

* 给出故障源快速定位方法

* 针对不同故障，给出应急启动、执行、恢复等流程、步骤和操作方案

## 适用范围

本文档适用于下面所述的故障应急

* 容器云平台。

* 容器云平台管理的客户的业务服务集群。

* 运行在容器云平台管理的业务服务集群上的，客户的业务服务。

## 紧急情况定义及处理方案

故障影响范围和故障严重程度定义请参见下表。

| 故障影响范围                                               | 故障严重程度                     | 分数 |
| ---------------------------------------------------------- | -------------------------------- | ---- |
| 平台 logo 等非功能部分客户某个多实例的业务服务中的部分实例 | 单次不再重现的告警信息           | 1    |
| 平台次要功能模块或客户某个业务服务                         | 持续告警                         | 2    |
| 平台主要功能模块或客户除核心业务外大部分业务服务           | 单次不再重现的预订的功能的错误   | 3    |
| 平台全部功能模块或客户核心业务服务                         | 部分功能错误，且不能自动恢复     | 4    |
| 平台不可访问或客户所有业务彻底中断                         | 全部预订功能错误，且不能自动恢复 | 5    |

故障级别=故障影响范围×故障严重程度请参见下表。

| 分数    | 故障级别                     | 通知范围                       | 操作                                                                             |
| ------- | ---------------------------- | ------------------------------ | -------------------------------------------------------------------------------- |
| 1-5分   | 需要记录                     | 无                             | 记录相关信息，并知会接班同事                                                     |
| 6-10分  | 需要检查并记录               | 值班领导                       | 记录相关信息，并检查平台和服务                                                   |
| 11-15分 | 马上发出警告并处理           | 上一级要通知的人及运维部门领导 | 记录并检查平台和服务，确认紧急情况发生按照应急预案独自处理                       |
| 16-20分 | 马上发出警告，并立即呼叫支援 | 上一级要通知的人及业务部门领导 | 执行上一步操作，并打电话寻求支持                                                 |
| 21-25分 | 最高级                       | 上一级要通知的人及公司领导     | 记录并检查平台和服务，确认紧急情况发生立即拉相关人员召开电话会议，并按照预案处理 |

## 人员角色及职责

| 序号 | 人员角色         | 隶属 | 职责                                   | 备注 |
| ---- | ---------------- | ---- | -------------------------------------- | ---- |
| 1    | 平台管理员       |      | 容器云平台管理                         |      |
| 2    | 业务服务运维     |      | 业务服务上线、调试、维护等日常运维人员 |      |
| 3    | 现场值班人员     |      | 业务和平台报障值班人员                 |      |
| 4    | 支援中心值班人员 |      | 24×7                                   |      |
| 5    | 业务服务开发     |      | 编写客户业务服务代码的开发人员         |      |
| 6    | 平台服务支持     |      | 容器云平台 24×7 服务支持               |      |
| 7    | 平台开发         |      | 容器云平台代码开发人员                 |      |

## 故障原因快速定位方法

### 平台不可访问

按照 LB、网络、域名解析、Kubernetes 集群、服务器、chart 状态、pod 状态等顺序排查。

### 平台上看不到业务服务

按照业务服务的 Kubernetes 集群状态、global 平台创建服务是否成功、网络等顺序排查。

### 平台上看不到业务服务集群

global 集群执行 `kubectl get clusters -n cpaas-system` ，确认是否删除集群。

### 业务服务功能不正常

查看业务服务 pod 状态、网络、 LB 顺序排查。

### 业务服务不可访问

按照 LB、域名解析、网络、pod 状态、pod 内进程状态等顺序排查。

## 应急操作手册

应急操作手册没有统一的步骤，需要根据不同的环境、不同的公司组织、不同的业务情况针对性的制订，下面给出的几个例子是比较常见且应急操作使用大部分环境的示例。

### 服务器故障造成某些定点部署的服务宕掉、集群资源不足，某些 pod 无法重新调度启动成功、造成业务服务中断

1. 确认故障级别，10 分以上开始处理。

2. 尝试登录故障服务器，如果不能登录转到 步骤 4。

3. 尝试启动 docker 和 kubelet ，恢复 Kubernetes node，如果不成功转到下一步。

4. 联系 iaas 管理员，尝试快速添加服务器扩容节点，如果是定点部署的服务或无法快速提供服务器转到下一步。

5. get pod，列出多实例的且负载不高的 pod，缩小实例，如果没有不成功转到下一步。

6. 列出重要程度低于这个服务的服务，关闭这些服务腾退资源，如果不成功进入容灾处理流程，切换到容灾的环境内。

7. 如果没有容灾环境，宣布处理失败。

### 域名解析故障造成业务服务无法访问

1. 紧急联系 dns 管理员，恢复 dns 服务器，如果不成功转到下一步。

2. 如果访问业务的的客户数量少，可以将域名解析写入 “/etc/hosts” 中。

3. 如果很多且不特定的客户访问业务，宣布处理失败。

### lb 故障造成 Kubernetes 集群或业务服务无法访问

1. 紧急联系网络管理员修复 lb，如果不成功转到下一步。

2. 和网络管理员确认 VIP 是否可以手动配置到某台服务器上，如果可以转入下一步，如果不行，转到步骤 4。

3. 在某个服务器上，手动启动 haproxy，并将 VIP 配置到这个服务器上，建议其他写好 haproxy.cfg。

4. 宣布处理失败。

### 新建的 pod 起不来，造成创建新的服务或更新老服务失败

1. 确认 pod 起不来的原因是资源不够问题还是 pod 本身问题，如果是资源问题，请参考 **服务器故障造成某些定点部署的服务宕掉、集群资源不足，某些 pod 无法重新调度启动成功、造成业务服务中断** 的步骤 4。

2. 如果 pod 配置错误，修改配置重启启动，如果部署转到下一步。

3. 在某台服务器上，用 docker run 的方式尝试用相同的镜像启动容器，如果不成功那就是容器构建问题，通知研发、测试，然后转到 步骤 5。

4. 在 Kubernetes 集群上，尝试用 yaml 的方式创建服务，如果成功那就是平台问题，通知平台管理员、厂商。

5. 宣布处理失败。

### 业务服务依赖的第三方组件故障，造成业务不正常

1. 联系第三方组件运维，紧急恢复，如果无法快速恢复，转到下一步。

2. 判断第三方组件是否有持久化的数据，如果没有，紧急在其他机器上启动，并修改服务配置，使用紧急搭建的第三方组件，这需要提前准备好第三方组件启动方式。

3. 宣布处理失败。

## 应急演练

应急演练在不同环境、不同公司组织、不同业务下都有不同，没有统一的，适合所有的演练方案，本文档给出的是一个示例。

### 演练目的

当灾难发生，相关人员能够迅速的反应，正确的按照既定流程、步骤处理，迅速、争取、不破坏数据、不扩大故障的前提下，快速恢复平台和业务服务。

### 演练周期

建议：

* 每个月一次涉及一个业务、一个部门的小范围小型演习。

* 每三个月一次整个平台主要故障的演习、涉及某个业务部门所有服务的中等规模演习。

* 每半年进行一次涉及整个平台、整个公司所有业务服务的大规模演习。

### 参与演练人员

* 小型演习只需要运维部门参加，业务部门可以提供电话值守人员帮助。

* 中等规模演运维部门全体参与，业务服务部门提供现场值班人员，研发提供电话值守人员。

* 大规模演习运维部门全体参与，业务服务部门大部门参与，研发提供现场值班，平台厂商提供电话值守帮助。

### 演练范围

* 小型演习：某个业务部门的某个业务服务。

* 中型演习：某个业务部门的所有服务，平台主要功能。

* 大型演习：全部业务服务，平台所有功能。

### 演练大纲