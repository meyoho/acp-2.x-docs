# 修订记录

| 版本 | 修订日期 | 修订人 | 修订内容 | 审核人及审核 jira | 文档生效日期 |
| ---- | ---- | ---- | ---- | ---- | ---- |
| v1.0 | 2020-03-17 | 王洪涛 | 重构 | | |

# ACP2.8对接外部kafka服务

* 需求：在ACP环境对接外部kafka服务
* 目标：能够在ACP环境中成功对接kafka

**重点**：下几项对客户提供的环境要求

## 前置条件

* 认证类型协议：必须是 plain 。 **目前仅支持用户名密码地址等参数的修改**
* 外部kafka环境：必须要知道kafka环境的用户名、密码，通过base64位加密后的用户名、密码,kafka主机地址等信息。请客户提供这些信息
* K8S环境：ACP2.8环境

## 环境准备

### kafka环境准备

获取kafka环境主机地址 base64加密的用户名和密码相关信息

### ACP2.8环境准备

使用 allinone 模式部署完成后ACP2.8环境所有组件正常可用。页面日志正常使用。（地址 192.168.133.200）



## 对接kafka操作配置

1 修改主机地址

在 sentry_values.yaml 这里有定义 host 信息（**注意**：修改为 kafka 的主机地址加端口没有协议）

修改前
```
kafka:
  install: true
  host: cpaas-kafka:9092     
  haSwitch: false
```


修改后

```
kafka:
  install: true
  host: 192.168.133.100:9092  ## 修改为外部kakfa服务地址：端口
  haSwitch: false
```

2 修改用户信息

```
kubectl edit secret -n cpaas-system acp-config-secret -oyaml    
KAFKA_PASSWORD: aFpZRlNmUUVoUEhtWWt2  #修改为kafka环境的密码 base64值
KAFKA_USER: bmRzcXN2bXVtdQ==    #修改为kafka环境的用户 base64值
```


## 更新ACP2.8环境组件服务

同上更新操作即可

## 验证

由于环境版本没办法验证，模拟es中无用户名密码验证。ACP2.8需要用户名密码验证。