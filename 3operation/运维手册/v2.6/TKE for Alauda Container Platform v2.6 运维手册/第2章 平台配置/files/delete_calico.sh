#!/bin/bash
kubectl delete -n kube-system configmap/cni-calico
kubectl delete customresourcedefinition.apiextensions.k8s.io/ips.kubeovn.io
kubectl delete customresourcedefinition.apiextensions.k8s.io/subnets.kubeovn.io
kubectl delete -n kube-system configmap/calico-config
kubectl delete customresourcedefinition.apiextensions.k8s.io/felixconfigurations.crd.projectcalico.org
kubectl delete customresourcedefinition.apiextensions.k8s.io/ipamblocks.crd.projectcalico.org
kubectl delete customresourcedefinition.apiextensions.k8s.io/blockaffinities.crd.projectcalico.org
kubectl delete customresourcedefinition.apiextensions.k8s.io/ipamhandles.crd.projectcalico.org
kubectl delete customresourcedefinition.apiextensions.k8s.io/ipamconfigs.crd.projectcalico.org
kubectl delete customresourcedefinition.apiextensions.k8s.io/bgppeers.crd.projectcalico.org
kubectl delete customresourcedefinition.apiextensions.k8s.io/bgpconfigurations.crd.projectcalico.org
kubectl delete customresourcedefinition.apiextensions.k8s.io/ippools.crd.projectcalico.org
kubectl delete customresourcedefinition.apiextensions.k8s.io/hostendpoints.crd.projectcalico.org
kubectl delete customresourcedefinition.apiextensions.k8s.io/clusterinformations.crd.projectcalico.org
kubectl delete customresourcedefinition.apiextensions.k8s.io/globalnetworkpolicies.crd.projectcalico.org
kubectl delete customresourcedefinition.apiextensions.k8s.io/globalnetworksets.crd.projectcalico.org
kubectl delete customresourcedefinition.apiextensions.k8s.io/networkpolicies.crd.projectcalico.org
kubectl delete customresourcedefinition.apiextensions.k8s.io/networksets.crd.projectcalico.org
kubectl delete clusterrole.rbac.authorization.k8s.io/calico-kube-controllers
kubectl delete clusterrolebinding.rbac.authorization.k8s.io/calico-kube-controllers
kubectl delete clusterrole.rbac.authorization.k8s.io/calico-node
kubectl delete clusterrolebinding.rbac.authorization.k8s.io/calico-node
kubectl delete -n kube-system daemonset.apps/calico-node
kubectl delete -n kube-system serviceaccount/calico-node
kubectl delete -n kube-system deployment.apps/calico-kube-controllers
kubectl delete -n kube-system serviceaccount/calico-kube-controllers
