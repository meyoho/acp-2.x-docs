#!/bin/bash
CIDR_SUBNET='10.160.0.1/16'       ## calico 使用的 ip 范围
IPIPMODE=Always                   ## 有Always，CrossSubnet, Never 三种模式，默认Always
# 执行参数，让执行脚本时，带的参数生效
for i in $@
do
    eval $i
done
cat <<EOF > default-subnet.yml 
apiVersion: kubeovn.io/v1
kind: Subnet
metadata:
  name: default-ipv4-ippool
spec:
  blockSize: 26
  cidrBlock: ${CIDR_SUBNET}
  default: true
  ipipMode: ${IPIPMODE}
  namespaces: []
  natOutgoing: true
  private: false
  protocol: IPv4
  vxlanMode: Never
EOF
