# 2.9 平台对接 ocp 集群

## 对接 openshift 3.11 

1. 部署 helm ：请参考 2.9 部署文档 4.4 章，helm 安装 sentry 这一节中，如果 业务集群没有安装 helm 怎样部署 helm 的步骤。
2. 部署 cpaas-lander ：请参考 2.9 部署文档 4.4 章，部署 cpaas-lander 这一节的内容
3. 在 ui 页面对接集群：
  * 集群地址填写lander代理的地址 eg：http://ip:32008
  * token 获取方式 `kubectl get secret -n kube-system | grep agg` 查看 `clusterrole-aggregation-controller-token-**** ` 获取 token
  * CA证书 /etc/kubernetes/pki/ca.crt  其余参数正常，即可对接完成



## 对接 openshift 4.2 [对接ocp4.2](http://confluence.alauda.cn/pages/viewpage.action?pageId=56996635)

1. 部署 helm ：请参考 2.9 部署文档 4.4 章，helm 安装 sentry 这一节中，如果 业务集群没有安装 helm 怎样部署 helm 的步骤。
2. 部署 cpaas-lander ：请参考 2.9 部署文档 4.4 章，部署 cpaas-lander 这一节的内容
3. 在 openshift 管理页面创建 router
  * router 的 service 选择 cpaas-lander (不用nodeport)
  * port 选择 8081
  * 选择开启 tls 加密。 passthrough 模式，如下图：

![](./images/ocp-4.2.png)

4. 修改[dex.sh](./files/dex.sh) 这个脚本中的变量，并执行，基于把生成的 router 地址 (location) ，签证书。并且把证书更新回 lander，让 lander 支持 https
5.  最终把 ocp 集群对接到 global 中之后，手工修改对应的 clusters 资源，增加 label:alauda.io/cluster-type: OCP  其中 alauda.io 根据当时环境进行对应的配置。 前端需要 labels 如下：

```
metadata:
  labels:
    alauda.io/cluster-type: OCP       ## 和下一个这两个标签，任选一个
    cpaas.io/cluster-type: OCP        ## 和上一个这两个标签，任选一个
```


## 对接 openshift 4.3 [acp2.9如何接入自签证书/未注册域名的ocp4.3](http://confluence.alauda.cn/pages/viewpage.action?pageId=67535790)

1. 部署 helm ：请参考 2.9 部署文档 4.4 章，helm 安装 sentry 这一节中，如果 业务集群没有安装 helm 怎样部署 helm 的步骤。
2. 部署 cpaas-lander ：请参考 2.9 部署文档 4.4 章，部署 cpaas-lander 这一节的内容。需要注意部署chart时 lander.openshift_server_address 这个参数建议配置值为:  kubernetes.default
3. 在 openshift 管理页面创建 router
4. 页面对接集群使用lander的token、CA证书 参考链接中的内容
