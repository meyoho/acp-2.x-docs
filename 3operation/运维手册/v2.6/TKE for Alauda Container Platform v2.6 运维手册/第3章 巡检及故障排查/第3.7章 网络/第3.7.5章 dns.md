# 修订记录

| 版本 | 修订日期 | 修订人 | 修订内容 | 审核人及审核 jira | 文档生效日期 |
| ---- | ---- | ---- | ---- | ---- | ---- |
| v1.0 | 2020-03-17 | 王洪涛 | 重构 | | |

# DNS

## kube-dns

## coredns

域名解析很慢，有差不多 5s 的延迟，配置 node local dns 可以缓解，[yaml](https://github.com/kubernetes/kubernetes/blob/master/cluster/addons/dns/nodelocaldns/nodelocaldns.yaml)。根本原因是内核问题。详细讨论见 [仅内部人员可见](http://confluence.alauda.cn/pages/viewpage.action?pageId=39333034)。

### kubernetes 配置自定义域名记录，通过 coredns

部分内网客户有增加自定义域名记录的需求。基本有 3 种方法：

* 客户存在内网dns，且该内网 dns 可以解析用户需要的域名。这种情况只需要确保 coredns 所在的宿主机的 `/etc/resolv.conf` 中配了该 dns 即可。
* 使用 kubernetes 原生的 hostalias 功能，对于每个 pod 额外配置 dns 记录，[参考信息](https://kubernetes.io/docs/concepts/services-networking/add-entries-to-pod-etc-hosts-with-host-aliases/)。

``` 
yaml
apiVersion: v1
kind: Pod
metadata:
  name: hostaliases-pod
spec:
  restartPolicy: Never
  hostAliases:
  - ip: "127.0.0.1"
    hostnames:
    - "foo.local"
    - "bar.local"
  - ip: "10.1.2.3"
    hostnames:
    - "foo.remote"
    - "bar.remote"
  containers:
  - name: cat-hosts
    image: busybox
    command:
    - cat
    args:
    - "/etc/hosts"
```

* 使用 coredns 来做集群内全局的记录增加。
  * 用 file 插件，执行 `kubectl -n kube-system edit cm coredns` 修改 coredns 的 configmap，默认情况下如下：
	
```
yaml
apiVersion: v1
data:
  Corefile: |
    .:53 {
        errors
        health
        kubernetes cluster.local in-addr.arpa ip6.arpa {
           pods insecure
           upstream
           fallthrough in-addr.arpa ip6.arpa
        }
        prometheus :9153
        proxy . /etc/resolv.conf
        cache 30
        loop
        reload
        loadbalance
    }
kind: ConfigMap
metadata:
  name: coredns
  namespace: kube-system
```
		
	将其修改为：
	
```
yaml
apiVersion: v1
data:
  Corefile: |
    .:53 {
        errors
        health
        kubernetes cluster.local in-addr.arpa ip6.arpa {
           pods insecure
           upstream
           fallthrough in-addr.arpa ip6.arpa
        }
        prometheus :9153
        file /etc/coredns/example.db example.org //使用中将example.org换为根域名
        proxy . /etc/resolv.conf
        cache 30
        loop
        reload
        loadbalance
    }
  example.db: |
    example.org.            IN      SOA     sns.dns.icann.org. noc.dns.icann.org. 2019062541 7200 3600 1209600 3600 // 修改example.org.换为对应的，不要漏了.
    xxx.example.org.            IN      A       1.1.1.1 // 对应a记录在这里添加,用户想加几个域名就仿照这行格式在下面增加
kind: ConfigMap
metadata:
  name: coredns
  namespace: kube-system
```
		
	修改后保存。 之后修改 coredns 的 deployment，将 example.db 挂载进去。并执行 `kubectl -n kube-system edit deploy coredns` 找到 volumes。
	
```
      volumes:
      - configMap:
          defaultMode: 420
          items:
          - key: Corefile
            path: Corefile
          name: coredns
        name: config-volume
```
	
	将其改为：
	
```
      volumes:
        - name: config-volume
          configMap:
            name: coredns
            items:
            - key: Corefile
              path: Corefile
            - key: example.db
              path: example.db
```

  * 用 hosts 插件，[参考信息](https://coredns.io/plugins/hosts/)。执行 `kubectl -n kube-system edit cm coredns` 修改 coredns 的 configmap， 默认情况如下：
	
```
yaml
apiVersion: v1
data:
  Corefile: |
    .:53 {
        errors
        health
        kubernetes cluster.local in-addr.arpa ip6.arpa {
           pods insecure
           upstream
           fallthrough in-addr.arpa ip6.arpa
        }
        prometheus :9153
        proxy . /etc/resolv.conf
        cache 30
        loop
        reload
        loadbalance
    }
kind: ConfigMap
metadata:
  name: coredns
  namespace: kube-system
```
	
	按下面的例子修改，并重建 coredns 的 pod 即可：
	
```
yaml
apiVersion: v1
data:
  Corefile: |
    .:53 {
        errors
        health
        kubernetes cluster.local in-addr.arpa ip6.arpa {
           pods insecure
           upstream
           fallthrough in-addr.arpa ip6.arpa
        }
        prometheus :9153
        hosts example.org { //修改这里，example.org为根域名
            10.0.0.1 bbb.example.org //这里就跟本地配host格式一样
            fallthrough
        }
        // 这里如果宿主机配了hosts，也可以直接写hosts，不用上面的写法
        proxy . /etc/resolv.conf
        cache 30
        loop
        reload
        loadbalance
    }
kind: ConfigMap
metadata:
  name: coredns
  namespace: kube-system
```

* 用 fowrad 插件，[参考信息](https://coredns.io/plugins/forward/)。执行 `kubectl -n kube-system edit cm coredns` 修改 coredns 的 configmap， 默认情况如下：

```
yaml
apiVersion: v1
data:
  Corefile: |
    .:53 {
        errors
        health
        kubernetes cluster.local in-addr.arpa ip6.arpa {
           pods insecure
           upstream
           fallthrough in-addr.arpa ip6.arpa
        }
        prometheus :9153
        proxy . /etc/resolv.conf
        cache 30
        loop
        reload
        loadbalance
    }
kind: ConfigMap
metadata:
  name: coredns
  namespace: kube-system
```

	按下面的例子修改，并重建 coredns 的 pod 即可：

```
yaml
apiVersion: v1
data:
  Corefile: |
    .:53 {
        errors
        health
        kubernetes cluster.local in-addr.arpa ip6.arpa {
           pods insecure
           upstream
           fallthrough in-addr.arpa ip6.arpa
        }
        prometheus :9153
        forward example.org. 127.0.0.1:9005 127.0.0.1:9006 // 这里example.org.是根域名，后面是客户的dns服务器地址，可以写多个
        proxy . /etc/resolv.conf
        cache 30
        loop
        reload
        loadbalance
    }
kind: ConfigMap
metadata:
  name: coredns
  namespace: kube-system
```