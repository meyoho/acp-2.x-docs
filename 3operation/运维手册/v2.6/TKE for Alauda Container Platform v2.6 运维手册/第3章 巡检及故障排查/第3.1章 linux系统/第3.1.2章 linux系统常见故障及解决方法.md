# 修订记录

| 版本 | 修订日期 | 修订人 | 修订内容 | 审核人及审核 jira | 文档生效日期 |
| ---- | ---- | ---- | ---- | ---- | ---- |
| v1.0 | 2020-03-17 | 王洪涛 | 重构 | | |

# linux 系统常见故障

* 执行df -Th卡住，排查方法
  1. 使用starce查看df -Th 执行状态,如果是卡在了 `/proc/sys/fs/binfmt_misc` 这个位置
  2. 执行 `systemctl restart proc-sys-fs-binfmt_misc.automount` 解决
* 系统报错内存不分配，系统日志报错SLUB: Unable to allocate memory on node -1时
  1. kubelet二进制文件中关闭kmem
  2. 或者释放内存来临时解决

更多参考链接：
http://confluence.alauda.cn/pages/viewpage.action?pageId=61911675

* 系统IO导致高负载，节点高负载会导致进程无法获得足够的 cpu 时间片来运行，通常表现为网络 timeout，健康检查失败，服务不可用。有时候即便 cpu ‘us’ (user) 不高但 cpu ‘id’ (idle) 很高的情况节点负载也很高，这通常是文件 IO 性能达到瓶颈导致 IO WAIT 过多，从而使得节点整体负载升高，影响其它进程的性能。使用 top 命令看下当前负载：

```
top - 19:42:06 up 23:59,  2 users,  load average: 34.64, 35.80, 35.76
Tasks: 679 total,   1 running, 678 sleeping,   0 stopped,   0 zombie
Cpu(s): 15.6%us,  1.7%sy,  0.0%ni, 74.7%id,  7.9%wa,  0.0%hi,  0.1%si,  0.0%st
Mem:  32865032k total, 30989168k used,  1875864k free,   370748k buffers
Swap:  8388604k total,     5440k used,  8383164k free,  7982424k cached
  PID USER      PR  NI  VIRT  RES  SHR S %CPU %MEM    TIME+  COMMAND
 9783 mysql     20   0 17.3g  16g 8104 S 186.9 52.3   3752:33 mysqld
 5700 nginx     20   0 1330m  66m 9496 S  8.9  0.2   0:20.82 php-fpm
 6424 nginx     20   0 1330m  65m 8372 S  8.3  0.2   0:04.97 php-fpm
 6573 nginx     20   0 1330m  64m 7368 S  8.3  0.2   0:01.49 php-fpm
```

wa (wait) 表示 IO WAIT 的 cpu 占用，默认看到的是所有核的平均值，要看每个核的 wa 值需要按下 “1”键:

```
top - 19:42:08 up 23:59,  2 users,  load average: 34.64, 35.80, 35.76
Tasks: 679 total,   1 running, 678 sleeping,   0 stopped,   0 zombie
Cpu0  : 29.5%us,  3.7%sy,  0.0%ni, 48.7%id, 17.9%wa,  0.0%hi,  0.1%si,  0.0%st
Cpu1  : 29.3%us,  3.7%sy,  0.0%ni, 48.9%id, 17.9%wa,  0.0%hi,  0.1%si,  0.0%st
Cpu2  : 26.1%us,  3.1%sy,  0.0%ni, 64.4%id,  6.0%wa,  0.0%hi,  0.3%si,  0.0%st
Cpu3  : 25.9%us,  3.1%sy,  0.0%ni, 65.5%id,  5.4%wa,  0.0%hi,  0.1%si,  0.0%st
```

wa 通常是 0%，如果经常在 1 之上，说明存储设备的速度已经太慢，无法跟上 cpu 的处理速度，可以使用 `iotop -oPa` 查看哪些进程占用磁盘 IO:

```
Total DISK READ: 15.02 K/s | Total DISK WRITE: 3.82 M/s
  PID  PRIO  USER     DISK READ  DISK WRITE  SWAPIN     IO>    COMMAND
 1930 be/4 root          0.00 B   1956.00 K  0.00 % 83.34 % [flush-8:0]
 5914 be/4 nginx         0.00 B      0.00 B  0.00 % 36.56 % nginx:cache manager process
  880 be/3 root          0.00 B     21.27 M  0.00 % 35.03 % [jbd2/sda5-8]
 5913 be/2 nginx        36.00 K   1000.00 K  0.00 %  8.94 % nginx: worker process
 5910 be/2 nginx         0.00 B   1048.00 K  0.00 %  8.43 % nginx: worker process
 5896 be/2 nginx        56.00 K    452.00 K  0.00 %  6.91 % nginx: worker process
```

结果中可以看到flush-8:0和nginx占用了大量IO

* es日志占用大量空间，执行下面的命令查看当前所有索引

```
curl -X GET -u <es-user>:<es-password> localhost:9200/_cat/indices
green open log-20200214        RTo0Sga9QDmZM5g_DpH2Ww 5 1  9873013   0  11.1gb   5.5gb
green open log-20200215        yDXUiN6GRT6GakhnvazXQw 5 1      172   0 430.4kb 215.2kb
green open event-20200215      Il3YJgQ7QZSMCK6o7zuvEg 5 1     5495 529  12.4mb   6.2mb
```

根据索引日期删除陈旧索引， 执行 `curl -XDELETE http://<es-user>:<es-password> localhost:9200/log-20200214`

更多参考链接：
http://confluence.alauda.cn/pages/viewpage.action?pageId=48729648 （清理过期日志的操作）
http://confluence.alauda.cn/pages/viewpage.action?pageId=50828051 （ES配置及吞性能测试）
