# 修订记录

| 版本 | 修订日期 | 修订人 | 修订内容 | 审核人及审核 jira | 文档生效日期 |
| ---- | ---- | ---- | ---- | ---- | ---- |
| v1.0 | 2020-03-17 | 王洪涛 | 重构 | | |

# 服务

## 说明

**应用创建流程**：通过调用 archon 高级 api --> erebus 代理-->业务集群 APIServer -->创建应用（同时 charon 监听状态）；而通过应用目录创建应用则调用 juno 高级 api --> captain -->创建应用（同时 charon 监听状态）。

## 相关组件

**archon**：部分公用高级 API、容器平台高级 API 。实现高级 API 的功能，提供应用创建，更新和删除功能，archon 需要通过 erebus(aproxy) 来访问 k8s 集群的 apiserver。

**charon**：应用状态 Controller ，业务集群内控制器。监听集群内的 pod 、deployment 等资源对象来更新 application 的状态，当集群内显示状态和界面上应用状态不一致时，可排查此组件。

**juno**：应用目录高级 API 。
应用市场页面的诸多功能都是原生 Kubernetes API (包括 CRD )所提供不了的或者实现起来比较繁琐的，Juno 的主要工作便是基于原生 Kubernetes API 提供这些高级 API ，如方便的通过模版仓库创建 API 。

**captain**：应用目录 Controller 组件，用来创建 Chart 资源。
更多 captain 介绍和运维可查看专门的文档《 captain 运维手册》。

## 故障排查流程

### 简介

**说明**：当出现服务无法访问时，先通过下面的服务模块故障图去判断问题，如果是 pod 文档，可以通过 巡检及故障排查 章节中，kubernetes 中，常见故障这个文档内的，pod 故障图解来查看分析；当定位属于网络故障时，可按照 巡检及故障排查 章节中，网络一节中的 kube-ovn、calico、flannel、alb2 等对应的网络去分析问题所在，一般出现网络故障时推荐使用 tcpduamp 来抓包分析故障点。

![服务模块故障图](../images/acp2.6服务模块故障图.png)

### 注释

**注释 1**：容器内抓包

本文提供脚本，可一键进入容器网络命名空间（ netns ），从而使用宿主机上的 tcpdump 进行抓包。
登录 Pod 所在节点，将如下脚本粘贴到 Shell ，注册函数到当前登录的 Shell：

```
function e() {
 set -eu
 ns=${2-"default"}
 pod=`kubectl -n $ns describe pod $1 | grep -A10 "^Containers:" | grep -Eo 'docker://.*$' | head -n 1 | sed 's/docker:\/\/\(.*\)$/\1/'`
 pid=`docker inspect -f {{.State.Pid}} $pod`
 echo "entering pod netns for $ns/$1"
 cmd="nsenter -n --target $pid"
 echo $cmd
 $cmd
}
```

之后执行以下命令，一键进入 Pod 所在的 netns

```
# e <POD_NAME> <NAMESPACE>  如# e nginx-58c7c7c646-m6568 cpaas-system
```

查看容器的网卡、监听端口

```
# ifconfig 
# netstat -tunlp
```

执行以下命令，使用 tcpdump 进行抓包
注意：如果是指定端口抓包，这里要使用容器的内部端口。比如，容器通过 -p 8180:80 做了端口 bind，那么这里要抓 80 端口，而非 8180 端口。

```
# tcpdump -i eth0  port 80
```

**注释 1 脚本原理**：

* 查看指定 Pod 运行的容器 ID：`kubectl describe pod <pod> -n mservice`
* 获得容器进程的 Pid： `docker inspect -f {{.State.Pid}} <container>`
* 进入该容器的 network namespace ：`nsenter -n --target <PID>`

### 参考

ACP2.x 容器平台故障排查：
http://confluence.alauda.cn/pages/viewpage.action?pageId=61915531





