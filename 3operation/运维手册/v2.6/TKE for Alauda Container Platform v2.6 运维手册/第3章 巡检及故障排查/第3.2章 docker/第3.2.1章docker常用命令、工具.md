# 修订记录

| 版本 | 修订日期 | 修订人 | 修订内容 | 审核人及审核 jira | 文档生效日期 |
| ---- | ---- | ---- | ---- | ---- | ---- |
| v1.0 | 2020-03-17 | 王洪涛 | 重构 | | |

# docker 常用命令工具

* 定位容器和运行状态

  1. 查看需要排查的容器或者状态非running的容器，执行 `docker ps -a`
  2. 查看容器的配置信息和运行时状态，执行 `docker inspect <container-id或contianer-name>`

* 查看容器应用日志

  1. 在命令行中输入下列命令，查看指定容器在stdout/stderr的日志。`docker logs <container-id或contianer-name>`  **注意** 使用 json-file 和 journald 之外的日志驱动程序时，容器标准输出的日志被日志驱动搜集走了， `docker logs` 命令获取不到日志了。
  2. 实时查看容器最后100行日志， 执行 `docker logs -f --tail=100 <container-id或contianer-name>`

* 查看容器进程信息

  1. 列出指定容器中运行的进程信息， 执行 `docker top <container-id或contianer-name>` **注意**：这里的PID显示的是宿主机操作系统上的PID信息
  2. 进入容器内查看进程， 执行 `docker exec nginx-pod ps -ef`
  3. 单独查看指定容器所属docker子进程的PID， 执行 `docker inspect -f '{{ .State.Pid }} {{ .Id }}' $(docker ps -aq)`

* 进入容器诊断问题

  1. 进入容器内部执行命令， 执行 `docker exec -ti <container-id或contianer-name> sh` **注意**：如果容器支持bash，也可以在shell命令行中输入bash来执行命令。
  2. 或Web控制台中也支持exec的功能

* 查看容器性能信息

  1. 查看容器的性能监控信息， 执行 `docker stats <container-id或contianer-name>`
  2. 或Web控制台中监控面板中也可查看容器的监控信息

* 查看Docker日志

  1. 有时因为系统原因，Docker Engine无法正常创建、删除、启动、停止容器，我们需要查询Docker Engine日志来排查信息， 执行  `journalctl -xefu docker`
