# 修订记录

| 版本 | 修订日期 | 修订人 | 修订内容 | 审核人及审核 jira | 文档生效日期 |
| ---- | ---- | ---- | ---- | ---- | ---- |
| v1.0 | 2020-03-17 | 王洪涛 | 重构 | | |
| v1.0 | 2020-04-22 | 王洪涛 | 更新 | | |


# captain 基本信息

## 介绍

Captain 是一个标准的 kubernetes controller, 以 helm v3 library 为基础，参考 helm v3 design proposal 而实现。用户可以通过 HelmRequest CRD 来描述对 helm charts 的部署需求，captain 负责同步 HelmRequest 的状态并部署 helm charts。

## 流程图

![1582876945889](../images//1582876945889.png)

## 基本操作

### 安装 captain （部署平台会自动安装，下面的方法仅供参考）

```
helm install --version <captain chart version> \
             --debug \
             --namespace=<ns> \
             --set global.registry.address=<init registry> \
             --set alaudaChartRepoURL=<init 节点上的 chart repo> \
             --set namespace=<ns> \
             --name=captain stable/captain --wait --timeout 3000
```

kubectl-captain  在安装目录下的 other 目录里，captain 依赖 cert-manager ，必须在 cert-manager 部署成功后，安装 captain

### 替换 helm ，已经通过 helm 部署的 chart 怎样迁移到 captain ,详见升级说明文档，大致流程如下：

1. 通过 kubectl captain 命令或者直接创建 helmrequest 资源，hr 资源存在 global 的集群内
2. captain 侦测到新的 helmrequest 资源，会自动部署 chart ，成功后，会生成 release 资源。chart 安装到那个集群，release 资源就存在那个集群内
3. helm 每成功安装一次 chart ，就会在 kube-system 的 ns 下创建一个 cm，cm 的名字是 chart 的 release 名字+ v[0-9]，比如 cert-manager.v1，是 cert-manager 这个 chart 第一次安装成功的版本，以后每更新一次这个 chart ，后面的数字就加1
4. 找到这个 cm，删掉之后执行 helm list -a 命令就看不到 chart 了，但是 chart 创建的资源还存在

### Helm 安装的 Captain 迁移至非 Helm 安装的 Captain

1. 简介：Captain 的最新版本去除了 cert-manager 的依赖，改用直接用 yaml 部署。已经用 helm 部署过的 Captain , 如果直接删除的话，会导致相关CRD及CR被删。所以本文描述的是手工清理掉相关资源并且用最新的方式安装 Captain 的文档。
2. 清理

```
kubectl delete mutatingwebhookconfigurations.admissionregistration.k8s.io captain
kubectl delete validatingwebhookconfigurations.admissionregistration.k8s.io captain
kubectl delete svc -n cpaas-system captain
kubectl delete issuer captain-selfsigned-issuer -n cpaas-system
kubectl delete cert captain-serving-cert -n cpaas-system
kubectl delete deploy captain -n cpaas-system
kubectl delete secret captain-webhook-cert -n cpaas-system    # (可能没有，出错可以忽略)
kubectl get cm -n kube-system |grep ^captain | awk '{print $1}' | xargs kubectl delete cm -n kube-system
```

3. 安装

```
wget -c https://raw.githubusercontent.com/alauda/captain/master/artifacts/all/release.yaml
# linux 环境 "" 去掉
sed -i "" 's/captain-system/cpaas-system/g' deploy.yaml
kubectl apply --validate=false -n cpaas-system -f deploy.yaml
```

## 功能与特性

  - 支持多集群：Helm v3的设计中并没有支持多集群的功能。Captain组件基于 cluster-registry项目实现了对多集群的支持。用户可以将HelmRequest安装到当前集群，指定集群，甚至是所有集群。这在企业场景下是非常有用的。
  - 依赖检查：Helm本身具备依赖概念，功能也比较复杂。Captain在Helm v3的基础上加了新的依赖检查。如果HelmRequest A 依赖于 HelmRequest B, 在声明好依赖之后。Captain 在同步HelmRequest A 时，会首先检查 HelmRequest B是否已经同步。只有在所有依赖都同步成功之后，当前的HelmRequest才会开始同步。
  - 集中式配置valuesFrom：在Helm v2中部署Chart 时，可以通过在命令行添加参数或者使用 values.yaml 文件来对 chart 部署添加一些自定义变量。在Captain中，除了对这种传统的方式进行进行支持外，也充分利用了kubernetes 中 ConfigMap 以及 Secret 作为独立配置的功能。用户可以在HelmRequest 的定义中通过 valuesFrom的来自外部的ConfigMap 或者Secret 作为 values 来源。在微服务场景下，用户通常需要用多个 HelmRequest 来描述不同chart 的安装需求，而这些 chart 之间通常又经常有很多需要共同的配置， valuesFrom 能很好地解决这个问题。
  - 资源冲突的处理： 用户在使用 helm v2 的时候，经常会碰到很多使用上的不便之处。因为资源已存在导致的安装失败，因为卸载不彻底进一步导致下一步安装失败，这其中就包含了经典的CRD管理的问题(因为要等带crd的api生效，引入了crd-install hook, 加了hook的CRD在删除 release时不会删除)。Captain 为了解决了这个问题，引入了类似于 `kubectl apply`的逻辑。当发现资源已存在的时候，会执行更新操作，一些无法更新的资源会进行删除重建操作。这样的策略能够极大地解决用户使用时遇到的各种不便之处


1. 通用操作

helm 中的很多概念在 captain 都通过 crd 实现了，可以用 kubectl 来进行查看及排错，常见的操作，如下: 

  - get: 查看
  - describe: 查看事件及可能的错误信息
  - edit: 更新

资源如果不是处于 Synced 状态即表示有问题，可以通过 describe 操作查看事件，如果想创建 ChartRepo 或者 HelmRequest， 直接用 kubectl + yaml 即可

2. 仓库

将helm 的仓库以配置文件的方式存储于本地目录中，在helm中可以通过 helm repo list 来查看. captain 有类似的仓库概念，可以通过如下命令查看: `kubectl get ctr -n <system-namespace>`

正常情况下，各个 ChartRepo 都应该处于 Synced 状态。

3. HelmRequest (hr)

HelmRequest (hr)  对应于一个 charts 的部署，其内容与 `helm install/upgrade` 等的参数类似，详细介绍可参考 [HelmRequest CRD](https://github.com/alauda/captain/blob/master/docs/helmrequest.md), 常用操作如下:

```
 kubectl get hr -n <ns>         ## 查看
 kubectl edit hr -n <ns>        ## 更新 hr
 kubectl describe hr -n <ns>    ## 查看 helmrequest 的详细信息
 kubectl captain create         ## 创建 一个helmrequest
 kubectl delete <name> -n <ns>  ## 删除 chart
 kubectl captain import         ## 从一个release 导入一个helmrequest
 kubectl captain rollback       ## 回滚一个helmrequest
 kubectl captain upgrade        ## 升级一个helmrequest
 kubectl captain trigger-update ## 触发更新一个helmrequest
 kubectl get release -n <ns>    ## 查看通过 captain 部署的 chart

```

## 常见问题排查

### 通用

一般来讲，主要的关注点都是 HelmRequest, 基本的排查流程如下:

1. 查看状态是否是 Synced， 不是 Synced 的都是未成功安装
2. 用 kubectl describe 查看 HelmRequest 的相关事件, 出错时会有更详细的信息

### 常见问题列表

1. cannot re-use a name that is still in use
  * 问题解释: Captain 会不断地尝试同步一个 HelmRequest, 在同步的过程中，会不断地创建 Release 资源(更新或重试都会产生新的), 如果在这个过程中，产生了pending或类似进行中状态的 Release, 下一次重试时就会碰到这个错误
  * 处理方式:

```
kubectl get rel -n <.spec.namespace>

# 删除所有的 release 资源
kubectl get rel -n <.spec.namespace>  | grep <.spec.releaseName>\. | awk '{print $1}' | xargs kubectl delete -n <.spec.namespace> rel

# 触发更新。或者直接用kubectl 编辑hr 增加一个任意的values即可
kubectl captain trigger-update <hr-name> -n <hr-namespace>
```

  * 后续改进：2.8 及之后版本添加代码 fix


2. could not get apiVersions from Kubernetes: unable to retrieve the complete list of server APIs: xxx: the server is currently unable to handle the request
  * 问题解释：此问题的原因是因为 CRD, 有的 crd 安装之后， apiserver 的 discovery 信息并未就绪，导致 captain 里面访问 k8s 出错
  * 处理方式：在目标集群，执行 `kubectl get apiservice`有的 apiservice 会显示为 false. 定位到具体的 group 后，可找相应的此功能的负责人去处理此事。如下:
    1. metrics: 一般是 metrics server没有部署
    2. devops： devops组件没有正常运行
    3. cert-manager: cert-manager没有正常运行
    4.  等到都 True 之后，重试或者触发更新即可
    5.  如果按上述执行仍无效果，可重建captain的Pod

  * 后续改进：2.9及之后代码fix


3. CAPTAIN: 一直处于init状态,无法正常启动
  * 问题现象：Captain Pod 启动时一直处于 `Init` 状态。查看日志如下所示：

```
kubectl logs -f -n cpaas-system <captain-pod-name> -c cert-init
 
## example output
creating certs in tmpdir /tmp/tmp.LfpiIH
Generating RSA private key, 2048 bit long modulus (2 primes)
..+++++
...........................................+++++
e is 65537 (0x010001)
secret namespace is cpaas-system
Error from server (AlreadyExists): secrets "captain-webhook-cert" already exists
secret/captain-webhook-cert annotated
    init-pod-rand: "271"
I'm continue to create secret
Error from server (NotFound): certificatesigningrequests.certificates.k8s.io "captain-webhook.cpaas-system" not found
Killed
```

  * 问题解释：Captain 自 2.6 之后(不包含), 用自带的脚本( initContainers 中)来生成 cert. 因为 initContainer 未设置 resource 限制，如果当前 Namespace 有 LimitRange 且所设默认值较小，initContainer 会因为 OOM 被一直 Killed
  * 处理方式：

```
kubectl get limitrange  -n cpaas-system
# For example, name=default
kubectl get limitrange default -n cpaas-system -o yaml
kubectl delete limitrange default -n cpaas-system
```

  * 后续改进：明确设置 initContainer 的 Resource (>=ACP2.9)


4. HR: repo <name> not found
  * 问题现象：HelmRequest的事件显示 repo not found. 但是 ChartRepo 是存在且 Synced
  * 问题解释：2.6.1 及之前的版本里，HelmRequest 对于 repo 的引用仍是沿用 helm 的格式，Captain 在容器里保留了 helm 格式的本地文件用于 供HelmRequest 使用。故二者有不一致的现象
  * 解决办法：一般不用处理，过一段时间自己会恢复
  * 后续改进：2.8 及之后，直接使用 ChartRepo 数据，如果找不到就说明 ChartRepo 不存在或未 Synced


5. HR:  由于Kubernetes版本升级导致的部分Resource的Group变更导致的HelmRequest upgrade失败
  * 问题解释：假设 k8s 版本由原地从 1.13 升级到 1.16 , 那么已经部署的 HelmRequest 和 Release 还在，且内容保持不变。但是集群中的 Deployment 等资源，其 apiVersion 会由 k8s 自动升级。那么在对这个 HelmRequest 做升级的时候，captain/helm 会对比当前 chart 的 resources 以及已经存在的 Release 里的数据。由于旧的 Release 方式里的 Resource 记录还是旧的 apiVersion ,故会报错找不到版本
  * 处理方式：删掉当前 HelmRequest 的 Release 数据, 具体 release 名字格式为 <.spec.releaseName>.v<NUm>.
触发一次 upgrade 。可通过编辑 values 或者 kubectl-captain 进行
  * 副作用：Service/Job会被删掉重建,一般无影响。PVC 不会被更新(无法 patch/update/ 直接 delete)，如果明确知道此次 upgrade 对 PVC 做了变更， 请手动更新
  * Captain Image tag：v1.0.0-3
  * 后续改进：无


6. HR： timed out waiting for the condition
  * 问题描述：HelmRequest处于 Failed 状态，且 Event 里显示错误信息:  timed out waiting for the condition
  * 问题解释：目前发现的原因是 Captain 在读取到相应目标集群的访问方式后，尝试去安装 Release CRD, 但是一直安装失败。如下图所示，图中所显示的错误为没有权限:

![](../images/captain-timeout.png)

  * 处理方式：需要排查相应的 Cluster 提供的访问方式是否正确，步骤如下

```
# 查看集群列表
 kubectl get clusters.clusterregistry.k8s.io  -n cpaas-system
 
# 查看目标集群的信息
kubectl get clusters.clusterregistry.k8s.io  -n cpaas-system <cluster-name> -o yaml
 
# 从上述 yaml 中拿到 endpoint及token(token位于yaml中所引用的Secret中,需base64 decode)
# 使用curl等方式查看是否可以访问目标集群
# link: https://kubernetes.io/docs/tasks/administer-cluster/access-cluster-api/
# 注意: 可以通过访问namespace或者pods列表api
# 如下仅为example,具体请根据具体情况修改
curl -X GET $APISERVER/api/v1/namespaces/default/pods --header "Authorization: Bearer $TOKEN" --insecure
```

  * 后续改进：优化 Message 提示