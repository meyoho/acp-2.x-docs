# 概述

## 读者对象

适用于具备基本的 linux、容器、Kubernetes 知识，想要安装和配置平台的用户。包括：

* 平台管理员

* 平台使用者

##  文档目的

本文档针对 TKE for Alauda Container Platform v2.3 容器平台，提供部署、配置、验证、使用、巡检、排障等日常维护操作的指导。

## 修订记录

| 序号 | 修订日期   | 修订人 | 版本 | 修订内容 | 审核人 | 文档生效日期 |
| ---- | ---------- | ------ | ---- | -------- | ------ | ------------ |
| 1    | 2019-10-11 | 王洪涛 | v0.1 | 创建文档 |        | 2019-10-30   |
| 2    | 2019-11-5  | 王洪涛 | v0.1 | 增加排查日志|        |              |
| 3    | 2019-11-19 | 王洪涛 | v0.1.1|增加排查监控告警错误|        |              |
| 4    | 2019-11-21 | 王洪涛 | v0.1.2|增加修改 logo |        |              |
| 5    | 2019-12-6  | 王洪涛  | v0.1.3|增加删除 ns 卡住的解决方法|        |              |
| 6    |            |        |      |          |        |              |

<div STYLE="page-break-after: always;"></div>

# 平台部署、配置及验证

## 平台部署

请参考 《TKE for Alauda Container Platform v2.3 部署文档》说明部署平台。



## 平台功能验证

请参考 《TKE for Alauda Container Platform v2.3 部署文档》中 “平台配置及测试” 章节中的内容验证平台功能。

## 平台配置管理

### 对接 es

1. 执行以下命令行，删除 cpaas-elasticsearch。

	```
	helm delete --purge cpaas-elasticsearch # 删除 es
	helm get values dex >/tmp/values_install_es.yaml #获取变量
	vim /tmp/values_install_es.yaml  #编辑文件中，esHost 的值为要对接的 es 的值
	```

2. 参考以下命令创建 es 的 secret。

  ```
  cat <<EOF >/tmp/es_secret.yaml
  apiVersion: v1
  kind: Secret
  metadata:
    annotations:
      app.alauda.io/create-method: ""
      app.alauda.io/display-name: "Service es, created by chart"
      owners.alauda.io/info: '[]'
    labels:
      chart: "elasticsearch"
      app_name: cpaas-es
      service_name: cpaas-es
    name: cpaas-es
    namespace: alauda-system
  type: Opaque
  data:
    password: <base64 加密过的 es 的密码>
    username: <base64 加密过的 es 的用户名>
  EOF
  kubectl create -f /tmp/es_secret.yaml
  ```

3. 执行以下命令行进行升级。

	```
	helm upgrade dex stable/dex -f /tmp/values_install_es.yaml ； sleep 10
	helm upgrade alauda-base stable/alauda-base -f /tmp/values_install_es.yaml --force ； sleep 30
	helm upgrade -f /tmp/values_install_es.yaml alauda-container-platform stable/alauda-container-platform
	```

### 对接存储

参考以下命令及备注说明，对接存储。

====================k8s对接ceph集群==================================

```
###1、在准备对接的集群所有节点安装ceph客户端
yum install  ceph-common ceph-fuse  -y
  
###2、腾讯的ceph集群默认为none模式，没有开启认证,通过配置文件获得monitor节点的配置（去腾讯的ceph集群的某个data节点操作）
conf=$(ps -fe | grep CLUSTERMON.*.conf | grep ceph-mon | awk '{print $(NF-2)}')
cat $conf | grep mon_addr           ##获得monitor节点的ip以及端口
    mon_addr = 10.0.129.175:6789
    mon_addr = 10.0.129.8:6791
    mon_addr = 10.0.129.73:6790
  
###3、准备好cephfs压缩包，将压缩包拷贝到某台master机器的/root下，并解压.
这个压缩包在init节点的安装包路径下的other目录。/root/alauda-k8s/other/ceph/cephfs.zip
scp cephfs.zip  root@1.1.1.1:/root/   ##需要将1.1.1.1换为业务集群的某台master机器的ip地址。
  
去刚传完文件的master的root下执行以下操作、
mv cephfs /tmp        ###为了防止当前目录下存在同名目录
unzip cephfs.zip
cd cephfs/
###将deployment.yaml中的image地址更换为自己环境的image地址（例如：10.0.128.173:60080/claas/cephfs-provisioner:latest）
kubectl create ns cephfs
NAMESPACE=cephfs
sed -r -i "s/namespace: [^ ]+/namespace: $NAMESPACE/g" /root/cephfs/*.yaml
sed -r -i "N;s/(name: PROVISIONER_SECRET_NAMESPACE.*\n[[:space:]]*)value:.*/\1value: $NAMESPACE/" /root/cephfs/deployment.yaml
kubectl apply -f /root/cephfs -n $NAMESPACE
  
  
###4、创建storageclass 
cat <<EOF > cephfs-storageclass.yaml
kind: StorageClass
apiVersion: storage.k8s.io/v1
metadata:
  name: cephfs
  selfLink: /apis/storage.k8s.io/v1/storageclasses/cephfs
  uid: 198bcd73-a1ff-11e8-aafa-5254004c757f
  resourceVersion: '11786274'
  annotations:
    ceph.sample: '{"kind":"PersistentVolumeClaim","apiVersion":"v1","metadata":{"name":"claim1","namespace":"sock-shop","selfLink":"/api/v1/namespaces/sock-shop/persistentvolumeclaims/claim1","uid":"7033b9fc-a213-11e8-aafa-5254004c757f","resourceVersion":"7437570","creationTimestamp":"2018-08-17T11:48:20Z","annotations":{"pv.kubernetes.io/bind-completed":"yes","pv.kubernetes.io/bound-by-controller":"yes","volume.beta.kubernetes.io/storage-class":"cephfs","volume.beta.kubernetes.io/storage-provisioner":"ceph.com/cephfs"},"finalizers":["kubernetes.io/pvc-protection"]},"spec":{"accessModes":["ReadWriteMany"],"resources":{"requests":{"storage":"1Gi"}},"volumeName":"pvc-7033b9fc-a213-11e8-aafa-5254004c757f","volumeMode":"Filesystem"},"status":{"phase":"Bound","accessModes":["ReadWriteMany"],"capacity":{"storage":"1Gi"}}}'
    ceph.type: cephfs
    kubectl.kubernetes.io/last-applied-configuration: |
      {"apiVersion":"storage.k8s.io/v1","kind":"StorageClass","metadata":{"annotations":{},"name":"cephfs","namespace":""},"parameters":{"adminId":"admin","adminSecretName":"ceph-admin-secret","adminSecretNamespace":"kube-system","claimRoot":"/volumes/kubernetes","monitors":"10.0.129.175:6789,10.0.129.8:6791,10.0.129.73:6790"},"provisioner":"ceph.com/cephfs","reclaimPolicy":"Delete","volumeBindingMode":"Immediate"}
provisioner: ceph.com/cephfs                             ## 上一行的ip+端口需要修改为第2步获得ceph集群的monitors的IP地址和端口
parameters:
  adminId: admin
  adminSecretName: cephfs-admin-secret
  adminSecretNamespace: kube-system
  claimRoot: /volumes/kubernetes
  monitors: '10.0.129.175:6789,10.0.129.8:6791,10.0.129.73:6790'         ## 需要修改为第2步获得ceph集群的monitors的IP地址和端口
reclaimPolicy: Delete
volumeBindingMode: Immediate
EOF
  
  
kubectl create -f cephfs-storageclass.yaml
  
  
  
###5、定义管理员密码
echo "AQCYfAxdbXTwCRAAnz9MvJgO3KselABH2OoKOA==" > /tmp/secret 
kubectl create secret generic cephfs-admin-secret --from-file=/tmp/secret --namespace=kube-system
kubectl create secret generic cephfs-secret-user --from-file=/tmp/secret
  
  
###6、创建pvc验证
  
  
cat <<EOF > cephfs.yaml
  
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: claim1
  namespace: default
  annotations:
    volume.beta.kubernetes.io/storage-class: cephfs
    volume.beta.kubernetes.io/storage-provisioner: ceph.com/cephfs
  finalizers:
    - kubernetes.io/pvc-protection
spec:
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 1Gi
  volumeMode: Filesystem
EOF
  
kubectl create -f cephfs.yaml
  
  
kubectl get pvc      ###查看刚创建的pvc为bound状态即成功
NAME     STATUS   VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS   AGE
claim1   Bound    pvc-b480aa29-a142-11e9-9d43-525400d63265   1Gi        RWX            cephfs         19m 
```

### 配置es ttl

1. 在 global 的 master 上，执行如下命令：

	```
	kubectl edit deployment cpaas-elasticsearch -n alauda-system
	```

2. 在当前版本中，支持配置以下变量：

	```
	ALAUDA_AUDIT_TTL=30d
	ALAUDA_EVENT_TTL=30d
	ALAUDA_ES_TTL=7d
	```

	上述配置表示：平台会保留 30 天的审计索引记录、30 天的事件索引记录、7 天的日志索引记录。请根据业务的实际需要修改对应的值即可。

### 更改 es 数据存储位置

1. 在 global 的 master 上，执行如下命令：

	```
	kubectl edit deployment cpaas-elasticsearch -n alauda-system
	```

2. 将挂载卷的文字改成其他目录。

### 增加 es 节点数量

**说明**：扩容后，主机数量尽量为奇数。

1. 查看 es pod 的 nodeSelector 。

   一般为：log: true。
   
   查看方法：
   
   `kubectl edit deployment cpaas-elasticsearch -n alauda-system`
   
   然后搜索：nodeSelector。

2. 给希望扩容的节点添加上述 Label。例如：给 node 添加如下标签：

	```
	kubectl label nodes <node-name> <label-key>=<label-value>
	比如：
	kubectl label nodes node1 log=true
	```

3. 修改 deployment。

   1. 获取新打 label 的 node 节点 IP

   	将该ip拼接到 `env:ALAUDA_ES_CLUSTERS` 后面，以逗号分割。

   2. 修改 replicas 数量为 5 即可扩充 es 集群。



### 更改 global 组件资源限制

* 方式 1

	1. 执行下面的命令找到 global 组件的名字。
	
		```
		kubectl get deploy -n alauda-system
		```
	
	2. 执行下面的命令，编辑组件资源。
	
		```
		kubectl edit deployment <组件名> -n alauda-system
		```

* 方式 2

	参考用户手册，在界面上，选择 global 集群，找到要调整的组件，调整资源。

### 更改 global 组件实例数量

* 方式 1

	global 组件默认加了反亲和，所以 global 组件最大的实例数不能超过 global 集群内，打了 `global=true` 这个标签的 node 的数量，首先扩充节点数量，再更改实例数，方式如下：

	```
	kubectl label nodes node1 global=true
	```

	1. 给 node1 这个节点打上标签，然后执行下面的命令找到 global 组件的名字。

		```
		kubectl get deploy -n alauda-system
		```

	2. 执行下面的命令扩容。

		```
		kubectl scale deployment -n alauda-system <要扩容的 global 组件名> —replicas=<要扩容的实例的数量>
		```

* 方式 2

	参考用户手册，在 UI 界面上，选择 global 集群，找到要扩容的组件，扩容实例。
	
### 替换登录页 Logo 和背景图片

支持用户替换登录页的 Logo 和背景图片。


**准备工作**

* 请提前准备好需要替换的 Logo 和背景图片。

	![](images/faqlogo.png?classes=big)
	
	
	平台默认的  Logo 和背景图片的格式及大小请参见下表。
	
	|  类型   |  名称   |  格式  |   大小（像素）    |
	|   ---- |  ----- | ----- | ------- |
	| 登录页左上角 Logo   | dex-logo   |   svg |  40\*60  |
	| 登录页背景图片  |  login-bg-logo  |   svg |  544*500  |

* 下载 themectl 文件。

	**提示**：在 global 集群的任意 Master 节点服务器上执行命令 `uname -s` 可查看服务器的操作系统，根据操作系统类型下载相应的 themectl 文件，单击以下链接可直接下载。
	
	* [Darwin](https://s3.cn-north-1.amazonaws.com.cn/get.alauda.cn/tool/themectl_darwin)
	
	* [Linux](https://s3.cn-north-1.amazonaws.com.cn/get.alauda.cn/tool/themectl_linux)
	
	**注意**：下载完成后，需要在服务器上执行 `chmod +x themectl` 命令，为该文件添加执行权限。

**操作步骤**

执行以下命令行单独更新/创建 Logo、背景图片配置字典。

```
./themectl update --kube-config ~/.kube/config --namespace alauda-system --configmap-name <aks-logo> --from-file=./aks-logo.svg
```

**说明**：命令行中的 <aks-logo> 需要替换为配置字典名称。配置字典名称对应关系请参见下表。

|  配置字典名称   |  类型   |  
|  ------- |  ------- | 
| dex-logo  | 登录页左上角 Logo |  
| login-bg-logo   |  登录页背景图片  |
| favicon-logo | 浏览器logo|  


	
<div STYLE="page-break-after: always;"></div>

# 平台使用

请参考 《TKE for Alauda Container Platform v2.3 用户手册》的内容了解平台的使用方法。

<div STYLE="page-break-after: always;"></div>

# 巡检

## 巡检平台功能

### 登录

![](images/00logoin1.png)

根据账号类型，选择合适的方式登录平台。

### UI 界面

打开浏览器，访问平台 UI。

![](images/view.png)

### 产品切换

管理员登录后，单击左上角，在各个产品间切换。


### 视图切换

分别在每个产品的 UI 页面上，单击右上角开关，进行管理视图和业务视图的切换。

#### 管理视图

在管理视图，当操作的资源和集群相关时，可灵活切换集群。

![](images/adminview1.png)

#### 业务视图

在业务视图，可灵活切换项目和命名空间。

![](images/userview.png)

### 集群状态

进入 Container Platform 产品的 UI，单击右上角，切换到管理视图。单击左侧导航栏中的集群，查看集群信息。

![](images/cluster.png)

单击集群右上角 ![](images/003point.png) 并选择集群管理，将跳转至集群管理页面。

![](images/clusterma.png)

单击集群详细页面，单击左上角基本信息、监控、项目。单击右上角操作下拉菜单，单击各个按钮检查是否正常。

![](images/clusteract.png)

### 日志

在管理视图，单击左侧导航栏中的日志，查看是否有日志信息。

在日志搜索框选择或输入查询条件，查询日志，例如：`cluster:global` 就是搜索 global 集群的日志。查询语句里，前面是条件，支持 cluster、node、application、path、source、project、instance ，后面是变量，根据集群实际情况填写。

![](images/log.png)



### 监控

在管理视图，单击左侧导航栏中的监控面板，查看不同集群监控情况。

可通过顶部导航栏中的集群切换入口，在不同集群的监控面板间进行切换。

![](images/monitor.png)

### 事件

在管理视图，单击左侧导航栏中的事件，查看不同集群的事件。

可通过顶部导航栏中的集群切换入口，在不同集群的监控面板间进行切换。

![](images/event.png)

### 告警

在管理视图，单击左侧导航栏中的告警，查看不同集群的告警配置。

可通过顶部导航栏中的集群切换入口，在不同集群的监控面板间进行切换。

![](images/alarm.png)

### 通知

在管理视图，单击左侧导航栏中的通知，单击各个菜单巡检功能是否正常。

![](images/notification.png)

### 存储

在管理视图，单击左侧导航栏中的存储，查看不同集群的存储配置。

可通过顶部导航栏中的集群切换入口，在不同集群的监控面板间进行切换。

![](images/pv.png)

### 网络

在管理视图，单击左侧导航栏中的网络，查看不同集群的负载均衡、子网、域名。

可通过顶部导航栏中的集群切换入口，在不同集群的监控面板间进行切换。

![](images/net.png)

### 项目

切换到业务视图，选择不同集群、选择不同项目。

![](images/project.png)

单击右侧进入按钮，进入项目的命名空间后，查看命名空间中的各项资源。

![](images/userview.png)

## 巡检 Kubernetes 集群

### k8sKubernetes 节点状态

#### 巡检节点 ssh 登录及网络

1. ping 各个节点的 ip。

2. 尝试 ssh 登录到各个节点。

#### 巡检节点时间

在各个节点执行data 这个命令，对比各个节点时间是否又差异。

#### 巡检节点磁盘

执行 `df -hT | awk -F '[ %]+' '{if ($6 > 80) print $0}'` 找到使用率大于80% 的磁盘。

#### 巡检节点域名解析

在各个节点执行命令 nslookup <平台域名>。

#### 巡检节点资源

在集群的 master 上执行 `kubectl get no` 找到节点名字，然后执行 `kubectl describe no <node name>` 查看节点详细信息。


### Kubernetes cluster 状态

1. 在 master 节点执行 `kubectl get no` 查看 node 状态。

2. 在 master 节点执行 `kubectl get cs` 查看集群状况。

### Kubernetes pod 状态

在 master 节点执行 `kubectl get pod --all-namespaces | awk -F '[ /]+' '{if ($3!=$4 || $5!="Running") print $0}'` 找到状态不正常的 pod。

### Kubernetes api server

执行 `curl -k https://<k8s api lb ip>:6443` 查看 api 的返回。

## 巡检 init 节点

### pkg-registry

在 init 节点执行 `docker exec pkg-registry ls` 检查容器是否正常。

在 init 节点执行 `curl 127.0.0.1:60080/v2/_catalog | jq` 查看功能是否正常。

### chart repo

在 init 节点执行 `docker exec chart-repo ls` 检查容器是否正常。

在 init 节点执行 `curl $(cat /cpaas/install_info | awk -F '=' '/^CHART_ENDPOINT/{print $2}' | sed 's#/$##')/api/charts | jq` 查看功能是否正常。

### 软件源

在 init 节点执行 `docker exec yum ls` 检查容器是否正常。

在 init 节点执行 `curl 127.0.0.1:7000` 查看功能是否正常。

## 常用巡检命令

### Kubernetes 常用命令

* 列出所有 pod

	`\# kubectl get pods --all-namespaces`

* 查看某个 pod

	`kubectl describe deployment <deployment-name> --namespace=<namespace_name>`

* 列出所有 deployment

	`kubectl get deployment --all-namespaces`

* 列出所有 jobs

	`kubectl get jobs --all-namespaces`

* 找到要查看的 job-name，然后查看该 job 的详情

	`kubectl describe job <job-name> --namespace=<namespace_name>`

* 列出所有 endpoint

	`kubectl get endpoint --all-namespaces`

* 列出所有的 namespaces

	`kubectl get ns`

* 列出所有的 service 

	`kubectl get svc --all-namespaces`

* 列出所有的 ingress

	`kubectl get ingress --all-namespaces`


### docker 常用命令

* 查看镜像

	`docker images`

* 查看所有容器

	`docker ps -a`

* 进入 docker 容器

	`docker exec <容器名> bash`

* 启动 docker 容器

	`docker start <name> `

* 停止 docker 容器

	`docker stop <name>`

* 把容器提交为镜像

	`docker commit <容器id> <镜像名>`

* 使用 Dockerfile 构建镜像

	`docker build -t <name:tag>` 

* 拉取镜像

	`docker pull <name>`

* 将一个已有的镜像

  `docker tag <老镜像名> <新镜像名>`

* push 镜像

  `docker push <镜像名>`

* 重启 docker

  `systemctl restart docker.service`

* 停止 docker

  `systemctl stop docker.service`

* 启动 docker

  `systemctl start docker.service`

* 启动一个容器

  `docker run -d -v /[docker_test:/yufei](http://docker_test/yufei) --name test centos`

* 强行删除容器

  `docker rm -f <容器名>`

* 强行删除所有容器

  `docker rm -f $(docker ps -qa) `

* 删除镜像

  `docker rmi <镜像名>`

* -i：允许我们对容器内的 (STDIN) 进行交互。

* -t：在新容器内指定一个伪终端或终端。

* -v：是挂在宿机目录， /docker_test是宿机目录，/yufei是当前docker容器的目录，宿机目录必须是绝对的。

* —name：是给容器起一个名字，可省略，省略的话docker会随机产生一个名字。

###  系统常用命令

* `date`：查看时间

* `ping <ip>`： 联通测试

* `nslookup`：域名解析

* `curl`：利用 URL 规则在命令行下工作的文件传输工具

* `netstat`：显示网络连接，路由表，接口状态，伪装连接，网络链路信息和组播成员组

* `nc`：同上

* `top`：查看系统负载

* `vmstat`：同上

* `du`：查看目录大小

* `df`：查看磁盘

* `free`：查看内存

<div STYLE="page-break-after: always;"></div>

# 平台备份

## init 节点

备份 “/cpaas” 目录和安装目录。

##  Kubernetes 集群

### etcd 备份

将安装目录下的 “backup_recovery.sh” 脚本拷贝到每一台 “master 的/root” 目录下，然后执行：

```
echo '1 3 * * * bash /root/backup_recovery.sh run_function=back_k8s_file


30 3 * * * find /cpaas/backup -mtime +30 -name "*.log" -exec rm -rf {} \;' | crontab
```

会添加计划任务，每天 3:01 备份 etcd 数据，3:30 删除 30 天前的备份。

**注意**： 上述命令会将原来的 crontab 全部删掉，建议使用 `crontab -e` 命令添加。

### global es 数据

默认不备份日志数据，如果客户有这样的需求，建议采用扩充 es 集群的节点数量，然后将副本数提高的方式备份，方式如下：

参考 **增加 es 节点数量**，然后修改 ***ALAUDA\_ES\_REPLICA*** 变量的值，默认是 1 副本，扩容节点数量后，可以扩到 2 或更多的副本数。

### 监控数据

默认不备份监控数据，如果客户有这样的需求，将每一个业务服务集群上，普罗米修斯运行的节点上的普罗米修斯挂卷的目录备份即可。


<div STYLE="page-break-after: always;"></div>

# 常见错误

## acp 无日志

<img src="images/日志排查流程.jpg" style="zoom:50%;" />

**排查技巧**：

* 确定日志是写错误还是读错误；

* 确定日志故障在服务端还是客户端。

范例：

* kafka 未正常工作，tiny 无法传送日志。

	查看 nevermore 日志有 500 报错，500 错误多为 kafka 错误	
	重启 kafka 容器后 访问正常。

* nevermore 显示 204 状态。

	nevermore 未正常工作，无法传送日志。

	常见错误 ：有些容器有日志， 有些容器无日志。可判断服务端日志组件应该正常。客户端日志组件出问题可能性比较大。

Nevermore如果是没有数据或者丢数据，查看nevermore的日志

tail -f /var/log/td-agent/td-agent.log，如果出现错误日志，表示服务端（tiny）有问题，需要检查服务端程序。如tiny 写入kafka失败

ruby 的问题话，其中，如果想调试nevermore 的话:

* 可以启动nevermore 容器，注释掉，run.sh 最后一句话，
* 然后执行run.sh 以后
* 手工启动，/opt/td-agent/embedded/bin/ruby -Eascii-8bit:ascii-8bit /usr/sbin/td-agent --daemon /var/run/td-agent/td-agent.pid --under-supervisor


## 有 pod 无法启动

1. 执行 `kubectl logs <pod-name>` 查看 pod 日志。

2. 执行 `kubectl get pod` ，找到不正常的 pod 名字，并执行 `kubectl describe pod <pod-name>` 查看错误信息。

3. 执行 `journalctl -u kubelet` 查看启动 pod 时的报错。

4. 执行 `df` ，检查磁盘用量是否超过 80%。

5. 执行 `journalctl -u kubelet` 查看日志，查看是否触发了 Kubernetes 的 gc。

## etcd 无法启动

执行 `journalctl -u kubelet` 看 kubelet 创建容器的日志。

## kubectl 命令提示无法连接 kube-api

* 如果是高可用集群，首先检查 kube-api 的 lb。

* 如果是非高可用集群，去集群的 master 上执行 `docker ps -a | grep api` 查看 api 容器，然后执行 `docker logs -f <kubeapi 容器名字>` 查看容器日志。

执行 `journalctl -u kubelet` 查看 kubelet 创建容器的信息。

## 启动 pod 出错，提示无法 pull 镜像

首先，在集群内，找到把 pod 调度到的那个 node ，在上面执行 `docker pull <镜像>` 查看是否能 pull 下来。

如果可以，检查这个镜像仓库是否需要 login，如果需要 login，就需要给 Kubernetes 添加一个secrets。

kubernetes pull 镜像用的是自己存储的 user/password，这个在部署的时候一般都配置好了。默认只配置了 default 这个 namespace(kubernetes的)。如果服务是属于其他的 namespace，在创建服务的时候如果 namespace 不存在会提前创建，并从 default 同步 pull 镜像的认证信息。但偶尔会因为各种原因导致 pull 镜像信息同步或者配置出错，导致 pull 镜像失败。

服务所属 namespace 的规则是 `<app_name>--<space_name>`, 不存在的为 default，两者都不存在时 `namespace=default`。

正常情况下：每一个 registry 在 kubernets 的每一个 namespace 下都有一个 secret 记录，每一个 namespace 有一个叫 default 的 serviceaccount，里面列出了本 namespace 所有的可用的 imagePullSecrets，每个服务创建时都会这里找相应的登录认证信息。

可能的原因及解决方法：

搭建时配置的 user/password 不对或者权限不对: 在所有已经存在的 namespace 里删除原有 secret 并重新添加。

其他 namespace 同步 default 的信息没有同步成功：也就是说 default 里面的信息是正确的。简单的处理方式是把出问题的 namespace 删掉(里面没有服务或者服务很少或者没有重要的服务)，然后重试服务，它会自动重建 namespace，一般就成功了。

服务属于 app 或者某个 space 时，简单的解决方式是换个app名字或者 space，还是不行的话参考以下步骤。

**排查步骤**：

查看 default namespace下是否有相应的 secret。

```
kubectl get secrets  # 获取到对应registry的secret name
kubectl get sa default -o yaml |grep <secret-name>
```

如果没有，则说明部署配置有问题。

如果已经有其他服务用同样 registry 的镜像部署成功，则说明配置的 username/password 是正确的，但也有可能是部分镜像没权限。也有可能是 username/password 配置的不对导致登录不了。这两种情况出现较少，如果觉得没问题可以跳过。如果不确定可以检查下：

```
kubectl get secret <secret-name> -o yaml
echo "<base64_encoded_data>" | base64 -d
docker login
docker pull
```

获取出问题的服务所在的 namespace: <app_name>--<space-name>。没有相应值的用 default 填充，两者都没有的话 `namespace=default`。


查看服务所在 namespace 下的 iamge pull secrets 是否正常。

```
kubectl get secrets -n <namespace> # 看是否存在相应的secrets
kubectl get sa default -n <namespace> -o yaml |grep <secret_name> # 查看是否配置了secrets
```

如果上面两步有问题，则说明从 default 同步有问题。如果这个 namespace 下服务只有这一个或者很少不重要可以删除，那么可以删除这个 namespace，然后重试服务：


`kubectl delete namespace `

如果服务很多不能删除 namespace，则可以创建缺失的 secrets 并且确保其存在于 serviceaccount 中：

```
# if secrets not exist, recreate
kubectl create secret docker-registry <pull-image-secret-name> --docker-server=DOCKER_REGISTRY_SERVER --docker-username=DOCKER_USER --docker-password=DOCKER_PASSWORD --docker-email=DOCKER_EMAIL
 
kubectl get serviceaccounts default -o yaml > ./sa.yaml
 
vi sa.yaml
# 在文件添加以下内容
imagePullSecrets:
- name: <pull-image-secret-name> # 以后每次创建一个secret，在这个列表加一行类似的
  
kubectl replace serviceaccount default -f ./sa.yaml
```

以上操作知识针对 default namespace，可以确保以后新创建的 namespace 基本不出问题。对于已经存在的 namespace，也要做这些步骤。

```
kubectl create secret .... -n <namespace>
kubectl get sa default -n <namespace> -o yaml > ./sa.yaml
...
  
kubectl replace sa default -n <namespace> -f ./sa.yaml
```

## 磁盘空间不足

**故障现象**

创建服务或应用不成功，容器一直处于 createing 状态。

**可能的原因**

* 通过df 命令查看可用空间。如果磁盘空间还有很多，通过 `df –i` 命令查看 inode。

* 机器上是否还有别的更大的磁盘，部署的时候没有使用。

* 查找 docker 目录下是否哪个目录下有大的数据文件，有可能是某个容器内部产生的数据。

* “/var/log” 目录下是否有大的磁盘文。

* 查找是否机器上还有别的进程或容器产生了大量日志，这些进程通过其它方式运行在本机上，不受平台的管辖。

**解决方法**

* 如果是磁盘空间已满，需要先删除不用的数据，可以运行 `python /alauda/gc.py` 命令来清除日志、非运行状态的容器及不被使用的镜像。

* 如果用户使用的磁盘较小，需要更换磁盘。

* 如果有某个容器内产生了大量的数据或日志，可以根据业务的需求决定是否清除。

* 清理 “/var/log/” 下的系统的日志。

* 停掉所有非平台管辖的进程或容器。

**验证方法**

* `docker run` 命令创建容器，错误信息清除。

* 查看 df，磁盘使用率已经小于100%。

## CPU 或内存资源不足

**故障现象**

创建服务或服务更新不成功，事件显示资源不足。

**可能的原因**

* 查看集群详情页面，看集群的cpu或者内存的使用率是否还有剩余。

* 集群中是否有打特殊tag的主机存在使得 cpu、mem 还有剩余，但是容器可运行的主机已不足。

* 查看服务的容器实例大小是否合适，判断运行容器内的进程是否需要更多的 cpu 或内存资源。

* 登录到 slave 每台虚拟机上，查看是否有通过其它方式运行的进程或容器占用了系统资源。

* 有之前未删除的容器在主机节点上运行。

**解决方法**

* 如果是机器资源不足，可以往集群中添加新的 slave。

* 如果因为特殊的 tag 存在使得，某个服务可运行的机器不足，但整体集群由富余的资源，则可以调整 tag 的分布。

* 如果服务需要更多的资源才能运行，则可以更改服务实例大小后，更新服务。

* 如果有其它进程或容器占用了主机资源，请删除它们。

**验证方法**

重新启动服务，如果可以运行表示问题已解决。

## 无法下载镜像

**故障现象**

创建服务或应用不成功，POD 状态显示 pullimageserror。

**可能的原因**

部署服务或应用的主机 docker 配置文件 “/etc/docker/daemon.json” 中没有镜像仓库地址。

**解决方法**

在主机节点上编辑 “/etc/docker/daemon.json” 配置文件。

1. 在 "insecure-registries" 字段增加镜像仓库的地址，格式参考下面的示例：

```
"insecure-registries": [

"10.1.0.23:5000",

"172.18.0.4:60080"

]，
```


2. 执行 `systemctl daemon-reload && systemctl start` 命令。

**验证方法**

`docker info` 查看最后几行，在 Insecure Registries 这部分，确认镜像仓库地址存在其中。


## 创建服务失败或服务无法访问

**故障现象**

创建服务或更新服务时，报错。服务的实例数为 0。

**可能的原因**

* 查看集群详情页，确认 cpu 和 mem 的使用率不高。有时即使还有一定的资源但是分散到每一台节点的资源其实已经不多了，这个时候，如果容器的大小较大，有可能造成任何一台机器都无法满足容器的需求。

* 无法下载指定的镜像，一般这种情况下是因为计算节点和 registry 的网络不通，或者权限不够。

* 容器会运行起来，但是过一会儿就自己退出，这个可能是因为容器内的应用程序有错误。

* 用户设置了健康检查，且健康检查的参数设置的不合理，导致服务时钟不能进入运行状态，最终因为超时被平台删掉。

* 镜像太大导致下载镜像非常慢。

* 服务已经创建成功，但是 Nginx 配置错误，导致容器无法访问。

**解决方法**

* 如果集群的计算节点不足，需要增加新的计算节点。或者将容器大小调小，使得可以跑在其中的一个计算节点上。

* 如果是镜像无法 pull 下来，需要检查网络、权限。

* 容器自己退出的解决办法是查找程序退出的原因，修改代码或者 Dockerfile。

* 因为健康检查设置导致的问题，需要调整参数，启动预估时间不应该太长，健康检查的 url 应该是正确的。

* 因为镜像太大，或者 pull 的较慢导致的问题，需要对镜像做优化，如：减小容器大小。

* 参考 **平台配置管理** 内容进行排查，确定 alb-nginx 的配置是否正确。

**验证方法**

再次创建服务成功。


## kubelet 日志报错：（journalctl -fu kubelet）

Kubernetes 出现故障，kubelet 无法启动。查看 kubelet 日志，日志报错显示 “Running with swap on is note supported ,please disable swap! or set --fail-swap-on flag to flase.”。

根本解决方法：

关闭 swap 分区：`swapoff -a`

**注意**：

kubelet 为什么关闭 swap？

* kubernetes 的想法是将实例紧密包装到尽可能接近 100％。 所有的部署应该与 CPU /内存限制固定在一起。 所以如果调度程序发送一个 pod 到一台机器，它不应该使用交换。 设计者不想交换，因为它会减慢速度。

* 关闭 swap 主要是为了性能考虑。

* 为了一些节省资源的场景，比如运行容器数量较多，可添加 kubelet 参数 `--fail-swap-on=false` 来解决。

* 所以需要关闭 swap 分区。

	
<div STYLE="page-break-after: always;"></div>	

## 没有监控

监控信息为空，一般是morgans获取的数据为空，或者请求过程出错。

排查Morgans请求。 监控信息由morgans 的query或者query_range请求获取，查看该请求的状态。用这个请求的 Request Id到Morgans pods的日志(/var/lib/mathilde/morgans.log)去过滤该id，下图是获取 id 的方法。

<img src="images/监控获取 id 方法.png" style="zoom:50%;" />

查看与id相关的日志，有无错误日志输出。可通过改变morgans日志级别（如设置LOG_LEVEL=10），查看更详细日志。

如日志有错，则根据错误日志分析。

如日志无错，请求正常，则可以根据该请求日志中解析出来的查询表达式“Query string”去相应集群的Proemtheus查询该表达式。可分解出该表达式中的基础Metric，依次查询哪些Metric不存在，确定问题。

## 告警实际值明显大于阈值，但告警状态显示正常

告警状态是从Prometheus Server评估的。可查看对应的Prometheus Server中该告警的状态。示例如下：

<img src="images/告警状态.png" style="zoom:50%;" />

上图即是查看 位于alauda-system ns的proemtheusrule cpaas-agon-rules的同步过来的告警，每个告警的详情可以查看到。可查看对应告警规则的expr是否正确，expr中metric的采集情况。

若对应Prometheus Server中没有该告警，则有可能是operator未能将Prometheusrule同步到Prometheus Server：

      可排查operator日志；

      可排查Prometheusrule的label是否匹配Prometheus的ruleNamespaceSelector和ruleSelector。


## 告警触发了，但没有收到通知

这个告警设置通知了吗？没有设定通知，不会发送通知呢

告警是prometheus server是通过alertmanager server转发给Morgans。看下Promethues server/Alertmanager server的日志，有没有错误。有错误排查错误；很大部分是alertmanager没有转发出去。

如果prometheus/alertmanager没问题，那就再看下Morgans的日志(pod的/var/lib/mathilde/morgans.log)，过滤下"router"；依最新的router请求的request_id去过滤日志，查看整个的请求过程。如下图所示：

<img src="images/通知.png" style="zoom:50%;" />

ACP2.2+上Courier处理了Morgans产生的NotificationMessage发送通知，可以查看这个NotificationMessagee的status字段，可以看到发送状态；

ACP2.0/2.1中，Morgans直接发送通知，可查看日志；

ACE中，Morgans将通知转发Lucifer，Lucifer具体发送通知，可查看Lucifer 日志。

## 删除 namespaces 但是长时间 ns 状态处在 Terminating 状态

k8s 的已知 bug，暂时没有找到原因，官方给出了解决办法

https://success.docker.com/article/kubernetes-namespace-stuck-in-terminating

可以在 master 上运行下面的命令删除所有 Terminating 状态的 ns，*** 注意：危险的命令，执行前一定要备份 etcd，并且审慎执行，错误删除后果很严重***  命令如下：

```
for ns in $(kubectl get ns | grep Terminating | awk '{print $1}')
do
    kubectl get ns $ns -o json | jq 'del(.spec.finalizers)' | curl -v -H "Content-Type: application/json" -X PUT --data-binary @- http://127.0.0.1:8080/api/v1/namespaces/$ns/finalize
done
```


#  应急预案

## 目的

本文档是在平台或平台管理的客户的业务服务遭遇突发事件，造成客户无法使用平台管理自己的业务服务，或者客户的业务服务无法完整的提供预订的功能，平台管理员紧急响应及恢复工作的指导文档。包括如下内容：

* 定义需要应急的突发事件

* 定义人员角色和职责

* 给出故障源快速定位方法

* 针对不同故障，给出应急启动、执行、恢复等流程、步骤和操作方案

## 适用范围

本文档适用于下面所述的故障应急

* 容器云平台。

* 容器云平台管理的客户的业务服务集群。

* 运行在容器云平台管理的业务服务集群上的，客户的业务服务。

## 紧急情况定义及处理方案

故障影响范围和故障严重程度定义请参见下表。

| 故障影响范围         | 故障严重程度                     | 分数 |
| ----------------- | -------------------------------- | ---- |
| 平台 logo 等非功能部分客户某个多实例的业务服务中的部分实例 | 单次不再重现的告警信息           | 1    |
| 平台次要功能模块或客户某个业务服务                         | 持续告警                         | 2    |
| 平台主要功能模块或客户除核心业务外大部分业务服务           | 单次不再重现的预订的功能的错误   | 3    |
| 平台全部功能模块或客户核心业务服务                         | 部分功能错误，且不能自动恢复     | 4    |
| 平台不可访问或客户所有业务彻底中断                         | 全部预订功能错误，且不能自动恢复 | 5    |

故障级别=故障影响范围×故障严重程度请参见下表。

| 分数    | 故障级别                     | 通知范围                       | 操作                                                         |
| ------- | ---------------------------- | ------------------------------ | ------------------------------------------------------------ |
| 1-5分   | 需要记录                     | 无                             | 记录相关信息，并知会接班同事                                 |
| 6-10分  | 需要检查并记录               | 值班领导                       | 记录相关信息，并检查平台和服务                               |
| 11-15分 | 马上发出警告并处理           | 上一级要通知的人及运维部门领导 | 记录并检查平台和服务，确认紧急情况发生按照应急预案独自处理   |
| 16-20分 | 马上发出警告，并立即呼叫支援 | 上一级要通知的人及业务部门领导 | 执行上一步操作，并打电话寻求支持                             |
| 21-25分 | 最高级                       | 上一级要通知的人及公司领导     | 记录并检查平台和服务，确认紧急情况发生立即拉相关人员召开电话会议，并按照预案处理 |

## 人员角色及职责

| 序号 | 人员角色         | 隶属 | 职责                                   | 备注 |
| ---- | ---------------- | ---- | -------------------------------------- | ---- |
| 1    | 平台管理员       |      | 容器云平台管理                         |      |
| 2    | 业务服务运维     |      | 业务服务上线、调试、维护等日常运维人员 |      |
| 3    | 现场值班人员     |      | 业务和平台报障值班人员                 |      |
| 4    | 支援中心值班人员 |      | 24×7                                   |      |
| 5    | 业务服务开发     |      | 编写客户业务服务代码的开发人员         |      |
| 6    | 平台服务支持     |      | 容器云平台 24×7 服务支持               |      |
| 7    | 平台开发         |      | 容器云平台代码开发人员                 |      |

## 故障原因快速定位方法

### 平台不可访问

按照 LB、网络、域名解析、Kubernetes 集群、服务器、chart 状态、pod 状态等顺序排查。

### 平台上看不到业务服务

按照业务服务的 Kubernetes 集群状态、global 平台创建服务是否成功、网络等顺序排查。

### 平台上看不到业务服务集群

global 集群执行 `kubectl get clusters -n alauda-system` ，确认是否删除集群。

### 业务服务功能不正常

查看业务服务 pod 状态、网络、 LB 顺序排查。

### 业务服务不可访问

按照 LB、域名解析、网络、pod 状态、pod 内进程状态等顺序排查。

## 应急操作手册

应急操作手册没有统一的步骤，需要根据不同的环境、不同的公司组织、不同的业务情况针对性的制订，下面给出的几个例子是比较常见且应急操作使用大部分环境的示例。

### 服务器故障造成某些定点部署的服务宕掉、集群资源不足，某些 pod 无法重新调度启动成功、造成业务服务中断

1. 确认故障级别，10 分以上开始处理。

2. 尝试登录故障服务器，如果不能登录转到 步骤 4。

3. 尝试启动 docker 和 kubelet ，恢复 Kubernetes node，如果不成功转到下一步。

4. 联系 iaas 管理员，尝试快速添加服务器扩容节点，如果是定点部署的服务或无法快速提供服务器转到下一步。

5. get pod，列出多实例的且负载不高的 pod，缩小实例，如果没有不成功转到下一步。

6. 列出重要程度低于这个服务的服务，关闭这些服务腾退资源，如果不成功进入容灾处理流程，切换到容灾的环境内。

7. 如果没有容灾环境，宣布处理失败。

### 域名解析故障造成业务服务无法访问

1. 紧急联系 dns 管理员，恢复 dns 服务器，如果不成功转到下一步。

2. 如果访问业务的的客户数量少，可以将域名解析写入 “/etc/hosts” 中。

3. 如果很多且不特定的客户访问业务，宣布处理失败。

### lb 故障造成 Kubernetes 集群或业务服务无法访问

1. 紧急联系网络管理员修复 lb，如果不成功转到下一步。

2. 和网络管理员确认 VIP 是否可以手动配置到某台服务器上，如果可以转入下一步，如果不行，转到步骤 4。

3. 在某个服务器上，手动启动 haproxy，并将 VIP 配置到这个服务器上，建议其他写好 haproxy.cfg。

4. 宣布处理失败。

### 新建的 pod 起不来，造成创建新的服务或更新老服务失败

1. 确认 pod 起不来的原因是资源不够问题还是 pod 本身问题，如果是资源问题，请参考 **服务器故障造成某些定点部署的服务宕掉、集群资源不足，某些 pod 无法重新调度启动成功、造成业务服务中断** 的步骤 4。

2. 如果 pod 配置错误，修改配置重启启动，如果部署转到下一步。

3. 在某台服务器上，用 docker run 的方式尝试用相同的镜像启动容器，如果不成功那就是容器构建问题，通知研发、测试，然后转到 步骤 5。

4. 在 Kubernetes 集群上，尝试用 yaml 的方式创建服务，如果成功那就是平台问题，通知平台管理员、厂商。

5. 宣布处理失败。

### 业务服务依赖的第三方组件故障，造成业务不正常

1. 联系第三方组件运维，紧急恢复，如果无法快速恢复，转到下一步。

2. 判断第三方组件是否有持久化的数据，如果没有，紧急在其他机器上启动，并修改服务配置，使用紧急搭建的第三方组件，这需要提前准备好第三方组件启动方式。

3. 宣布处理失败。

## 应急演练

应急演练在不同环境、不同公司组织、不同业务下都有不同，没有统一的，适合所有的演练方案，本文档给出的是一个示例。

### 演练目的

当灾难发生，相关人员能够迅速的反应，正确的按照既定流程、步骤处理，迅速、争取、不破坏数据、不扩大故障的前提下，快速恢复平台和业务服务。

### 演练周期

建议：

* 每个月一次涉及一个业务、一个部门的小范围小型演习。

* 每三个月一次整个平台主要故障的演习、涉及某个业务部门所有服务的中等规模演习。

* 每半年进行一次涉及整个平台、整个公司所有业务服务的大规模演习。

### 参与演练人员

* 小型演习只需要运维部门参加，业务部门可以提供电话值守人员帮助。

* 中等规模演运维部门全体参与，业务服务部门提供现场值班人员，研发提供电话值守人员。

* 大规模演习运维部门全体参与，业务服务部门大部门参与，研发提供现场值班，平台厂商提供电话值守帮助。

### 演练范围

* 小型演习：某个业务部门的某个业务服务。

* 中型演习：某个业务部门的所有服务，平台主要功能。

* 大型演习：全部业务服务，平台所有功能。

### 演练大纲