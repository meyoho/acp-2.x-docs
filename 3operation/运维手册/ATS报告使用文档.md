# ATS报告使用文档

#1 技术及架构

ATS报告项目延续了ATS项目，使用Python开发，采用多线程并发执行任务，利用ansible的play及role来实现相关具体功能。页面与数据采用Jinja2模板，方便数据与页面的结合。项目打包成二进制可执行文件，方便部署。以下是技术架构图：

![ats-report](./ats-report.jpg)

#2 ATS报告做什么

ATS报告通过执行检查，对global及业务集群进行健康度检查，通过多个指标综合评价集群运行健康状态。给用户对集群一个直观的认识。

一期具体实现评价参考：[1、Kubernetes系统集群状态概览](http://confluence.alauda.cn/pages/viewpage.action?pageId=54854919)

后续会提供更多的评价维度。

# 3 使用方法

把ats可执行命令属性设置为755，当前用户可以执行。然后配置ats.cfg配置文件。模板：

```yaml
#
# Alauda Trouble Shooting configuration file
# Usage : ./ats web -f ats.cfg
# Date 15/11/2019
# Mail ops@alauda.io
 
[ssh]
# ssh集群机器所用账号
user = root
 
# ssh的密码，若使用密钥登录，password留空即可
password =
 
# ssh端口
port = 22
 
# ssh机器所用的密钥文件路径，若使用密码登录，则留空
private_key_file = /root/.ssh/id_rsa
 
 
[nodes]
# 集群master节点的ip列表，多个ip之间使用逗号分隔
masters = 129.28.162.232,94.191.76.27,129.28.158.246
 
# 集群slaves节点的ip列表，多个ip之间使用逗号分隔
slaves = 118.24.245.252,94.191.73.46,118.24.222.130
 
# 其他节点的ip列表，比如某些环境db、es等单独部署，则将此类机器的ip地址配置到下面，若没有则留空
others =
 
# 如果集群中某些节点无法用[ssh]中配置的方式登陆，则可以单独配置在此，多个使用逗号分隔，比如：
# 有一台节点ip为192.168.0.2,密码为12345，则配置成ip=192.168.0.2 ansible_ssh_pass=12345，
# 有一台节点ip为192.168.0.3, 使用密钥登陆，则配置成ip=192.168.8.3 ansible_ssh_private_key_file=/home/user1/.ssh/id_rsa,路径需替换为真实key地址
# spec_login_nodes:  ip=192.168.0.2 ansible_ssh_pass=12345, ip=192.168.8.3 ansible_ssh_private_key_file=/home/user1/.ssh/id_rsa
spec_login_nodes:
 
[mod]
# 配置支持的功能模块
# 所有支持的模块为：report
# 目前只支持report功能模块
modules = report
 
[etcd]
# etcd的信息，如果是etcd集群，多个ip之间使用逗号分隔
ips = 10.0.128.241,10.0.128.139,10.0.128.169
 
# etcd默认服务端口
port = 2379
 
# etcd根证书地址，如果etcd没有使用证书，或者使用了证书并且证书路径为/etc/kubernetes/pki/etcd/ca.crt，则该配置项可以配置空，否则配置etcd证书路径真实路径
cacert =
 
# etcd集群证书的私钥文件，如果etcd没有使用证书，或者使用了证书并且证书路径为/etc/kubernetes/pki/etcd/ca.crt，则该配置项可以配置空，否则配置etcd证书路径真实路径
key =
 
# etcd基于根证书生成的加密证书地址，如果etcd没有使用证书，或者使用了证书并且证书路径为/etc/kubernetes/pki/etcd/ca.crt，则该配置项可以配置空，否则配置etcd证书路径真实路径
cert =
 
 
[prometheus]
# 若集群对接了prometheus，配置prometheus的地址，检测工具会调用prometheus的api额外获取检测指标
# 未对接则留空
url = http://192.168.8.33:30900
 
[clusterstatus]
# 默认检测kube-system alauda-system下的非正常pod，无需再次配置kube-system alauda-system,如果有其他namespace，可以添加到下面，多个namespace以空格分隔
namespace =
# 配置pod重启次数，若重启超过配置数，则认为非正常
restart = 50
 
# 获取的日志，存储在该目录下
[log]
download_path = /tmp/logtest
```

建议在SSH互信的服务器上执行，比如init节点。ats命令介绍：

参数：web  #启动web服务器，接收浏览器请求

-f  指定ats的配置文件。

-p  指定启动web服务器的端口。

例子：

chmod 755 ats

./ats web -f ats.cfg -p 60000

执行当前目录下的ats命令，执行web模块，-f 加载当前目录下的ats.cfg配置文件，启动web服务器，监听端口为60000。

执行效果：

![image2019-11-12_11-51-20](./image2019-11-12_11-51-20.png)

web服务器就启动完毕，等待接收用户浏览器的请求。用WEB浏览器（推荐使用Chrome）访问服务器IP地址+端口。

例如我的服务器地址是：118.24.213.153，启动监听了60000端口。

浏览器输入：http://118.24.213.153:60000/

执行时间可能会比较长，根据集群规模大小，执行时间约在3分钟左右。执行中后台会打印执行具体任务信息。执行完毕后，浏览器会显示结果。

执行结果参考下图：

![image2019-11-12_11-56-42](./image2019-11-12_11-56-42.png)