#  目录
1. 关于本文档
	* 文档受众
	* 文档目的
	* 修订记录
2. 部署和配置简介
3. 部署过程概述
	* 平台部署架构图
	* 平台的部署流程图
4. 平台部署
	* 检查部署需求是否满足
	* 解决不满足的需求
	* 和 2.3 相比部署差异
	* 开始部署
	* 业务服务集群部署
	* 业务服务集群组件、插件部署
5. 平台配置及测试
	* 平台访问地址及登录用户名、密码
	* 部署成功后操作
	* 平台功能测试
	* 添加监控、告警
6. FAQ
	* 部署错误
	* 运行错误

<div STYLE="page-break-after: always;"></div>
# 第一部分  前言

本文档介绍了如何安装和配置平台。

## 读者对象

《TKE for Alauda Container Platform 部署文档》适用于具备基本的 linux、容器、Kubernetes 知识，想要安装和配置平台的用户。包括：

* 实施顾问和平台管理员

* 规划平台架构的售前工程师

* 负责整个项目生命周期的项目经理

## 文档目的

* 实施工程师依据本文档部署平台。

* 实施工程师依据本文档，配置平台参数。

* 平台管理员依据本文档测试平台功能。

## 修订记录

| 文档版本 | 发布日期   | 修订内容                                      |
| -------- | ---------- | --------------------------------------------- |
| v1.0 | 2019-10-08 | 第一版，适用于 TKE for Alauda Container Platform 私有部署版本。 |
| v1.1 | 2019-11-26 | 第二版，增加对 all in one 部署方式的注意事项。 |



<div STYLE="page-break-after: always;"></div>
# 第二部分  部署和配置简介

平台平台有多种部署方案，在规划平台架构之前，请阅读《TKE for Alauda Container Platform v2.4 部署白皮书》中部署方案部分。

平台由 global 、global 插件、业务服务集群和集群组件构成，global 是平台的核心，管理、运维功能由 global 提供。global 提供了丰富的插件来扩展 global 的功能，详细内容请看《TKE for Alauda Container Platform v2.4 部署白皮书》。

业务服务集群是 global 管理的运行客户业务服务的 Kubernetes 集群，集群组件是运行在客户业务服务集群之上的功能模块，提供平台功能、搜集相关数据。

平台可以部署在物理机和虚拟机上，服务器的数量、配置要求等信息请阅读《TKE for Alauda Container Platform v2.4 部署白皮书》中硬件需求部分。

平台的部署需要域名、vip、ip 范围、lb 等网络资源，网络资源的具体需求请阅读《TKE for Alauda Container Platform v2.4 部署白皮书》中网络需求部分。

平台支持在 centos 和 ubuntu 操作系统上部署，操作系统版本和配置请阅读本文档中软件需求部分。

平台中的 global 、global 插件、业务服务器集群、业务服务集群插件的部署请看本文第四部分。

<div STYLE="page-break-after: always;"></div>
# 第三部分  平台及部署过程概述

平台是一款复杂的产品，具有多个需要安装和配置的插件、组件，为确保部署成功，请了解部署架构图和部署步骤的流程顺序图。

## 平台逻辑架构图

请参考《TKE for Alauda Container Platform v2.4 部署白皮书》。

## 平台部署架构图

请参考《TKE for Alauda Container Platform v2.4 部署白皮书》。

## 平台的部署流程图

<img src="images/部署步骤.png" style="zoom:50%;" />

<div STYLE="page-break-after: always;"></div>
# 第四部分 平台部署

## 检查部署需求是否满足

### 软件需求

请参考《TKE for Alauda Container Platform v2.4 部署白皮书》。

### 硬件需求

请参考《TKE for Alauda Container Platform v2.4 部署白皮书》。

### 网络需求

请参考《TKE for Alauda Container Platform v2.4 部署白皮书》。

## 解决不满足的需求

### 软件需求

|需求项|具体要求|不满足后的解决办法|
| ------------- | --------------- | ------------- |
| 操作系统| centos 7.6 |<mark>小版本可以有差别，比如：7.5 7.4 也可以安装部署。</mark>|
| kernel 版本  | 大于 3.10.0-957|<mark>必须满足。</mark>|
| 操作系统安装要求|最小安装|<mark>如果不满足，可能出现配置不同造成部署失败。</mark>|
| 工具软件  |curl tar ip ssh sshpass jq netstat timedatectl ntpdate nslookup base64 tr head openssl md5sum|<mark>手动添加源，并安装软件，请看表格后面的操作方式。</mark>|
|用户权限|root|<mark>具备sudo 权限的用户也可部署，但有一定几率部署失败。</mark>|
|swap|关闭|<mark>必须关闭</mark>|
|防火墙|关闭|<mark>必须关闭</mark>|
|selinux|关闭|<mark>必须关闭</mark>|
|时间同步|所有服务器要求时间必须同步，误差不得超过 2 秒。|<mark>必须同步</mark>|
|时区|所有服务器时区必须统一|<mark>必须统一</mark>|
|/etc/sysctl.conf|vm.max_map_count=262144   net.ipv4.ip_forward = 1|<mark>必须满足</mark>|
|hostname 格式|字母开头，只能是字母、数字和短横线-组成，不能用短横线结尾，长度在 4-23 之间。|<mark>必须满足</mark>|
|/etc/hosts|所有服务器可以通过hostname 解析成 ip，可以将localhost解析成127.0.0.1，hosts 文件内，不能有重复的 hostname|<mark>必须满足</mark>|
|/tmp/权限| 要求 `/tmp` 目录的权限是 `777`。|<mark>必须满足</mark>|

#### 解决工具软件不满足的步骤

1. 解压缩安装包

1. 将安装包内的 kaldr 目录，拷贝到每一台服务器的 `/tmp` 目录下。

	在每一台服务器上执行如下命令，添加软件源。

	* 适用 于 Centos ：

		```
		mkdir /etc/yum.repos.d/old
		mv /etc/yum.repos.d/*.repo /etc/yum.repos.d/old
		cat <<EOF >/etc/yum.repos.d/alauda.repo 
		[alauda]
		name=Alauda
		baseurl=file:///tmp/kaldr/yum/
		enabled=1
		gpgcheck=0
		EOF
		yum clean all && yum makecache
		```

	* 适用于 Ubuntu：

		```
		mv /etc/apt/sources.list /etc/apt/sources.list.old
		echo "deb [arch=amd64] file:///tmp/kaldr/apt/ ubuntu-xenial main" >/etc/apt/sources.list
		add-apt-repository "deb [arch=amd64] file:///tmp/kaldr/apt/ ubuntu-xenial main"
		cat /tmp/kaldr/apt/alauda.key | apt-key add -
		apt-get update
		```
	
	**补充说明**：可通过以下命令行，确认某个软件属于哪个包。
	
	* 适用 于 Centos：`yum provides <程序名>`
	
	* 适用于 Ubuntu ：`apt-file search <程序名>`
	
1. 安装软件包，参考以下命令行。

	* 适用 于 Centos：`yum install -y <上一步获取到的软件包的名字>`
	* 适用于 Ubuntu ：`apt-get install -y <上一步获取到的软件包的名字>`

### 硬件需求

<mark>这是实施部署的前提条件，是必须满足硬件需求，不满足硬件需求就无法保证正常部署，或部署成功的平台无法长时间稳定运行。</mark>

#### 网络需求

<mark>这是实施部署的前提条件，是必须满足硬件需求，不满足硬件需求就无法保证正常部署，或部署成功的平台无法长时间稳定运行。</mark>

## 和 v2.3 相比部署差异

### 增加的参数

* `--kube-cluster-subnet`     指定 k8s 集群svc 的 cluster ip 范围，默认是10.96.0.0/12，掩码建议不要小于16位
* `--kube-cluster-dns-ip`     k8s 集群的 dns 的svc 的地址，如果不指定，默认是kube-cluster-subnet 地址范围，去掉'/'后面的掩码，将 ip 最后一位从0 改成10，语法是 KUBE_CLUSTER_DNS_IP=$(echo ${KUBE_CLUSTER_SUBNET} | sed 's#0/[0-9]*#10#')
* `--global-network-mode`     部署 glboal 集群时，使用那种网络，默认是 kube-ovn，可以选择使用 flannel 


### 改变的参数

`--enabled-features`        以前只有 acp、devops、Service Mesh 和 tke 四个，现在增加了 aml

## 开始部署

## server_list.json 文件格式说明

```
"server_role": {         #服务器角色
  "master":true,         #有这个键值，代表这台服务器角色是 k8s 的 master
  "global":true,         #有这个键值，代表这是运行 global 组件的服务器
  "log":true             #有这个键值，代表这是运行日志组件的服务器
},
"ip_addr": "1.1.1.1",    #服务器 ip 地址
"ssh_port": "22",        #ssh 端口
"ssh_user": "root",      #ssh 用户名
"ssh_passwd": "",        #ssh 密码
"ssh_key_file": "/root/.ssh/id_rsa" #ssh 密钥
```


## all in one 方案

部署命令执行的位置：all in one 服务器上

部署过程：

1.  解压缩安装包。

1. 进入到解压缩后的目录。

1. 执行以下脚本部署：

	
	**注 1**：只有下面例子中的参数经过部署测试，改变参数的做法很可能造成部署不成功。
	
	**注 2**：因为 all in one 的部署方式还未纳入到功能测试列表中，这种部署方式仅仅为了在简单的 poc 中体验平台，暂时不能保证部署出来的平台功能完整，功能的可用和可靠也不能保证。

	```
	bash ./up-cpaas-single.sh \
	     --network-interface=<网卡名，默认eth0> \
	     --enabled-features=<acp,devops,tke>
	```
	
## poc 部署方案

部署命令执行的位置： init 服务器上

**部署过程**：

1. 解压缩安装包。

1. 进入到解压缩后的目录。

1. 在 up-cpaas.sh 脚本所在的目录下创建 server_list.json 文件，示例如下：

	``` 
	 [
	  {
	    "server_role": {
	      "master":true,
	      "global":true,
	      "log":true
	    },
	    "ip_addr": "1.1.1.1",
	    "ssh_port": "22",
	    "ssh_user": "root",
	    "ssh_passwd": "",
	    "ssh_key_file": "/root/.ssh/id_rsa"
	  },
	  {
	    "server_role": {
	      "master":true,
	      "global":true,
	      "log":true
	    },
	    "ip_addr": "2.2.2.2",
	    "ssh_port": "22",
	    "ssh_user": "root",
	    "ssh_passwd": "",
	    "ssh_key_file": "/root/.ssh/id_rsa"
	  },
	  {
	    "server_role": {
	      "master":true,
	      "global":true,
	      "log":true
	    },
	    "ip_addr": "3.3.3.3",
	    "ssh_port": "22",
	    "ssh_user": "root",
	    "ssh_passwd": "",
	    "ssh_key_file": "/root/.ssh/id_rsa"
	  }
	]
	```

1. 执行以下命令进行部署：

	```
	bash ./up-cpaas.sh \
	     --network-interface=<网卡名，默认是eth0> \
	     --enabled-features=<需要安装那些产品，默认什么都不装，支持acp,devops,tke,asm,aml> \
	     --global-network-mode=<选择 global 集群使用那种网络，默认是 ovn，也可以支持 flannel> \
	     --kube-cluster-subnet<global 集群的cluster ip 范围，默认是10.96.0.0/12，掩码建议不要小于16位> \
	     --domain-name=<域名> \
	     --kube_controlplane_endpoint=<k8s vip> \
	     --key-file=<域名证书私钥文件> \
	     --cert-file=<域名证书公钥文件> \
	     --root-username=<管理员用户名，必须是邮箱格式，不加这个参数默认是 admin@cpaas.io>
	```
	**说明**：如果客户没有提供 lb 和 vip ，也可以用下面的命令进行部署：

	```
	bash ./up-cpaas.sh \
	     --network-interface=<网卡名，默认是eth0> \
	     --make-k8s-lb \
	     --enabled-features=<需要安装那些产品，默认什么都不装，支持acp,devops,tke,asm,aml> \
	     --use-http
	```


## 正式生产环境部署方案

部署命令执行的位置： init 服务器上

**部署过程**：

1. 解压缩安装包。

1. 进入到解压缩后的目录。

1. 在 up-cpaas.sh 脚本所在的目录下创建 server_list.json 文件，示例如下：

	* 6 节点 server_list.json 文件详情：

		```
		[
		  {
		    "server_role": {
		      "master":true,
		      "global":true
		    },
		    "ip_addr": "1.1.1.1",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  },
		  {
		    "server_role": {
		      "master":true,
		      "global":true
		    },
		    "ip_addr": "2.2.2.2",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  },
		  {
		    "server_role": {
		      "master":true,
		      "global":true
		    },
		    "ip_addr": "3.3.3.3",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  },
		  {
		    "server_role": {
		      "log":true
		    },
		    "ip_addr": "4.4.4.4",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  },
		  {
		    "server_role": {
		      "log":true
		    },
		    "ip_addr": "5.5.5.5",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  },
		  {
		    "server_role": {
		      "log":true
		    },
		    "ip_addr": "6.6.6.6",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  }
		]
		```

	* 9 节点server_list.json 文件详情：

		```
		[
		  {
		    "server_role": {
		      "master":true
		    },
		    "ip_addr": "1.1.1.1",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  },
		  {
		    "server_role": {
		      "master":true
		    },
		    "ip_addr": "2.2.2.2",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  },
		  {
		    "server_role": {
		      "master":true
		    },
		    "ip_addr": "3.3.3.3",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  },
		  {
		    "server_role": {
		      "log":true
		    },
		    "ip_addr": "4.4.4.4",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  },
		  {
		    "server_role": {
		      "log":true
		    },
		    "ip_addr": "5.5.5.5",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  },
		  {
		    "server_role": {
		      "log":true
		    },
		    "ip_addr": "6.6.6.6",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  },
		  {
		    "server_role": {
		      "global":true
		    },
		    "ip_addr": "7.7.7.7",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  },
		  {
		    "server_role": {
		      "global":true
		    },
		    "ip_addr": "8.8.8.8",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  },
		  {
		    "server_role": {
		      "global":true
		    },
		    "ip_addr": "9.9.9.9",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  }
		]
		```

1. 执行如下命令部署：

	```
	bash ./up-cpaas.sh \
	     --network-interface=<网卡名，默认是eth0> \
	     --enabled-features=<需要安装那些产品，默认什么都不装，支持acp,devops,tke,asm,aml> \
	     --global-network-mode=<选择 global 集群使用那种网络，默认是 ovn，也可以支持 flannel> \
	     --kube-cluster-subnet<global 集群的cluster ip 范围，默认是10.96.0.0/12，掩码建议不要小于16位> \
	     --domain-name=<域名> \
	     --kube_controlplane_endpoint=<k8s vip> \
	     --key-file=<域名证书私钥文件> \
	     --cert-file=<域名证书公钥文件> \
	     --root-username=<管理员用户名，必须是邮箱格式，不加这个参数默认是 admin@cpaas.io>
	```
	
	**说明 1**：如果客户没有提供证书，也可以用下面的命令进行部署，脚本会自动创建一个证书，但是浏览器访问 ui 的时候会提示证书不是正规机构颁发的，有安全隐患：
	
	```
	bash ./up-cpaas.sh \
	     --network-interface=<网卡名，默认是eth0> \
	     --enabled-features=<需要安装那些产品，默认什么都不装，支持acp,devops,tke,asm,aml> \
	     --global-network-mode=<选择 global 集群使用那种网络，默认是 ovn，也可以支持 flannel> \
	     --kube-cluster-subnet<global 集群的cluster ip 范围，默认是10.96.0.0/12，掩码建议不要小于16位> \
	     --domain-name=<域名> \
	     --kube_controlplane_endpoint=<k8s vip> \
	     --root-username=<管理员用户名，必须是邮箱格式，不加这个参数默认是 admin@cpaas.io>
	```
	
	**说明 2**：如果客户没有提供证书，也不希望使用脚本自动创建的证书，可以选择使用 http 协议访问，执行如下命令部署：
	
	```
	bash ./up-cpaas.sh \
	     --use-http \
	     --network-interface=<网卡名，默认是eth0> \
	     --enabled-features=<需要安装那些产品，默认什么都不装，支持acp,devops,tke,asm,aml> \
	     --global-network-mode=<选择 global 集群使用那种网络，默认是 ovn，也可以支持 flannel> \
	     --kube-cluster-subnet<global 集群的cluster ip 范围，默认是10.96.0.0/12，掩码建议不要小于16位> \
	     --domain-name=<域名> \
	     --kube_controlplane_endpoint=<k8s vip> \
	     --root-username=<管理员用户名，必须是邮箱格式，不加这个参数默认是 admin@cpaas.io>
	```


## 业务服务集群部署

<mark>登录 global 的 UI 后，请参考用户手册，部署业务服务集群或对接业务服务集群。</mark>

## 业务服务集群组件、插件部署


### 在业务集群部署 cert-manager

**注意**：alauda-cluster-base 需要依赖 cert-manager，因此在业务集群部署 alauda-cluster-base 时，需要先安装 cert-manager 在业务集群的一个 master 节点。但是在 global 集群部署 alauda-cluster-base 时，不需要部署 cert-manager，直接部署 alauda-cluster-baseJ 即可，因为 global 已经默认安装 cert-manager。

```
registry=$(docker info |grep 60080  |tr -d ' ')
helm install \
     --name cert-manager \
     --namespace alauda-system \
     --set global.registry.address=$registry \
     stable/cert-manager
```

### 部署业务集群必备组件 alauda-cluster-base

```
email=admin@alauda.io     ####默认为admin@cpaas.io，需要与登录global界面时使用的邮箱一致。
registry=$(docker info |grep 60080  |tr -d ' ')
helm repo update
helm install \
     --debug \
     stable/alauda-cluster-base \
     --name alauda-cluster-base \
     --namespace alauda-system  \
     --set global.registry.address=$registry  \
     --set global.auth.default_admin=$email
```

###  业务集群部署 nevermore

```
region_name=test              ###需要修改为正在部署的集群名称
apigateway=1.1.1.1            ###需要修改为global界面的访问ip或者域名
registry=$(docker info |grep 60080  |tr -d ' ')
token=$(kubectl describe secrets  $(kubectl get secret -n kube-system | awk '/^clusterrole-aggregation-controller-token/{print $1}') -n kube-system|grep ^token|awk '{print $2}')
helm install \
     --name alauda-log-agent \
     --namespace alauda-system  \
     --set nevermore.region=$region_name \
     --set nevermore.apiGatewayHost=https://$apigateway \
     --set nevermore.token=$token \
     --set global.registry.address=$registry \
     stable/alauda-log-agent
```

###  业务集群部署普罗米修斯

**说明**：

* 部署普罗米修斯依赖 alauda-cluster-base。
	
* 部署 Service Mesh 依赖 ingress、alauda-cluster-base。
	
* 需要改的地方已经标注，没标注的按文档顺序操作即可。


因此部署之前先执行以下命令判断是否已经安装。

```
helm list | grep  alauda-cluster-base
helm list | grep ingress 
```

若没有安装，则必须先按照业务集群部署文档部署 ingress 以及 alauda-cluster-base。

###  开始普罗米修斯部署

**说明**：部署普罗米修斯默认部署在 alauda-system 命名空间下，若无特别需要，请勿修改 namespace，Service Mesh 等需要依赖普罗米修斯，修改 namespace 没填对，会增大排查难度。


1. 准备工作，监控 etcd 先判断 etcd-ca 是否存在。

	`kubectl get secrets -n alauda-system | grep etcd-ca`

	若不存在则按以下命令添加，若存在，跳过第一步。

	```
	kubectl get secrets -n kube-system etcd-ca -o yaml >/tmp/etcd-ca.yaml
	sed -i  '/namespace:/{s/kube-system/alauda-system/g}' /tmp/etcd-ca.yaml
	kubectl apply -f /tmp/etcd-ca.yaml
	```

1. 安装 prometheus-operator。

	```
	registry=$(docker info |grep 60080  |tr -d ' ')
	helm install \
	     -n prometheus-operator \
	     --namespace=alauda-system \
	     --set global.registry.address=$registry \
	     --timeout=3000
	     stable/prometheus-operator \
	```

1. 安装 kube-prometheus。

	安装 kube-prometheus 有以下两种方式，一种为使用本地目录的方式存储监控数据，一种为使用 pvc 的方式存储。可自行选择。若无特殊需要，本地目录的方式即可。

	* 本地目录方式部署 kube-prometheus
  
		给集群中的一个 node 添加 monitoring=enabled 的 label，用于 local volume 的调度，命令如下：

		`kubectl label --overwrite nodes test monitoring=enabled`
		
		**注意**：需要将 test 替换为这个 node 的实际 hostname。<br>在该 node 上创建以下目录用作持久化目录，保证空间 granafa 5G/prometheus 30G/alertmanager 5G，命令如下：

		```
		mkdir -p /var/lib/monitoring/{grafana,prometheus,alertmanager}
		registry=$(docker info |grep 60080  |tr -d ' ')
		global_vip=1.1.1.1   ###需要修改为 global 界面的访问地址
		region_name=test456  ###需要将 test456 替换为当前集群的集群名字
		 
		cat << eof > /tmp/prometheus.sh
		helm install \\
		     -n kube-prometheus \\
		     --namespace alauda-system stable/kube-prometheus \\
		     --timeout=30000 \\
		     --set global.platform=ACP \\
		     --set prometheus.service.type=NodePort \\
		     --set grafana.service.type=NodePort \\
		     --set global.registry.address=$registry \\
		     --set grafana.storageSpec.persistentVolumeSpec.local.path=/var/lib/monitoring/grafana \\
		     --set prometheus.storageSpec.persistentVolumeSpec.local.path=/var/lib/monitoring/prometheus \\
		     --set alertmanager.storageSpec.persistentVolumeSpec.local.path=/var/lib/monitoring/alertmanager \\
		     --set alertmanager.configForACP.receivers[0].name=default-receiver \\
		     --set alertmanager.configForACP.receivers[0].webhook_configs[0].url=https://$global_vip/v1/alerts/$region_name/router
		eof
		sh /tmp/prometheus.sh
		```

	* pvc 方式部署 kube-prometheus

		```
		registry=$(docker info |grep 60080  |tr -d ' ')
		global_vip=1.1.1.1   ###需要修改为 global 界面的访问地址
		region_name=test456  ###需要将 test456 替换为当前集群的集群名字
		storage=test         ###需要将 test 替换为当前集群的 StorageClass 的名字 
		 
		cat << eof > /tmp/prometheus.sh
		helm install -n kube-prometheus \\
		     --namespace alauda-system stable/kube-prometheus \\
		     --timeout=30000 \\
		     --set global.platform=ACP \\
		     --set prometheus.service.type=NodePort \\
		     --set grafana.service.type=NodePort \\
		     --set global.registry.address=$registry \\
		     --set grafana.storageSpec.volumeClaimTemplate.spec.storageClassName=$storage \\
		     --set prometheus.storageSpec.volumeClaimTemplate.spec.storageClassName=$storage \\
		     --set alertmanager.storageSpec.volumeClaimTemplate.spec.storageClassName=$storage \\
		     --set alertmanager.configForACP.receivers[0].name=default-receiver \\
		     --set alertmanager.configForACP.receivers[0].webhook_configs[0].url=https://$global_vip/v1/alerts/$region_name/router
		eof
		sh /tmp/prometheus.sh
		```

1. 查看所有 pod 是否正常启动（pod 为 Running 或者 Completed 状态）。

	`kubectl get pods -n alauda-system | grep prometheus`

1. 集群对接监控。
	
	```
	ip=2.2.2.2   ###需要修改为业务集群任意一个 master 节点的外网 ip，若没有外网地址，使用默认的实际ip
	cat << eof > /tmp/prometheus-feature.yaml
	apiVersion: infrastructure.alauda.io/v1alpha1
	kind: Feature
	metadata:
	  labels:
	    instanceType: prometheus
	    type: monitoring
	  name: prometheus
	spec:
	  accessInfo:
	    grafanaAdminPassword: admin                # grafana 默认密码
	    grafanaAdminUser: admin                    # grafana 默认用户
	    grafanaUrl: http://$ip:30902               # grafana 地址
	    name: kube-prometheus                      # 安装 kube-prometheus chart 时指定的名称
	    namespace: alauda-system                   # kube-prometheus chart 所在的命名空间
	    prometheusTimeout: 10                      # prometheus 请求超时时间
	    prometheusUrl: http://$ip:30900            # prometheus 地址
	  instanceType: prometheus
	  type: monitoring
	  version: "1.0"
	eof
	 
	kubectl apply -f /tmp/prometheus-feature.yaml
	```

### Service Mesh 部署

**说明**：业务集群部署 Service Mesh，至少必须要有一个 slave 节点，且这个节点上不能部署 alb 和 ingress，即：不能有 `alb2=ture` 和 `ingress=true` 的标签。
如果在部署 global 的时候，`--enabled-features` 这个参数没有选择 Service Mesh ，那么就需要手动部署 Service Mesh 的 globla 组件。在 global 集群的 master 节点上，执行如下操作：
注：<> 代表要根据环境情况替换尖括号内的值，替换完毕之后，不包括尖括号。

```
ACP_NAMESPACE=<alauda-system>  ## 改成部署时， --acp2-namespaces 参数指定的值，默认是alauda-system
https_or_http=<https>          ## 默认是 https，如果在部署时，使用了 --use-http 这个参数，那么需要改成 http
DOMAIN_NAME=<域名>              ## 改成部署时--domain-name 这个参数指定的值
registry=$(docker info |grep 60080  |tr -d ' ')

helm install \
     --debug \
     --set install.mode="global" \
     --namespace=${ACP_NAMESPACE} \
     --name=asm-init stable/asm-init
helm install \
     --debug \
     --set global.host=${DOMAIN_NAME} \
     --set global.scheme='${https_or_http}' \
     -f /tmp/install_values.yaml \
     --namespace=${ACP_NAMESPACE} \
     --name=global-asm stable/global-asm


```

部署成功之后，在 ui 界面上单击左上角产品菜单，能看到 Service Mesh ，单击进入，选择管理视图，开始部署  Service Mesh 的业务集群组件，步骤如下：

#### 首先搜集如下信息

1. 有几个信息提前收集一下：

	* ES【部署在 global 上的】 ，例如：
	
		地址：http://10.0.0.17:9200
			
		用户：alaudaes                ### 默认值
			
		密码：es_password_1qaz2wsx    ### 随机生成的值
			
		可以通过在 global 集群上的一个 master 上执行如下命令，获取密码。
			
		``` 
		kubectl get deployments -n alauda-system cpaas-elasticsearch -o yaml | grep ALAUDA_ES_PASSWORD -A 1 | grep value | awk '{print $NF}' 
		```
	
	* prometheus【部署在对应集群上的】 ，例如：
	
		地址：http://10.0.0.12:30900
	
	* ingress 地址【部署在对应集群上的】 ，获取方式：
	
		``` 
		cat /etc/kubernetes/kubelet.conf | grep server: | grep -oE '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' 
		```
	
	* IP 范围【对应集群的】 ，获取方式：
	
		```
		cat /etc/kubeadm/kubeadmcfg.yaml | grep podSubnet |awk -F : '{print $5 $4}'|tr -d 'a-z,A-Z}'|sed 's+ +\\,+g'|sed 's/^..//g'|sed 's/..$//g' 
		```

2. 在 Service Mesh 页面的“管理视图”里创建“服务网格”，如下图：

	<img src="images/asm 部署-ui.png" style="zoom:50%;" />

	这里的“组件高可用”代表用多副本方式启动 Service Mesh 组件
	
	<img src="images/asm 部署，多副本选择.png" style="zoom:50%;" />

3. 在“管理视图”的“项目”里，启动 ns 的服务网格。

	<img src="images/asm 部署，启动 ns 的服务网格.png" style="zoom:50%;" />

	在“管理视图”的“健康检查”里可以看到各组件的状态，则启动成功（组件启动需要一段时间，请耐心等待）。

	<img src="images/asm 部署-健康检查.png" style="zoom:50%;" />


### 部署 Machine Learning


安装包请到 ftp 中下载四个组件（global-aml、volcano、aml-core、aml-ambassador）


* Global 集群中需要部署：

	global-aml
	
* 业务集群部署（如果 global 集群也用作业务集群）：

	volcano
	aml-core
	aml-ambassador


#### global 下部署 Machine Learning 的 global-aml

* 部署 global-aml
	
	```
	# alaudaConsole:
	apiAddress: https://172.21.16.15
	# apigateway的地址
	oidcIssuerUrl: https://172.21.16.15/dex
	# dex地址
	oidcRedirectUrl: https://172.21.16.15/console-platform/
	```

	```
	registry=$(docker info |grep 60080  |tr -d ' ')
	ingress_address=$(cat /etc/kubernetes/kubelet.conf  | grep server: | grep -oE '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}')
	cat << eof > /tmp/global-aml.sh
	helm install ./global-aml/global-aml --name global-aml --namespace alauda-system \
		--set alaudaConsole.apiAddress=https://172.21.16.15 \
		--set alaudaConsole.oidcRedirectUrl=https://172.21.16.15 \
		--set alaudaConsole.oidcIssuerUrl=https://172.21.16.15/dex \
		--set global.registry.address=$registry \
		--set global.ingress.host=$ingress_address
	eof
	sh /tmp/global-aml.sh
	```

* 删除 global-aml

	```
	helm delete global-aml --purge
	kubectl delete clusterrolebindings.rbac.authorization.k8s.io global-aml:alauda-system:cluster-admin
	```

#### 业务集群下部署 aml-volcano

* 部署volcano

	```
	registry=$(docker info |grep 60080  |tr -d ' ')
	helm install ./volcano --name volcano --namespace alauda-system --set global.registry.address=$registry
	```
* 删除volcano

	```
	helm delete volcano --purge
	kubectl delete customresourcedefinitions.apiextensions.k8s.io podgroups.scheduling.incubator.k8s.io
	kubectl delete customresourcedefinitions.apiextensions.k8s.io queues.scheduling.incubator.k8s.io
	kubectl delete validatingwebhookconfigurations.admissionregistration.k8s.io `kubectl get validatingwebhookconfigurations.admissionregistration.k8s.io | grep volcano | awk '{print $1}'`
	kubectl delete customresourcedefinitions.apiextensions.k8s.io `kubectl get customresourcedefinitions.apiextensions.k8s.io | grep volcano | awk '{print $1}'`
	kubectl delete mutatingwebhookconfigurations.admissionregistration.k8s.io `kubectl get mutatingwebhookconfigurations.admissionregistration.k8s.io | grep volcano | awk '{print $1}'`
	kubectl delete clusterroles.rbac.authorization.k8s.io `kubectl get clusterroles.rbac.authorization.k8s.io|grep volcano | awk '{print $1}'`
	```

#### 业务集群下部署 aml-core

* 部署 aml-core

	```
	registry=$(docker info |grep 60080  |tr -d ' ')
	ingress_address=$(cat /etc/kubernetes/kubelet.conf  | grep server: | grep -oE '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}')
	helm install ./aml-core --name aml-core --namespace alauda-system \
	--set global.ingress.host=$ingress_address \
	--set global.registry.address=$registry \
	--set tf-job.operator.enableGangScheduling=true
	```
* 删除aml-core

	```
	helm delete aml-core --purge

	kubectl delete customresourcedefinitions.apiextensions.k8s.io pytorchjobs.kubeflow.org
	kubectl delete customresourcedefinitions.apiextensions.k8s.io tfjobs.kubeflow.org
	kubectl delete clusterrolebindings.rbac.authorization.k8s.io aml-bus:alauda-system:cluster-admin
	kubectl delete clusterrolebindings.rbac.authorization.k8s.io aml-bus:alauda-system:cluster-admin
	kubectl delete clusterroles.rbac.authorization.k8s.io pytorch-operator
	```
	
#### 业务集群下部署 aml-ambassador

* 部署 aml-ambassador

	```
	registry=$(docker info |grep 60080  |tr -d ' ')
	helm install ./aml-ambassador --name aml-ambassador --namespace alauda-system --set global.registry.address=$registry
	```
* 删除aml-ambassador

	```
	helm del --purge aml-ambassador
 
	kubectl delete customresourcedefinitions.apiextensions.k8s.io `kubectl get customresourcedefinitions.apiextensions.k8s.io | grep ambassador | awk '{print $1}'`
	```

### 业务集群部署 alb2

部署 alb 必须提供单独的机器，且这个机器上不能有 `ingress=true`，`istio=true` 的标签。ingress 默认部署在 master 身上，alb2 不能和 ingress、Service Mesh 的 gateway 部署在一个节点 alb2 使用的标签为：`alb2=true` ，ingress 使用的标签为 `ingress=true`，Service Mesh 使用的标签为 `istio=true`。请注意以上三个标签一个主机只能有一个，即：一个节点只能作为以上三个角色中的一个角色。

**准备工作**：

在准备做 alb 的节点打上 `alb2=true` 的标签。

```kubectl label node node1  alb2=true```   ####注意将 node1 改为准备部署 alb 的机器的主机名，若部署高可用 alb，则在所有准备做 alb 的节点上打上标签。

```replicas=1```                            ####若部署高可用的 alb，准备部署几个实例，就改为几，若非高可用 alb，不用修改。

```address=1.1.1.1```                       ####若部署高可用的 alb，这里改为 alb 的 vip，若非高可用，改为部署 alb 机器的实际 ip。


  ```
  registry=$(docker info |grep 60080  |tr -d ' ')

  helm install stable/alauda-alb2 --name alauda-alb2  --namespace alauda-system --set replicas=$replicas --set loadbalancerName=alb2  --set global.registry.address=$registry --set address=$address   --set namespace=alauda-system
  ```


### harbor、jenkins、gitlab 部署

若使用 pvc 方式部署以下插件。需要事先准备好对应的 pvc，pvc 默认我们提供的是对接 cephfs，因此需要先确保集群对接了 ceph，然后创建对应的 pvc。

```
cat <<EOF > cephfs.yaml
   
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: claim1                 ###  对应 pvc 的名字
  namespace: default
  annotations:
    volume.beta.kubernetes.io/storage-class: cephfs
    volume.beta.kubernetes.io/storage-provisioner: ceph.com/cephfs
  finalizers:
    - kubernetes.io/pvc-protection
spec:
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 1Gi              ###对应 pvc 的大小
  volumeMode: Filesystem
EOF
   
kubectl create -f cephfs.yaml
```

#### 部署 jenkins

jenkins 的部署方式有以下四种，我们推荐在业务集群部署 jenkins，且最好以 pvc 的方式部署。

* 在 global 集群以本地目录的方式部署 jenkins。

	```
	NODE_NAME="acp2-master-1"            ###需要修改为集群中实际存放jenkins数据的某个节点的hostname
	path="/root/alauda/jenkins"          ###默认数据目录为/root/alauda/jenkins，若有需要可更改。
	password="Jenkins12345"              ###默认密码为Jenkins12345，若有需要可更改
	REGISTRY=$(docker info |grep 60080  |tr -d ' ')
	helm install stable/jenkins --name jenkins --namespace default \
	    --set global.registry.address=${REGISTRY} \
	    --set Master.ServiceType=NodePort \
	    --set Master.NodePort=32001 \
	    --set Master.AdminPassword="$password" \
	    --set Persistence.Enabled=false \
	    --set Persistence.Host.NodeName=${NODE_NAME} \
	    --set Persistence.Host.Path=$path\
	    --set AlaudaACP.Enabled=true \
	    --set alaudapipelineplugin.consoleURL="" \
	    --set alaudapipelineplugin.apiEndpoint="" \
	    --set alaudapipelineplugin.apiToken="" \
	    --set alaudapipelineplugin.account="" \
	    --set alaudapipelineplugin.spaceName="" \
	    --set alaudapipelineplugin.clusterName="" \
	    --set alaudapipelineplugin.namespace="" \
	    --set AlaudaDevOpsCredentialsProvider.globalNamespaces="global-credentials\,alauda-system" \
	```

* 在 global 以 pvc 的方式部署 jenkins。

	```
	password="Jenkins12345"              ###默认密码为 Jenkins12345，若有需要可更改
	pvc_name="jenkinspvc"                ###默认 pvc 的名字为 jenkinspvc，需要事先在 default 命名空间下准备好这个 pvc，可更改，但也需要事先创建好对应的 pvc。
	REGISTRY=$(docker info |grep 60080  |tr -d ' ')
	helm install stable/jenkins --name jenkins --namespace default \
	    --set global.registry.address=${REGISTRY} \
	    --set Master.ServiceType=NodePort \
	    --set Master.NodePort=32001 \
	    --set Master.AdminPassword="$password" \
	    --set Persistence.Enabled=true \
	    --set Persistence.ExistingClaim="$pvc_name" \
	    --set AlaudaACP.Enabled=true \
	    --set alaudapipelineplugin.consoleURL="" \
	    --set alaudapipelineplugin.apiEndpoint="" \
	    --set alaudapipelineplugin.apiToken="" \
	    --set alaudapipelineplugin.account="" \
	    --set alaudapipelineplugin.spaceName="" \
	    --set alaudapipelineplugin.clusterName="" \
	    --set alaudapipelineplugin.namespace="" \
	    --set AlaudaDevOpsCredentialsProvider.globalNamespaces="global-credentials\,alauda-system" \
	```

* 在业务集群以本地路径的方式部署 jenkins。

	```
	NODE_NAME="acp2-master-1"            ###需要修改为集群中实际存放 jenkins 数据的某个节点的 hostname
	global_vip="1.1.1.1"                 ###需要修改为 global 机器 apiserver 使用的 vip 的地址，这个值可在global集群任意一个master节点执行cat /etc/kubernetes/kubelet.conf  | grep server: | grep -oE '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}'     命令获得
	path="/root/alauda/jenkins"          ###默认数据目录为/root/alauda/jenkins，若有需要可更改。
	password="Jenkins12345"              ###默认密码为Jenkins12345，若有需要可更改
	REGISTRY=$(docker info |grep 60080  |tr -d ' ')
	TOKEN=       ### 如何获取token，到devops-apiserver所在集群（一般为global集群）执行：echo $(kubectl get secret -n alauda-system $(kubectl get secret -n alauda-system  | grep devops-apiserver-token |awk '{print $1}') -o jsonpath={.data.token} |base64 -d)
	helm install stable/jenkins --name jenkins --namespace default \
	    --set global.registry.address=${REGISTRY} \
	    --set Master.ServiceType=NodePort \
	    --set Master.NodePort=32001 \
	    --set Master.AdminPassword="$password" \
	    --set Persistence.Enabled=false \
	    --set Persistence.Host.NodeName=${NODE_NAME} \
	    --set Persistence.Host.Path=$path\
	    --set AlaudaACP.Enabled=false \
	    --set alaudapipelineplugin.consoleURL="" \
	    --set alaudapipelineplugin.apiEndpoint="" \
	    --set alaudapipelineplugin.apiToken="" \
	    --set alaudapipelineplugin.account="" \
	    --set alaudapipelineplugin.spaceName="" \
	    --set alaudapipelineplugin.clusterName="" \
	    --set alaudapipelineplugin.namespace="" \
	    --set AlaudaDevOpsCredentialsProvider.globalNamespaces="global-credentials\,alauda-system\,kube-system" \
	    --set AlaudaDevOpsCluster.Cluster.masterUrl="https://$global_vip:6443" \
	    --set AlaudaDevOpsCluster.Cluster.token=${TOKEN} \
	    --set Erebus.URL="https://$global_vip:443/kubernetes" \
	```

* 在业务集群以 pvc 的方式部署 jenkins。

	```
	global_vip="1.1.1.1"                 ###需要修改为global机器apiserver使用的vip的地址，这个值可在global集群任意一个master节点执行cat /etc/kubernetes/kubelet.conf  | grep server: | grep -oE '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}'     命令获得
	password="Jenkins12345"              ###默认密码为Jenkins12345，若有需要可更改
	pvc_name="jenkinspvc"                ###默认pvc的名字为jenkinspvc，需要事先在default命名空间下准备好这个pvc，可更改，但也需要事先创建好对应的pvc。
	REGISTRY=$(docker info |grep 60080  |tr -d ' ')
	helm install stable/jenkins --name jenkins --namespace default \
	    --set global.registry.address=${REGISTRY} \
	    --set Master.ServiceType=NodePort \
	    --set Master.NodePort=32001 \
	    --set Master.AdminPassword="$password" \
	    --set Persistence.Enabled=true \
	    --set Persistence.ExistingClaim="$pvc_name" \
	    --set AlaudaACP.Enabled=false \
	    --set alaudapipelineplugin.consoleURL="" \
	    --set alaudapipelineplugin.apiEndpoint="" \
	    --set alaudapipelineplugin.apiToken="" \
	    --set alaudapipelineplugin.account="" \
	    --set alaudapipelineplugin.spaceName="" \
	    --set alaudapipelineplugin.clusterName="" \
	    --set alaudapipelineplugin.namespace="" \
	    --set AlaudaDevOpsCredentialsProvider.globalNamespaces="global-credentials\,alauda-system\,kube-system" \
	    --set AlaudaDevOpsCluster.Cluster.masterUrl="https://$global_vip:6443" \
	    --set Erebus.URL="https://$global_vip:443/kubernetes" \
	```

	部署成功后到 Jenkins 修改配置访问集群任意一个节点 ip+32001 端口，访问 jenkins 页面，用户名：admin   密码：Jenkins12345。

	单击 **系统管理->系统设置**
  
    a. Alauda Jenkins Sync 里***启用*** 按钮勾选，添加 ***Jenkins 服务名称***，例如：jenkinstest，名称不能和别的已经部署的 jekins 服务名称一样(比如：global 里的 jenkins)，界面集成的时候必须与这个名字一致。
  
    b. 到 Kubernetes Cluster Configuration 下的 Credentials，单击添加，添加一个 serviceaccount token （类型选择 secret text， Secret 位置输入token， ID 输入任意凭据名称（例如：test-token））。<br>如何获取 token？到 devops-apiserver 所在集群（一般为 global 集群）执行命令行：<br>`echo $(kubectl get secret -n alauda-system $(kubectl get secret -n alauda-system  | grep devops-apiserver-token |awk '{print $1}') -o jsonpath={.data.token} |base64 -d)`。<br>添加之后。Credentials 选择刚才创建的 secret。
  
    c. 都配置成功后，在 Kubernetes Cluster Configuration 下单击 [Test Connection] 测试连接，提示 `Connect to  succeed` 后，单击保存。


#### 部署 gitlab


 部署 gitlab 有以下两种方式，推荐部署在业务集群，且以 pvc 的方式部署。

* 以本地路径的方式部署

	```
	REGISTRY=$(docker info |grep 60080  |tr -d ' ')
	NODE_NAME=acp2-master-1      ###需要修改为选择部署gialab的节点的hostname
	NODE_IP="1.1.1.1"            ###这个ip为gitlab的访问地址，需要修改为部署集群中任意master节点一个节点的实际ip
	potal_path="/root/alauda/gitlab/portal"         ###potal的数据目录，一般不需要修改，若有规划，可修改为别的目录
	database_path="/root/alauda/gitlab/database"    ###database的目录，一般不需要修改，若有规划，可修改为别的目录
	redis_path="/root/alauda/gitlab/redis"          ###redis的目录，一般不需要修改，若有规划，可修改为别的目录
	helm install stable/gitlab-ce --name gitlab-ce --namespace default \
	    --set global.registry.address=${REGISTRY} \
	    --set portal.debug=true \
	    --set gitlabHost=${NODE_IP} \
	    --set gitlabRootPassword=Gitlab12345 \
	    --set service.type=NodePort \
	    --set service.ports.http.nodePort=31101 \
	    --set service.ports.ssh.nodePort=31102 \
	    --set service.ports.https.nodePort=31103 \
	    --set portal.persistence.enabled=false \
	    --set portal.persistence.host.nodeName=${NODE_NAME} \
	    --set portal.persistence.host.path="$potal_path" \
	    --set portal.persistence.host.nodeName="${NODE_NAME}" \
	    --set database.persistence.enabled=false \
	    --set database.persistence.host.nodeName=${NODE_NAME} \
	    --set database.persistence.host.path="$database_path" \
	    --set database.persistence.host.nodeName="${NODE_NAME}" \
	    --set redis.persistence.enabled=false \
	    --set redis.persistence.host.nodeName=${NODE_NAME} \
	    --set redis.persistence.host.path="$redis_path" \
	    --set redis.persistence.host.nodeName="${NODE_NAME}" \
	```

* pvc 的方式部署 gitlab

	```
	NODE_IP=1.1.1.1   ###此参数为部署时指定的访问地址，写当前集群中任意一个master节点的ip即可
	portal_pvc="portalpvc"          ###默认pvc的名字为portalpvc，需要事先在default命名空间下准备好这个pvc，可更改，但也需要事先创建好对应的pvc。
	database_pvc="databasepvc"      ###默认pvc的名字为databasepvc，需要事先在default命名空间下准备好这个pvc，可更改，但也需要事先创建好对应的pvc。
	redis_pvc="redispvc"           ###默认pvc的名字为redispvc，需要事先在default命名空间下准备好这个pvc，可更改，但也需要事先创建好对应的pvc。
	REGISTRY=$(docker info |grep 60080  |tr -d ' ')
	helm install stable/gitlab-ce --name gitlab-ce --namespace default \
	    --set global.registry.address=${REGISTRY} \
	    --set portal.debug=true \
	    --set gitlabHost=${NODE_IP} \
	    --set gitlabRootPassword=Gitlab12345 \
	    --set service.type=NodePort \
	    --set service.ports.http.nodePort=31101 \
	    --set service.ports.ssh.nodePort=31102 \
	    --set service.ports.https.nodePort=31103 \
	    --set portal.persistence.enabled=true \
	    --set portal.persistence.existingClaim=$portal_pvc \
	    --set database.persistence.enabled=true \
	    --set database.persistence.existingClaim=$database_pvc \
	    --set redis.persistence.enabled=true \
	    --set redis.persistence.existingClaim=$redis_pvc \
	```

	部署完成之后，通过传入的部署 ip+31101 端口访问 gitlab，默认用户名为：root，密码为：Gitlab12345。

#### 部署 harbor 镜像仓库

  部署 harbor 有以下两种部署方式，推荐部署在业务集群，且以 pvc 的方式部署。

* 本地目录的方式部署 harbor

	```
	REGISTRY=$(docker info |grep 60080  |tr -d ' ')
	NODE_IP="1.1.1.1"          ###此参数为部署时指定的访问地址，写当前集群中任意一个master节点的ip即可
	NODE_NAME="acp-master-1"   ###需要修改为选择部署harbor节点的hostname
	HOST_PATH=/alauda/harbor   ###这个目录为harbor的数据目录路径，一般不需要修改，若有别的规划，可修改。
	harbor_password="Harbor12345"  ####harbor的密码，默认不需要修改，若有规划，可改
	db_password="Harbor4567"       ####harbor数据库的密码，默认不需要修改，若有规划，可改
	redis_password="Redis789"      ###harbor的redis的密码，默认不需要修改，若有规划，可改
	helm install --name harbor --namespace default stable/harbor \
	    --set global.registry.address=${REGISTRY} \
	    --set externalURL=http://${NODE_IP}:31104 \
	    --set harborAdminPassword=$harbor_password \
	    --set ingress.enabled=false \
	    --set service.type=NodePort \
	    --set service.ports.http.nodePort=31104 \
	    --set service.ports.ssh.nodePort=31105 \
	    --set service.ports.https.nodePort=31106 \
	    --set database.password=$db_password \
	    --set redis.usePassword=true \
	    --set redis.password=$redis_password \
	    --set database.persistence.enabled=false \
	    --set database.persistence.host.nodeName=${NODE_NAME} \
	    --set database.persistence.host.path=${HOST_PATH}/database \
	    --set redis.persistence.enabled=false \
	    --set redis.persistence.host.nodeName=${NODE_NAME} \
	    --set redis.persistence.host.path=${HOST_PATH}/redis \
	    --set chartmuseum.persistence.enabled=false \
	    --set chartmuseum.persistence.host.nodeName=${NODE_NAME} \
	    --set chartmuseum.persistence.host.path=${HOST_PATH}/chartmuseum \
	    --set registry.persistence.enabled=false \
	    --set registry.persistence.host.nodeName=${NODE_NAME} \
	    --set registry.persistence.host.path=${HOST_PATH}/registry \
	    --set jobservice.persistence.enabled=false \
	    --set jobservice.persistence.host.nodeName=${NODE_NAME} \
	    --set jobservice.persistence.host.path=${HOST_PATH}/jobservice \
	    --set AlaudaACP.Enabled=false \
	```

* pvc 的方式部署 harbor

	```
	REGISTRY=$(docker info |grep 60080  |tr -d ' ')
	NODE_IP="1.1.1.1"             ######此参数为部署时指定的访问地址，写当前集群中任意一个master节点的ip即可
	database_pvc=habordatabase   ###harbor数据库使用的pvc，需要事先在default下创建这个pvc
	redis_pvc=harborredis        ###harbor的redis使用的pvc，需要事先在default下创建这个pvc
	chartmuseum_pvc=harbormuseum   ###harbor使用的pvc，需要事先在default下创建这个pvc
	registry_pvc=harborregistry     ###harbor的registry使用的pvc，需要事先在default下创建这个pvc
	jobservice_pvc=harborjob        ###harbor使用的pvc，需要事先在default下创建这个pvc
	harbor_password="Harbor12345"  ####harbor的密码，默认不需要修改，若有规划，可改
	db_password="Harbor4567"       ####harbor数据库的密码，默认不需要修改，若有规划，可改
	redis_password="Redis789"      ###harbor的redis的密码，默认不需要修改，若有规划，可改
	helm install --name harbor --namespace default stable/harbor \
	    --set global.registry.address=${REGISTRY} \
	    --set externalURL=http://${NODE_IP}:31104 \
	    --set harborAdminPassword=$harbor_password \
	    --set ingress.enabled=false \
	    --set service.type=NodePort \
	    --set service.ports.http.nodePort=31104 \
	    --set service.ports.ssh.nodePort=31105 \
	    --set service.ports.https.nodePort=31106 \
	    --set database.password=$db_password \
	    --set redis.usePassword=true \
	    --set redis.password=$redis_password \
	    --set database.persistence.enabled=true \
	    --set database.persistence.existingClaim=${database_pvc} \
	    --set redis.persistence.enabled=true \
	    --set redis.persistence.existingClaim=${redis_pvc} \
	    --set chartmuseum.persistence.enabled=true \
	    --set chartmuseum.persistence.existingClaim=${chartmuseum_pvc} \
	    --set registry.persistence.enabled=true \
	    --set registry.persistence.existingClaim=${registry_pvc} \
	    --set jobservice.persistence.enabled=true \
	    --set jobservice.persistence.existingClaim=${jobservice_pvc} \
	    --set AlaudaACP.Enabled=false \
	```

	部署完成后通过传入的部署 ip+31104 端口访问 harbor。默认用户名为：admin，密码为：Harbor12345。

### （可选）业务集群部署 tapp 【若客户没有 tapp 需求，可不部署】

```
registry=$(docker info |grep 60080  |tr -d ' ')
helm install --name tapp-controller --namespace alauda-system --set global.registry.address=$registry  stable/tapp-controller
```

 * metrics-server 部署【devops 自动扩缩容需要用到这个组件】

	```
	apiVersion: extensions/v1beta1
	kind: Deployment
	metadata:
	  name: metrics-server
	  namespace: alauda-system
	  labels:
	    k8s-app: metrics-server
	spec:
	  selector:
	    matchLabels:
	      k8s-app: metrics-server
	  template:
	    metadata:
	      name: metrics-server
	      labels:
	        k8s-app: metrics-server
	    spec:
	      containers:
	      - name: metrics-server
	        image: index.alauda.cn/alaudak8s/metrics-server-amd64:v0.3.1            ###需要更换镜像地址为私有环境镜像仓库地址
	        imagePullPolicy: IfNotPresent
	        command:
	        - /metrics-server
	        - --kubelet-preferred-address-types=InternalIP
	        - --kubelet-insecure-tls
	        volumeMounts:
	        - name: tmp-dir
	          mountPath: /tmp
	      volumes:
	      # mount in tmp so we can safely use from-scratch images and/or read-only containers
	      - name: tmp-dir
	        emptyDir: {}
	 
	--
	apiVersion: v1
	kind: Service
	metadata:
	  name: metrics-server
	  namespace: alauda-system
	  labels:
	    kubernetes.io/name: "Metrics-server"
	spec:
	  selector:
	    k8s-app: metrics-server
	  ports:
	  - port: 443
	    protocol: TCP
	    targetPort: 443
	--
	apiVersion: apiregistration.k8s.io/v1beta1
	kind: APIService
	metadata:
	  name: v1beta1.metrics.k8s.io
	spec:
	  service:
	    name: metrics-server
	    namespace: alauda-system
	  group: metrics.k8s.io
	  version: v1beta1
	  insecureSkipTLSVerify: true
	  groupPriorityMinimum: 100
	  versionPriority: 100
	```


 <div STYLE="page-break-after: always;"></div>
# 第五部分 平台配置及测试

## 平台访问地址及登录用户名、密码

### 平台访问地址

<mark>all in one 部署方案</mark>和客户没有提供域名的<mark> poc 部署方案</mark> 的访问地址：

* 没有使用 `--use-http` 参数的平台访问地址：`https://<init 节点 ip>` 

* 使用了 `--use-http` 参数的平台访问地址：`http://<init 节点 ip>`

<mark>正式生产环境部署方案</mark>和客户提供域名的<mark> poc 部署方案</mark> 的访问地址：

* 没有使用 `--use-http` 参数的平台访问地址：`https://<--domain-name 参数指定的地址>` 

* 使用了 `--use-http` 参数的平台访问地址：`http://<--domain-name 参数指定的地址>`

### 平台登录权限

用户名：`--root-username` 参数指定的值，默认为 admin@cpaas.io。

密码：password

## 部署成功后操作

### 备份etcd 数据

1. 拷贝安装目录下的 backup_recovery.sh 和 function.sh 脚本到每一个 master 服务器的 `/tmp` 下。

2. 在每一台 master 上，执行如下命令：

	```
	cd /tmp
	bash backup_recovery.sh run_function=back_k8s_file
	```


### 添加 gc 脚本

将安装目录下的 gc.sh 脚本 cp 到各个服务器/root 目录下，然后为每个服务器添加 crontab ，每天凌晨运行一次即可，用于清理系统日志、本机没有容器使用的镜像和处在 Terminating、Evicted、Completed 这三种状态一天以上的 pod。
执行如下命令：

`echo '1 2 * * * bash /tmp/gc.sh' | crontab`

**注意**： 上述命令会将原来的 crontab 全部删掉，建议使用它 crontab -e 命令添加。

## 平台功能测试

请参考《TKE for Alauda Container Platfrom POC 测试用例.docx》。

## 添加监控、告警

<div STYLE="page-break-after: always;"></div>
# 第六部分 FAQ

## 平台部署错误

* 安装 chart 失败：

	**提示信息**： 'install chart error .......'，安装日志里会显示出错的 chart 名字。
	
	**出错原因**： 脚本通过 helm install 命令部署 chart 出错，详细日志会保存到第一台 master 节点的/cpaas/chart_install.log 文件内。
	
	**错误处理流程**：首先登录到第一台 master 节点上，执行 `helm list -a` 命令检查 chart ，发现错误的 chart 后，执行`helm delete --purge <chart name>` 删掉 chart ，然后执行`bash /tmp/install_chart.sh` 继续安装 chart，如果还是不成功，建议清空环境重新部署。依旧出错，请反馈给运维。
  
* 对接 global 集群失败：
  
	**提示信息**：'get global token Repeat 10 times to get global token failed'，之后对接 global 集群也会失败，提示'access global region error'。
	
	**出错原因**：这种情况一般是chart 安装失败，global 的 chart 没起来，或者因为资源不够、global 集群的服务器速度太慢，获取 token 的时候 global 组件还未正常运行，所以获取token 失败了，没有正确的 token，global 没起来调用 api 自然失败。
	
	**错误处理流程**：chart 安装失败，请参照上面的方法处理，等待所有 global 组件的 pod 状态都正常后，去 init 节点上，执行`bash /tmp/get_token.sh` 获取 token （一个很长的字符串)，然后编辑/tmp/add_global_region.sh 这个脚本，在`Bearer` 字符后，把获取到的 token 粘贴进去，然后执行这个脚本即可。如下图所示。
  
	<img src="images/对接 global 集群错误.png" style="zoom:50%;" />



## 平台功能错误

请参考《TKE for Alauda Container Platfrom v2.4 运维手册》。








单击