#  目录

1. 关于本文档
	* 文档受众
	* 文档目的
	* 修订记录
2. 升级过程
	* 升级前准备
	* 平台升级
	* 集群升级
	* 回滚

<div STYLE="page-break-after: always;"></div>
# 第一部分  前言

本文档介绍了如何将平台版本从 v2.3 升级到 v2.4。

## 读者对象

《TKE for Alauda Container Platform 升级文档》适用于具备基本的 linux、容器、Kubernetes 知识，想要安装和配置平台的用户。包括：

* 实施顾问和平台管理员

* 对平台进行维护的运维人员

* 负责整个项目生命周期的项目经理

## 文档目的

平台运维工程师依据本文档升级平台版本。


## 修订记录

| 文档版本 | 发布日期   | 修订内容                                      |
| -------- | ---------- | --------------------------------------------- |
| V1.0| 2019-10-31 | 第一版，适用于 TKE for Alauda Container Platform 私有部署版本的 v2.3 升级至 v2.4。 |
| v1.1| 2019-11-25 | 修复备份过程的错误。  |
| v1.2| 2019-11-27 | 增加回滚配置信息。|


<div STYLE="page-break-after: always;"></div>
# 第二部分  升级过程

## 升级前准备

### 备份数据

* 执行位置：global 集群的 master 和业务服务集群的 master 节点。
* 操作步骤：首先将安装目录下的 “backup_recovery.sh” 脚本复制到 master 节点上。
* 执行的命令： 
	
	```
	mkdir /cpaas/back && cp -r /var/lib/etcd/ /cpaas/back && cp -r /etc/kubernetes/ /cpaas/back
	bash ./backup_recovery.sh run_function=back_k8s_file
	```
	
	命令作用：备份集群的 etcd。


## 平台升级

### 下载 v2.4 的安装包

* 执行位置：init 节点
* 操作步骤：下载安装包并解压缩

### 上传 v2.4 的镜像和 chart 到 init 节点的私有仓库和 chart repo 内

* 执行位置：init 节点
* 操作步骤：进入 v2.4 的安装目录，执行 `bash ./upload-images-chart.sh`

  **注意**：执行过程需要一个小时左右，视 init 服务器 cpu 和硬盘速度不同，会缩短或延迟，注意安装目录所在分区的空间，上传之前至少要保证 10G 的空余空间。

### 更新 kaldr

* 执行位置：init 节点
* 操作步骤：执行如下命令

	```
	mkdir /cpaas
	REGISTRY_ENDPOINT=$(docker info |grep 60080  |tr -d ' ')
	for i in $(curl ${REGISTRY_ENDPOINT}/v2/alaudaorg/kaldr/tags/list 2>/dev/null | jq '.tags' | sed -e '1d' -e '$d' -e 's/"//g' -e 's/,//g'); do
	        docker pull ${REGISTRY_ENDPOINT}/alaudaorg/kaldr:$i >/dev/null 2>&1
	        echo $i $(date -d $(docker inspect ${REGISTRY_ENDPOINT}/alaudaorg/kaldr:$i -f '{{.Created}}') +%s)
	done | sort -k2n,2 | awk 'END{print $1}' > /tmp/dd
	KALDR_IMAGE=${REGISTRY_ENDPOINT}/alaudaorg/kaldr:$(cat /tmp/dd)
	docker rm -f yum
	mv -f /cpaas/.run_kaldr.sh /cpaas/.run_kaldr.sh.old
	cat /cpaas/.run_kaldr.sh.old | awk '{for(i=1;i<NF;i++){printf $i" "}print "'$KALDR_IMAGE'"}' >/cpaas/.run_kaldr.sh
	bash /cpaas/.run_kaldr.sh
	```

	执行完毕后，yum 容器会被重建，请检查是否重建。


### 停掉认证组件的 pod

* 执行位置：global 集群的第一个 master 上
* 操作步骤：执行如下命令
	
	```
	mkdir /cpaas/backup
	kubectl get cm -n alauda-system dex-configmap -o yaml > /cpaas/backup/dex-configmap.yaml.new
	kubectl scale deployment -n alauda-system auth-controller2 dex --replicas=0
	```

### 生成 install_values.yaml 文件

* 执行位置：global 集群的第一个 master 上
* 操作步骤：执行如下命令
	
	```
	mv /tmp/install_values.yaml /cpaas/backup/install_values.yaml.old
	helm get values dex >/cpaas/install_values.yaml
	```

### 创建 v2.4 需要的 cm，并刷新 repo

* 执行位置：global 集群的第一个 master 上
* 操作步骤：执行如下命令
	
	```
	kubectl create configmap acp-config -n alauda-system --from-file /cpaas/install_values.yaml
	helm repo update
	```

### 更新平台 chart

* 执行位置：global 集群的第一个 master 上
* 操作步骤：执行如下命令

	```
	for i in dex alauda-base kafka-zookeeper elasticsearch cert-manager nginx-ingress download-server alauda-container-platform alauda-devops alauda-cluster-base alauda-log-agent captain ; do helm upgrade $i stable/$i >>/cpaas/upgrade_chart.log || echo "upgrade $i error" ; sleep 10 ; done
	 
	for i in kube-prometheus prometheus-operator ; do helm upgrade $i stable/$i --timeout=3000 --wait --force >>/cpaas/upgrade_chart.log || echo "upgrade $i error" ; sleep 10 ; done
	```
	
	**注意**：如果 alauda-base 升级失败，执行以下命令：
	
	```
	helm rollback alauda-base  <版本号>
	helm status alauda-base | grep -A 10 Deployment  ##用这个命令找到 alauda-base 的所有 deploy ，然后kubectl delete deploy  删掉
	helm upgrade alauda-base stable/alauda-base
	```
	如果还是失败，执行以下命令：
	
	```
	helm fetch stable/alauda-base --untar
	cd alauda-base
	vim ./templates/archon/feature-gates/stages.yaml
	```
	这个文件里面的 3 个资源增加 annotations `"helm.sh/hook": post-install`，如图所示。
	
	<img src="images/升级 alauda-base.png" style="zoom:50%;" />
	
	
	删掉 alauda-base 所有的 deploy ，然后执行：
	
	```
	helm upgrade alauda-base . 
	```


### 更新 Service Mesh (业务集群升级)

* 执行位置：安装 Service Mesh  集群的第一个 master 上
* 操作步骤：执行如下命令
	
	```
	helm up
	helm fetch stable/asm-init --untar   #获取 asm-init chart
	kubectl -n alauda-system delete casemonitors.asm.alauda.io --all    #删除casemonitor老的资源
	cd asm-init
	helm template  .  -x templates/casemonitor.yaml  | kubectl apply -f -    #手工升级casemonitor crd
	```

* 执行位置：global 集群的第一个 master 上
* 操作步骤：查询 asm-init 的 helmrequest，将其版本信息，改成新版本的 chart 的版本，其中需要将 $$asm$$ 换成对应业务集群名字。

	```
	kubectl -n alauda-system  edit  hr asm-$$asm$$-asm-init
	kubectl -n alauda-system  edit  hr asm-$$asm$$-istio-init
	kubectl -n alauda-system  edit  hr asm-$$asm$$-jaeger-operator
	kubectl -n alauda-system  edit  hr asm-$$asm$$-alauda-service-mesh
	kubectl -n alauda-system  edit  hr asm-$$asm$$-istio
	```
	
	**示例**：修改 asm-asm-asm-init 的版本号信息。
	
	```
	apiVersion: app.alauda.io/v1alpha1
	kind: HelmRequest
	metadata:
	annotations:
	kubectl.kubernetes.io/last-applied-configuration: |
	{"apiVersion":"app.alauda.io/v1alpha1","kind":"HelmRequest","metadata":{"annotations":{},"name":"asm-asm-asm-init","namespace":"alauda-system"},"spec":{"chart":"release/asm-init","namespace":"alauda-system","releaseName":"asm-asm-asm-init","values":{"install":{"mode":"global"}},"version":"v2.2.9"}}
	creationTimestamp: "2019-10-17T07:37:28Z"
	finalizers:
	- captain.alauda.io
	generation: 1
	name: asm-asm-asm-init
	namespace: alauda-system
	resourceVersion: "94086381"
	selfLink: /apis/app.alauda.io/v1alpha1/namespaces/alauda-system/helmrequests/asm-asm-asm-init
	uid: f8a6e22e-f0b0-11e9-b758-ea3e455b3c6e
	spec:
	chart: release/asm-init
	namespace: alauda-system
	releaseName: asm-asm-asm-init
	values:
	install:
	mode: global
	version: v2.2.9   
	```

### 更新 Service Mesh (global)

* 执行位置：global 集群的第一个 master 上
* 操作步骤：执行如下命令

	```
	helm fetch stable/asm-init --untar
	cd asm-init/
	helm upgrade asm-init . --set install.mode=global
	helm upgrade global-asm stable/global-asm
	
	```

### 启动认证组件

* 执行位置：global 集群的第一个 master 上
* 操作步骤：执行如下命令

	```
	kubectl scale deployment -n alauda-system auth-controller2 dex --replicas=2
	```


### 安装 cpaas-monitor

* 执行位置：global 集群的第一个 master 上
* 操作步骤：执行如下命令

	```
	ACP_NAMESPACE=$(cat /cpaas/install_values.yaml | awk '/^  namespace:/{print $2}')
	REGISTRY_ENDPOINT=$(cat /cpaas/install_values.yaml | awk '/^    address:/{print $2}')
	helm install --debug --namespace=${ACP_NAMESPACE} --name=cpaas-monitor --set global.registry.address=${REGISTRY_ENDPOINT} stable/cpaas-monitor
	```

### 数据初始化

* 执行位置：global 集群的第一个 master 上
* 操作步骤：执行如下命令

	```
	kubectl exec -it -n $(kubectl get po --all-namespaces | grep courier | awk '{print $1" "$2}' | head -n 1) python /migration/migrate.py   #初始化 courier 数据
	kubectl exec -it -n $(kubectl get po --all-namespaces | grep morgans | awk '{print $1" "$2}' | head -n 1) python /morgans/migration/api/migrate.py   # 初始化 morgans 数据
	```


### 升级之后验证

* 执行位置：global 集群的第一个 master 上
* 操作步骤：执行命令 `helm list -a ` 检查 chart 版本是否更新到了 2.4。


## 集群升级

### 升级 alauda-cluster-base

* 执行位置：每一个集群的第一个 master 上
* 操作步骤：执行如下命令

	```
	helm upgrade alauda-cluster-base stable/alauda-cluster-base
	```


### nevermore 升级

* 执行位置：每一个集群的第一个 master 上
* 操作步骤：执行如下命令

	```
	helm upgrade alauda-log-agent stable/alauda-log-agent
	```

### ingress升级

* 执行位置：每一个集群的第一个 master 上
* 操作步骤：执行如下命令

	```
	helm upgrade nginx-ingress stable/nginx-ingress
	```

### 普罗米修斯升级

* 执行位置：每一个集群的第一个 master 上
* 操作步骤：执行如下命令

	```
	helm upgrade prometheus-operator stable/prometheus-operator
	sleep 30
	helm upgrade kube-prometheus stable/kube-prometheus
	```


### alb升级

* 执行位置：每一个集群的第一个 master 上
* 操作步骤：执行如下命令

	```
	helm upgrade alauda-alb2 stable/alauda-alb2 --reuse-values --set global.images.alb2.tag=v2.4.0
	```
	**注意**：需要修改 alb 的 yaml ，alb 的 yaml 的 nodeselector 要手动的重新加上 `alb2:true` 标签。

**注意 **

v2.3 升级到 v2.4 之后，建议不要升级 alb。



###  Tapp升级

* 执行位置：每一个集群的第一个 master 上
* 操作步骤：执行如下命令

	```
	helm upgrade tapp-controller stable/tapp-controller
	```

### gitlab-ce 升级

* 执行位置：每一个集群的第一个 master 上
* 操作步骤：执行如下命令

	```
	helm upgrade gitlab-ce  stable/gitlab-ce
	```

### harbor 升级

* 执行位置：每一个集群的第一个 master 上
* 操作步骤：执行如下命令

	```
	helm upgrade  harbor stable/harbor
	```


### jenkins 升级

* 执行位置：每一个集群的第一个 master 上
* 操作步骤：执行如下命令

	```
	helm upgrade  jenkins stable/jenkins
	```

### 注意事项

#### devops-controller 中的 toolx 下的 yaml 更新问题

如果 Kubernetes 集群中已经存在相应的 tooltype 的资源，toolx 下的 yaml 更新可能不会更新 Kubernetes 集群中已经存在的 resource。

在 v2.4 中，更新的解决方案是：在 Kubernetes 集群中删除想要更新的 tooltype 的 resource 后重启 devops-controller。

示例：更新 name 为 jira 的 tooltype

1. 执行命令：`kubectl delete tooltype jira`

2. 执行命令重启 devops-controller。

	```
	DC=`kubectl -n alauda-system get pod |grep devops-controller|wc -l`&&kubectl -n alauda-system scale deploy/devops-controller --replicas=0 && kubectl -n alauda-system scale deploy/devops-controller --replicas=$DC
	```

## 回滚

1. 停掉所有 master 节点的 kubelet 服务。

	**执行命令的环境：** 要恢复 Kubernetes 集群的所有 master 节点上
	
	**执行的命令：** ```systemctl stop kubelet```

2. 删掉 etcd 容器。

	**执行命令的环境：** 要恢复 Kubernetes 集群的所有 master 节点上
	
	**执行的命令：** ```docker rm -f $(docker ps -a | awk '/_etcd/{print $NF}')```
	
	**命令的结果：** 所有 etcd 的容器都被删除。

3. 判断 etcdctl 是否存在第一台 master 节点上，一般执行 **backup_recovery.sh** 这个备份 etcd 的脚本，会自动将 etcdctl 拷贝到 `/usr/bin/etcdctl` ，如果不存在，需要自行手动拷贝出来。

	**执行命令的环境：** 要恢复 Kubernetes 集群的第一台 master 节点上
	
	**执行的命令：** ```whereis etcdctl```
	
	**命令的结果：** 应该打印处 etcdctl 的路径，如果没有就是错的。

4. 通过备份的快照恢复 etcd。

	**执行命令的环境：** 要恢复 Kubernetes 集群的第一台 master 节点上
	
	**执行的命令：** 

	```
	mkdir /tmp/dd ; cd /tmp/dd
	ETCD_SERVER=($(kubectl get no -o wide | awk '{if ($3 == "master")print $6}'))
	snapshot_db=<备份时，导出的 etcd 快照文件名，必须是绝对路径>
	for i in ${ETCD_SERVER[@]}
	do
	    export ETCDCTL_API=3
	    etcdctl snapshot restore ${snapshot_db} \
	    --cert=/etc/kubernetes/pki/etcd/server.crt \
	    --key=/etc/kubernetes/pki/etcd/server.key \
	    --cacert=/etc/kubernetes/pki/etcd/ca.crt \
	    --data-dir=/var/lib/etcd \
	    --name ${i} \
	    --initial-cluster ${ETCD_SERVER[0]}=https://${ETCD_SERVER[0]}:2380,${ETCD_SERVER[1]}=https://${ETCD_SERVER[1]}:2380,${ETCD_SERVER[2]}=https://${ETCD_SERVER[2]}:2380 \
	    --initial-advertise-peer-urls https://$i:2380 && \
	mv /var/lib/etcd/ etcd_$i
	done
	```
	
	**命令的结果：** 会生成 etcd_<ip 地址> 这样的三个目录，将这三个目录下的 member 目录拷贝到对应ip的服务器的 `/root` 内。

5. 迁移恢复的数据。

	**执行命令的环境：** 要恢复 Kubernetes 集群的所有 master 节点上
	
	**执行的命令：** 
	
	```
	mv /var/lib/etcd/member /cpaas/backup
	mv /root/var/lib/etcd/member  /var/lib/etcd
	```
	
	**命令的结果：** 会把 etcd 的数据迁移到备份目录下，然后将上一步生成的目录拷贝到 `/var/lib/etcd` 里。

6.  启动 etcd。

	**执行命令的环境：** 要恢复 Kubernetes 集群的所有 master 节点上
	
	**执行的命令：** ```systemctl start kubelet```
	
	**命令的结果：** kubelet 服务会启动，kubelet 会自动创建 etcd 的 pod，这个时候执行 ```docker ps -a | grep etcd``` 可以找到 etcd 容器。

