#  目录

1. 关于本文档
	* 文档受众
	* 文档目的
	* 修订记录
2. 升级过程
	* 升级前准备
	* 平台升级
	* 集群升级
	* 回滚

<div STYLE="page-break-after: always;"></div>
# 第一部分  前言

本文档介绍了如何将平台版本从 v2.4 升级到 v2.5。

## 读者对象

本文档适用于具备基本的 linux、容器、Kubernetes 知识，想要安装和配置平台的用户。包括：

* 实施顾问和平台管理员

* 对平台进行维护的运维人员

* 负责整个项目生命周期的项目经理

## 文档目的

平台运维工程师依据本文档升级平台版本。


## 修订记录

| 文档版本 | 发布日期   | 修订内容                                      |
| -------- | ---------- | --------------------------------------------- |
|    | 2019-10-31 | 第一版，适用于 TKE for Alauda Container Platform 私有部署版本的 v2.4 升级至 v2.5。 |


<div STYLE="page-break-after: always;"></div>
# 第二部分  升级过程

## 升级前准备

### 备份数据

* 执行位置：global 集群的 master 和业务服务集群的 master 节点。
* 操作步骤：首先将安装目录下的 “backup_recovery.sh” 脚本复制到 master 节点上。
* 执行的命令： 
	
	```
	mkdir /cpaas/backup && cp -r /var/lib/etcd/ /cpaas/backup && cp -r /etc/kubernetes/ /cpaas/backup
	bash ./backup_recovery.sh run_function=back_k8s_file
	```
* 命令作用：备份集群的 etcd


## 平台升级

### 下载 v2.5 的安装包

* 执行位置：init 节点
* 操作步骤：下载安装包并解压缩

### 上传 v2.5 的镜像和 chart 到 init 节点的私有仓库和 chart repo 内

* 执行位置：init 节点
* 操作步骤：进入 v2.5 的安装目录，执行 `bash ./upload-images-chart.sh`

**注意**：执行过程需要一个小时左右，视 init 服务器 cpu 和硬盘速度不同，会缩短或延迟，注意安装目录所在分区的空间，上传之前至少要保证 10G 的空余空间。

### 更新 kaldr

* 执行位置：init 节点
* 操作步骤：执行如下命令

	```
	mkdir /cpaas
	REGISTRY_ENDPOINT=$(docker info |grep 60080  |tr -d ' ')
	for i in $(curl ${REGISTRY_ENDPOINT}/v2/alaudaorg/kaldr/tags/list 2>/dev/null | jq '.tags' | sed -e '1d' -e '$d' -e 's/"//g' -e 's/,//g'); do
	        docker pull ${REGISTRY_ENDPOINT}/alaudaorg/kaldr:$i >/dev/null 2>&1
	        echo $i $(date -d $(docker inspect ${REGISTRY_ENDPOINT}/alaudaorg/kaldr:$i -f '{{.Created}}') +%s)
	done | sort -k2n,2 | awk 'END{print $1}' > /tmp/dd
	KALDR_IMAGE=${REGISTRY_ENDPOINT}/alaudaorg/kaldr:$(cat /tmp/dd)
	docker rm -f yum
	mv -f /cpaas/.run_kaldr.sh /cpaas/.run_kaldr.sh.old
	cat /cpaas/.run_kaldr.sh.old | awk '{for(i=1;i<NF;i++){printf $i" "}print "'$KALDR_IMAGE'"}' >/cpaas/.run_kaldr.sh
	bash /cpaas/.run_kaldr.sh
	```

	执行完毕后，yum 容器会被重建，请检查是否重建。


### 停掉认证组件的 pod

* 执行位置：global 集群的第一个 master 上
* 操作步骤：执行如下命令
	```
	mkdir /cpaas/backup
	kubectl get cm -n alauda-system dex-configmap -o yaml > /cpaas/backup/dex-configmap.yaml.new
	kubectl scale deployment -n alauda-system auth-controller2 dex --replicas=0
	```

### 生成 install_values.yaml 文件

* 执行位置：global 集群的第一个 master 上
* 操作步骤：执行如下命令
	```
	mv /tmp/install_values.yaml /cpaas/backup/install_values.yaml.old
	helm get values dex >/cpaas/install_values.yaml
	```

### 修改 install_values.yaml 文件

* 执行位置：global 集群的第一个 master 上
* 操作步骤：执行如下命令
	
	```
	vim /cpaas/install_values.yaml
	```

	修改成如下内容：
	
	```
	kafka:
	  ha_switch: true                                          #这个不动
	  zk_host: 10.0.128.132,10.0.129.50,10.0.129.31  # 新增，将原来zk_connect 的值填到这里
	  zk_connect: cpaas-zookeeper                     # zk_connect 的值改成这个
	  retention_hours: 2                                      # 新增
	  roll_hours: 1                                               # 新增
	```

	找到 `consoledevops:` 及之后的两行，这这三行复制下来，添加到其之后，如图：
	
	<img src="images/up_add_aml.png" style="zoom:50%;" />

	
	检查 `kafkaHost: cpaas-kafka:9092  # 这个key 的值是不是 cpaas-kafka:9092`。
	
	并增加以下内容： 
	
	```
	clusterRegistry:
	   supportedVersions: v1.13,v1.14
	devopsApiServer:
	   etcdServer: https://etcd.kube-system:2379,https://etcd.kube-system:2379,https://etcd.kube-system:2379   
	```

### 刷新 repo 并更新平台 chart

* 执行位置：global 集群的第一个 master 上
* 操作步骤：执行如下命令

	```
	helm repo update
	helm upgrade kafka-zookeeper stable/kafka-zookeeper -f /cpaas/install_values.yaml
	for i in dex elasticsearch cert-manager nginx-ingress download-server alauda-container-platform alauda-devops dashboard alauda-log-agent captain; do helm upgrade $i stable/$i >>/cpaas/upgrade_chart.log && echo "upgrade $i success" || echo "upgrade $i error" ; sleep 10 ; done
	 
	for i in kube-prometheus prometheus-operator ; do helm upgrade $i stable/$i --timeout=3000 --wait --force >>/cpaas/upgrade_chart.log && echo "upgrade $i success" || echo "upgrade $i error" ; sleep 10 ; done
	```
	
	**注意**：alauda-base 单独升级，执行以下命令：

	```
	mkdir /tmp/ddd ； cd /tmp/ddd
	helm fetch stable/alauda-base --untar
	cd alauda-base
	sed -i -e '309,350s/crd-install/pre-upgrade/g' templates/auth/crds.yaml
	cat <<EOF >/tmp/replace-fr.sh
	#!/bin/bash
	files=\$(ls -alt ./templates/auth/function-resources/|awk '{print \$9}')
	for file in \$files
	do
	  sed '6 i\ \ \ \ helm.sh/hook: post-upgrade' -i ./templates/auth/function-resources/\$file
	  echo replace \$file
	done
	EOF
	cat <<EOF >/tmp/replace-rt.sh
	#!/bin/bash
	files=\$(ls -alt ./templates/auth/role-templates/|awk '{print \$9}')
	for file in \$files
	do
	  sed '5 i\ \ \ \ helm.sh/hook: post-upgrade' -i ./templates/auth/role-templates/\$file
	  echo replace \$file
	done
	EOF
	bash /tmp/replace-fr.sh ; bash /tmp/replace-rt.sh
	kubectl delete crd clusteralaudafeaturegates.alauda.io
	helm upgrade alauda-base .
	```

	**注意**：cpaas-monitor 单独升级，执行以下命令：
	
	```
	global_namespace=$(helm list -a | awk '/^cpaas-monitor /{print $NF}')
	helm upgrade cpaas-monitor --set global.namespace=${global_namespace} --set cluster.isGlobal='true' --set cluster.name=global --set cluster.prometheusName=kube-prometheus stable/cpaas-monitor
	
	```

### 更新 Service Mesh 暂时不支持 v2.4 至 v2.5 的升级方案

### 启动认证组件

* 执行位置：global 集群的第一个 master 上
* 操作步骤：执行如下命令

	```
	kubectl scale deployment -n alauda-system auth-controller2 dex --replicas=2
	```


### 数据初始化

* 执行位置：global 集群的第一个 master 上
* 操作步骤：执行如下命令

	```
	kubectl exec -it -n $(kubectl get po --all-namespaces | grep courier | awk '{print $1" "$2}' | head -n 1) python /migration/migrate.py   #初始化 courier 数据
	kubectl exec -it -n $(kubectl get po --all-namespaces | grep morgans | awk '{print $1" "$2}' | head -n 1) python /morgans/migration/api/migrate.py   # 初始化 morgans 数据
	```


### 升级之后验证

* 执行位置：global 集群的第一个 master 上
* 操作步骤：执行命令 `helm list -a ` 检查 chart 版本是否到了 2.5。


## 集群升级

### 升级 alauda-cluster-base

* 执行位置：每一个集群的第一个 master 上
* 操作步骤：执行如下命令

	```
	kubectl delete crd clusteralaudafeaturegates.alauda.io
	helm up
	helm upgrade alauda-cluster-base stable/alauda-cluster-base
	```


### nevermore 升级

* 执行位置：每一个集群的第一个 master 上
* 操作步骤：执行如下命令

	```
	helm upgrade alauda-log-agent stable/alauda-log-agent
	```
	
### dashboard 升级

* 执行位置：每一个集群的第一个 master 上
* 操作步骤：执行如下命令

	```
	helm upgrade dashboard stable/dashboard
	```

### cert-manager 升级

* 执行位置：每一个集群的第一个 master 上
* 操作步骤：执行如下命令

	```
	helm upgrade cert-manager stable/cert-manager
	```
	
### ingress升级

* 执行位置：每一个集群的第一个 master 上
* 操作步骤：执行如下命令

	```
	helm upgrade nginx-ingress stable/nginx-ingress
	```

### 普罗米修斯升级

* 执行位置：每一个集群的第一个 master 上
* 操作步骤：执行如下命令

	```
	helm upgrade prometheus-operator stable/prometheus-operator
	sleep 30
	helm upgrade kube-prometheus stable/kube-prometheus
	```


### alb升级

* 执行位置：平台 UI
* 操作步骤：执行如下命令

	1. 用管理员身份登录平台，单击左上角的图标切换产品，选择 Container Platform 。然后单击右上角的视图开关进入管理视图，如下图所示。
	
		<img src="images/升级 alb 1.png" style="zoom:50%;" />
	
	2. 单击左侧导航栏的资源管理，选择正确的 ns 和集群，找到 helmrequest 这个资源，单击更新，如下图所示。
	
		<img src="images/升级 alb 2.png" style="zoom:50%;" />
	
	3. 在 spec 下修改 version 这个 key，如果没有，添加 version。这个 key 的值就是你希望升级 alb 的目标版本，在集群的 master 下，执行 `helm search | grep alb` 就能找到最新的 alb 的版本，如下图所示。
	
		<img src="images/升级 alb 3.png" style="zoom:50%;" />
	
	4. 修改完毕后，点击右下角的更新，稍等片刻 alb 就开始更新了。
	
	5. 在集群上，执行 `kubectl get deploy -n <ns> <alb 名称> -o wide` 检查 alb 的 deploy 的状态，看看版本是否变化确认审计是否成功。


###  Tapp 升级

* 执行位置：每一个集群的第一个 master 上
* 操作步骤：执行如下命令

	```
	helm upgrade tapp-controller stable/tapp-controller
	```

### gitlab-ce 升级

* 执行位置：每一个集群的第一个 master 上
* 操作步骤：执行如下命令

	```
	helm upgrade gitlab-ce  stable/gitlab-ce
	```

### harbor 升级

* 执行位置：每一个集群的第一个 master 上
* 操作步骤：执行如下命令

	```
	helm upgrade  harbor stable/harbor
	```


### jenkins 升级

* 执行位置：每一个集群的第一个 master 上
* 操作步骤：执行如下命令

	```
	helm upgrade  jenkins stable/jenkins
	```

### 注意事项

#### devops-controller 中的 toolx 下的 yaml 更新问题

如果 Kubernetes 集群中已经存在相应的 tooltype 的资源，toolx 下的 yaml 更新可能不会更新 Kubernetes 集群中已经存在的 resource。

在 v2.4 中，更新的解决方案是：在 Kubernetes 集群中删除想要更新的 tooltype 的 resource 后重启 devops-controller。

示例：更新 name 为 jira 的 tooltype

1. 执行命令：`kubectl delete tooltype jira`

2. 执行命令重启 devops-controller。

	```
	DC=`kubectl -n alauda-system get pod |grep devops-controller|wc -l`&&kubectl -n alauda-system scale deploy/devops-controller --replicas=0 && kubectl -n alauda-system scale deploy/devops-controller --replicas=$DC
	```


## 回滚

1. 停掉所有 master 节点的 kubelet 服务。

	**执行命令的环境：** 要恢复 Kubernetes 集群的所有 master 节点上
	
	**执行的命令：** ```systemctl stop kubelet```

2. 删掉 etcd 容器。

	**执行命令的环境：** 要恢复 Kubernetes 集群的所有 master 节点上
	
	**执行的命令：** ```docker rm -f $(docker ps -a | awk '/_etcd/{print $NF}')```
	
	**命令的结果：** 所有 etcd 的容器都被删除。

3. 判断 etcdctl 是否存在第一台 master 节点上，一般执行 `backup_recovery.sh` 这个备份 etcd 的脚本，会自动将 etcdctl 拷贝到 `/usr/bin/etcdctl`，如果不存在，需要自行手动拷贝出来。

	**执行命令的环境：** 要恢复 Kubernetes 集群的第一台 master 节点上
	
	**执行的命令：** ```whereis etcdctl```
	
	**命令的结果：** 应该打印处 etcdctl 的路径，如果没有就是错的。

4. 通过备份的快照恢复 etcd。

	**执行命令的环境：** 要恢复 Kubernetes 集群的第一台 master 节点上
	
	**执行的命令：** 
	
	```
	mkdir /tmp/dd ; cd /tmp/dd
	ETCD_SERVER=($(kubectl get no -o wide | awk '{if ($3 == "master")print $6}'))
	snapshot_db=<备份时，导出的 etcd 快照文件名，必须是绝对路径>
	for i in ${ETCD_SERVER[@]}
	do
	    export ETCDCTL_API=3
	    etcdctl snapshot restore ${snapshot_db} \
	    --cert=/etc/kubernetes/pki/etcd/server.crt \
	    --key=/etc/kubernetes/pki/etcd/server.key \
	    --cacert=/etc/kubernetes/pki/etcd/ca.crt \
	    --data-dir=/var/lib/etcd \
	    --name ${i} \
	    --initial-cluster ${ETCD_SERVER[0]}=https://${ETCD_SERVER[0]}:2380,${ETCD_SERVER[1]}=https://${ETCD_SERVER[1]}:2380,${ETCD_SERVER[2]}=https://${ETCD_SERVER[2]}:2380 \
	    --initial-advertise-peer-urls https://$i:2380 && \
	mv /var/lib/etcd/ etcd_$i
	done
	```
	
	**命令的结果：** 会生成 `etcd_<ip 地址>` 这样的三个目录，将这三个目录下的 member 目录拷贝到对应ip的服务器的 `/root` 内。

5. 迁移恢复的数据。

	**执行命令的环境：** 要恢复 Kubernetes 集群的所有 master 节点上
	
	**执行的命令：** 
	
	```
	mv /var/lib/etcd/member /cpaas/backup
	mv /root/var/lib/etcd/member  /var/lib/etcd
	```
	
	**命令的结果：** 会把 etcd 的数据挪到备份目录下，然后将上一步生成的目录拷贝到 `/var/lib/etcd` 里。

6.  启动 etcd。

	**执行命令的环境：** 要恢复 Kubernetes 集群的所有 master 节点上
	
	**执行的命令：** ```systemctl start kubelet```
	
	**命令的结果：** kubelet 服务会启动，kubelet 会自动创建 etcd 的 pod，这个时候执行 ```docker ps -a | grep etcd``` 会找到 etcd 容器。

