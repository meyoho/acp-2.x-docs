#  目录
1. 关于本文档
	* 文档受众
	* 文档目的
	* 修订记录
2. 部署和配置简介
3. 部署过程概述
	* 平台部署架构图
	* 平台的部署流程图
4. 开始部署
	* 检查部署需求是否满足
	* 解决不满足的需求
	* 和 v2.4 相比部署差异
	* global 平台部署
	* 业务服务集群部署
	* 业务服务集群组件和第三方插件部署
5. 平台配置及测试
	* 平台访问地址及登录用户名、密码
	* 部署成功后操作
	* 平台功能测试
	* 添加监控、告警
6. FAQ
	* 部署错误
	* 运行错误

<div STYLE="page-break-after: always;"></div>
# 第一部分  前言

本文档介绍了如何安装和配置平台。

## 读者对象

本文档适用于具备基本的 linux、网络、存储、容器、Kubernetes 知识，想要安装和配置平台的用户，包括：

* 实施顾问和平台管理员

* 规划平台架构的售前工程师

* 负责整个项目生命周期的项目经理

## 文档目的

* 实施工程师依据本文档部署平台。

* 实施工程师依据本文档，配置平台参数。

* 平台管理员依据本文档测试平台功能。

## 修订记录
| 文档版本 | 发布日期   | 修订内容                                                     |
| -------- | ---------- | ------------------------------------------------------------ |
| v1.0     | 2019-11-21 | 第一版，适用于 TKE for Alauda Container Platform 私有部署版本 v2.5。 |


<div STYLE="page-break-after: always;"></div>
# 第二部分  部署和配置简介

平台平台有多种部署方案，在规划平台架构之前，请参考《TKE for Alauda Container Platform v2.5 部署白皮书》中部署方案部分。

平台由 global 、global 插件、业务服务集群和集群组件构成，global 是平台的核心，管理、运维功能由 global 提供。global 提供了丰富的插件来扩展 global 的功能，详细内容请参考《TKE for Alauda Container Platform v2.5 部署白皮书》。

业务服务集群是 global 管理的运行客户业务服务的 Kubernetes 集群，集群组件是运行在客户业务服务集群之上的功能模块，提供平台功能、搜集相关数据。

平台可以部署在物理机和虚拟机上，服务器的数量、配置要求等信息请参考《TKE for Alauda Container Platform v2.5 部署白皮书》中硬件需求部分。

平台的部署需要域名、vip、ip 范围、lb 等网络资源，网络资源的具体需求请参考《TKE for Alauda Container Platform v2.5 部署白皮书》中网络需求部分。

平台支持在 centos 和 ubuntu 操作系统上部署，操作系统版本和配置请参考本文档中软件需求部分。

平台中的 global 、global 插件、业务服务器集群、业务服务集群插件的部署请看本文第四部分。

<div STYLE="page-break-after: always;"></div>
# 第三部分  平台及部署过程概述

平台是一款复杂的产品，具有多个需要安装和配置的插件、组件，为确保部署成功，请了解部署架构图和部署步骤的流程顺序图。

## 平台逻辑架构图

请参考《TKE for Alauda Container Platform v2.5 部署白皮书》。

## 平台部署架构图

请参考《TKE for Alauda Container Platform v2.5 部署白皮书》。

## 平台的部署流程图

<img src="images/部署步骤.png" style="zoom:50%;" />

<div STYLE="page-break-after: always;"></div>
# 第四部分 开始部署

## 检查部署需求是否满足

### 软件需求

请参考《TKE for Alauda Container Platform v2.5 部署白皮书》。

### 硬件需求

请参考《TKE for Alauda Container Platform v2.5 部署白皮书》。

### 网络需求

请参考《TKE for Alauda Container Platform v2.5 部署白皮书》。

## 解决不满足的需求

### 软件需求

|需求项|具体要求|不满足后的解决办法|
| ------------- | --------------- | ------------- |
| 操作系统| centos 7.6 |<mark>小版本可以有差别，比如：7.5 7.4 也可以安装部署。</mark>|
| kernel 版本  | 大于 3.10.0-957|<mark>必须满足。</mark>|
| 操作系统安装要求|最小安装|<mark>如果不满足，可能出现配置不同造成部署失败。</mark>|
| 工具软件  |curl tar ip ssh sshpass jq netstat timedatectl ntpdate nslookup base64 tr head openssl md5sum|<mark>手动添加源，并安装软件，请看表格后面的操作方式。</mark>|
|用户权限|root|<mark>具备sudo 权限的用户也可部署，但有一定几率部署失败。</mark>|
|swap|关闭|<mark>必须关闭</mark>|
|防火墙|关闭|<mark>必须关闭</mark>|
|selinux|关闭|<mark>必须关闭</mark>|
|时间同步|所有服务器要求时间必须同步，误差不得超过 2 秒。|<mark>必须同步</mark>|
|时区|所有服务器时区必须统一|<mark>必须统一</mark>|
|/etc/sysctl.conf|vm.max_map_count=262144   net.ipv4.ip_forward = 1|<mark>必须满足</mark>|
|hostname 格式|字母开头，只能是字母、数字和短横线-组成，不能用短横线结尾，长度在 4-23 之间。|<mark>必须满足</mark>|
|/etc/hosts|所有服务器可以通过hostname 解析成 ip，可以将localhost解析成127.0.0.1，hosts 文件内，不能有重复的 hostname|<mark>必须满足</mark>|
|/tmp/权限| 要求 `/tmp` 目录的权限是 `777`。|<mark>必须满足</mark>|

#### 解决工具软件不满足的步骤

1. 解压缩安装包

1. 将安装包内的 kaldr 目录，拷贝到每一台服务器的 `/tmp` 目录下。

	在每一台服务器上执行如下命令，添加软件源。

	* 适用 于 Centos ：

		```
		mkdir /etc/yum.repos.d/old
		mv /etc/yum.repos.d/*.repo /etc/yum.repos.d/old
		cat <<EOF >/etc/yum.repos.d/alauda.repo 
		[alauda]
		name=Alauda
		baseurl=file:///tmp/kaldr/yum/
		enabled=1
		gpgcheck=0
		EOF
		yum clean all && yum makecache
		```

	* 适用于 Ubuntu：

		```
		mv /etc/apt/sources.list /etc/apt/sources.list.old
		echo "deb [arch=amd64] file:///tmp/kaldr/apt/ ubuntu-xenial main" >/etc/apt/sources.list
		add-apt-repository "deb [arch=amd64] file:///tmp/kaldr/apt/ ubuntu-xenial main"
		cat /tmp/kaldr/apt/alauda.key | apt-key add -
		apt-get update
		```
	
	**补充说明**：可通过以下命令行，确认某个软件属于哪个包。
	
	* 适用 于 Centos：`yum provides <程序名>`
	
	* 适用于 Ubuntu ：`apt-file search <程序名>`
	
1. 安装软件包，参考以下命令行。

   - 适用 于 Centos：`yum install -y <上一步获取到的软件包的名字>`
   
   - 适用于 Ubuntu ：`apt-get install -y <上一步获取到的软件包的名字>`

### 硬件需求

<mark>这是实施部署的前提条件，必须满足硬件需求，不满足硬件需求就无法保证正常部署，或部署成功的平台无法长时间稳定运行。</mark>

#### 网络需求

<mark>这是实施部署的前提条件，必须满足网络需求，不满足网络需求就无法保证正常部署，或部署成功的平台无法长时间稳定运行。</mark>

## 和 v2.4 相比部署差异

### 增加的参数

* `--cpaas-namespaces`：指定 cpaas 平台部署到 Kubernetes 集群的哪个 namespaces 下。

* `--cert-sans`：容灾使用的，两个容灾集群的 api 的 vip 的 vip，用于签发到 api 的证书里。

### 改变的参数
* `--enabled-features`：以前只有容器平台、DevOps 平台、微服务治理平台和 TKE 四个，现在增加了 **机器学习平台**（Machine Learning）。

### 其他改变

默认日志存储时间改变，kafka 存 2 小时，Elasticsearch 存 1 天。

## global 平台部署

### server_list.json 文件格式说明

```
"server_role": {         #服务器角色
  "master":true,         #有这个键值，代表这台服务器角色是 k8s 的 master
  "global":true,         #有这个键值，代表这是运行 global 组件的服务器
  "log":true             #有这个键值，代表这是运行日志组件的服务器
},
"ip_addr": "1.1.1.1",    #服务器 ip 地址
"ssh_port": "22",        #ssh 端口
"ssh_user": "root",      #ssh 用户名
"ssh_passwd": "",        #ssh 密码
"ssh_key_file": "/root/.ssh/id_rsa" #ssh 密钥，如果这个值不为空，那么就优先通过密钥登录，就是出现错误也不会选择 password 认证登录。
```


### all in one 方案

部署命令执行的位置：all in one 服务器上

**部署过程**：

1. 解压缩安装包。

1. 进入到解压缩后的目录。

1. 执行以下脚本部署一个通过 `https://<本机 ip>` 访问的环境：

	```
	bash ./up-cpaas-single.sh \
	     --network-interface=<网卡名，默认eth0> \
	     --enabled-features=<acp,devops,tke>
	```
  
1. 执行以下脚本部署一个通过 `http://<本机 ip>` 访问的环境：

	```
	bash ./up-cpaas-single.sh \
	     --network-interface=<网卡名，默认eth0> \
	     --enabled-features=<acp,devops,tke> \
	     --use-http
	```

1. 执行以下脚本部署一个通过 `https://<域名>` 访问的环境：

	```
	bash ./up-cpaas-single.sh \
	     --network-interface=<网卡名，默认eth0> \
	     --enabled-features=<acp,devops,tke> \
	     --domain-name=<域名> \
	     --key-file=<域名证书私钥文件> \
	     --cert-file=<域名证书公钥文件> \
	```
	

	**注 1**：只有下面例子中的参数经过部署测试，改变参数的做法很可能造成部署不成功。
		
	**注 2**：所有有默认值的参数，都是可选的，如果部署时不添加这个参数，就使用默认值
	

### poc 部署方案

部署命令执行的位置： init 服务器上

**部署过程**：

1. 解压缩安装包。

1. 进入到解压缩后的目录。

1. 在 up-cpaas.sh 脚本所在的目录下创建 server_list.json 文件，示例如下：

	``` 
	 [
	  {
	    "server_role": {
	      "master":true,
	      "global":true,
	      "log":true
	    },
	    "ip_addr": "1.1.1.1",
	    "ssh_port": "22",
	    "ssh_user": "root",
	    "ssh_passwd": "",
	    "ssh_key_file": "/root/.ssh/id_rsa"
	  },
	  {
	    "server_role": {
	      "master":true,
	      "global":true,
	      "log":true
	    },
	    "ip_addr": "2.2.2.2",
	    "ssh_port": "22",
	    "ssh_user": "root",
	    "ssh_passwd": "",
	    "ssh_key_file": "/root/.ssh/id_rsa"
	  },
	  {
	    "server_role": {
	      "master":true,
	      "global":true,
	      "log":true
	    },
	    "ip_addr": "3.3.3.3",
	    "ssh_port": "22",
	    "ssh_user": "root",
	    "ssh_passwd": "",
	    "ssh_key_file": "/root/.ssh/id_rsa"
	  }
	]
	```

1. 执行以下命令进行部署：

	```
	bash ./up-cpaas.sh \
	     --network-interface=<网卡名，默认是eth0> \
	     --cpaas-namespaces=<ns 名，默认是cpaas-system> \
	     --enabled-features=<需要安装那些产品，默认什么都不装，支持acp,devops,tke,asm,aml> \
	     --global-network-mode=<选择 global 集群使用那种网络，默认是 ovn，可选 flannel> \
	     --kube-cluster-subnet<global 集群的cluster ip 范围，默认是10.96.0.0/12，掩码建议不要小于16位> \
	     --domain-name=<域名> \
	     --kube_controlplane_endpoint=<k8s vip> \
	     --key-file=<域名证书私钥文件> \
	     --cert-file=<域名证书公钥文件> \
	     --root-username=<管理员用户名，必须是邮箱格式，默认是 admin@cpaas.io>
	```
	**说明**：如果客户没有提供 lb 和 vip ，也可以用下面的命令进行部署，但不再是高可用的环境了。

	```
	bash ./up-cpaas.sh \
	     --network-interface=<网卡名，默认是eth0> \
	     --make-k8s-lb \
	     --domain-name=<域名> \
	     --key-file=<域名证书私钥文件> \
	     --cert-file=<域名证书公钥文件> \
	     --enabled-features=<需要安装那些产品，默认什么都不装，支持acp,devops,tke,asm,aml>
	```
	**说明**：如果客户连域名也不提供，也可以用下面的命令进行部署，但不再是高可用的环境了。

	```
	bash ./up-cpaas.sh \
	     --network-interface=<网卡名，默认是eth0> \
	     --make-k8s-lb \
	     --enabled-features=<需要安装那些产品，默认什么都不装，支持acp,devops,tke,asm,aml> \
	     --use-http
	```
	

**注 1**： 只支持上面给出的部署例子，部署参数组合在上述例子之外的都未经测试，不能保证支持。

**注 2**： 所有有默认值的参数，都是可选的，如果部署时不添加这个参数，就使用默认值。

### 正式生产环境部署方案

部署命令执行的位置： init 服务器上

**部署过程**：

1. 解压缩安装包。

1. 进入到解压缩后的目录。

1. 在 up-cpaas.sh 脚本所在的目录下创建 server_list.json 文件，示例如下：

	* 6 节点 server_list.json 文件详情：

		```
		[
		  {
		    "server_role": {
		      "master":true,
		      "global":true
		    },
		    "ip_addr": "1.1.1.1",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  },
		  {
		    "server_role": {
		      "master":true,
		      "global":true
		    },
		    "ip_addr": "2.2.2.2",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  },
		  {
		    "server_role": {
		      "master":true,
		      "global":true
		    },
		    "ip_addr": "3.3.3.3",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  },
		  {
		    "server_role": {
		      "log":true
		    },
		    "ip_addr": "4.4.4.4",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  },
		  {
		    "server_role": {
		      "log":true
		    },
		    "ip_addr": "5.5.5.5",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  },
		  {
		    "server_role": {
		      "log":true
		    },
		    "ip_addr": "6.6.6.6",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  }
		]
		```

	* 9 节点server_list.json 文件详情：

		```
		[
		  {
		    "server_role": {
		      "master":true
		    },
		    "ip_addr": "1.1.1.1",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  },
		  {
		    "server_role": {
		      "master":true
		    },
		    "ip_addr": "2.2.2.2",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  },
		  {
		    "server_role": {
		      "master":true
		    },
		    "ip_addr": "3.3.3.3",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  },
		  {
		    "server_role": {
		      "log":true
		    },
		    "ip_addr": "4.4.4.4",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  },
		  {
		    "server_role": {
		      "log":true
		    },
		    "ip_addr": "5.5.5.5",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  },
		  {
		    "server_role": {
		      "log":true
		    },
		    "ip_addr": "6.6.6.6",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  },
		  {
		    "server_role": {
		      "global":true
		    },
		    "ip_addr": "7.7.7.7",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  },
		  {
		    "server_role": {
		      "global":true
		    },
		    "ip_addr": "8.8.8.8",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  },
		  {
		    "server_role": {
		      "global":true
		    },
		    "ip_addr": "9.9.9.9",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  }
		]
		```

1. 执行如下命令部署：

	```
	bash ./up-cpaas.sh \
	     --network-interface=<网卡名，默认是eth0> \
	     --cpaas-namespaces=<ns 名，默认是cpaas-system> \
	     
	     --enabled-features=<需要安装那些产品，默认什么都不装，支持acp,devops,tke,asm,aml> \
	     --global-network-mode=<选择 global 集群使用那种网络，默认是 ovn，可选 flannel> \
	     --kube-cluster-subnet<global 集群的cluster ip 范围，默认是10.96.0.0/12，掩码建议不要小于16位> \
	     --domain-name=<域名> \
	     --kube_controlplane_endpoint=<k8s vip> \
	     --cert-sans=<可选参数，如果没有容灾需求可以不加，两地 global 集群 vip 的 vip> \
	     --key-file=<域名证书私钥文件> \
	     --cert-file=<域名证书公钥文件> \
	     --root-username=<管理员用户名，必须是邮箱格式，默认是 admin@cpaas.io>
	```
	
	**说明 1**：如果客户没有提供证书，也可以用下面的命令进行部署，脚本会自动创建一个证书，但是浏览器访问 ui 的时候会提示证书不是正规机构颁发的，有安全隐患。
	
	```
	bash ./up-cpaas.sh \
	     --network-interface=<网卡名，默认是eth0> \
	     --cpaas-namespaces=<ns 名，默认是cpaas-system> \
	     --enabled-features=<需要安装那些产品，默认什么都不装，支持acp,devops,tke,asm,aml> \
	     --global-network-mode=<选择 global 集群使用那种网络，默认是 ovn，可选 flannel> \
	     --kube-cluster-subnet<global 集群的cluster ip 范围，默认是10.96.0.0/12，掩码建议不要小于16位> \
	     --domain-name=<域名> \
	     --kube_controlplane_endpoint=<k8s vip> \
	     --cert-sans=<可选参数，如果没有容灾需求可以不加，两地 global 集群 vip 的 vip> \
	     --root-username=<管理员用户名，必须是邮箱格式，默认是 admin@cpaas.io>
	```
	
	**说明 2**：如果客户没有提供证书，也不希望浏览器提示证书不是正规机构颁发，可以选择使用 http 协议访问，执行如下命令部署。
	
	```
	bash ./up-cpaas.sh \
	     --use-http \
	     --network-interface=<网卡名，默认是eth0> \
	     --cpaas-namespaces=<ns 名，默认是cpaas-system> \
	     --enabled-features=<需要安装那些产品，默认什么都不装，支持acp,devops,tke,asm,aml> \
	     --global-network-mode=<选择 global 集群使用那种网络，默认是 ovn，可选 flannel> \
	     --kube-cluster-subnet<global 集群的cluster ip 范围，默认是10.96.0.0/12，掩码建议不要小于16位> \
	     --domain-name=<vip，不能是域名> \
	     --kube_controlplane_endpoint=<k8s vip> \
	     --cert-sans=<可选参数，如果没有容灾需求可以不加，两地 global 集群 vip 的 vip> \
	     --root-username=<管理员用户名，必须是邮箱格式，默认是 admin@cpaas.io>
	```

**注 1**： 只支持上面给出的部署例子，部署参数组合在上述例子之外的都未经测试，不保证能够支持。

**注 2**： 所有有默认值的参数，都是可选的，如果部署时不添加这个参数，就使用默认值。



## 业务服务集群部署

<mark>登录 global 的 UI 后，请参考用户手册，部署业务服务集群或对接业务服务集群。</mark>

## 业务服务集群组件和第三方插件部署


### 在业务集群部署 cert-manager

**注意**：alauda-cluster-base 需要依赖 cert-manager，因此在业务集群部署 alauda-cluster-base 时，需要先安装 cert-manager 在业务集群的一个 master 节点。但是在 global 集群部署 alauda-cluster-base 时，不需要部署 cert-manager，直接部署 alauda-cluster-base 即可，因为 global 已经默认安装 cert-manager。

```
registry=$(docker info |grep 60080  |tr -d ' ')
helm install \
     --name cert-manager \
     --namespace cpaas-system \
     --set global.registry.address=$registry \
     stable/cert-manager
```

### 部署业务集群必备组件 alauda-cluster-base

```
email=admin@cpaas.io     ####默认为admin@cpaas.io，需要与登录global界面时使用的邮箱一致。
registry=$(docker info |grep 60080  |tr -d ' ')
helm repo update
helm install \
     --debug \
     stable/alauda-cluster-base \
     --name alauda-cluster-base \
     --namespace cpaas-system \
     --set global.namespace=cpaas-system \
     --set global.registry.address=$registry  \
     --set global.auth.default_admin=$email
```

###  业务集群部署 nevermore

```
region_name=test              ###需要修改为正在部署的集群名称
apigateway=1.1.1.1            ###需要修改为global界面的访问ip或者域名
registry=$(docker info |grep 60080  |tr -d ' ')
token=$(kubectl describe secrets  $(kubectl get secret -n kube-system | awk '/^clusterrole-aggregation-controller-token/{print $1}') -n kube-system|grep ^token|awk '{print $2}')
helm install \
     --name alauda-log-agent \
     --namespace cpaas-system  \
     --set nevermore.region=$region_name \
     --set nevermore.apiGatewayHost=https://$apigateway \
     --set nevermore.token=$token \
     --set global.registry.address=$registry \
     --set global.namespace=cpaas-system \
     stable/alauda-log-agent
```

### 业务集群部署 nginx-ingress

在集群内选择运行 ingress 的节点，建议选择让 ingress 运行在 master 节点上。在选定的节点上，添加 `ingress=true` 的 label，然后执行如下命令部署：

```
replicas=$(kubectl get no | awk '{if ($3 == "master")print $1}' | wc -l)
registry=$(docker info |grep 60080  |tr -d ' ')
for i in $(kubectl get no | awk '{if ($3 == "master")print $1}'); do echo kubectl label nodes $i ingress=true --overwrite ; done
helm install --name=nginx-ingress \
             --debug \
             --namespace=cpaas-system \
             --set global.replicas=${replicas} \
             --set global.registry.address=$registry \
             --set global.namespace=cpaas-system \
             stable/nginx-ingress
```


###  业务集群部署普罗米修斯

**说明**：

* 部署普罗米修斯依赖 alauda-cluster-base。
	
* 需要改的地方已经标注，没标注的按文档顺序操作即可。

* 本文档旨在说明如何在 Kubernetes 集群安装 prometheus operator 和 kube-prometheus ，操作的前提是当前 Kubernetes 集群已经安装好了 helm。


因此部署之前先执行以下命令判断是否已经安装。

```
helm list | grep  alauda-cluster-base
helm list | grep ingress 
```

若没有安装，则必须先按照业务集群部署文档部署 ingress 以及 alauda-cluster-base。

**说明**：部署普罗米修斯默认部署在 cpaas-system 命名空间下，若无特别需要，请勿修改 namespace、Service Mesh 等需要依赖普罗米修斯，修改 namespace 时如果填写错误，会增大排查难度。


1. 准备工作，监控 etcd 先判断 etcd-ca 是否存在。

	`kubectl get secrets -n cpaas-system | grep etcd-ca`

	若不存在则按以下命令添加，若存在，跳过第一步。

	```
	kubectl get secrets -n kube-system etcd-ca -o yaml >/tmp/etcd-ca.yaml
	sed -i  '/namespace:/{s/kube-system/cpaas-system/g}' /tmp/etcd-ca.yaml
	kubectl apply -f /tmp/etcd-ca.yaml
	```

1. 安装 prometheus-operator。

	```
	registry=$(docker info |grep 60080  |tr -d ' ')
	helm install \
	     -n prometheus-operator \
	     --namespace=cpaas-system \
	     --set global.registry.address=$registry \
	     --timeout=3000
	     stable/prometheus-operator \
	```

1. 安装 kube-prometheus。

	安装 kube-prometheus 有以下两种方式，一种为使用本地目录的方式存储监控数据，一种为使用 pvc 的方式存储。可自行选择。若无特殊需要，本地目录的方式即可。

	* 本地目录方式部署 kube-prometheus。
  
		给集群中的一个 node 添加 `monitoring=enabled` 的 label，用于 local volume 的调度，命令如下：

		```
		kubectl label --overwrite nodes test monitoring=enabled
		```
		
		**注意**：需要将 test 替换为这个 node 的实际 hostname。在该 node 上创建以下目录用作持久化目录，保证空间 granafa 5G/prometheus 30G/alertmanager 5G，命令如下：

		```
		mkdir -p /var/lib/monitoring/{grafana,prometheus,alertmanager}
		chmod -R 777 /var/lib/monitoring/
		registry=$(docker info |grep 60080  |tr -d ' ')
		global_vip=1.1.1.1   ###需要修改为 global 界面的访问地址
		region_name=test456  ###需要将 test456 替换为当前集群的集群名字
		 
		cat << eof > /tmp/prometheus.sh
		helm install \\
		     -n kube-prometheus \\
		     --namespace cpaas-system stable/kube-prometheus \\
		     --timeout=30000 \\
		     --set global.platform=ACP \\
		     --set prometheus.service.type=NodePort \\
		     --set grafana.service.type=NodePort \\
		     --set global.registry.address=$registry \\
		     --set grafana.storageSpec.persistentVolumeSpec.local.path=/var/lib/monitoring/grafana \\
		     --set prometheus.storageSpec.persistentVolumeSpec.local.path=/var/lib/monitoring/prometheus \\
		     --set alertmanager.storageSpec.persistentVolumeSpec.local.path=/var/lib/monitoring/alertmanager \\
		     --set alertmanager.configForACP.receivers[0].name=default-receiver \\
		     --set alertmanager.configForACP.receivers[0].webhook_configs[0].url=https://$global_vip/v1/alerts/$region_name/router \\
		     --set alertmanager.configForACP.receivers[0].webhook_configs[0].send_resolved=false
		eof
		sh /tmp/prometheus.sh
		```

	* PVC 方式部署 kube-prometheus。

		```
		registry=$(docker info |grep 60080  |tr -d ' ')
		global_vip=1.1.1.1   ###需要修改为 global 界面的访问地址
		region_name=test456  ###需要将 test456 替换为当前集群的集群名字
		storage=test         ###需要将 test 替换为当前集群的 StorageClass 的名字 
		 
		cat << eof > /tmp/prometheus.sh
		helm install -n kube-prometheus \\
		     --namespace cpaas-system stable/kube-prometheus \\
		     --timeout=30000 \\
		     --set global.platform=ACP \\
		     --set prometheus.service.type=NodePort \\
		     --set grafana.service.type=NodePort \\
		     --set global.registry.address=$registry \\
		     --set grafana.storageSpec.volumeClaimTemplate.spec.storageClassName=$storage \\
		     --set prometheus.storageSpec.volumeClaimTemplate.spec.storageClassName=$storage \\
		     --set alertmanager.storageSpec.volumeClaimTemplate.spec.storageClassName=$storage \\
		     --set alertmanager.configForACP.receivers[0].name=default-receiver \\
		     --set alertmanager.configForACP.receivers[0].webhook_configs[0].url=https://$global_vip/v1/alerts/$region_name/router \\
		     --set alertmanager.configForACP.receivers[0].webhook_configs[0].send_resolved=false
		eof
		sh /tmp/prometheus.sh
		```

1. 查看所有 pod 是否正常启动（pod 为 Running 或者 Completed 状态）。

	`kubectl get pods -n cpaas-system | grep prometheus`

1. 集群对接监控。
	
	```
	ip=2.2.2.2   ###需要修改为业务集群任意一个 master 节点的外网 ip，若没有外网地址，使用默认的实际ip
	cat << eof > /tmp/prometheus-feature.yaml
	apiVersion: infrastructure.alauda.io/v1alpha1
	kind: Feature
	metadata:
	  labels:
	    instanceType: prometheus
	    type: monitoring
	  name: prometheus
	spec:
	  accessInfo:
	    grafanaAdminPassword: admin                # grafana 默认密码
	    grafanaAdminUser: admin                    # grafana 默认用户
	    grafanaUrl: http://$ip:30902               # grafana 地址
	    name: kube-prometheus                      # 安装 kube-prometheus chart 时指定的名称
	    namespace: cpaas-system                    # kube-prometheus chart 所在的命名空间
	    prometheusTimeout: 10                      # prometheus 请求超时时间
	    prometheusUrl: http://$ip:30900            # prometheus 地址
	  instanceType: prometheus
	  type: monitoring
	  version: "1.0"
	eof
	 
	kubectl apply -f /tmp/prometheus-feature.yaml
	```

### Service Mesh （微服务治理平台）部署

**说明**：业务集群部署 Service Mesh，至少必须要有一个 slave 节点，且这个节点上不能部署 alb 和 ingress，即：不能有 `alb2=ture` 和 `ingress=true` 的标签。

部署 Service Mesh 依赖 ingress、alauda-cluster-base。

如果在部署 global 的时候，`--enabled-features` 这个参数没有选择 asm ，需要手动部署微服务治理平台的 globla 组件。在 global 集群的 master 节点上，执行如下操作：

**注意**：<> 代表要根据环境情况替换尖括号内的值，替换完毕之后，不包括尖括号。

```
ACP_NAMESPACE=<cpaas-system>  ## 改成部署时， --acp2-namespaces 参数指定的值，默认是cpaas-system
https_or_http=<https>         ## 默认是 https，如果在部署时，使用了 --use-http 这个参数，那么需要改成 http
DOMAIN_NAME=<域名>             ## 改成部署时--domain-name 这个参数指定的值
registry=$(docker info |grep 60080  |tr -d ' ')

helm install \
     --debug \
     --set install.mode="global" \
     --namespace=${ACP_NAMESPACE} \
     --name=asm-init stable/asm-init
helm install \
     --debug \
     --set global.host=${DOMAIN_NAME} \
     --set global.scheme='${https_or_http}' \
     -f /tmp/install_values.yaml \
     --namespace=${ACP_NAMESPACE} \
     --name=global-asm stable/global-asm
```

**注意**：如果找不到  `/tmp/install_values.yaml` 文件，那就执行 ``` helm get values dex | sed 's/host: null/host:/'>/tmp/install_values.yaml ```。


部署成功之后，在界面上单击左上角产品菜单，能看到 Service Mesh 。单击进入，选择管理视图，开始部署  Service Mesh 的业务集群组件，步骤如下：

#### 首先搜集如下信息

1. 提前收集以下几个信息：

	* ES【部署在 global 上的】 ，例如：

		地址：http://10.0.0.17:9200
		
		用户：alaudaes                ### 默认值
		
		密码：es_password_1qaz2wsx    ### 随机生成的值

		可以通过在 global 集群上的一个 master 上执行如下命令，获取密码

		```
		kubectl get deployments -n cpaas-system cpaas-elasticsearch -o yaml | grep ALAUDA_ES_PASSWORD -A 1 | grep value | awk '{print $NF}' 
		```

	* prometheus【部署在对应集群上的】 ，例如：

		地址：http://10.0.0.12:30900

	* ingress 地址【部署在对应集群上的】 ，获取方式：

		``` 
		cat /etc/kubernetes/kubelet.conf | grep server: | grep -oE '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' 
		```

	* IP 范围【对应集群的】 ，获取方式：

		```
		cat /etc/kubeadm/kubeadmcfg.yaml | grep podSubnet |awk -F : '{print $5 $4}'|tr -d 'a-z,A-Z}'|sed 's+ +\\,+g'|sed 's/^..//g'|sed 's/..$//g' 
		```

1. 在微服务治理平台页面的“管理视图”里创建“服务网格”，如下图：

	<img src="images/asm 部署-ui.png" style="zoom:50%;" />

	这里的“组件高可用”代表用多副本方式启动微服务治理平台组件。

	<img src="images/asm 部署，多副本选择.png" style="zoom:50%;" />

1. 在“管理视图”的“项目”里，启动 ns 的服务网格。

	<img src="images/asm 部署，启动 ns 的服务网格.png" style="zoom:50%;" />

	在“管理视图”的“健康检查”里可以看到各组件的状态即可，组件启动需要一段时间，请耐心等待。

	<img src="images/asm 部署-健康检查.png" style="zoom:50%;" />


### 部署机器学习平台

一共有四个组件需要部署。

* Global 集群中需要部署：global-aml

* 业来务集群部署（如果 global 集群也用作业务集群）：

	* volcano
	* aml-core
	* aml-ambassador


#### global 下部署机器学习平台的 global-aml

* 部署 global-aml。

	如果在部署平台的时候，`--enabled-features` 这个参数没有选择 aml ，那么就需要手动部署。在 global 集群的 master 节点上，执行如下操作：

	**注意**：<> 代表要根据环境情况替换尖括号内的值，替换完毕之后，不包括尖括号。
	
	
	```
	helm install --name global-aml \
	             --namespace cpaas-system \
	             -f /tmp/install_values.yaml \
	             --debug \
	             stable/global-aml
	```
	
	**注意**： 如果找不到 `/tmp/install_values.yaml` 文件，那就执行 ``` helm get values dex | sed 's/host: null/host:/'>/tmp/install_values.yaml ```。


* 删除 global-aml。

	```
	helm delete global-aml --purge
	kubectl delete clusterrolebindings.rbac.authorization.k8s.io global-aml:cpaas-system:cluster-admin
	```

#### 业务集群下部署 aml-volcano

* 部署 volcano

	```
	registry=$(docker info |grep 60080  |tr -d ' ')
	helm install --name volcano \
	             --namespace cpaas-system \
	             --set global.namespace=cpaas-system \
	             --set global.registry.address=$registry \
	             stable/volcano
	```
* 删除 volcano

	```
	helm delete volcano --purge
	kubectl delete customresourcedefinitions.apiextensions.k8s.io podgroups.scheduling.incubator.k8s.io
	kubectl delete customresourcedefinitions.apiextensions.k8s.io queues.scheduling.incubator.k8s.io
	kubectl delete validatingwebhookconfigurations.admissionregistration.k8s.io `kubectl get validatingwebhookconfigurations.admissionregistration.k8s.io | grep volcano | awk '{print $1}'`
	kubectl delete customresourcedefinitions.apiextensions.k8s.io `kubectl get customresourcedefinitions.apiextensions.k8s.io | grep volcano | awk '{print $1}'`
	kubectl delete mutatingwebhookconfigurations.admissionregistration.k8s.io `kubectl get mutatingwebhookconfigurations.admissionregistration.k8s.io | grep volcano | awk '{print $1}'`
	kubectl delete clusterroles.rbac.authorization.k8s.io `kubectl get clusterroles.rbac.authorization.k8s.io|grep volcano | awk '{print $1}'`
	```

#### 业务集群下部署 aml-core

* 部署 aml-core。

	```
	registry=$(docker info |grep 60080  |tr -d ' ')
	ingress_address=$(cat /etc/kubernetes/kubelet.conf  | grep server: | grep -oE '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}')
	helm install --name aml-core --namespace cpaas-system \
	             --set global.namespace=cpaas-system \
	             --set global.ingress.host=$ingress_address \
	             --set global.registry.address=$registry \
	             --set tf-job.operator.enableGangScheduling=true \
	             stable/aml-core
	```
* 删除 aml-core。

	```
	helm delete aml-core --purge

	kubectl delete customresourcedefinitions.apiextensions.k8s.io pytorchjobs.kubeflow.org
	kubectl delete customresourcedefinitions.apiextensions.k8s.io tfjobs.kubeflow.org
	kubectl delete clusterrolebindings.rbac.authorization.k8s.io aml-bus:cpaas-system:cluster-admin
	kubectl delete clusterrolebindings.rbac.authorization.k8s.io aml-bus:cpaas-system:cluster-admin
	kubectl delete clusterroles.rbac.authorization.k8s.io pytorch-operator
	```
	
#### 业务集群下部署 aml-ambassador

* 部署 aml-ambassador。

	```
	registry=$(docker info |grep 60080  |tr -d ' ')
	helm install --name aml-ambassador \
	             --namespace cpaas-system \
	             --set global.registry.address=$registry \
	             stable/aml-ambassador
	```
* 删除 aml-ambassador。

	```
	helm del --purge aml-ambassador
 
	kubectl delete customresourcedefinitions.apiextensions.k8s.io `kubectl get customresourcedefinitions.apiextensions.k8s.io | grep ambassador | awk '{print $1}'`
	```


### 业务集群部署 alb2

部署 alb 必须提供单独的机器，且这个机器上不能有 `ingress=true`、`istio=true` 的标签。ingress 默认部署在 master 身上，alb2 不能和 ingress、Service Mesh 的 gateway 部署在一个节点 alb2 使用的标签为：`alb2=true` ，ingress 使用的标签为 `ingress=true`，Service Mesh 使用的标签为 `istio=true`。<br>请注意以上三个标签一个主机只能有一个，即一个节点只能作为以上三个角色中的一个角色。

**准备工作**：

在准备做 alb 的节点打上 `alb2=true` 的标签。

* ```kubectl label node node1  alb2=true```   ####注意将 node1 改为准备部署 alb 的机器的主机名，若部署高可用 alb，则在所有准备做 alb 的节点上打上标签。

* ```replicas=1```                            ####若部署高可用的 alb，准备部署几个实例，就改为几，若非高可用 alb，不用修改。

* ```address=1.1.1.1```                       ####若部署高可用的 alb，这里改为 alb 的 vip，若非高可用，改为部署 alb 机器的实际 ip。


```
  registry=$(docker info |grep 60080  |tr -d ' ')

  helm install --name alauda-alb2 \
               --namespace cpaas-system \
               --set replicas=$replicas \
               --set loadbalancerName=alb2 \
               --set global.registry.address=$registry \
               --set address=$address \
               --set global.namespace=cpaas-system \
               stable/alauda-alb2
```


### harbor、jenkins、gitlab 部署

若使用 PVC 方式部署以下插件。需要事先准备好对应的 PVC，PVC 默认我们提供的是对接 cephfs，因此需要先确保集群对接了 ceph，然后创建对应的 PVC。

```
cat <<EOF > cephfs.yaml
   
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: claim1                 ###  对应 pvc 的名字
  namespace: default
  annotations:
    volume.beta.kubernetes.io/storage-class: cephfs
    volume.beta.kubernetes.io/storage-provisioner: ceph.com/cephfs
  finalizers:
    - kubernetes.io/pvc-protection
spec:
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 1Gi              ###对应 pvc 的大小
  volumeMode: Filesystem
EOF
   
kubectl create -f cephfs.yaml
```

#### 部署 jenkins

jenkins 的部署方式有以下四种，我们推荐在业务集群部署 jenkins，且最好以 PVC 的方式部署。

* 在 global 集群以本地目录的方式部署 jenkins，（对接了GitLab）。

	```
	NODE_NAME="acp2-master-1"            ###需要修改为集群中实际存放jenkins数据的某个节点的hostname
	path="/root/alauda/jenkins"          ###默认数据目录为/root/alauda/jenkins，若有需要可更改。
	password="Jenkins12345"              ###默认密码为Jenkins12345，若有需要可更改
	REGISTRY=$(docker info |grep 60080  |tr -d ' ')
	 
	cat <<EOF > values.yaml
	 
	global:
	  registry:
	    address: ${REGISTRY}
	Master:
	  ServiceType: NodePort
	  NodePort: 32001
	  AdminPassword: "$password"
	  gitlabConfigs:
	    - name: <GitLab服务器名字>
	      manageHooks: true
	      serverUrl: <GitLab服务器地址>
	      token: <GitLab凭据token>
	  Location:
	    url: <Jenkins Location URL>
	Persistence:
	  Enabled: false
	  Host:
	    NodeName: ${NODE_NAME}
	    Path: $path
	AlaudaACP:
	  Enabled: true
	alaudapipelineplugin:
	  consoleURL: ""
	  apiEndpoint: ""
	  apiToken: ""
	  account: ""
	  spaceName: ""
	  clusterName: ""
	  namespace: ""
	AlaudaDevOpsCredentialsProvider:
	  globalNamespaces: "cpaas-system-global-credentials,cpaas-system"
	Erebus:
	  Namespace: "cpaas-system"
	  URL: "https://erebus.cpaas-system.svc.cluster.local:443/kubernetes"
	 
	EOF
	 
	helm install stable/jenkins --name jenkins --namespace default -f values.yaml
	```

* 在 global 以 pvc 的方式部署 jenkins，（对接了GitLab）。

	```
	password="Jenkins12345"              ###默认密码为Jenkins12345，若有需要可更改
	pvc_name="jenkinspvc"                ###默认pvc的名字为jenkinspvc，需要事先在default命名空间下准备好这个pvc，可更改，但也需要事先创建好对应的pvc。
	REGISTRY=$(docker info |grep 60080  |tr -d ' ')
	 
	cat <<EOF > values.yaml
	 
	global:
	  registry:
	    address: ${REGISTRY}
	Master:
	  ServiceType: NodePort
	  NodePort: 32001
	  AdminPassword: "$password"
	  gitlabConfigs:
	    - name: <GitLab服务器名字>
	      manageHooks: true
	      serverUrl: <GitLab服务器地址>
	      token: <GitLab凭据token>
	  Location:
	    url: <Jenkins Location URL>
	Persistence:
	  Enabled: true
	  ExistingClaim: "$pvc_name"
	AlaudaACP:
	  Enabled: true
	alaudapipelineplugin:
	  consoleURL: ""
	  apiEndpoint: ""
	  apiToken: ""
	  account: ""
	  spaceName: ""
	  clusterName: ""
	  namespace: ""
	AlaudaDevOpsCredentialsProvider:
	  globalNamespaces: "cpaas-system-global-credentials,cpaas-system"
	Erebus:
	  Namespace: "cpaas-system"
	  URL: "https://erebus.cpaas-system.svc.cluster.local:443/kubernetes"
	 
	EOF
	 
	helm install stable/jenkins --name jenkins --namespace default -f values.yaml
	
	```

* 在业务集群以本地路径的方式部署 jenkins，（对接了GitLab）。

	```
	NODE_NAME="acp2-master-1"            ###需要修改为集群中实际存放jenkins数据的某个节点的hostname
	global_vip="1.1.1.1"                 ###需要修改为平台的访问地址，如果访问地址是域名，就必须配置成域名，因为 jenkins 需要访问 global 平台的 erebus，如果平台是域名访问的话，erebus 的 ingress 策略会配置成只能域名访问。
	path="/root/alauda/jenkins"          ###默认数据目录为/root/alauda/jenkins，若有需要可更改。
	password="Jenkins12345"              ###默认密码为Jenkins12345，若有需要可更改
	REGISTRY=$(docker info |grep 60080  |tr -d ' ')
	TOKEN=       ### 如何获取token，到devops-apiserver所在集群（一般为global集群）执行：echo $(kubectl get secret -n cpaas-system $(kubectl get secret -n cpaas-system | grep devops-apiserver-token |awk '{print $1}') -o jsonpath={.data.token} |base64 --d)
	 
	cat <<EOF > values.yaml
	  
	global:
	  registry:
	    address: ${REGISTRY}
	Master:
	  ServiceType: NodePort
	  NodePort: 32001
	  AdminPassword: "$password"
	  gitlabConfigs:
	    - name: <GitLab服务器名字>
	      manageHooks: true
	      serverUrl: <GitLab服务器地址>
	      token: <GitLab凭据token>
	  Location:
	    url: <Jenkins Location URL>
	Persistence:
	  Enabled: false
	  Host:
	    NodeName: ${NODE_NAME}
	    Path: $path
	AlaudaACP:
	  Enabled: false
	alaudapipelineplugin:
	  consoleURL: ""
	  apiEndpoint: ""
	  apiToken: ""
	  account: ""
	  spaceName: ""
	  clusterName: ""
	  namespace: ""
	AlaudaDevOpsCredentialsProvider:
	  globalNamespaces: "cpaas-system-global-credentials,cpaas-system,kube-system"
	AlaudaDevOpsCluster:
	  Cluster:
	    masterUrl: "https://$global_vip:6443"
	    token: ${TOKEN}
	Erebus:
	  Namespace: "cpaas-system"
	  URL: "https://$global_vip:443/kubernetes"
	 
	EOF
	 
	helm install stable/jenkins --name jenkins --namespace default -f values.yaml
	
	```

* 在业务集群以 PVC 的方式部署 jenkins，（对接了GitLab）。

	```
	global_vip="1.1.1.1"                 ###需要修改为平台的访问地址，如果访问地址是域名，就必须配置成域名，因为 jenkins 需要访问 global 平台的 erebus，如果平台是域名访问的话，erebus 的 ingress 策略会配置成只能域名访问。
	password="Jenkins12345"              ###默认密码为Jenkins12345，若有需要可更改
	pvc_name="jenkinspvc"                ###默认pvc的名字为jenkinspvc，需要事先在default命名空间下准备好这个pvc，可更改，但也需要事先创建好对应的pvc。
	REGISTRY=$(docker info |grep 60080  |tr -d ' ')
	TOKEN=       ### 如何获取token，到devops-apiserver所在集群（一般为global集群）执行：echo $(kubectl get secret -n cpaas-system $(kubectl get secret -n cpaas-system | grep devops-apiserver-token |awk '{print $1}') -o jsonpath={.data.token} |base64 -d)
	 
	cat <<EOF > values.yaml
	  
	global:
	  registry:
	    address: ${REGISTRY}
	Master:
	  ServiceType: NodePort
	  NodePort: 32001
	  AdminPassword: "$password"
	  gitlabConfigs:
	    - name: <GitLab服务器名字>
	      manageHooks: true
	      serverUrl: <GitLab服务器地址>
	      token: <GitLab凭据token>
	  Location:
	    url: <Jenkins Location URL>
	Persistence:
	  Enabled: true
	  ExistingClaim: "$pvc_name"
	AlaudaACP:
	  Enabled: false
	alaudapipelineplugin:
	  consoleURL: ""
	  apiEndpoint: ""
	  apiToken: ""
	  account: ""
	  spaceName: ""
	  clusterName: ""
	  namespace: ""
	AlaudaDevOpsCredentialsProvider:
	  globalNamespaces: "cpaas-system-global-credentials,cpaas-system,kube-system"
	AlaudaDevOpsCluster:
	  Cluster:
	    masterUrl: "https://$global_vip:6443"
	    token: ${TOKEN}
	Erebus:
	  Namespace: "cpaas-system"
	  URL: "https://$global_vip:443/kubernetes"
	 
	EOF
	 
	helm install stable/jenkins --name jenkins --namespace default -f values.yaml
	```

	部署成功后到 Jenkins 修改配置访问集群任意一个节点 ip+32001 端口，访问 jenkins 页面，用户名：admin   密码：Jenkins12345。

	单击 **系统管理->系统设置**：
  
    a. Alauda Jenkins Syn 里***启用*** 按钮勾选，添加 ***Jenkins 服务名称***，例如：jenkinstest，名称不能和别的已经部署的 jekins 服务名称一样(比如：global 里的 jenkins)，界面集成的时候必须与这个名字一致。
  
    b. 到 Kubernetes Cluster Configuration 下的 Credentials，单击添加，添加一个 serviceaccount token （类型选择 secret text， Secret 位置输入token， ID 输入任意凭据名称（例如：test-token））。<br>如何获取 token？到 devops-apiserver 所在集群（一般为 global 集群）执行命令行：<br>`echo $(kubectl get secret -n cpaas-system $(kubectl get secret -n cpaas-system  | grep devops-apiserver-token |awk '{print $1}') -o jsonpath={.data.token} |base64 -d)`。<br>添加之后。Credentials 选择刚才创建的 secret。
  
    c. 都配置成功后，在 Kubernetes Cluster Configuration 下单击 [Test Connection] 测试连接，提示 `Connect to  succeed` 后，单击保存。


#### 部署 gitlab


 部署 gitlab 有以下两种方式，推荐部署在业务集群，且以 PVC 的方式部署。

* 以本地路径的方式部署。

	```
	REGISTRY=$(docker info |grep 60080  |tr -d ' ')
	NODE_NAME=acp2-master-1      ###需要修改为选择部署gialab的节点的hostname
	NODE_IP="1.1.1.1"            ###这个ip为gitlab的访问地址，需要修改为部署集群中任意master节点一个节点的实际ip
	potal_path="/root/alauda/gitlab/portal"         ###potal的数据目录，一般不需要修改，若有规划，可修改为别的目录
	database_path="/root/alauda/gitlab/database"    ###database的目录，一般不需要修改，若有规划，可修改为别的目录
	redis_path="/root/alauda/gitlab/redis"          ###redis的目录，一般不需要修改，若有规划，可修改为别的目录
	helm install stable/gitlab-ce --name gitlab-ce --namespace default \
	    --set global.registry.address=${REGISTRY} \
	    --set portal.debug=true \
	    --set gitlabHost=${NODE_IP} \
	    --set gitlabRootPassword=Gitlab12345 \
	    --set service.type=NodePort \
	    --set service.ports.http.nodePort=31101 \
	    --set service.ports.ssh.nodePort=31102 \
	    --set service.ports.https.nodePort=31103 \
	    --set portal.persistence.enabled=false \
	    --set portal.persistence.host.nodeName=${NODE_NAME} \
	    --set portal.persistence.host.path="$potal_path" \
	    --set portal.persistence.host.nodeName="${NODE_NAME}" \
	    --set database.persistence.enabled=false \
	    --set database.persistence.host.nodeName=${NODE_NAME} \
	    --set database.persistence.host.path="$database_path" \
	    --set database.persistence.host.nodeName="${NODE_NAME}" \
	    --set redis.persistence.enabled=false \
	    --set redis.persistence.host.nodeName=${NODE_NAME} \
	    --set redis.persistence.host.path="$redis_path" \
	    --set redis.persistence.host.nodeName="${NODE_NAME}" \
	```

* PVC 的方式部署 gitlab

	```
	NODE_IP=1.1.1.1   ###此参数为部署时指定的访问地址，写当前集群中任意一个master节点的ip即可
	portal_pvc="portalpvc"          ###默认pvc的名字为portalpvc，需要事先在default命名空间下准备好这个pvc，可更改，但也需要事先创建好对应的pvc。
	database_pvc="databasepvc"      ###默认pvc的名字为databasepvc，需要事先在default命名空间下准备好这个pvc，可更改，但也需要事先创建好对应的pvc。
	redis_pvc="redispvc"           ###默认pvc的名字为redispvc，需要事先在default命名空间下准备好这个pvc，可更改，但也需要事先创建好对应的pvc。
	REGISTRY=$(docker info |grep 60080  |tr -d ' ')
	helm install stable/gitlab-ce --name gitlab-ce --namespace default \
	    --set global.registry.address=${REGISTRY} \
	    --set portal.debug=true \
	    --set gitlabHost=${NODE_IP} \
	    --set gitlabRootPassword=Gitlab12345 \
	    --set service.type=NodePort \
	    --set service.ports.http.nodePort=31101 \
	    --set service.ports.ssh.nodePort=31102 \
	    --set service.ports.https.nodePort=31103 \
	    --set portal.persistence.enabled=true \
	    --set portal.persistence.existingClaim=$portal_pvc \
	    --set database.persistence.enabled=true \
	    --set database.persistence.existingClaim=$database_pvc \
	    --set redis.persistence.enabled=true \
	    --set redis.persistence.existingClaim=$redis_pvc \
	```

	部署完成之后，通过传入的部署 ip+31101 端口访问 gitlab，默认用户名为：root，密码为：Gitlab12345。

#### 部署 harbor 镜像仓库

  部署 harbor 有以下两种部署方式，推荐部署在业务集群，且以 PVC 的方式部署。

* 本地目录的方式部署 harbor。

	```
	REGISTRY=$(docker info |grep 60080  |tr -d ' ')
	NODE_IP="1.1.1.1"          ###此参数为部署时指定的访问地址，写当前集群中任意一个master节点的ip即可
	NODE_NAME="acp-master-1"   ###需要修改为选择部署harbor节点的hostname
	HOST_PATH=/alauda/harbor   ###这个目录为harbor的数据目录路径，一般不需要修改，若有别的规划，可修改。
	harbor_password="Harbor12345"  ####harbor的密码，默认不需要修改，若有规划，可改
	db_password="Harbor4567"       ####harbor数据库的密码，默认不需要修改，若有规划，可改
	redis_password="Redis789"      ###harbor的redis的密码，默认不需要修改，若有规划，可改
	helm install --name harbor --namespace default stable/harbor \
	    --set global.registry.address=${REGISTRY} \
	    --set externalURL=http://${NODE_IP}:31104 \
	    --set harborAdminPassword=$harbor_password \
	    --set ingress.enabled=false \
	    --set service.type=NodePort \
	    --set service.ports.http.nodePort=31104 \
	    --set service.ports.ssh.nodePort=31105 \
	    --set service.ports.https.nodePort=31106 \
	    --set database.password=$db_password \
	    --set redis.usePassword=true \
	    --set redis.password=$redis_password \
	    --set database.persistence.enabled=false \
	    --set database.persistence.host.nodeName=${NODE_NAME} \
	    --set database.persistence.host.path=${HOST_PATH}/database \
	    --set redis.persistence.enabled=false \
	    --set redis.persistence.host.nodeName=${NODE_NAME} \
	    --set redis.persistence.host.path=${HOST_PATH}/redis \
	    --set chartmuseum.persistence.enabled=false \
	    --set chartmuseum.persistence.host.nodeName=${NODE_NAME} \
	    --set chartmuseum.persistence.host.path=${HOST_PATH}/chartmuseum \
	    --set registry.persistence.enabled=false \
	    --set registry.persistence.host.nodeName=${NODE_NAME} \
	    --set registry.persistence.host.path=${HOST_PATH}/registry \
	    --set jobservice.persistence.enabled=false \
	    --set jobservice.persistence.host.nodeName=${NODE_NAME} \
	    --set jobservice.persistence.host.path=${HOST_PATH}/jobservice \
	    --set AlaudaACP.Enabled=false \
	```

* PVC 的方式部署 harbor。

	```
	REGISTRY=$(docker info |grep 60080  |tr -d ' ')
	NODE_IP="1.1.1.1"             ######此参数为部署时指定的访问地址，写当前集群中任意一个master节点的ip即可
	database_pvc=habordatabase   ###harbor数据库使用的pvc，需要事先在default下创建这个pvc
	redis_pvc=harborredis        ###harbor的redis使用的pvc，需要事先在default下创建这个pvc
	chartmuseum_pvc=harbormuseum   ###harbor使用的pvc，需要事先在default下创建这个pvc
	registry_pvc=harborregistry     ###harbor的registry使用的pvc，需要事先在default下创建这个pvc
	jobservice_pvc=harborjob        ###harbor使用的pvc，需要事先在default下创建这个pvc
	harbor_password="Harbor12345"  ####harbor的密码，默认不需要修改，若有规划，可改
	db_password="Harbor4567"       ####harbor数据库的密码，默认不需要修改，若有规划，可改
	redis_password="Redis789"      ###harbor的redis的密码，默认不需要修改，若有规划，可改
	helm install --name harbor --namespace default stable/harbor \
	    --set global.registry.address=${REGISTRY} \
	    --set externalURL=http://${NODE_IP}:31104 \
	    --set harborAdminPassword=$harbor_password \
	    --set ingress.enabled=false \
	    --set service.type=NodePort \
	    --set service.ports.http.nodePort=31104 \
	    --set service.ports.ssh.nodePort=31105 \
	    --set service.ports.https.nodePort=31106 \
	    --set database.password=$db_password \
	    --set redis.usePassword=true \
	    --set redis.password=$redis_password \
	    --set database.persistence.enabled=true \
	    --set database.persistence.existingClaim=${database_pvc} \
	    --set redis.persistence.enabled=true \
	    --set redis.persistence.existingClaim=${redis_pvc} \
	    --set chartmuseum.persistence.enabled=true \
	    --set chartmuseum.persistence.existingClaim=${chartmuseum_pvc} \
	    --set registry.persistence.enabled=true \
	    --set registry.persistence.existingClaim=${registry_pvc} \
	    --set jobservice.persistence.enabled=true \
	    --set jobservice.persistence.existingClaim=${jobservice_pvc} \
	    --set AlaudaACP.Enabled=false \
	```

	部署完成后通过传入的部署 ip+31104 端口访问 harbor。默认用户名为：admin，密码为：Harbor12345。

### 部署 nexus

存储方式：host path  或  PVC（推荐）。

* 以 hostPath 方式部署。

	```
	NODE_IP=<要集群的 master 的 ip，nexus 暴露的端口是 nodePorts，集群内所有服务器都可以，但 master 节点最好，因为不用担心 master 节点被删除>
	  NODE_NAME=node1
	  HOST_PATH=/alauda/nexus
	  REGISTRY=$(docker info |grep 60080  |tr -d ' ')
	  helm install stable/nexus --name nexus \
	      --set global.registry.address=${REGISTRY} \
	      --set nexus.service.nodePort=32010 \
	      --set nexusProxy.env.nexusHttpHost=${NODE_IP} \
	      --set nexusProxy.env.nexusDockerHost=${NODE_IP} \
	      --set persistence.host.nodeName=${NODE_NAME} \
	      --set persistence.host.path=${HOST_PATH}
	```

* 以 StorageClass 方式，自动创建 PVC 部署, 需要提前创建好对应 STORAGE_CLASS 的 PV。

	```
	NODE_IP=<要集群的 master 的 ip，nexus 暴露的端口是 nodePorts，集群内所有服务器都可以，但 master 节点最好，因为不用担心 master 节点被删除>
	  STORAGE_CLASS=nexus-volume
	  REGISTRY=$(docker info |grep 60080  |tr -d ' ')
	  helm install stable/nexus --name nexus \
	      --set global.registry.address=${REGISTRY} \
	      --set nexus.service.nodePort=32010 \
	      --set nexusProxy.env.nexusHttpHost=${NODE_IP} \
	      --set nexusProxy.env.nexusDockerHost=${NODE_IP} \
	      --set persistence.enabled=true
	      --set persistence.storageClass=${STORAGE_CLASS}   
	```

* 已经存在 PVC 时，可以直接用 PVC 部署，需要提前创建好对应 PVC。

	```
	 NODE_IP=<要集群的 master 的 ip，nexus 暴露的端口是 nodePorts，集群内所有服务器都可以，但 master 节点最好，因为不用担心 master 节点被删除>
	  NEXUS_PVC=nexus-pvc
	  REGISTRY=$(docker info |grep 60080  |tr -d ' ')
	  helm install stable/nexus --name nexus \
	      --set global.registry.address=${REGISTRY} \
	      --set nexus.service.nodePort=32010 \
	      --set nexusProxy.env.nexusHttpHost=${NODE_IP} \
	      --set nexusProxy.env.nexusDockerHost=${NODE_IP} \
	      --set persistence.enabled=true
	      --set persistence.existingClaim=${NEXUS_PVC}
	```

如需开启 nexusBackup，需要``` --set nexusBackup.enabled=true ```，以及 nexusBackup 对应的 persistence，同上方式，具体参数见 chart 中 `README.md`。

默认帐号 admin 的密码由 nexus 随机生成，需要到 pod 中 `cat /nexus-data/admin.password`，第一次登录后，会要求更改密码。

### 部署 sonarqube

sonarqube 版本：7.9.1-community

在安装 chart 前，请先确认将用哪种方式来存储数据:

* Persistent Volume Claim (建议)

* Host path

如果 Kubernetes 集群已经有可用的 StorageClass 和 provisioner，在安装 chart 过程中会自动创建 PVC 来存储数据。 想了解更多关于 StorageClass 和 PVC 的内容，可以参考 Kubernetes Documentation。


* **参考命令（使用 PVC）：**

	```
	registry=$(docker info |grep 60080  |tr -d ' ')
	helm install stable/sonarqube \
	        --name sonarqube \
	        --set plugins.useDefaultPluginsPackage=true \
	        --set global.registry.address=$registry \
	        --namespace=cpaas-system \
	        --set global.namespace=cpaas-system \
	        --set service.type=NodePort \
	        --set service.nodePort=<node port 端口号，默认31342> \
	        --set postgresql.database.persistence.enabled=true \
	        --set postgresql.database.persistence.existingClaim=<pvc name>
	```

* **参考命令（使用 storageClass）：**

	```
	registry=$(docker info |grep 60080  |tr -d ' ')
	helm install stable/sonarqube \
	        --name sonarqube \
	        --set plugins.useDefaultPluginsPackage=true \
	        --set global.registry.address=$registry \
	        --namespace=cpaas-system \
	        --set global.namespace=cpaas-system \
	        --set service.type=NodePort \
	        --set service.nodePort=<node port 端口号，默认31342> \
	        --set postgresql.database.persistence.enabled=true \
	        --set postgresql.database.persistence.storageClass=<storageclass name>
	```

* **参考命令（使用 hostpath）：**

	```
	registry=$(docker info |grep 60080  |tr -d ' ')
	helm install stable/sonarqube \
	        --name sonarqube \
	        --set plugins.useDefaultPluginsPackage=true \
	        --set global.registry.address=$registry \
	        --namespace=cpaas-system \
	        --set global.namespace=cpaas-system \
	        --set service.type=NodePort \
	        --set service.nodePort=<node port 端口号，默认31342> \
	        --set postgresql.database.persistence.enabled=false \
	        --set postgresql.database.persistence.host.nodeName=<node name> \
	        --set postgresql.database.persistence.host.path=<path on host to store data>
	```



### 业务集群部署 cron-hpa-controller【定时扩缩容的组件，可不部署】

```
registry=$(docker info |grep 60080  |tr -d ' ')
helm install \
     --name cron-hpa-controller \
     --namespace cpaas-system \
     --set global.namespace=cpaas-system \
     --set global.registry.address=$registry \
     stable/cron-hpa-controller
```


### 业务集群部署 tapp 【若客户没有 tapp 需求，可不部署】

```
registry=$(docker info |grep 60080  |tr -d ' ')
helm install --name tapp-controller \
             --namespace cpaas-system \
             --set global.registry.address=$registry \
             --set global.namespace=cpaas-system \
             stable/tapp-controller
```

#### metrics-server 部署【devops 自动扩缩容需要用到这个组件】

```
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: metrics-server
  namespace: cpaas-system
  labels:
    k8s-app: metrics-server
spec:
  selector:
    matchLabels:
      k8s-app: metrics-server
  template:
    metadata:
      name: metrics-server
      labels:
        k8s-app: metrics-server
    spec:
      containers:
      - name: metrics-server
        image: index.alauda.cn/alaudak8s/metrics-server-amd64:v0.3.1            ###需要更换镜像地址为私有环境镜像仓库地址
        imagePullPolicy: IfNotPresent
        command:
        - /metrics-server
        - --kubelet-preferred-address-types=InternalIP
        - --kubelet-insecure-tls
        volumeMounts:
        - name: tmp-dir
          mountPath: /tmp
      volumes:
      # mount in tmp so we can safely use from-scratch images and/or read-only containers
      - name: tmp-dir
        emptyDir: {}
 
--
apiVersion: v1
kind: Service
metadata:
  name: metrics-server
  namespace: cpaas-system
  labels:
    kubernetes.io/name: "Metrics-server"
spec:
  selector:
    k8s-app: metrics-server
  ports:
  - port: 443
    protocol: TCP
    targetPort: 443
--
apiVersion: apiregistration.k8s.io/v1beta1
kind: APIService
metadata:
  name: v1beta1.metrics.k8s.io
spec:
  service:
    name: metrics-server
    namespace: cpaas-system
  group: metrics.k8s.io
  version: v1beta1
  insecureSkipTLSVerify: true
  groupPriorityMinimum: 100
  versionPriority: 100
```

### 部署 CSP （ceph）

CSP 可以支持三种存储模式：对象存储、ceph rbd（块存储）、cephfs（文件存储），一个主机选做了三种存储模式中一种，则无法复用其余角色。

**说明**：默认部署会修改 hostname 为 ip，由于 cephrbd 不支持多 Pod 挂载读写，cephfs 支持，所以容器平台默认支持 cephfs 模式。

* **主机需求**

	* 对象存储：需要 4 个 4C8G 的主机，1 个 installer 节点，3 个数据节点（暂不支持在 init 节点和 global 节点部署 ceph，需要额外的机器）。 
	
	* ceph rbd（块存储） ：需要 4 个 4C8G 的主机，1 个 installer 节点，3 个数据节点（暂不支持在 init 节点和 global 节点部署 ceph，需要额外的机器）。
	
	* cephfs（文件存储）：需要 4 个 4C8G 的主机，1 个 installer 节点，3 个数据节点（暂不支持在 init 节点和 global 节点部署 ceph，需要额外的机器）。

* **磁盘需求**

每个数据节点必须要有单独的外挂盘做 ceph 的数据存储，磁盘的大小根据资源使用规划即可（最低50G）。


**部署步骤**：

1. 准备安装包，ceph 的安装包在我们的压缩包里，具体路径为解压后的安装包路径下的 `/other/ceph` 下。

	```
	[root@init ceph]# pwd
	/<安装目录>/other/ceph
	[root@init ceph]# ls
	cephfs.yaml ceph-rbd.yaml CSP-V2.4.10.288.tar.gz
	cephfs.zip ceph-storageclass.yaml rbd
	```

1. 通过 `scp` 命令将 `CSP-V2.4.10.288.tar.gz` 拷贝到准备好的 installer 节点上。


1. 解压安装包，并部署。

	```
	tar zxvf CSP-V2.4.10.288.tar.gz
	cd csp-pkg-2.4.10.288/
	systemctl stop ntpd  && systemctl disable ntpd 
	./cspcmd install --controller 10.0.128.135             ####需要将10.0.128.135这个ip换为本机的实际ip
	```

	等待部署完成之后，会显示访问地址以及端口，一般为 `IP+8089 端口` 。

1. 修改配置文件，支持页面创建 cephfs 存储池。

	```
	sed -i 's/FILE_STORAGE: false/FILE_STORAGE: true/g' /data/csp-ins/current/csp-console/server/config/index.js
	systemctl restart csp-console.service
	```

1. 通过刚才部署时候的命令传入的 `ip+8089` 访问页面，用户名和密码默认为：admin/admin。

	1. 创建集群。

		输入集群名称，输入集群名称单击【下一步】，NTP Server IP 默认 `127.0.0.1 `如下图：

		<img src="images/ceph 4.1 创建集群.png" style="zoom:50%;" />

	2. 添加主机。

		* 添加主机- ip
		
		* 填写节点序列号，按顺序填SN1   SN2  SN3填即可
		
		* 数据中心统一填 dc1
		
		* 机架位置统一填 rack1
		
		* 输入存储服务器root密码
		
		* 输入ssh 端口   如下图：

			<img src="images/ceph 4.2 添加主机.png" style="zoom:50%;" />
			
* 单击【提交】按钮，等待添加主机完成操作，需等待 3 分钟，如下图：
		
	<img src="images/ceph 4.2 提交.png" style="zoom:50%;" />
		
1. 主机分配。
	
	分配主机：勾选你要配置的主机 ip ，有且仅需要3台，单击【完成】后创建集群，存储集群主机初始化完成,如下图：

 	<img src="images/ceph 5 分配主机.png" style="zoom:50%;" />

1. 创建 cephfs 存储池，单击 **文件存储->元数据节点->创建存储池**。
	
	* 填写存储池数据。
	
	* 选择存储节点 ip。
		
	* 本次部署选择的为极简模式，需自己分配数据盘（高级模式）。
	
	* 选择 **3 副本** 冗余策略。<br>**说明**：默认是 3 副本冗余策略
	
	* 选择数据安全级别节点。<br>**注意**：存储节点分配，全选即可（操作系统盘不能用作对象存储池，系统做了屏蔽，因此在该页面中，看不到机器的系统盘），如下图所示。

		 <img src="images/ceph 6 创建存储池.png" style="zoom:50%;" />

	* 单击 **确定** 完成对象存储池的创建。<br>预计等待 2 分钟,等待创建完成，创建完成之后对接即可。


### 对接 ceph 集群

1. 在准备对接的集群所有节点安装 ceph 客户端。

	```
	yum install  ceph-common ceph-fuse  -y
	```
  
2. 腾讯的 ceph 集群默认为 none 模式，没有开启认证，通过配置文件获得 monitor 节点的配置。

	**说明**：需要去腾讯的 ceph 集群的某个 data 节点操作。
	
	```
	conf=$(ps -fe | grep CLUSTERMON.*.conf | grep ceph-mon | awk '{print $(NF-2)}')
	cat $conf | grep mon_addr           ##获得monitor节点的ip以及端口
	mon_addr = 10.0.129.175:6789
	mon_addr = 10.0.129.8:6791
	mon_addr = 10.0.129.73:6790
	```

3. 准备好cephfs 压缩包，将压缩包拷贝到某台 master 机器的 `/root` 下，并解压。

	这个压缩包在init节点的安装包路径下的 other 目录，`/<安装目录>/other/ceph/cephfs.zip`。
	
	```scp cephfs.zip  root@1.1.1.1:/root/```   ##需要将1.1.1.1换为业务集群的某台master机器的ip地址。
  
	1. 去刚传完文件的 master 的 root 下执行以下命令行：

		```
		mv cephfs /tmp        ###为了防止当前目录下存在同名目录
		unzip cephfs.zip
		cd cephfs/
		```

	2. 将 `deployment.yaml` 中的 image 地址更换为自己环境的 image 地址。<br>例如：`10.0.128.173:60080/claas/cephfs-provisioner:latest`。

	3. 创建 ns：```kubectl create ns cephfs```
	
		**说明**：若为 ocp 集群，则需要使用如下 yaml 创建 namespace。
	
		```
		cat <<EOF >/root/cephfs/ns.yaml
		apiVersion: v1
		kind: Namespace
		metadata:
		  annotations:
		    openshift.io/sa.scc.supplemental-groups: 1000000000/0
		    openshift.io/sa.scc.uid-range: 1000000000/0
		  name: cephfs
		spec:
		  finalizers:
		  - kubernetes
		status:
		  phase: Active
		EOF
		kubectl create -f /root/cephfs/ns.yaml
		```
	
	4. 创建相关资源
	
		``` 
		NAMESPACE=cephfs
		sed -r -i "s/namespace: [^ ]+/namespace: $NAMESPACE/g" /root/cephfs/*.yaml
		sed -r -i "N;s/(name: PROVISIONER_SECRET_NAMESPACE.*\n[[:space:]]*)value:.*/\1value: $NAMESPACE/" /root/cephfs/deployment.yaml
		kubectl apply -f /root/cephfs -n $NAMESPACE
		```

4. 创建 storageclass。

	```
	cat <<EOF > cephfs-storageclass.yaml
	kind: StorageClass
	apiVersion: storage.k8s.io/v1
	metadata:
	  name: cephfs
	  selfLink: /apis/storage.k8s.io/v1/storageclasses/cephfs
	  uid: 198bcd73-a1ff-11e8-aafa-5254004c757f
	  resourceVersion: '11786274'
	  annotations:
	    ceph.type: cephfs
	provisioner: ceph.com/cephfs                          
	parameters:
	  adminId: admin
	  adminSecretName: cephfs-admin-secret
	  adminSecretNamespace: kube-system
	  claimRoot: /volumes/kubernetes
	  monitors: '10.0.129.175:6789,10.0.129.8:6791,10.0.129.73:6790'         ## 需要修改为第2步获得ceph集群的monitors的IP地址和端口
	reclaimPolicy: Delete
	volumeBindingMode: Immediate
	EOF
	  
	  
	kubectl create -f cephfs-storageclass.yaml
	```
  
5. 定义管理员密码。

	```
	echo "AQCYfAxdbXTwCRAAnz9MvJgO3KselABH2OoKOA==" > /tmp/secret 
	kubectl create secret generic cephfs-admin-secret --from-file=/tmp/secret --namespace=kube-system
	kubectl create secret generic cephfs-secret-user --from-file=/tmp/secret
	```
  
6. 创建 PVC 验证。
  
	```
	cat <<EOF > cephfs.yaml
	  
	kind: PersistentVolumeClaim
	apiVersion: v1
	metadata:
	  name: claim1
	  namespace: default
	  annotations:
	    volume.beta.kubernetes.io/storage-class: cephfs
	    volume.beta.kubernetes.io/storage-provisioner: ceph.com/cephfs
	  finalizers:
	    - kubernetes.io/pvc-protection
	spec:
	  accessModes:
	    - ReadWriteMany
	  resources:
	    requests:
	      storage: 1Gi
	  volumeMode: Filesystem
	EOF
	  
	kubectl create -f cephfs.yaml
	```

	检查对接是否成功。

	```  
	kubectl get pvc      ###查看刚创建的pvc为bound状态即成功
	NAME     STATUS   VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS   AGE
	claim1   Bound    pvc-b480aa29-a142-11e9-9d43-525400d63265   1Gi        RWX            cephfs         19m
	```


 <div STYLE="page-break-after: always;"></div>
# 第五部分 平台配置及测试

## 平台访问地址及登录用户名、密码，以及兼容的浏览器

### 平台访问地址

<mark>all in one 部署方案</mark>和客户没有提供域名的<mark> poc 部署方案</mark> 的访问地址：

* 没有使用 `--use-http` 参数的平台访问地址：`https://<init 节点 ip>` 

* 使用了 `--use-http` 参数的平台访问地址：`http://<init 节点 ip>`

<mark>正式生产环境部署方案</mark>和客户提供域名的<mark> poc 部署方案</mark> 的访问地址：

* 没有使用 `--use-http` 参数的平台访问地址：`https://<--domain-name 参数指定的地址>` 

* 使用了 `--use-http` 参数的平台访问地址：`http://<--domain-name 参数指定的地址>`

### 平台登录权限

用户名：`--root-username` 参数指定的值，默认为 admin@cpaas.io。

密码：password

### 兼容的浏览器版本

**仅兼容 Chrome 浏览器，且是最近的 3 个版本之一**。


## 部署成功后操作

### 备份 etcd 数据

1. 拷贝安装目录下的 backup_recovery.sh 和 function.sh 脚本到每一个 master 服务器的 `/tmp` 下。

2. 在每一台 master 上，执行如下命令：

	```
	cd /tmp
	bash backup_recovery.sh run_function=back_k8s_file
	```


### 添加 gc 脚本

将安装目录下的 gc.sh 脚本 cp 到各个服务器/root 目录下，然后为每个服务器添加 crontab ，每天凌晨运行一次即可，用于清理系统日志、本机没有容器使用的镜像和处在 Terminating、Evicted、Completed 这三种状态一天以上的 pod。
执行如下命令：

`echo '1 2 * * * bash /tmp/gc.sh' | crontab`

**注意**： 上述命令会将原来的 crontab 全部删掉，建议使用它 `crontab -e` 命令添加。

## 平台功能测试

请参考《TKE for Alauda Container Platfrom POC 测试用例.docx》。

## 添加监控、告警

<div STYLE="page-break-after: always;"></div>
# 第六部分 FAQ

## 平台部署错误

1. 安装 chart 失败：
  
    **提示信息**： install chart error，安装日志里会显示出错的 chart 名字。
    
    **出错原因**： 脚本通过 helm install 命令部署 chart 出错，详细日志会保存到第一台 master 节点的/cpaas/chart_install.log 文件内。
    
	**错误处理流程**：首先登录到第一台 master 节点上，执行 `helm list -a` 命令检查 chart ，发现错误的 chart 后，执行`helm delete --purge <chart name>` 删掉 chart ，然后执行`bash /tmp/install_chart.sh` 继续安装 chart，如果还是不成功，建议清空环境重新部署。依旧出错，请反馈给运维。
    
2. 对接 global 集群失败：

	**提示信息**：get global token Repeat 10 times to get global token failed，之后对接 global 集群也会失败，提示 `access global region error`。
  
	**出错原因**：这种情况一般是chart 安装失败，global 的 chart 没起来，或者因为资源不够、global 集群的服务器速度太慢，获取 token 的时候 global 组件还未正常运行，所以获取token 失败了，没有正确的 token，global 没起来调用 api 自然失败。
  
	**错误处理流程**：chart 安装失败，请参照上面的方法处理，等待所有 global 组件的 pod 状态都正常后，去 init 节点上，执行以下命令行后对接成功：
	
	```
	cat /tmp/add_global_region.sh | sed 's/Bearer "/Bearer '$(bash /tmp/get_token.sh )'"/g' >/tmp/dd.sh ; bash /tmp/dd.sh
	```



## 平台功能错误

请参考《TKE for Alauda Container Platfrom 运维手册》。







