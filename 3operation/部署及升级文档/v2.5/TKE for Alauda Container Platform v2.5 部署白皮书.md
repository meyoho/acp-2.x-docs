
# 目录
1. 前言
	* 读者对象
	* 文档目的
	* 修订记录
2. 部署需求
	* 软件需求
	* 硬件需求
	* 网络需求
	* 网络架构图
	* lb 配置要求
3. 部署方案
	* 说明
	* all in one 方案
	* POC 部署方案
	* 正式生产环境部署方案


<div STYLE="page-break-after: always;"></div>

# 第一部分 前言

本文档介绍了安装平台所需的资源、配置以及三种典型的部署方案。

## 读者对象

《TKE for Alauda Container Platform 部署白皮书》适用于具备基本的 linux、容器、k8s 及网络知识，想要安装和配置平台的实施工程师和平台管理员。

规划平台架构的售前工程师。

负责整个项目生命周期的项目经理。

## 文档目的

售前工程师依据本文档，规划部署方案。

项目经理依据本文档，向客户要求硬件和网络资源，要求软件版本、配置。

实施工程师依据本文档，检查实施环境是否满足实施需求。

平台管理员可以通过本文档，了解平台所需资源、部署方案。

## 修订记录

| 文档版本 | 发布日期   | 修订内容                                      |
| -------- | ---------- | --------------------------------------------- |
|   | 2019-11-21 | 第一版，适用于 TKE for Alauda Container Platform 私有部署版本。 |

<div STYLE="page-break-after: always;"></div>

# 第二部分 部署需求

## 软件需求

### 服务器软件需求

|需求项|具体要求|说明|
|------------- |--------------- | ------------- |
| 操作系统| centos 7.6 | |
| kernel 版本  | 大于等于 3.10.0-957| |
| 操作系统安装要求|最小安装| |
| 工具软件  |curl tar ip ssh sshpass jq netstat timedatectl ntpdate nslookup base64 tr head openssl md5sum||
|用户权限|root|必须使用 root 用户部署，各个服务器也必须允许 root 用户 ssh 登录。|
|swap|关闭|如果不满足，系统会有一定几率出现 io 飙升，造成 docker 卡死。|
|防火墙|关闭 | k8s 官方要求。 |
|selinux|关闭| k8s 官方要求|
|时间同步|所有服务器要求时间必须同步，误差不得超过 2 秒 |docker 和 k8s 官方要求。|
|时区|所有服务器时区必须统一|建议设置为 `Asia/Shanghai`。|
|/etc/sysctl.conf|vm.max_map_count=262144   net.ipv4.ip_forward = 1|第一个是 es 要求，第二个是 k8s 要求。|
|hostname 格式|字母开头，只能是字母、数字和短横线 `-` 组成，不能用短横线结尾，长度在  4-23 之间。||
|/etc/hosts| 所有服务器可以通过 hostname 解析成 ip，可以将 localhost 解析成 `127.0.0.1`。<br>**注意**：hosts 文件内，不能有重复的 hostname。 ||
| `/tmp/` 权限 | 要求 `/tmp` 目录的权限是 `777`。||

### 客户端软件需求

#### 兼容的浏览器版本

***仅兼容的Chrome浏览器，且是最近3个版本***

## 硬件需求

**cpu、内存和硬盘的性能要求**

|需求项|具体要求|推荐型号或配置|
| ------------- | --------------- | ------------- |
|cpu|在 iaas 层不得超售，主频不得小于2.6GHz|intel E5-2660V3|
|内存|在 iaas 层不得超售 | DDR4 2133|
|硬盘|iops > 500 吞吐量>200M/s|ssd|

**不同角色服务器的硬件配置要求表**

|角色|角色说明|cpu 数量|内存容量|/ 分区容量|/cpaas 分区容量|/var/lib/docker 分区容量|/var/lib/docker分区格式|
|:--|:-:| :-:| :-:| :-:| :-:| :-:| --:|
|init|部署服务器|2	|4G|50G||100G|xfs|
|global|运行平台组件的节点	|8|16G| 50G ||100G|xfs|
|log|运行日志组件的节点|8|32G| 50G |300G|100G|xfs|
|master|K8S master 节点|4|16G| 50G ||100G|xfs|
|slave	|K8S 计算节点||| 50G ||100G|xfs|
|all in one|单点的平台|16|32G| 50G |300G|100G|xfs|

## 网络需求

|资源|数量|说明|
| ------------ | :--------  |  ----- |
|域名|1|生产环境必须，访问 global 的域名，ui、api、软件源等等 global 组件使用这个域名提供服务，组件也会通过这个域名访问 dex 做认证，域名解析成 global vip。|
|证书|1|和域名一并提供，如果不提供证书，部署脚本会自动生成一个证书，但是浏览器访问平台 UI 会提示安全警告，因为证书不是认证机构签发的。|
|global VIP|1|生产环境必须，域名解析出来的 ip 地址，配到外网 lb 上。|
|k8s api server VIP|多个|生产环境必须，给高可用的 k8s 集群的 kube-api 使用，每一个高可用的 k8s 集群都需要一个 vip。 |
|ALB VIP|多个|如果客户使用 alb ，每个客户业务服务集群的 alb 需要一个 VIP。 |
|内网 LB|1|生产环境必须，类似 F5 的负载均衡设备，k8s api server  vip 配置到这个负载均衡设备上。|
|外网 LB|1|生产环境必须，如果客户只有一套网络，可以和内网 lb 复用。global vip 配置到这个负载均衡设备上。|
|网络速率| 1000M/s | 不低于千兆，建议万兆。|
|安全及防火墙||平台的服务器之间，无防火墙限制。|
|ip 地址范围||部署平台的服务器，不得使用 172.16-32  网段的 ip，如果已经使用，无法更改，就需要修改每一台服务器上的 docker 的配置，加上 bip 参数，躲过这个 ip 段，k8s 集群使用10.96.0.0/12 作为 cluster IP 范围段，这段地址客户不能使用。|
|协议||支持 ipv6。|
|路由| | 服务器有 default 或指向 `0.0.0.0` 这个地址的路由。|

## 网络架构图

<img src="images/网络架构图.png" style="zoom:50%;" />

## 平台逻辑架构图（只有日志监控组件）

<img src="images/标准版部署架构图-日志监控.png" style="zoom:50%;" />
<div STYLE="page-break-after: always;"></div>

## 平台逻辑架构图（除了日志监控组件）

<img src="images/标准版部署架构图.png" style="zoom:50%;" />
<div STYLE="page-break-after: always;"></div>

## 平台部署架构图 - all in one

<img src="images/部署架构-allinone.png" style="zoom:50%;" />
<div STYLE="page-break-after: always;"></div>

## 平台部署架构图 - poc（不提供 LB）

<img src="images/部署架构-poc-nolb.png" style="zoom:50%;" />
<div STYLE="page-break-after: always;"></div>

## 平台部署架构图 - poc（提供 LB）

<img src="images/部署架构-poc-lb.png" style="zoom:50%;" />
<div STYLE="page-break-after: always;"></div>

## 平台部署架构图 - 正式生产环境（6节点）

<img src="images/正式生产环境部署方案-6.png" style="zoom:50%;" />
<div STYLE="page-break-after: always;"></div>

## 平台部署架构图 - 正式生产环境（9节点）

<img src="images/正式生产环境部署方案-9.png" style="zoom:50%;" />
<div STYLE="page-break-after: always;"></div>


## lb 配置要求

### 名词解释

平台：是指承载容器云平台的 k8s 集群的所有节点
k8s集群：承载客户业务服务的 k8s 集群的所有节点

### global 平台四层转发规则

|目的IP|目的端口|资源池|协议|源 IP|源端口|备注|
|  ----- | ------- | ------- |  ---------- | ----------   | ----------- |---------- |
|global VIP|80|承载平台的 k8s 集群的 master 节点|tcp|平台、k8s 集群|any|平台 http 服务|
|global VIP|443|承载平台平台的 k8s 集群的 master 节点|tcp|平台、k8s 集群和调用 api 的设备|any|平台出口|
|global VIP|30900|承载平台的 k8s 集群的所有节点|tcp|平台、和操作人员的电|any|普罗米修斯|
|global VIP|30902|承载平台的 k8s 集群的所有节点|tcp|平台、和操作人员的电脑|any|grafana|
|k8s api的vip|6443|承载平台的 k8s 集群的 master 节点|tcp|平台、k8s集群|any|k8s 集群 api|

### 客户业务服务集群转发规则

|目的IP|目的端口|资源池|协议|源 IP|源端口|备注|
|  ----- | ------- | ------- |  ---------- | ----------   | ----------- |---------- |
|alb VIP|80|客户业务服务集群的 alb 的所有节点|tcp|访问业务服务的设备|any|客户的 http 服务|
|alb VIP|443|客户业务服务集群的 alb 的所有节点|tcp|访问业务服务的设备|any|客户的 https 服务|
|k8s api 的vip|6443|客户业务服务 k8s 集群的 master 节点|tcp|平台、k8s 集群|any|k8s 集群 api|

<div STYLE="page-break-after: always;"></div>

# 第三部分 部署方案

## all in one 方案

### 适用范围

只能提供很少数量的服务器的 POC 阶段。

### 硬件资源要求
**注1 硬件具体要求，请看本文档前面的《不同角色服务器的硬件配置要求表》**

|编号|服务器角色|服务器用途|
| -----  |   ------- | --------------- |
|1| all in one|平台软件源、镜像仓库、chart repo 、global 本身。|
|2| master & slave|用于跑客户服务的集群，计算节点和集群管理节点再同一台服务器上，如果选最低的 4c16g 的配置，这个集群只能跑几个 hello-world 服务了。|
|3| slave|可选，集群的计算节点，用于跑客户的服务。|

### 网络资源要求

本文档 **第二部分 部署需求** 中，只需要满足路由、协议、地址范围、安全及防火墙、网络速率这五个要求，即可以部署。

## POC 部署方案

### 适用范围

对平台所有功能，包括可靠性、可用性甚至容灾都进行测试验证的 POC 阶段。

### 硬件资源要求
**注1 硬件具体要求，请看本文档前面的《不同角色服务器的硬件配置要求表》**

|编号|服务器角色|服务器用途|
| -----  |   ------- | --------------- |
|1| init|启动部署平台所需的软件源、镜像仓库和 chart repo。|
|2|master & slave & global & es|运行平台组件的 k8s 集群的 master ，同时作为计算节点，运行平台所有组件。|
|3|master & slave & global & es|运行平台组件的 k8s 集群的 master ，同时作为计算节点，运行平台所有组件。|
|4|master & slave & global & es|运行平台组件的 k8s 集群的 master ，同时作为计算节点，运行平台所有组件。|
|5| master & slave|用于跑客户服务的集群，计算节点和集群管理节点再同一台服务器上，如果选最低的 4c16g 的配置，这个集群只能跑几个 hello-world 服务了。|
|6| slave|可选，集群的计算节点，用于运行客户的服务。|

### 网络资源要求

本文档 **第二部分 部署需求** 中，只需要满足路由、协议、地址范围、安全及防火墙、网络速率这五个要求，即可以部署。

## 正式生产环境部署方案

### 适用范围

平台真正用于生产的阶段。

### 硬件资源要求
**注1 硬件具体要求，请看本文档前面的《不同角色服务器的硬件配置要求表》**

#### 6 节点环境资源要求

|编号|服务器角色|服务器用途|
| -----  |   ------- | --------------- |
|1| init|启动部署平台所需的软件源、镜像仓库和 chart repo。|
|2|master & slave & global |运行平台组件的 k8s 集群的 master ，同时作为计算节点，运行平台除日志外所有组件。|
|3|master & slave & global |运行平台组件的 k8s 集群的 master ，同时作为计算节点，运行平台除日志外所有组件。|
|4|master & slave & global |运行平台组件的 k8s 集群的 master ，同时作为计算节点，运行平台除日志外所有组件。|
|5|slave & log |运行平台日志组件。|
|6|slave & log |运行平台日志组件。|
|7|slave & log |运行平台日志组件。|
|8| master & slave|用于跑客户服务的集群，计算节点和集群管理节点再同一台服务器上，如果选最低的4c16g 的配置，这个集群只能跑几个 hello-world 服务了。|
|9| slave|可选，集群的计算节点，用于跑客户的服务。|

#### 9 节点环境资源要求

|编号|服务器角色|服务器用途|
| -----  |   ------- | --------------- |
|1| init|启动部署平台所需的软件源、镜像仓库和 chart repo。|
|2|master |运行平台组件的 k8s 集群的 master。|
|3|master |运行平台组件的 k8s 集群的 master。|
|4|master |运行平台组件的 k8s 集群的 master。|
|5|slave & global |运行平台 glboal 组件。|
|6|slave & glboal |运行平台 global 组件。|
|7|slave & global |运行平台 global 组件。|
|8|slave & log |运行平台日志组件。|
|9|slave & log |运行平台日志组件。|
|10|slave & log |运行平台日志组件。|
|11| master|客户业务服务集群的 master。|
|12| master|客户业务服务集群的 master。|
|13| master|客户业务服务集群的 master。|
|14| slave|可选，客户业务服务集群的计算节点，用于跑客户的服务。|

### 网络资源要求

必须满足本文档 **第二部分 部署需求** 中，网络需求部分所有项目。



