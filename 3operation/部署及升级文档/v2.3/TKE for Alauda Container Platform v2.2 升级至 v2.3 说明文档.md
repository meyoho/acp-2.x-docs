#  目录

1. 关于本文档
	* 文档受众
	* 文档目的
	* 修订记录
2. 升级过程
	* 升级前准备
	* 平台升级
	* 集群升级
	* 回滚

<div STYLE="page-break-after: always;"></div>
# 第一部分  前言

本文档介绍了如何将平台版本从 v2.2 升级到 v2.3。

## 读者对象

《TKE for Alauda Container Platform 升级文档》适用于具备基本的 linux、容器、Kubernetes 知识，想要安装和配置平台的用户。包括：

* 实施顾问和平台管理员

* 对平台进行维护的运维人员

* 负责整个项目生命周期的项目经理

## 文档目的

平台运维工程师依据本文档升级平台版本。


## 修订记录

| 文档版本 | 发布日期   | 修订内容                                      |
| -------- | ---------- | --------------------------------------------- |
| v1.0 | 2019-11-6 | 第一版，适用于 TKE for Alauda Container Platform 私有部署版本的 v2.2 升级至 v2.3。 |
| v1.1 | 2019-11-16 | 第二版，修复发现的错误。 |
| v1.2 | 2019-11-22 | 第三版，修复升级 alb 的错误。 |
| v1.3 | 2019-11-27 | 第四版，增加回滚配置说明。 |
| v1.4 | 2020-02-18 | 第五版，增加安装 Captain 说明。 |


<div STYLE="page-break-after: always;"></div>
# 第二部分  升级过程

## 升级前准备

### 备份数据

* 执行位置：global 集群的 master 和业务服务集群的 master 节点。
* 操作步骤：首先将安装目录下的 “backup_recovery.sh” 脚本复制到 master 节点上。
* 执行命令： 
	```
	mkdir /cpaas/backup && cp -r /var/lib/etcd/ /cpaas/backup && cp -r /etc/kubernetes/ /cpaas/backup
	bash ./backup_recovery.sh run_function=back_k8s_file
	```
	
	命令作用：备份集群的 etcd


## 平台升级

### 下载 v2.3 的安装包

* 执行位置：init 节点
* 操作步骤：下载安装包并解压缩

### 拷贝/alauda 目录

* 执行位置：init 节点
* 操作步骤：复制 /alauda 目录到/cpaas 下
* 执行的命令： ``` cp -Ra /alauda /cpaas ```

  **注意**：“/alauda” 目录下，会存储安装过程中产生的诸如安装日志、配置文件、安装环境的信息等文档，总大小不超过 10M ，这些需要复制到 “/cpaas” 目录下。如果部署的时候，将安装包放到了这里，在执行这步的时候请注意，不需要拷贝安装包和安装目录。

### 上传 v2.3 的镜像和 chart 到 init 节点的私有仓库和 chart repo 内

* 执行位置：init 节点
* 操作步骤：进入 v2.3 的安装目录，执行 `bash ./upload-images-chart.sh`

  **注意**：执行过程需要一个小时左右，视 init 服务器 cpu 和硬盘速度不同，会缩短或延迟，注意安装目录所在分区的空间，上传之前镜像仓库挂载的卷至少要保证 15G 的空余空间。

### 更新 kaldr

* 执行位置：init 节点
* 操作步骤：执行如下命令

	```
mkdir /cpaas
REGISTRY_ENDPOINT=$(docker info |grep 60080  |tr -d ' ')
for i in $(curl ${REGISTRY_ENDPOINT}/v2/alaudaorg/kaldr/tags/list 2>/dev/null | jq '.tags' | sed -e '1d' -e '$d' -e 's/"//g' -e 's/,//g'); do
        docker pull ${REGISTRY_ENDPOINT}/alaudaorg/kaldr:$i >/dev/null 2>&1
        echo $i $(date -d $(docker inspect ${REGISTRY_ENDPOINT}/alaudaorg/kaldr:$i -f '{{.Created}}') +%s)
done | sort -k2n,2 | awk 'END{print $1}' > /tmp/dd
KALDR_IMAGE=${REGISTRY_ENDPOINT}/alaudaorg/kaldr:$(cat /tmp/dd)
docker rm -f yum
mv -f /alauda/.run_kaldr.sh /alauda/.run_kaldr.sh.old
cat /alauda/.run_kaldr.sh.old | awk '{for(i=1;i<NF;i++){printf $i" "}print "'$KALDR_IMAGE'"}' >/cpaas/.run_kaldr.sh
bash /cpaas/.run_kaldr.sh

	```
* 执行完毕后，yum 容器会被重建，请检查是否重建。

### 安装 captain 

容器平台 v2.2 还没有默认部署 captain ，所以上传 v2.3 镜像和 chart 完毕后，需要安装 captain。

* 执行位置：global 集群的第一个 master 节点上
* 操作步骤：执行如下命令

	```
	ACP_NAMESPACE=<cpaas-system>  ## 改成部署平台时， --acp2-namespaces 参数指定的值
	REGISTRY_ENDPOINT=$(docker info |grep 60080  |tr -d ' ')
	CHART_ENDPOINT=$(helm repo list | awk '/^stable/{print $NF}')
	helm up
	helm install --debug \
	                 --namespace=${ACP_NAMESPACE} \
	                 --set global.registry.address=${REGISTRY_ENDPOINT} \
	                 --set alaudaChartRepoURL=${CHART_ENDPOINT} \
	                 --set namespace=${ACP_NAMESPACE} \
	                 --name=captain stable/captain
	```


### 更新 install_values.yaml 文件

* 执行位置：global 集群的第一个 master 上
* 操作步骤：执行如下命令

	```
	mkdir /cpaas/backup
	kubectl get cm -n alauda-system dex-configmap -o yaml > /cpaas/backup/dex-configmap.yaml.new
	kubectl scale deployment -n alauda-system auth-controller2 dex --replicas=0   【不能遗漏，否则数据会丢失】
	
	mv /tmp/install_values.yaml /cpaas/backup/install_values.yaml.old
	helm get values dex >/cpaas/install_values.yaml
	
	helm repo update
	```

### 修改配置文件

* 执行位置：global 集群的第一个 master 上
* 操作步骤：
	
	1. 执行``` vim /cpaas/install_values.yaml ``` 

	2. 增加如下值：

		<img src="images/修改 values 1.png" style="zoom:50%;" />
		
		is\_install_auth: false  和 tke: false 的值改成和图片一样
		
		<img src="images/修改 values 2.png" style="zoom:50%;" />
	
		tiny 和 mars 下增加 esHost。
	
	3. 执行命令：

		``` sed -i -e 's/alauda-elasticsearch/cpaas-elasticsearch/g' /cpaas/install_values.yaml ```
	
	**说明**：若之前 v2.2 环境部署完成后修改了用户，或者对接了客户的 es 等，需要打开这个文件，找到对应的 values 的值进行修改。


### 停掉认证组件的 pod

* 执行位置：global 集群的第一个 master 上
* 操作步骤：执行如下命令
	```
	mkdir /cpaas/backup
	kubectl get cm -n alauda-system dex-configmap -o yaml > /cpaas/backup/dex-configmap.yaml.new
	kubectl scale deployment -n alauda-system auth-controller2 dex --replicas=0
	```

### 更新日志组件

* 执行位置：global 集群的第一个 master 上
* 操作步骤：

	1. 执行如下命令。

		```
		kubectl scale deployment -n alauda-system  kube-prometheus-exporter-kube-state kube-prometheus-grafana prometheus-operator --replicas=0
		
		helm delete --purge alauda-kafka-zookeeper alauda-elasticsearch
		```
	
	2. 在添加了 log=true 标签的服务器上，执行：
	
		``` 
		mkdir /cpaas ; cp -Ra /alauda/* /cpaas 
		```

		**注意**：
		
		* 必须所有 log 的机器上都执行。
		
		* es、kafka、zk 默认的挂载目录，从 “/alauda” 改成了 “/cpaas”，如果 “/alauda” 是单独挂载的一块硬盘，升级完毕之后，还需要停掉这三个服务，将硬盘挂载到 cpaas 下，然后启动监控组件。
	
		  
		
	3. 停掉挂本地卷的 deploy。
	
		```
		kubectl scale deployment -n alauda-system kube-prometheus-exporter-kube-state kube-prometheus-grafana prometheus-operator --replicas=1 
		```

	4. 安装新的日志组件。
	
		```
		ACP_NAMESPACE=alauda-system
	
		helm install --debug -f /cpaas/install_values.yaml --namespace=${ACP_NAMESPACE} --name=kafka-zookeeper stable/kafka-zookeeper
	
		sleep 30
	
		helm install --debug -f /cpaas/install_values.yaml --namespace=${ACP_NAMESPACE} --name=elasticsearch stable/elasticsearch
		```


### 更新平台 chart

* 执行位置：global 集群的第一个 master 上
* 操作步骤：

	1. 执行如下命令。

		```
		for i in dex alauda-base cert-manager nginx-ingress download-server alauda-container-platform alauda-devops alauda-cluster-base alauda-log-agent captain ; do helm upgrade $i stable/$i >>/cpaas/upgrade_chart.log || echo "upgrade $i error" ; sleep 10 ; done
		 
		 helm upgrade global-asm  --namespace alauda-system stable/global-asm
		
		for i in kube-prometheus prometheus-operator ; do helm upgrade $i stable/$i --timeout=3000 --wait --force >>/cpaas/upgrade_chart.log || echo "upgrade $i error" ; sleep 10 ; done
		```
	
		**注意**：如果 alauda-base 升级失败，执行以下命令：
	
		```
		helm rollback alauda-base  <版本号>
		helm status alauda-base | grep -A 10 Deployment  ##用这个命令找到 alauda-base 的所有 deploy ，然后kubectl delete deploy  删掉
		helm upgrade alauda-base stable/alauda-base
		```
		
		如果还是失败，执行以下命令：
		
		```
		helm fetch stable/alauda-base --untar
		cd alauda-base
		vim ./templates/archon/feature-gates/stages.yaml
		```
		这个文件里面的 3 个资源增加 `annotations  :"helm.sh/hook": post-install`, 如图所示。
	
		<img src="images/升级 alauda-base.png" style="zoom:50%;" />

	1. 删掉 alauda-base 所有的 deploy ，然后执行：
	
		``` 
		helm upgrade alauda-base . 
		```


### 更新 Service Mesh (业务集群升级)

* 执行位置：安装 Service Mesh  集群的第一个 master 上
* 操作步骤：执行如下命令
	
	```
	helm up
	helm fetch stable/asm-init --untar   #获取 asm-init chart
	kubectl -n alauda-system delete casemonitors.asm.alauda.io --all    #删除casemonitor老的资源
	cd asm-init
	helm template  .  -x templates/casemonitor.yaml  | kubectl apply -f -    #手工升级casemonitor crd
	```

* 执行位置：global 集群的第一个 master 上
* 操作步骤：查询 asm-init 的 helmrequest，将其版本信息，改成新版本的 chart 的版本信息，其中将 ***$$asm$$*** 换成对应业务集群名字。

	```
	kubectl -n alauda-system  edit  hr asm-$$asm$$-asm-init
	kubectl -n alauda-system  edit  hr asm-$$asm$$-istio-init
	kubectl -n alauda-system  edit  hr asm-$$asm$$-jaeger-operator
	kubectl -n alauda-system  edit  hr asm-$$asm$$-alauda-service-mesh
	kubectl -n alauda-system  edit  hr asm-$$asm$$-istio
	```

	示例：修改 asm-asm-asm-init 的版本号信息。
	
	```
	apiVersion: app.alauda.io/v1alpha1
	kind: HelmRequest
	metadata:
	  annotations:
	    kubectl.kubernetes.io/last-applied-configuration: |
	      {"apiVersion":"app.alauda.io/v1alpha1","kind":"HelmRequest","metadata":{"annotations":{},"name":"asm-asm-asm-init","namespace":"alauda-system"},"spec":{"chart":"release/asm-init","namespace":"alauda-system","releaseName":"asm-asm-asm-init","values":{"install":{"mode":"global"}},"version":"v2.2.9"}}
	  creationTimestamp: "2019-10-17T07:37:28Z"
	  finalizers:
	  - captain.alauda.io
	  generation: 1
	  name: asm-asm-asm-init
	  namespace: alauda-system
	  resourceVersion: "94086381"
	  selfLink: /apis/app.alauda.io/v1alpha1/namespaces/alauda-system/helmrequests/asm-asm-asm-init
	  uid: f8a6e22e-f0b0-11e9-b758-ea3e455b3c6e
	spec:
	  chart: release/asm-init
	  namespace: alauda-system
	  releaseName: asm-asm-asm-init
	  values:
	    install:
	      mode: global
	  version: v2.2.9   
	```


### 更新 Service Mesh (global)

* 执行位置：global 集群的第一个 master 上
* 操作步骤：执行如下命令

	```
	helm fetch stable/asm-init --untar
	cd asm-init/
	helm upgrade asm-init . --set install.mode=global
	helm upgrade global-asm stable/global-asm
	
	```

### 启动认证组件

* 执行位置：global 集群的第一个 master 上
* 操作步骤：执行如下命令

	```
	kubectl scale deployment -n alauda-system auth-controller2 dex --replicas=2
	```

### （可选）迁移 ldap 的 configmap  

**提示**：若没有对接 ldap，可跳过此步。

平台界面访问并在平台管理界面对接 ldap，对接之后单击同步用户，需要确保同步完成。

需要等待同步完成。



### 数据初始化

* 执行位置：global 集群的第一个 master 上
* 操作步骤：执行如下命令

	```
	kubectl exec -it -n $(kubectl get po --all-namespaces | grep courier | awk '{print $1" "$2}' | head -n 1) python /migration/migrate.py   #初始化 courier 数据
	kubectl exec -it -n $(kubectl get po --all-namespaces | grep morgans | awk '{print $1" "$2}' | head -n 1) python /morgans/migration/api/migrate.py   # 初始化 morgans 数据
	```

### 升级告警模板

1. 在平台界面上，选择一个告警模板，单击列表页或详情页的更新按钮。
2. 单击添加操作指令，选择通知，单击更新按钮。


### 升级之后验证

* 执行位置：global 集群的第一个 master 上
* 操作步骤：

	1. 执行如下命令 `执行helm list -a`检查 chart 版本是否到了 v2.3。

		``` 
		kubectl get crd | grep logs.aiops.alauda.io 
		```

	1. 检查是否有这个 crd 如果没有创建。

		 ```
		 cat <<EOF > /tmp/logs-crd.yaml
		apiVersion: apiextensions.k8s.io/v1beta1
		kind: CustomResourceDefinition
		metadata:
		  name: logs.aiops.alauda.io
		  annotations:
		    "helm.sh/hook": crd-install
		spec:
		  group: aiops.alauda.io
		  versions:
		    - name: v1beta1
		      served: true
		      storage: true
		  scope: Namespaced
		  names:
		    plural: logs
		    singular: logs
		    kind: Logs
		EOF
		kubectl create -f /tmp/logs-crd.yaml
		```

### 创建alauda-es-config

* 执行位置：global 集群的第一个 master 上
* 操作步骤：执行如下命令

	```
	cat <<EOF >/tmp/esconf-logs.yaml
	apiVersion: aiops.alauda.io/v1beta1
	kind: Logs
	metadata:
	  labels:
	    chart: alauda-container-platform-v2.2.3-2.2
	  name: alauda-es-config
	  namespace: alauda-system
	spec:
	  AUDIT_TTL: 7
	  EVENT_TTL: 30
	  LOG_TTL: 30
	EOF
	kubectl create -f /tmp/esconf-logs.yaml
	```


## 集群升级

### 首先备份 etcd，方式和备份平台的 etcd 一样

### 升级alauda-cluster-base

* 执行位置：每一个集群的第一个 master 上
* 操作步骤：执行如下命令

	```
	helm upgrade alauda-cluster-base stable/alauda-cluster-base
	```


### nevermore 升级

* 执行位置：每一个集群的第一个 master 上
* 操作步骤：执行如下命令

	```
	helm upgrade alauda-log-agent stable/alauda-log-agent
	```

### ingress升级

* 执行位置：每一个集群的第一个 master 上
* 操作步骤：执行如下命令

	```
	helm upgrade nginx-ingress stable/nginx-ingress
	```

### 普罗米修斯升级

* 执行位置：每一个集群的第一个 master 上
* 操作步骤：执行如下命令

	```
	helm upgrade prometheus-operator stable/prometheus-operator
	sleep 30
	helm upgrade kube-prometheus stable/kube-prometheus
	```


### alb升级

* 执行位置：每一个集群的第一个 master 上
* 操作步骤：执行如下命令

	```
	helm upgrade alauda-alb2 stable/alauda-alb2
	```

**注意**：需要修改 alb 的 yaml ，alb 的 yaml 的 nodeselector 要手动的重新加上 `alb2:true `标签。


###  tapp升级

* 执行位置：每一个集群的第一个 master 上
* 操作步骤：执行如下命令

	```
	helm upgrade tapp-controller stable/tapp-controller
	```

### gitlab-ce 升级

* 执行位置：每一个集群的第一个 master 上
* 操作步骤：执行如下命令

	```
	helm upgrade gitlab-ce  stable/gitlab-ce
	```

### harbor 升级

* 执行位置：每一个集群的第一个 master 上
* 操作步骤：执行如下命令

	```
	helm upgrade  harbor stable/harbor
	```


### jenkins 升级

* 执行位置：每一个集群的第一个 master 上
* 操作步骤：执行如下命令

	```
	helm upgrade  jenkins stable/jenkins
	```

### 注意事项

#### devops-controller 中的 toolx 下的 yaml 更新问题

如果 Kubernetes 集群中已经存在相应的 tooltype 的资源，toolx 下的 yaml 更新可能不会更新 Kubernetes 集群中已经存在的 resource。

更新的解决方案：首先在 Kubernetes 集群中删除想要更新的 tooltype 的 resource，然后再重启 devops-controller。

示例：更新 name 为 jira 的 tooltype。

1. 执行命令：

	```
	kubectl delete tooltype jira
	```


2. 执行命令重启 devops-controller：

	```
	DC=`kubectl -n alauda-system get pod |grep devops-controller|wc -l`&&kubectl -n alauda-system scale deploy/devops-controller --replicas=0 && kubectl -n alauda-system scale deploy/devops-controller --replicas=$DC
	```

#### 若升级完成之后，平台页面无法创建应用

报错如下图所示：

<img src="images/无法创建应用.png" style="zoom:50%;" />

**解决办法**：

在 global 集群，执行命令：``` helm upgrade alauda-base stable/alauda-base```


#### 升级之后无法访问 Service Mesh 的监控

在部署 alauda-service-mesh 的业务集群，执行：`kubectl edit  ingress -n istio-system grafana` 命令，改动红框里的两行配置，即：删除 host 那一行。

改动前如下图所示。

<img src="images/asm 监控-改动前.png" style="zoom:50%;" />

改动后如下图所示。

<img src="images/asm 监控-改动后.png" style="zoom:50%;" />


## 回滚

1. 停掉所有 master 节点的 kubelet 服务。

	**执行命令的环境：** 要恢复k8s 集群的所有 master 节点上
	
	**执行的命令：** ```systemctl stop kubelet```

2. 删掉 etcd 容器。

	**执行命令的环境：** 要恢复k8s 集群的所有 master 节点上
	
	**执行的命令：** ```docker rm -f $(docker ps -a | awk '/_etcd/{print $NF}')```
	
	**命令的结果：** 所有 etcd 的容器都被删除

3. 判断 etcdctl 是否存在第一台 master 节点上，一般执行 **backup_recovery.sh** 这个备份 etcd 的脚本，会自动将 etcdctl 拷贝到 `/usr/bin/etcdctl` ，如果不存在，需要自行手动拷贝出来。

	**执行命令的环境：** 要恢复k8s 集群的第一台 master 节点上
	
	**执行的命令：** ```whereis etcdctl```
	
	**命令的结果：** 应该打印处 etcdctl 的路径，如果没有就是错的

4. 通过备份的快照恢复 etcd。

	**执行命令的环境：** 要恢复k8s 集群的第一台 master 节点上
	
	**执行的命令：** 
	
	```
	mkdir /tmp/dd ; cd /tmp/dd
	ETCD_SERVER=($(kubectl get no -o wide | awk '{if ($3 == "master")print $6}'))
	snapshot_db=<备份时，导出的 etcd 快照文件名，必须是绝对路径>
	for i in ${ETCD_SERVER[@]}
	do
	    export ETCDCTL_API=3
	    etcdctl snapshot restore ${snapshot_db} \
	    --cert=/etc/kubernetes/pki/etcd/server.crt \
	    --key=/etc/kubernetes/pki/etcd/server.key \
	    --cacert=/etc/kubernetes/pki/etcd/ca.crt \
	    --data-dir=/var/lib/etcd \
	    --name ${i} \
	    --initial-cluster ${ETCD_SERVER[0]}=https://${ETCD_SERVER[0]}:2380,${ETCD_SERVER[1]}=https://${ETCD_SERVER[1]}:2380,${ETCD_SERVER[2]}=https://${ETCD_SERVER[2]}:2380 \
	    --initial-advertise-peer-urls https://$i:2380 && \
	mv /var/lib/etcd/ etcd_$i
	done
	```
	
	**命令的结果：** 会生成 etcd_<ip 地址> 这样的三个目录，将这三个目录下的 member 目录拷贝到对应 ip 的服务器的 `/root` 内。

5. 迁移恢复的数据。

	**执行命令的环境：** 要恢复 Kubernetes 集群的所有 master 节点上
	
	**执行的命令：** 
	
	```
	mv /var/lib/etcd/member /cpaas/backup
	mv /root/var/lib/etcd/member  /var/lib/etcd
	```

**命令的结果：** 会把 etcd 的数据迁移到备份目录下，然后将上一步生成的目录拷贝到 `/var/lib/etcd` 里。

6.  启动 etcd。

	**执行命令的环境：** 要恢复Kubernetes 集群的所有 master 节点上
	
	**执行的命令：** ```systemctl start kubelet```
	
	**命令的结果：** kubelet 服务会启动，kubelet 会自动创建 etcd 的 pod，这个时候执行 ```docker ps -a | grep etcd``` 可以找到 etcd 容器。
	
	

