# 第一部分  前言

本文档介绍了如何安装和配置平台。

## 读者对象


本文档适用于具备基本的 linux、网络、存储、容器、Kubernetes 知识，想要安装和配置平台的用户，包括：

* 实施顾问和平台管理员

* 规划平台架构的售前工程师

* 负责整个项目生命周期的项目经理

## 文档目的

* 实施工程师依据本文档部署平台。

* 实施工程师依据本文档，配置平台参数。

* 平台管理员依据本文档测试平台功能。

## 修订记录

| 文档版本 | 发布日期   | 修订内容 |
| :--- | :---:| --- |
| v1.0 | 2019-12-17 | 第一版，适用于 TKE for Alauda Container Platform 私有部署版本 v2.6。 |
| v1.1 | 2020-01-19 | 修订了部分脚本错误。 |
| v1.2 | 2020-02-03 | 删除安装 kube-prometheus 脚本中多余的空格。 |
| v1.3 | 2020-02-21 | 增加了部署普罗米修斯时，提示找不到 etcd-ca 的错误时的处理方法。 |
| v1.4 | 2020-02-24 | 补充定点部署 devops 工具链的内容，node name 修改措辞，内容优化。|
| v1.5 | 2020-02-25 | 修改部署 nexus 脚本错误。|
| v1.6 | 2020-02-25 | 修改部署 nevermore 中，变量错误。 |
| v1.7 | 2020-02-25 | 更新安装包下载链接。 |
| v1.8 | 2020-03-03 | 根据反馈修改错误，增加对 jenkins 的部署建议。 |
| v1.9 | 2020-03-05 | 修改在平台部署完毕后，再部署微服务治理平台时出现的缺少变量的错误。 |
| v1.10 | 2020-03-06 | v2.6.1 发版，AMP（API 管理平台） 部署方式更新。 |
| v1.11 | 2020-03-12 | 安装包更新脚本中的一个 bug |
| v1.12 | 2020-04-11 | v2.6.2 发版 |
| v1.12 | 2020-04-26 | v2.6.3 发版 |






<div STYLE="page-break-after: always;"></div>
# 第二部分  部署和配置简介

平台有多种部署方案，在规划平台架构之前，请参考《TKE for Alauda Container Platform v2.6 部署白皮书》中部署方案部分。

平台由 global 、global 插件、业务服务集群和集群组件构成，global 是平台的核心，管理、运维功能由 global 提供。global 提供了丰富的插件来扩展 global 的功能，详细内容请参考《TKE for Alauda Container Platform v2.6 部署白皮书》。

业务服务集群是 global 管理的运行客户业务服务的 Kubernetes 集群，集群组件是运行在客户业务服务集群之上的功能模块，提供平台功能、搜集相关数据。

平台可以部署在物理机和虚拟机上，服务器的数量、配置要求等信息请参考《TKE for Alauda Container Platform v2.6 部署白皮书》中硬件需求部分。

平台的部署需要域名、vip、ip 范围、lb 等网络资源，网络资源的具体需求请参考《TKE for Alauda Container Platform v2.6 部署白皮书》中网络需求部分。

平台支持在 centos  操作系统上部署，操作系统版本和配置请参考本文档中软件需求部分。

平台中的 global 、global 插件、业务服务器集群、业务服务集群插件的部署请看本文第四部分。

<div STYLE="page-break-after: always;"></div>
# 第三部分  平台及部署过程概述

平台是一款复杂的产品，具有多个需要安装和配置的插件、组件，为确保部署成功，请了解部署架构图和部署步骤的流程顺序图。

## 安装包信息及下载链接

**ftp 服务器权限信息**

**注意**：每周更换密码。

 http://confluence.alauda.cn/pages/viewpage.action?pageId=48732098 

### 安装包下载信息

|下载链接 |安装包大小| | md5| 备注|
|:--| :---:| :---:| :---:| ---|
|ftp://ftp.alauda.cn:2121/tke-for-alauda/2.6/cpaas-2.6.3-20200426.tgz |20121093740 |19G | 4d744bbf94223562e689bb466d5e5477 |主安装包，包括 container platform 和 devops |
|ftp://118.24.210.206:2121/tke-for-alauda/2.6/cpaas-aml-1.2-20200410.tgz| 3937826534 |3.5G |11629084277923145bfb98658555f4b1| 机器学习平台 Machine Learning 的安装包|
|ftp://118.24.210.206:2121/tke-for-alauda/2.6/cpaas-amp-1.5-20200315.tgz| 751227396 | 700M| 5ec8d85bdbc5416d80f0dc2466818589 | API 管理平台 API Management Platform 的安装包|
|ftp://118.24.210.206:2121/tke-for-alauda/2.6/cpaas-asm-1.6-20200410.tgz|1358888452| 1G| b17dbbc25580de3dde91f8b754bbcc8f| 微服务治理平台 Service Mesh 的安装包|
|ftp://118.24.210.206:2121/tke-for-alauda/aml-images-package-201911241244/cpaas-aml-demo_images-20191124.tgz|388610646|300M|c4d29780b65ff1b641b4da9d7413ec34|demo  的镜像|
|ftp://118.24.210.206:2121/tke-for-alauda/aml-images-package-201911241244/cpaas-aml-full_images-20191124.tgz|17214941481|17G|fa7be19f3495edb3a027e173e328fd71|所有的机器学习镜像|

### 小产品安装包或附加镜像包使用方法

* 在平台部署的时候，部署参数 `--enabled-features` 中选择 Service Mesh、Machine Learning、AMP 等小产品，可以正常部署这些小产品，但是 pod 会因为找不到镜像而无法启动。等平台部署完毕后，按照下面的方法上传小产品的镜像 （例如：amp cpaas-amp-20200109.tgz ）的镜像到 init 节点的镜像仓库，再稍等片刻，AMP 的 pod 就可以自动 pull 下镜像启动成功了。

* 如果在平台部署成功之后，想要部署小产品，只需要下载小产品的安装包，然后按照下面的方法 push 镜像到 init 节点的镜像仓库之后，安装部署文档部署即可。

**上传小产品镜像的方法**

```
rm -rf /tmp/cpaas-package-tmp ; mkdir /tmp/cpaas-package-tmp
tar zxvf <小产品或附加镜像包的名字，例如cpaas-amp-20191204.tgz> -C /tmp/cpaas-package-tmp
cd /tmp/cpaas-package-tmp/cpaas ; bash ./upload-images-chart.sh
```
上传成功后，可以删掉 `/tmp/cpaas-package-tmp` 目录。

## 平台逻辑架构图

请参考《TKE for Alauda Container Platform v2.6 部署白皮书》。

## 平台部署架构图

请参考《TKE for Alauda Container Platform v2.6 部署白皮书》。

## 平台的部署流程图

<img src="images/部署步骤.png" style="zoom:50%;" />

<div STYLE="page-break-after: always;"></div>
# 第四部分 开始部署

## 检查部署需求是否满足

### 软件需求

请参考《TKE for Alauda Container Platform v2.6 部署白皮书》。

### 硬件需求

请参考《TKE for Alauda Container Platform v2.6 部署白皮书》。

### 网络需求

请参考《TKE for Alauda Container Platform v2.6 部署白皮书》。

## 解决不满足的需求


**注意**：因环境差异，kaldre 目录做本地源不会100% 安装软件成功，此时，就需要自行配置软件源去安装。

### 软件需求

|需求项|具体要求|不满足后的解决办法|
| ------------- | --------------- | ------------- |
| 操作系统| centos 7.6 |7.5、7.4 也可以安装部署，但需要升级 kernel (ovn 要求) 和手动安装 docker|
| kernel 版本  | 大于 3.10.0-957|必须满足|
| 操作系统安装要求|最小安装|如果不满足，可能出现配置不同造成部署失败|
| 工具软件  |curl tar ip ssh sshpass jq netstat timedatectl ntpdate nslookup base64 tr head openssl md5sum|手动添加源，并安装软件，请看表格后面的操作方式|
|用户权限|root|具备 sudo 权限的用户也可部署，但有一定几率部署失败|
|swap|关闭|必须关闭|
|防火墙|关闭|必须关闭|
|selinux|关闭|必须关闭|
|时间同步|所有服务器要求时间必须同步，误差不得超过 2 秒|必须同步|
|时区|所有服务器时区必须统一|必须统一|
|/etc/sysctl.conf| `vm.max_map_count=262144   net.ipv4.ip_forward = 1` |必须满足|
|hostname 格式|字母开头，只能是字母、数字和短横线-组成，不能用短横线结尾，长度在 4-23 之间。|必须满足|
|/etc/hosts|所有服务器可以通过hostname 解析成 ip，可以将localhost解析成 `127.0.0.1`，hosts 文件内，不能有重复的 hostname|必须满足|
|/tmp/权限| 要求 `/tmp` 目录的权限是 `777`|必须满足|

#### 解决工具软件不满足的步骤

1. 解压缩安装包

2. 将安装包内的 kaldr 目录，拷贝到每一台服务器的 `/tmp` 目录下。

	在每一台服务器上执行如下命令，添加软件源。

	* 适用 于 Centos ：

		```
		mkdir /etc/yum.repos.d/old
		mv /etc/yum.repos.d/*.repo /etc/yum.repos.d/old
		cat <<EOF >/etc/yum.repos.d/alauda.repo 
		[alauda]
		name=Alauda
		baseurl=file:///tmp/kaldr/yum/
		enabled=1
		gpgcheck=0
		EOF
		yum clean all && yum makecache
		```

	* 适用于 Ubuntu：

		```
		mv /etc/apt/sources.list /etc/apt/sources.list.old
		echo "deb [arch=amd64] file:///tmp/kaldr/apt/ ubuntu-xenial main" >/etc/apt/sources.list
		add-apt-repository "deb [arch=amd64] file:///tmp/kaldr/apt/ ubuntu-xenial main"
		cat /tmp/kaldr/apt/alauda.key | apt-key add -
		apt-get update
		```
	
	**补充说明**：可通过以下命令行，确认某个软件属于哪个包。
	
	* 适用 于 Centos：`yum provides <程序名>`
	
	* 适用于 Ubuntu ：`apt-file search <程序名>`
	
3. 安装软件包，参考以下命令行。

	* 适用 于 Centos：`yum install -y <上一步获取到的软件包的名字>`
  
	* 适用于 Ubuntu ：`apt-get install -y <上一步获取到的软件包的名字>`

### 硬件需求

**注意**：这是实施部署的前提条件，必须满足硬件需求，不满足硬件需求就无法保证正常部署，或部署成功的平台无法长时间稳定运行。

### 网络需求

**注意**：这是实施部署的前提条件，必须满足网络需求，不满足网络需求就无法保证正常部署，或部署成功的平台无法长时间稳定运行。

## 和 v2.5 、 v2.6.0 相比部署差异

### 改变的参数

* `--enabled-features`：增加了默认值，如果不指定这个参数，默认部署 Container Platform，devops，Service Mesh 和 tke。在原来的 Container Platform、devops、tke、Service Mesh、Machine Learning 这五个产品的基础上，现在增加了 amp、ace3。

	**注意**：在 `--enabled-features` 中添加 amp 自动部署的是非高可用的 amp（init 节点启动 DB），如果上生产，就需要对接外部的高可用 db ，对接方法请看本文档第四章，‘业务服务集群组件、global 产品和第三方插件部署’ 这一节中 amp 部署部分。

*  `--db-info`:  增加了这个参数，部署 amp 的时候，需要指定 amp 所需的 db 信息，例如：  `--db-info='DB_HOST=1.1.1.1;DB_PORT=222;DB_USER=root;DB_PASSWORD=asdfqwerhi29i38u;DB_ENGINE=postgresql'`
*  `--make-db`： 如果部署 amp 的时候，提供不了 db，可以加上这个参数在 init 节点自动部署一个 postgresql 数据库。

	**注意**：这个数据库不是高可用的，不能用于生产，仅适用于 poc 测试场景。

*  `--single-mode`： 如果想要部署一个单点的环境，请加上这个参数，安装包删掉了up-cpaas-single.sh 这个脚本，用 up-cpaas.sh 这个脚本加参数 `--single-mode` 来代替。
*   增加了对接 openshift 的内容


### 其他改变

* 除了 dex 和 captain 及 captain 所依赖的 cert-manager 这三个 chart 用 helm 安装外 ，剩下所有组件都是通过 captain 部署的，captain 使用及运维，请参考 v2.6 运维手册。

* v2.6.1 带的 amp 的版本从 2.6.0 带的 1.4 升级到 1.5 ，如果初次部署 `--enabled-features` 就添加 amp 部署，此时 amp 安装包中的 chart 和镜像还未上传，**会出现创建 amp 资源的时候卡住**，解决办法是在平台部署成功之后再部署 amp 或者在 up-cpaas.sh 脚本执行1分钟后，立即开一个终端，将 amp 的安装包解压缩并执行 upload-images-chart.sh 上传镜像和 chart。

* 部署 amp 成功后，访问 apm 的 ui ，会在右上角弹出一个无任何内容的报错信息，这是已知 bug ，可以按照如下方式修复：

	```
	#1 在 global 集群 增加一个 名为 portal-logo 的 configmap
	#2 修改名为 portal-configmap 的configmap: ，在 logos.yaml: 中添加一项，格式如：
	
	    - # logos
	
	      name: "portal"
	
	      url: "/assets/logos/portal.svg"
	```

* amp 部署成功后，需要给数据库建立索引，方式如下：

	```
	#1 首先执行下面的命令登录数据库
	psql -h $DB_HOST -p $DB_PORT -U $DB_USER -d ampfile
	
	#2 登录数据库成功后，执行下面的命令建立索引
	CREATE INDEX file_name_index ON files (file_name);
	
	#3 执行下面的命令查看索引是否成功创建
	\d files
	```
* amp 部署成功会，有可能出现在平台 ui 上跳转到 amp 失败的情况，需要排查 ingress 是否正确，执行 `kubectl get ingress --all-namespaces` 如下图：

![](images/amp-ingress.png)

* 如果如上图所示， amp-dashboad 和其他 ingress 在 hosts 这一列不同，就需要修改，在 global 的 master 上执行 `kubectl edit hr -n cpaas-system amp` 在如下图的位置，添加 `host: <域名>`

![](images/amp-edit-hr.png)


## global 平台部署

### server_list.json 文件格式说明

```
"server_role": {         #服务器角色
  "master":true,         #有这个键值，代表这台服务器角色是 k8s 的 master
  "global":true,         #有这个键值，代表这是运行 global 组件的服务器
  "log":true             #有这个键值，代表这是运行日志组件的服务器
},
"ip_addr": "1.1.1.1",    #服务器 ip 地址
"ssh_port": "22",        #ssh 端口
"ssh_user": "root",      #ssh 用户名
"ssh_passwd": "",        #ssh 密码
"ssh_key_file": "/root/.ssh/id_rsa" #ssh 密钥，如果这个值不为空，那么就优先通过密钥登录，就是出现错误也不会选择 password 认证登录。
```


### all in one 方案

部署命令执行的位置：all in one 服务器上，**部署过程**：

1. 解压缩安装包。

2. 进入到解压缩后的目录。

3. 执行以下命令进行部署：

   **部署方式 1**： 部署脚本配置如下参数，部署一个通过 `https://<本机 ip>` 访问的环境：

	```
	bash ./up-cpaas.sh \
	     --network-interface=<网卡名，默认eth0> \
	     --enabled-features=<需要安装那些产品，默认acp,devops,tke,asm，支持acp,devops,tke,asm,aml,amp,ace3> \
	     --single-mode
	```
  
   **部署方式 2**： 部署脚本配置如下参数，部署一个通过 `http://<本机 ip>` 访问的环境：

	```
	bash ./up-cpaas.sh \
	     --network-interface=<网卡名，默认eth0> \
	     --enabled-features=<需要安装那些产品，默认acp,devops,tke,asm，支持acp,devops,tke,asm,aml,amp,ace3> \
	     --use-http \
	     --single-mode
	```
   
   **部署方式 3**： 部署脚本配置如下参数，部署一个通过 `https://<域名>` 访问的环境：

	```
	bash ./up-cpaas.sh \
	     --network-interface=<网卡名，默认eth0> \
	     --enabled-features=<需要安装那些产品，默认acp,devops,tke,asm，支持acp,devops,tke,asm,aml,amp,ace3> \
	     --domain-name=<域名> \
	     --key-file=<域名证书私钥文件> \
	     --cert-file=<域名证书公钥文件> \
	     --single-mode
	```

**注 1**：只有上述例子中的参数经过测试，改变参数的做法很可能造成部署不成功。

**注 2**：所有有默认值的参数，都是可选的，如果部署时不添加这个参数，就使用默认值。

**注 3**：all in one 不支持部署 API 管理平台。

### poc 部署方案

部署命令执行的位置： init 服务器上，**部署过程**：

1. 解压缩安装包。

2. 进入到解压缩后的目录。

3. 在 up-cpaas.sh 脚本所在的目录下创建 server_list.json 文件，示例如下：

	``` 
	 [
	  {
	    "server_role": {
	      "master":true,
	      "global":true,
	      "log":true
	    },
	    "ip_addr": "1.1.1.1",
	    "ssh_port": "22",
	    "ssh_user": "root",
	    "ssh_passwd": "",
	    "ssh_key_file": "/root/.ssh/id_rsa"
	  },
	  {
	    "server_role": {
	      "master":true,
	      "global":true,
	      "log":true
	    },
	    "ip_addr": "2.2.2.2",
	    "ssh_port": "22",
	    "ssh_user": "root",
	    "ssh_passwd": "",
	    "ssh_key_file": "/root/.ssh/id_rsa"
	  },
	  {
	    "server_role": {
	      "master":true,
	      "global":true,
	      "log":true
	    },
	    "ip_addr": "3.3.3.3",
	    "ssh_port": "22",
	    "ssh_user": "root",
	    "ssh_passwd": "",
	    "ssh_key_file": "/root/.ssh/id_rsa"
	  }
	]
	```

4. 执行以下命令进行部署：

	**部署方式 1**：部署一个通过 `https://<域名>` 访问的高可用环境：

	```
	bash ./up-cpaas.sh \
	     --network-interface=<网卡名，默认是eth0> \
	     --cpaas-namespaces=<ns 名，默认是cpaas-system> \
	     --enabled-features=<需要安装那些产品，默认acp,devops,tke,asm，支持acp,devops,tke,asm,aml,amp,ace3> \
	     --global-network-mode=<选择 global 集群使用那种网络，默认是 ovn，可选 flannel 和 calico> \
	     --kube-cluster-subnet<global 集群的cluster ip 范围，默认是10.96.0.0/12，掩码建议不要小于16位> \
	     --domain-name=<域名> \
	     --kube_controlplane_endpoint=<k8s vip> \
	     --key-file=<域名证书私钥文件> \
	     --cert-file=<域名证书公钥文件> \
	     --db-info='<如果enabled-features 参数中有 amp，就必须加上 db-info 或 make-db 参数，否则会报错退出>' \
	     --root-username=<管理员用户名，必须是邮箱格式，默认是 admin@cpaas.io>
	     
	```
	**部署方式 2**：如果客户没有提供 lb 和 vip ，也可以用下面的命令进行部署，但不再是高可用的环境了。

	```
	bash ./up-cpaas.sh \
	     --network-interface=<网卡名，默认是eth0> \
	     --make-k8s-lb \
	     --domain-name=<域名> \
	     --key-file=<域名证书私钥文件> \
	     --cert-file=<域名证书公钥文件> \
	     --db-info='<如果enabled-features 参数中有 amp，就必须加上 db-info 或 make-db 参数，否则会报错退出>' \
	     --enabled-features=<需要安装那些产品，默认acp,devops,tke,asm，支持acp,devops,tke,asm,aml,amp,ace3> 
	```
	**部署方式 3**：如果客户连域名也不提供，也可以用下面的命令进行部署，但不再是高可用的环境了。

	```
	bash ./up-cpaas.sh \
	     --network-interface=<网卡名，默认是eth0> \
	     --make-k8s-lb \
	     --enabled-features=<需要安装那些产品，默认acp,devops,tke,asm，支持acp,devops,tke,asm,aml,amp,ace3> \
	     --db-info='<如果enabled-features 参数中有 amp，就必须加上 db-info 或 make-db 参数，否则会报错退出>' \
	     --use-http
	```
	

**注 1**： 只支持上面给出的部署例子，部署参数组合在上述例子之外的都未经测试，不能保证支持。

**注 2**： 所有有默认值的参数，都是可选的，如果部署时不添加这个参数，就使用默认值。

**注 3**： 不提供域名证书，也不使用--use-http 参数，脚本会自动签发证书，但是 chrome 浏览器会判断出这个证书不是合法机构签发的，会报错禁止浏览，需要更改 chrome 浏览器的的启动参数，容忍不合法的证书，修改方法请自行搜索。


### 正式生产环境部署方案

部署命令执行的位置： init 服务器上，**部署过程**：

1. 解压缩安装包。

2. 进入到解压缩后的目录。

3. 在 up-cpaas.sh 脚本所在的目录下创建 server_list.json 文件，示例如下：

	* 6 节点 server_list.json 文件详情：

		```
		[
		  {
		    "server_role": {
		      "master":true,
		      "global":true
		    },
		    "ip_addr": "1.1.1.1",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  },
		  {
		    "server_role": {
		      "master":true,
		      "global":true
		    },
		    "ip_addr": "2.2.2.2",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  },
		  {
		    "server_role": {
		      "master":true,
		      "global":true
		    },
		    "ip_addr": "3.3.3.3",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  },
		  {
		    "server_role": {
		      "log":true
		    },
		    "ip_addr": "4.4.4.4",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  },
		  {
		    "server_role": {
		      "log":true
		    },
		    "ip_addr": "5.5.5.5",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  },
		  {
		    "server_role": {
		      "log":true
		    },
		    "ip_addr": "6.6.6.6",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  }
		]
		```

	* 9 节点 server_list.json 文件详情：

		```
		[
		  {
		    "server_role": {
		      "master":true
		    },
		    "ip_addr": "1.1.1.1",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  },
		  {
		    "server_role": {
		      "master":true
		    },
		    "ip_addr": "2.2.2.2",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  },
		  {
		    "server_role": {
		      "master":true
		    },
		    "ip_addr": "3.3.3.3",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  },
		  {
		    "server_role": {
		      "log":true
		    },
		    "ip_addr": "4.4.4.4",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  },
		  {
		    "server_role": {
		      "log":true
		    },
		    "ip_addr": "5.5.5.5",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  },
		  {
		    "server_role": {
		      "log":true
		    },
		    "ip_addr": "6.6.6.6",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  },
		  {
		    "server_role": {
		      "global":true
		    },
		    "ip_addr": "7.7.7.7",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  },
		  {
		    "server_role": {
		      "global":true
		    },
		    "ip_addr": "8.8.8.8",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  },
		  {
		    "server_role": {
		      "global":true
		    },
		    "ip_addr": "9.9.9.9",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  }
		]
		```

4. 执行如下命令部署：

	**部署方式 1**：部署一个通过` https://<域名>` 访问的高可用环境：

	```
	bash ./up-cpaas.sh \
	     --network-interface=<网卡名，默认是eth0> \
	     --cpaas-namespaces=<ns 名，默认是cpaas-system> \
	     --enabled-features=<需要安装那些产品，默认acp,devops,tke,asm，支持acp,devops,tke,asm,aml,amp,ace3> \
	     --global-network-mode=<选择 global 集群使用那种网络，默认是 kube-ovn，可选 flannel 和 calico> \
	     --kube-cluster-subnet<global 集群的cluster ip 范围，默认是10.96.0.0/12，掩码建议不要小于16位> \
	     --domain-name=<域名> \
	     --kube_controlplane_endpoint=<k8s vip> \
	     --cert-sans=<可选参数，如果没有容灾需求可以不加，两地 global 集群 vip 的 vip> \
	     --key-file=<域名证书私钥文件> \
	     --cert-file=<域名证书公钥文件> \
	     --db-info='<如果enabled-features 参数中有 amp，就必须加上 db-info 参数，否则会报错退出>' \
	     --root-username=<管理员用户名，必须是邮箱格式，默认是 admin@cpaas.io>
	```
	
	**部署方式 2**：如果客户没有提供证书，也可以用下面的命令进行部署，脚本会自动创建一个证书，但是浏览器访问 ui 的时候会提示证书不是正规机构颁发的，有安全隐患。
	
	```
	bash ./up-cpaas.sh \
	     --network-interface=<网卡名，默认是eth0> \
	     --cpaas-namespaces=<ns 名，默认是cpaas-system> \
	     --enabled-features=<需要安装那些产品，默认acp,devops,tke,asm，支持acp,devops,tke,asm,aml,amp,ace3> \
	     --global-network-mode=<选择 global 集群使用那种网络，默认是 kube-ovn，可选 flannel 和 calico> \
	     --kube-cluster-subnet<global 集群的cluster ip 范围，默认是10.96.0.0/12，掩码建议不要小于16位> \
	     --domain-name=<域名> \
	     --kube_controlplane_endpoint=<k8s vip> \
	     --cert-sans=<可选参数，如果没有容灾需求可以不加，两地 global 集群 vip 的 vip> \
	     --db-info='<如果enabled-features 参数中有 amp，就必须加上 db-info 参数，否则会报错退出>' \
	     --root-username=<管理员用户名，必须是邮箱格式，默认是 admin@cpaas.io>
	```
	
	**部署方式 3**：如果客户没有提供证书，也不希望浏览器提示证书不是正规机构颁发，可以选择使用 http 协议访问，执行如下命令部署。
	
	```
	bash ./up-cpaas.sh \
	     --use-http \
	     --network-interface=<网卡名，默认是eth0> \
	     --cpaas-namespaces=<ns 名，默认是cpaas-system> \
	     --enabled-features=<需要安装那些产品，默认acp,devops,tke,asm，支持acp,devops,tke,asm,aml,amp,ace3> \
	     --global-network-mode=<选择 global 集群使用那种网络，默认是 kube-ovn，可选 flannel 和 calico> \
	     --kube-cluster-subnet<global 集群的cluster ip 范围，默认是10.96.0.0/12，掩码建议不要小于16位> \
	     --domain-name=<域名> \
	     --kube_controlplane_endpoint=<k8s vip> \
	     --cert-sans=<可选参数，如果没有容灾需求可以不加，两地 global 集群 vip 的 vip> \
	     --db-info='<如果enabled-features 参数中有 amp，就必须加上 db-info 参数，否则会报错退出>' \
	     --root-username=<管理员用户名，必须是邮箱格式，默认是 admin@cpaas.io>
	```

**注 1**： 只支持上面给出的部署例子，部署参数组合在上述例子之外的都未经测试，不保证能够支持。

**注 2**： 所有有默认值的参数，都是可选的，如果部署时不添加这个参数，就使用默认值。

**注 3**： 不提供域名证书，也不使用--use-http 参数，脚本会自动签发证书，但是 chrome 浏览器会判断出这个证书不是合法机构签发的，会报错禁止浏览，需要更改 chrome 浏览器的启动参数，容忍不合法的证书，修改方法请自行搜索。


## 业务服务集群部署

登录 global 的 UI 后，请参考用户手册，部署业务服务集群或对接业务服务集群。

## 业务服务集群组件、global 产品和第三方插件部署

### 业务集群必备组件

#### 业务集群必备组件 cert-manager

**前提条件**：本文档旨在说明如何在 Kubernetes 集群安装 cert-manager ，操作的前提是当前 Kubernetes 集群已经安装好了 helm。

```
registry=$(docker info |grep 60080  |tr -d ' ')
ACP_NAMESPACE=<cpaas-system>     ## 改成部署时， --acp2-namespaces 参数指定的值，默认是cpaas-system
helm install \
     --name cert-manager \
     --namespace ${ACP_NAMESPACE} \
     --set global.registry.address=$registry \
     stable/cert-manager
```

#### 业务集群必备组件 alauda-cluster-base

* 部署 alauda-cluster-base 依赖 cert-manager 和 globla 集群的 captain。

* 本文档旨在说明如何在 Kubernetes 集群安装 alauda-cluster-base。

在 global 的第一台 master 节点上执行以下命令：

```
ROOT_USERNAME=admin@cpaas.io     ## 默认为admin@cpaas.io，需要与登录global界面时使用的邮箱一致。
REGISTRY_ENDPOINT=$(docker info |grep 60080  |tr -d ' ')
REGION_NAME=<name>               ## 想要部署 alauda-cluster-base 的集群的名字
ACP_NAMESPACE=<cpaas-system>     ## 改成部署时， --acp2-namespaces 参数指定的值，默认是cpaas-system

mkdir /tmp/region_helmrequest
cat << EOF >/tmp/region_helmrequest/${REGION_NAME}-alauda-cluster-base.yaml
apiVersion: app.alauda.io/v1alpha1
kind: HelmRequest
metadata:
  finalizers:
  - captain.alauda.io
  generation: 1
  name: ${REGION_NAME}-alauda-cluster-base
  namespace: ${ACP_NAMESPACE}
spec:
  chart: stable/alauda-cluster-base
  namespace: ${ACP_NAMESPACE}
  releaseName: ${REGION_NAME}-alauda-cluster-base
  clusterName: ${REGION_NAME}
  values:
    global:
      auth:
        default_admin: ${ROOT_USERNAME}
      labelBaseDomain: cpaas.io
      namespace: ${ACP_NAMESPACE}
      registry:
        address: ${REGISTRY_ENDPOINT}
  version: $(helm search | grep '^stable/alauda-cluster-base ' | awk '{print $2}')
EOF
kubectl create -f /tmp/region_helmrequest/${REGION_NAME}-alauda-cluster-base.yaml
```

#### 业务集群必备组件-普罗米修斯

部署普罗米修斯依赖 alauda-cluster-base ，部署前一定要确认 alauda-cluster-base 已经安装部署成功。

本文档旨在说明如何在 Kubernetes 集群安装 prometheus operator 和 kube-prometheus ，操作的前提是当前 Kubernetes 集群已经安装好了 helm。

部署普罗米修斯默认部署在变量 ACP_NAMESPACE 定义的命名空间下，若无特别需要，请勿修改 namespace、Service Mesh 等需要依赖普罗米修斯，修改 namespace 时如果填写错误，会增大排查难度。


1. 准备工作，监控 etcd 先判断 etcd-ca 是否存在，在要部署普罗米修斯的集群的 master 节点操作。

	`kubectl get secrets -n ${ACP_NAMESPACE} | grep etcd-ca`

	若不存在则按以下命令添加，若存在，跳过这一步。
	
	**注意**：命令要修改 ns ，不能复制粘贴。

	```
	kubectl get secrets -n kube-system etcd-ca -o yaml >/tmp/etcd-ca.yaml
	sed -i  '/namespace:/{s/kube-system/<改成部署时 配置的 ns 的值>/g}' /tmp/etcd-ca.yaml
	kubectl apply -f /tmp/etcd-ca.yaml
	```
	
	**注意**：如果上一步报错，提示找不到 etcd-ca，执行下面的命令创建
	
	```
	kubectl create secret tls etcd-ca --cert=/etc/kubernetes/pki/etcd/ca.crt --key=/etc/kubernetes/pki/etcd/ca.key -n <改成部署时 配置的 ns 的值>
	
	kubectl create secret tls etcd-peer --cert=/etc/kubernetes/pki/etcd/peer.crt --key=/etc/kubernetes/pki/etcd/peer.key -n <改成部署时 配置的 ns 的值>
	```
	
2. 安装 prometheus-operator，在要部署的业务集群的第一台 master 节点上执行以下命令：

	```
	REGISTRY_ENDPOINT=$(docker info |grep 60080  |tr -d ' ')
	REGION_NAME=<name>                  ## 想要部署 alauda-cluster-base 的集群的名字
	ACP_NAMESPACE=<cpaas-system>        ## 改成部署时， --acp2-namespaces 参数指定的值，默认是cpaas-system
		
	helm install --version $(helm search | grep '^stable/prometheus-operator ' | awk '{print $2}') \
	             --namespace=${ACP_NAMESPACE} \
	             --name=prometheus-operator \
	             --set global.namespace=${ACP_NAMESPACE} \
	             --set global.registry.address=${REGISTRY_ENDPOINT} \
	             stable/prometheus-operator --wait --timeout 3000
	
	```

3. 安装 kube-prometheus。

	安装 kube-prometheus 有以下两种方式，一种为使用本地目录的方式存储监控数据，一种为使用 pvc 的方式存储。可自行选择。若无特殊需要，本地目录的方式即可。

	* 本地目录方式部署 kube-prometheus。
  
		给集群中的一个 node 添加 `monitoring=enabled` 的 label，用于 local volume 的调度，在要部署普罗米修斯的集群的 master 节点上操作，命令如下：

		```kubectl label --overwrite nodes test monitoring=enabled```
		
		**注意**：需要将 test 替换为这个 node 的实际 hostname。在该 node 上创建以下目录用作持久化目录，保证空间 granafa 5G/prometheus 30G/alertmanager 5G，ssh 到这台服务器上，执行如下命令：

		```
		mkdir -p /cpaas/monitoring/{grafana,prometheus,alertmanager}
		chmod -R 777 /cpaas/monitoring
		```
		**注意**：下面的操作在要部署的业务集群的第一台 master 节点上操作，执行如下命令：

		```
		REGISTRY_ENDPOINT=$(docker info |grep 60080  |tr -d ' ')
		DOMAIN_NAME=<vip or domainname>     ## 需要修改为 global 界面的访问地址，也就是部署 global 的时候，--domain-name 参数的值
		REGION_NAME=<name>                  ## 想要部署 alauda-cluster-base 的集群的名字
		ACP_NAMESPACE=<cpaas-system>        ## 改成部署 global 时， --acp2-namespaces 参数指定的值，默认是cpaas-system
		
		helm install --version $(helm search | grep '^stable/kube-prometheus ' | awk '{print $2}') \
		             --namespace=${ACP_NAMESPACE} \
		             --name=kube-prometheus \
		             --set global.namespace=${ACP_NAMESPACE} \
		             --set global.platform=ACP \
		             --set global.labelBaseDomain=cpaas.io \
		             --set prometheus.service.type=NodePort \
		             --set grafana.service.type=NodePort \
		             --set global.registry.address=${REGISTRY_ENDPOINT} \
		             --set grafana.storageSpec.persistentVolumeSpec.local.path=/cpaas/monitoring/grafana \
		             --set prometheus.storageSpec.persistentVolumeSpec.local.path=/cpaas/monitoring/prometheus \
		             --set alertmanager.storageSpec.persistentVolumeSpec.local.path=/cpaas/monitoring/alertmanager \
		             --set alertmanager.configForACP.receivers[0].name=default-receiver \
		             --set alertmanager.configForACP.receivers[0].webhook_configs[0].url=https://${DOMAIN_NAME}/v1/alerts/${REGION_NAME}/router \
		             stable/kube-prometheus
		             		
		```

	* PVC 方式部署 kube-prometheus，在要部署普罗米修斯的 Kubernetes 集群的第一台 master 节点上执行以下命令：

		```
		REGISTRY_ENDPOINT=$(docker info |grep 60080  |tr -d ' ')
		DOMAIN_NAME=<vip or domainname>     ## 需要修改为 global 界面的访问地址，也就是部署 global 的时候，--domain-name 参数的值
		REGION_NAME=<name>                  ## 想要部署 alauda-cluster-base 的集群的名字
		ACP_NAMESPACE=<cpaas-system>        ## 改成部署 global 时， --acp2-namespaces 参数指定的值，默认是cpaas-system
		storage=test                        ## 需要将 test 替换为当前集群的 StorageClass 的名字 
		
		helm install --version $(helm search | grep '^stable/kube-prometheus ' | awk '{print $2}') \
		             --namespace=${ACP_NAMESPACE} \
		             --name=kube-prometheus \
		             --set global.namespace=${ACP_NAMESPACE} \
		             --set global.platform=ACP \
		             --set global.labelBaseDomain=cpaas.io \
		             --set prometheus.service.type=NodePort \
		             --set grafana.service.type=NodePort \
		             --set global.registry.address=${REGISTRY_ENDPOINT} \
		             --set grafana.storageSpec.volumeClaimTemplate.spec.storageClassName=${storage} \
		             --set prometheus.storageSpec.volumeClaimTemplate.spec.storageClassName=${storage} \
		             --set alertmanager.storageSpec.volumeClaimTemplate.spec.storageClassName=${storage} \
		             --set alertmanager.configForACP.receivers[0].name=default-receiver \
		             --set alertmanager.configForACP.receivers[0].webhook_configs[0].url=https://${DOMAIN_NAME}/v1/alerts/${REGION_NAME}/router \
		             --set alertmanager.configForACP.receivers[0].webhook_configs[0].send_resolved='false' \		             stable/kube-prometheus

		```

4. 查看所有 pod 是否正常启动（pod 为 Running 或者 Completed 状态），在部署普罗米修斯的集群的 master 节点上执行以下命令：

	`kubectl get pods -n ${ACP_NAMESPACE} | grep prometheus`

5. 集群对接监控，在部署普罗米修斯的集群的 master 节点上操作。
	
	```
	ip=2.2.2.2                          ## 需要修改为业务集群任意一个 master 节点的外网 ip，若没有外网地址，使用默认的实际ip
	ACP_NAMESPACE=<cpaas-system>        ## 改成部署时， --acp2-namespaces 参数指定的值，默认是cpaas-system
	cat << eof > /tmp/prometheus-feature.yaml
	apiVersion: infrastructure.alauda.io/v1alpha1
	kind: Feature
	metadata:
	  labels:
	    instanceType: prometheus
	    type: monitoring
	  name: prometheus
	spec:
	  accessInfo:
	    grafanaAdminPassword: admin                # grafana 默认密码
	    grafanaAdminUser: admin                    # grafana 默认用户
	    grafanaUrl: http://$ip:30902               # grafana 地址
	    name: kube-prometheus                      # 安装 kube-prometheus chart 时指定的名称
	    namespace: ${ACP_NAMESPACE}                # kube-prometheus chart 所在的命名空间
	    prometheusTimeout: 10                      # prometheus 请求超时时间
	    prometheusUrl: http://$ip:30900            # prometheus 地址
	  instanceType: prometheus
	  type: monitoring
	  version: "1.0"
	eof
	 
	kubectl apply -f /tmp/prometheus-feature.yaml
	```

### 业务集群可选组件

#### 业务集群部署 alb2

**说明**：在 UI 页面部署。

#### nevermore

在 global 的第一台 master 节点上执行以下命令：

```
REGISTRY_ENDPOINT=$(docker info |grep 60080  |tr -d ' ')
DOMAIN_NAME=<vip or domainname>      ## 需要修改为 global 界面的访问地址 
REGION_NAME=<name>                   ## 想要部署 nevermore 的集群的名字
ACP_NAMESPACE=<cpaas-system>         ## 改成部署时， --acp2-namespaces 参数指定的值，默认是cpaas-system
https_or_http=<https>                ## 默认值是 https，如果部署的时候使用了--use-http 参数，就需要改成 http
token=$(kubectl describe secrets  $(kubectl get secret -n kube-system | awk '/^clusterrole-aggregation-controller-token/{print $1}') -n kube-system|grep ^token|awk '{print $2}')
IS_OCP=false                         ## 默认是 false ，如果对接的是OCP集群，这个值需要改成true
CONTAINER_ENGINE=docker              ## 默认是 docker，如果底层使用的是crio管理容器，这个值需要改成 crio

mkdir /tmp/region_helmrequest
cat << EOF >/tmp/region_helmrequest/${REGION_NAME}-alauda-log-agent.yaml
apiVersion: app.alauda.io/v1alpha1
kind: HelmRequest
metadata:
  finalizers:
  - captain.alauda.io
  generation: 1
  name: ${REGION_NAME}-alauda-log-agent
  namespace: ${ACP_NAMESPACE}
spec:
  chart: stable/alauda-log-agent
  namespace: ${ACP_NAMESPACE}
  releaseName: ${REGION_NAME}-alauda-log-agent
  clusterName: ${REGION_NAME}
  values:
    global:
      labelBaseDomain: cpaas.io
      namespace: ${ACP_NAMESPACE}
      isOCP: ${IS_OCP}
      registry:
        address: ${REGISTRY_ENDPOINT}
    nevermore:
      apiGatewayHost: ${https_or_http}://${DOMAIN_NAME}
      token: ${token}
      region: ${REGION_NAME}
      containerEngine: ${CONTAINER_ENGINE}
  version: $(helm search | grep '^stable/alauda-log-agent ' | awk '{print $2}')
EOF
kubectl create -f /tmp/region_helmrequest/${REGION_NAME}-alauda-log-agent.yaml
```


#### nginx-ingress


1. 在集群内选择运行 ingress 的节点，建议选择让 ingress 运行在 master 节点上。在 master 节点上，添加 `ingress=true` 的 label，可以在要部署 ingress 的 master 节点上执行 ``` for i in $(kubectl get no | awk '{if ($3 == "master")print $1}'); do echo kubectl label nodes $i ingress=true --overwrite ; done ```这个命令，即可给所有的 master 节点上添加这个标签。

2. 在 global 的第一台 master 节点上执行以下命令：

	```
	REGISTRY_ENDPOINT=$(docker info |grep 60080  |tr -d ' ')
	REGION_NAME=<name>                  ## 想要部署 ingress 的集群的名字
	ACP_NAMESPACE=<cpaas-system>        ## 改成部署时， --acp2-namespaces 参数指定的值，默认是cpaas-system
	replicas=<1>                        ## ingress 的副本数，默认1，我们建议让 ingress 部署在 master 节点上，可以在业务集群的 master 节点上执行下面的命令，获取副本数 kubectl get no | awk '{if ($3 == "master")print $1}' | wc -l
	
	mkdir /tmp/region_helmrequest
	cat << EOF >/tmp/region_helmrequest/${REGION_NAME}-nginx-ingress.yaml
	apiVersion: app.alauda.io/v1alpha1
	kind: HelmRequest
	metadata:
	  finalizers:
	  - captain.alauda.io
	  generation: 1
	  name: ${REGION_NAME}-nginx-ingress
	  namespace: ${ACP_NAMESPACE}
	spec:
	  chart: stable/nginx-ingress
	  namespace: ${ACP_NAMESPACE}
	  releaseName: ${REGION_NAME}-nginx-ingress
	  clusterName: ${REGION_NAME}
	  values:
	    global:
	      replicas: ${replicas}
	      labelBaseDomain: cpaas.io
	      namespace: ${ACP_NAMESPACE}
	      registry:
	        address: ${REGISTRY_ENDPOINT}
	  version: $(helm search | grep '^stable/nginx-ingress ' | awk '{print $2}')
	
	EOF
	kubectl create -f /tmp/region_helmrequest/${REGION_NAME}-nginx-ingress.yaml
	```


#### cron-hpa-controller（定时扩缩容的组件，可不部署）

在 global 的第一台 master 节点上执行以下命令：

```
REGISTRY_ENDPOINT=$(docker info |grep 60080  |tr -d ' ')
REGION_NAME=<name>                  ## 想要部署 cron-hpa-controller 的集群的名字
ACP_NAMESPACE=<cpaas-system>        ## 改成部署时， --acp2-namespaces 参数指定的值，默认是cpaas-system

mkdir /tmp/region_helmrequest
cat << EOF >/tmp/region_helmrequest/${REGION_NAME}-cron-hpa-controller.yaml
apiVersion: app.alauda.io/v1alpha1
kind: HelmRequest
metadata:
  finalizers:
  - captain.alauda.io
  generation: 1
  name: ${REGION_NAME}-cron-hpa-controller
  namespace: ${ACP_NAMESPACE}
spec:
  chart: stable/cron-hpa-controller
  namespace: ${ACP_NAMESPACE}
  releaseName: ${REGION_NAME}-cron-hpa-controller
  clusterName: ${REGION_NAME}
  values:
    global:
      namespace: ${ACP_NAMESPACE}
      registry:
        address: ${REGISTRY_ENDPOINT}
  version: $(helm search | grep '^stable/cron-hpa-controller ' | awk '{print $2}')

EOF
kubectl create -f /tmp/region_helmrequest/${REGION_NAME}-cron-hpa-controller.yaml
```

#### Tapp （若客户没有 Tapp 需求，可不部署）

**说明**：不支持在 ake 集群部署，在 tke 集群是通过页面扩展组件一键部署。

#### metrics-server （devops 自动扩缩容需要这个组件）dashboard 包括这个组件，如果已经部署了dashboard，就无需再部署

在想要部署 metrics-server 的集群的第一台 master 节点上执行以下命令：

```
REGISTRY_ENDPOINT=$(docker info |grep 60080  |tr -d ' ')
ACP_NAMESPACE=<cpaas-system>        ## 改成部署时， --acp2-namespaces 参数指定的值，默认是cpaas-system

cat <<EOF>/tmp/metrics-server.yaml
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: metrics-server
  namespace: ${ACP_NAMESPACE}
  labels:
    k8s-app: metrics-server
spec:
  selector:
    matchLabels:
      k8s-app: metrics-server
  template:
    metadata:
      name: metrics-server
      labels:
        k8s-app: metrics-server
    spec:
      containers:
      - name: metrics-server
        image: ${REGISTRY_ENDPOINT}/alaudak8s/metrics-server-amd64:v0.3.1
        imagePullPolicy: IfNotPresent
        command:
        - /metrics-server
        - --kubelet-preferred-address-types=InternalIP
        - --kubelet-insecure-tls
        volumeMounts:
        - name: tmp-dir
          mountPath: /tmp
      volumes:
      # mount in tmp so we can safely use from-scratch images and/or read-only containers
      - name: tmp-dir
        emptyDir: {}
 
---
apiVersion: v1
kind: Service
metadata:
  name: metrics-server
  namespace: ${ACP_NAMESPACE}
  labels:
    kubernetes.io/name: "Metrics-server"
spec:
  selector:
    k8s-app: metrics-server
  ports:
  - port: 443
    protocol: TCP
    targetPort: 443
---
apiVersion: apiregistration.k8s.io/v1beta1
kind: APIService
metadata:
  name: v1beta1.metrics.k8s.io
spec:
  service:
    name: metrics-server
    namespace: ${ACP_NAMESPACE}
  group: metrics.k8s.io
  version: v1beta1
  insecureSkipTLSVerify: true
  groupPriorityMinimum: 100
  versionPriority: 100
EOF
kubectl create -f /tmp/metrics-server.yaml

```

#### cpaas-lander（对接 openshift 需要）

1. 背景

由于 openshift 账号认证跟原生 k8s 有所区别，其具有独立的用户认证系统。而 ACP2.x 在接入原生 k8s 时采用的标准 OIDC 的认证方式，为了使 ACP2.x 也能平滑接入 openshift 集群，灵雀提供了 cpaas-lander 组件，在 openshift api-server 上层封装了一层代理，ACP2.x 通过这层代理接入。

2. 部署说明：

所需部署的 Chart 名为：cpaas-lander，只能部署在 openshift 集群。需要手动为该组件创建所需 namespace，安装包默认定义的 ns 为 cpaas-lander。该组件默认是通过 nodeport 方式提供服务，默认端口为 32008 。如 https://<openshift ip>:32008。由于 openshift 集群集成的 k8s 版本过低，需要给组件 cluster-registry-manager 增加环境变量 SUPPORTED_VERSIONS 指定 openshift 内置的 k8s 版本号。dex 接入的第三方账号，在 openshift 里再接入一次，以便于 idp 用户通过 oc 命令行账号密码方式登录，此步骤非必须，不接也不影响接入 openshift 。

3.  生成 ocp 集群证书，在 init 节点上执行如下命令：

```
mkdir -p /cpaas/ocp-ssl
DEX_IP_LIST="<ocp集群域名>,<ocp 集群ip>"
PWD_LOCAL=<安装目录路径，比如 /opt/install-package/cpaas ，最后没有/>

## 生成创建证书的yaml 文件
cat << EOF > /cpaas/ocp-ssl/kubeadmcfg.yaml
apiVersion: kubeadm.k8s.io/v1beta1
kind: InitConfiguration
---
apiServer:
  certSANs: [${DEX_IP_LIST[*]}]
apiVersion: kubeadm.k8s.io/v1beta1
certificatesDir: /cpaas/ocp-ssl/
kind: ClusterConfiguration
kubernetesVersion: v1.13.12
EOF

${PWD_LOCAL}/other/kubeadm init phase certs ca --config /cpaas/ocp-ssl/kubeadmcfg.yaml >/dev/null
${PWD_LOCAL}/other/kubeadm init phase certs apiserver --config /cpaas/ocp-ssl/kubeadmcfg.yaml 

mv /cpaas/ocp-ssl/apiserver.crt /cpaas/ocp-ssl/cert.pem
mv /cpaas/ocp-ssl/apiserver.key /cpaas/ocp-ssl/key.pem
```

4. 把 /cpaas/ocp-ssl/cert.pem 和 /cpaas/ocp-ssl/key.pem 拷贝到 ocp 集群 master 的同名目录下
5. 安装 capps-lander，在 ocp 集群上，执行如下命令:

```
helm install stable/cpaas-lander --name=cpaas-lander \
    --set lander.oidc_issuer_url=https://<平台域名>/dex \
    --set lander.openshift_server_address=<openshift ip>:8443 \
    --set nodePort.port=32008 \
    --set lander.tls_secure=true \
    --set secret.tlsCrt="$(cat /cpaas/ocp-ssl/cert.pem | base64 -w 0)" \
    --set secret.tlsKey="$(cat /cpaas/ocp-ssl/key.pem | base64 -w 0)" \
```



Charts 参数说明：

```
lander:
  oidc_issuer_url: https://129.28.182.197/dex   dex issuer address, eg: https://129.28.182.197/dex （必须是https的地址）
  oidc_client_id: alauda-auth                   dex client id, default: alauda-auth
  openshift_server_proto: https                 openshift apiserver schema (http or https)
  openshift_server_address: 10.0.128.51:8443    openshift apiserver address, eg: 10.0.128.51:8443
  tls_secure: false                             是否开启https
 
nodePort:
  enabled: true                                 是否开启nodeport，默认开启
  port: 32008                                   端口，默认32008
 
global:
  namespace: cpaas-lander
  replicas: 1
 
secret:
  tlsName: lander.tls
  tlsCrt: Cg==
  tlsKey: Cg==
```

### global 产品和第三方插件部署

#### ACE 3.3

**说明**：如果在部署 global 的时候，`--enabled-features` 这个参数没有加上 ace3 ，需要手动部署容器管理平台的 globla 组件，在 global 集群的第一个 master 节点上，执行如下操作：

```
ACP_NAMESPACE=<cpaas-system>  ## 改成部署平台时， --acp2-namespaces 参数指定的值，默认是cpaas-system
kubectl captain create \
        --version $(helm search | grep '^stable/alauda-cloud-enterprise ' | awk '{print $2}') \
        --configmap=acp-config \
        --namespace=${ACP_NAMESPACE} \
        --chart=stable/alauda-cloud-enterprise \
        alauda-cloud-enterprise
```

#### Container-platform（容器管理平台）

**说明**：

* `<>` 代表要根据环境情况替换尖括号内的值，替换完毕之后，不包括尖括号。

* 如果在部署 global 的时候，`--enabled-features` 这个参数没有加上  acp ，需要手动部署容器管理平台的 globla 组件。

在 global 集群的第一个 master 节点上执行以下命令：

```
ACP_NAMESPACE=<cpaas-system>  ## 改成部署平台时， --acp2-namespaces 参数指定的值，默认是cpaas-system
REGISTRY_ENDPOINT =$(docker info |grep 60080  |tr -d ' ')
kubectl captain create \
           --version $(helm search | grep '^stable/alauda-container-platform' | awk '{print $2}') \
           --configmap=acp-config \
           --namespace=${ACP_NAMESPACE} \
           --chart=stable/alauda-container-platform \
           alauda-container-platform
           
kubectl captain create \
           --version $(helm search | grep '^stable/public-chart-repo' | awk '{print $2}') \
           --set global.registry.address=${REGISTRY_ENDPOINT} \
           --set global.namespace=${ACP_NAMESPACE} \
           --namespace=${ACP_NAMESPACE} \
           --chart=stable/public-chart-repo \
           public-chart-repo
```

#### devops

**说明**：

* `<>` 代表要根据环境情况替换尖括号内的值，替换完毕之后，不包括尖括号。

* 如果在部署 global 的时候，`--enabled-features` 这个参数没有加上 devops ，需要手动部署 devops 的 globla 组件。

在 global 集群的第一个 master 节点上执行以下命令：

```
ACP_NAMESPACE=<cpaas-system>  ## 改成部署平台时， --acp2-namespaces 参数指定的值，默认是cpaas-system
kubectl captain create \
        --version $(helm search | grep '^stable/alauda-devops ' | awk '{print $2}') \
        --configmap=acp-config \
        --namespace=${ACP_NAMESPACE} \
        --chart=stable/alauda-devops \
        alauda-devops
```

##### devops 所需的 harbor、jenkins、gitlab、nexus、sonarqube 第三方插件部署

**说明**：若使用 PVC 方式部署以下插件。需要事先准备好对应的 PVC，PVC 默认我们提供的是对接 cephfs，因此需要先确保集群对接了 ceph，然后创建对应的 PVC。

```
cat <<EOF > cephfs.yaml
   
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: claim1                 ###  对应 pvc 的名字
  namespace: default
  annotations:
    volume.beta.kubernetes.io/storage-class: cephfs
    volume.beta.kubernetes.io/storage-provisioner: ceph.com/cephfs
  finalizers:
    - kubernetes.io/pvc-protection
spec:
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 1Gi              ###对应 pvc 的大小
  volumeMode: Filesystem
EOF
   
kubectl create -f cephfs.yaml
```

###### 部署 jenkins

jenkins 的部署方式有以下四种，我们推荐在业务集群部署 jenkins，且最好以 PVC 的方式部署。***建议将 jenkins 部署到单独的机器上，以免 devops 的构建 pod 调度到 jenkins 所在所在主机执行构建任务从而影响jenkins的稳定性***，可以采用设置污点的方式。

**相关指令：**

* 设置污点：kubectl taint nodes node1 key1=value1:NoSchedule

* 去除污点：kubectl taint nodes node1 key1:NoSchedule-

**部署操作：**

* 在 global 集群以本地目录的方式部署 jenkins，（对接了GitLab）。

	```
	NODE_NAME="acp2-master-1"            ###需要修改为集群中实际存放jenkins数据的某个节点的name，通过 kubectl get no 命令获取到的 name
	path="/root/alauda/jenkins"          ###默认数据目录为/root/alauda/jenkins，若有需要可更改。
	password="Jenkins12345"              ###默认密码为Jenkins12345，若有需要可更改
	REGISTRY=$(docker info |grep 60080  |tr -d ' ')
	ACP_NAMESPACE=<cpaas-system>     ## 改成部署时， --acp2-namespaces 参数指定的值，默认是cpaas-system
	 
	cat <<EOF > values.yaml
	 
	global:
	  registry:
	    address: ${REGISTRY}
	Master:
	  ServiceType: NodePort
	  NodePort: 32001
	  AdminPassword: "$password"
	  gitlabConfigs:                      ###可选参数，用来配置GitLab服务器，如没有，这块可以删除，或者安装之后通过Jenkins的页面进行操作
	    - name: <GitLab服务器名字>          ###GitLab服务器名字，是DevOps集成平台上面对应的显示名称。DevOps管理视图->DevOps工具链 对应的GitLab服务名字
	      manageHooks: true               ###自动添加GitLab的Webhook
	      serverUrl: <GitLab服务器地址>     ###GitLab服务器的地址，访问地址
	      token: <GitLab凭据token>         ###GitLab的Personal access tokens，需要在GitLab上生成
	  Location:
	    url: <Jenkins Location URL>       ###Jenkins的访问地址，用来做GitLab的webhook回调使用
	Persistence:
	  Enabled: false
	  Host:
	    NodeName: ${NODE_NAME}
	    Path: $path
	AlaudaACP:
	  Enabled: true
	alaudapipelineplugin:
	  consoleURL: ""
	  apiEndpoint: ""
	  apiToken: ""
	  account: ""
	  spaceName: ""
	  clusterName: ""
	  namespace: ""
	AlaudaDevOpsCredentialsProvider:
	  globalNamespaces: "${ACP_NAMESPACE}-global-credentials,${ACP_NAMESPACE}"
	Erebus:
	  Namespace: "${ACP_NAMESPACE}"
	  URL: "https://erebus.${ACP_NAMESPACE}.svc.cluster.local:443/kubernetes"
	 
	EOF
	
	helm install stable/jenkins --name jenkins --namespace default -f values.yaml
	```

* 在 global 以 pvc 的方式部署 jenkins，（对接了GitLab）。

	```
	password="Jenkins12345"              ###默认密码为Jenkins12345，若有需要可更改
	pvc_name="jenkinspvc"                ###默认pvc的名字为jenkinspvc，需要事先在default命名空间下准备好这个pvc，可更改，但也需要事先创建好对应的pvc。
	REGISTRY=$(docker info |grep 60080  |tr -d ' ')
	ACP_NAMESPACE=<cpaas-system>     ## 改成部署时， --acp2-namespaces 参数指定的值，默认是cpaas-system

	cat <<EOF > values.yaml
	 
	global:
	  registry:
	    address: ${REGISTRY}
	Master:
	  ServiceType: NodePort
	  NodePort: 32001
	  AdminPassword: "$password"
	  gitlabConfigs:                      ###可选参数，用来配置GitLab服务器，如没有，这块可以删除，或者安装之后通过Jenkins的页面进行操作
	    - name: <GitLab服务器名字>          ###GitLab服务器名字，是DevOps集成平台上面对应的显示名称。DevOps管理视图->DevOps工具链 对应的GitLab服务名字
	      manageHooks: true               ###自动添加GitLab的Webhook
	      serverUrl: <GitLab服务器地址>     ###GitLab服务器的地址，访问地址
	      token: <GitLab凭据token>         ###GitLab的Personal access tokens，需要在GitLab上生成
	  Location:
	    url: <Jenkins Location URL>       ###Jenkins的访问地址，用来做GitLab的webhook回调使用
	Persistence:
	  Enabled: true
	  ExistingClaim: "$pvc_name"
	AlaudaACP:
	  Enabled: true
	alaudapipelineplugin:
	  consoleURL: ""
	  apiEndpoint: ""
	  apiToken: ""
	  account: ""
	  spaceName: ""
	  clusterName: ""
	  namespace: ""
	AlaudaDevOpsCredentialsProvider:
	  globalNamespaces: "${ACP_NAMESPACE}-global-credentials,${ACP_NAMESPACE}"
	Erebus:
	  Namespace: "${ACP_NAMESPACE}"
	  URL: "https://erebus.${ACP_NAMESPACE}.svc.cluster.local:443/kubernetes"
	 
	EOF
	 
	helm install stable/jenkins --name jenkins --namespace default -f values.yaml
	
	```

* 在业务集群以本地路径的方式部署 jenkins，（对接了GitLab）。

	```
	NODE_NAME="acp2-master-1"            ###需要修改为集群中实际存放jenkins数据的某个节点的name，通过 kubectl get no 命令获取到的 name
	global_vip="1.1.1.1"                 ###需要修改为平台的访问地址，如果访问地址是域名，就必须配置成域名，因为 jenkins 需要访问 global 平台的 erebus，如果平台是域名访问的话，erebus 的 ingress 策略会配置成只能域名访问。
	path="/root/alauda/jenkins"          ###默认数据目录为/root/alauda/jenkins，若有需要可更改。
	password="Jenkins12345"              ###默认密码为Jenkins12345，若有需要可更改
	REGISTRY=$(docker info |grep 60080  |tr -d ' ')
	TOKEN=       ### 如何获取token，到devops-apiserver所在集群（一般为global集群）执行：echo $(kubectl get secret -n ${ACP_NAMESPACE} $(kubectl get secret -n ${ACP_NAMESPACE} | grep devops-apiserver-token |awk '{print $1}') -o jsonpath={.data.token} |base64 --d)
	ACP_NAMESPACE=<cpaas-system>     ## 改成部署时， --acp2-namespaces 参数指定的值，默认是cpaas-system

	cat <<EOF > values.yaml
	  
	global:
	  registry:
	    address: ${REGISTRY}
	Master:
	  ServiceType: NodePort
	  NodePort: 32001
	  AdminPassword: "$password"
	  gitlabConfigs:                      ###可选参数，用来配置GitLab服务器，如没有，这块可以删除，或者安装之后通过Jenkins的页面进行操作
	    - name: <GitLab服务器名字>          ###GitLab服务器名字，是DevOps集成平台上面对应的显示名称。DevOps管理视图->DevOps工具链 对应的GitLab服务名字
	      manageHooks: true               ###自动添加GitLab的Webhook
	      serverUrl: <GitLab服务器地址>     ###GitLab服务器的地址，访问地址
	      token: <GitLab凭据token>         ###GitLab的Personal access tokens，需要在GitLab上生成
	  Location:
	    url: <Jenkins Location URL>       ###Jenkins的访问地址，用来做GitLab的webhook回调使用
	Persistence:
	  Enabled: false
	  Host:
	    NodeName: ${NODE_NAME}
	    Path: $path
	AlaudaACP:
	  Enabled: false
	alaudapipelineplugin:
	  consoleURL: ""
	  apiEndpoint: ""
	  apiToken: ""
	  account: ""
	  spaceName: ""
	  clusterName: ""
	  namespace: ""
	AlaudaDevOpsCredentialsProvider:
	  globalNamespaces: "${ACP_NAMESPACE}-global-credentials,${ACP_NAMESPACE},kube-system"
	AlaudaDevOpsCluster:
	  Cluster:
	    masterUrl: "https://$global_vip:6443"
	    token: ${TOKEN}
	Erebus:
	  Namespace: "${ACP_NAMESPACE}"
	  URL: "https://$global_vip:443/kubernetes"
	 
	EOF
	 
	helm install stable/jenkins --name jenkins --namespace default -f values.yaml
	
	```

* 在业务集群以 PVC 的方式部署 jenkins，（对接了GitLab）。

	```
	global_vip="1.1.1.1"                 ###需要修改为平台的访问地址，如果访问地址是域名，就必须配置成域名，因为 jenkins 需要访问 global 平台的 erebus，如果平台是域名访问的话，erebus 的 ingress 策略会配置成只能域名访问。
	password="Jenkins12345"              ###默认密码为Jenkins12345，若有需要可更改
	pvc_name="jenkinspvc"                ###默认pvc的名字为jenkinspvc，需要事先在default命名空间下准备好这个pvc，可更改，但也需要事先创建好对应的pvc。
	REGISTRY=$(docker info |grep 60080  |tr -d ' ')
	TOKEN=       ### 如何获取token，到devops-apiserver所在集群（一般为global集群）执行：echo $(kubectl get secret -n ${ACP_NAMESPACE} $(kubectl get secret -n ${ACP_NAMESPACE} | grep devops-apiserver-token |awk '{print $1}') -o jsonpath={.data.token} |base64 -d)
	ACP_NAMESPACE=<cpaas-system>     ## 改成部署时， --acp2-namespaces 参数指定的值，默认是cpaas-system

	cat <<EOF > values.yaml
	  
	global:
	  registry:
	    address: ${REGISTRY}
	Master:
	  ServiceType: NodePort
	  NodePort: 32001
	  AdminPassword: "$password"
	  gitlabConfigs:                      ###可选参数，用来配置GitLab服务器，如没有，这块可以删除，或者安装之后通过Jenkins的页面进行操作
	    - name: <GitLab服务器名字>          ###GitLab服务器名字，是DevOps集成平台上面对应的显示名称。DevOps管理视图->DevOps工具链 对应的GitLab服务名字
	      manageHooks: true               ###自动添加GitLab的Webhook
	      serverUrl: <GitLab服务器地址>     ###GitLab服务器的地址，访问地址
	      token: <GitLab凭据token>         ###GitLab的Personal access tokens，需要在GitLab上生成
	  Location:
	    url: <Jenkins Location URL>       ###Jenkins的访问地址，用来做GitLab的webhook回调使用
	Persistence:
	  Enabled: true
	  ExistingClaim: "$pvc_name"
	AlaudaACP:
	  Enabled: false
	alaudapipelineplugin:
	  consoleURL: ""
	  apiEndpoint: ""
	  apiToken: ""
	  account: ""
	  spaceName: ""
	  clusterName: ""
	  namespace: ""
	AlaudaDevOpsCredentialsProvider:
	  globalNamespaces: "${ACP_NAMESPACE}-global-credentials,${ACP_NAMESPACE},kube-system"
	AlaudaDevOpsCluster:
	  Cluster:
	    masterUrl: "https://$global_vip:6443"
	    token: ${TOKEN}
	Erebus:
	  Namespace: "${ACP_NAMESPACE}"
	  URL: "https://$global_vip:443/kubernetes"
	 
	EOF
	 
	helm install stable/jenkins --name jenkins --namespace default -f values.yaml
	```

	部署成功后到 Jenkins 修改配置访问集群任意一个节点 ip+32001 端口，访问 jenkins 页面，用户名：admin   密码：Jenkins12345。

	单击 **系统管理->系统设置**：
	
	a. Alauda Jenkins Sync 里勾选 **启用** 按钮，添加 ***Jenkins 服务名称***，例如：jenkinstest，名称不能和别的已经部署的 jekins 服务名称一样（例如：global 里的 jenkins），界面集成的时候必须与这个名字一致。
  
	b. 到 Kubernetes Cluster Configuration 下的 Credentials，单击添加，添加一个 serviceaccount token （类型选择 secret text， Secret 位置输入token， ID 输入任意凭据名称（例如：test-token））。<br>如何获取 token？到 devops-apiserver 所在集群（一般为 global 集群）执行命令行：<br>`echo $(kubectl get secret -n ${ACP_NAMESPACE} $(kubectl get secret -n ${ACP_NAMESPACE}  | grep devops-apiserver-token |awk '{print $1}') -o jsonpath={.data.token} |base64 -d)`。<br>添加之后。Credentials 选择刚才创建的 secret。
  
	c. 都配置成功后，在 Kubernetes Cluster Configuration 下单击 [Test Connection] 测试连接，提示 `Connect to  succeed` 后，单击保存。


###### 部署 gitlab


部署 gitlab 有以下两种方式，推荐部署在业务集群，且以 PVC 的方式部署。

* 以本地路径的方式部署。

	```
	REGISTRY=$(docker info |grep 60080  |tr -d ' ')
	NODE_NAME=acp2-master-1      ###需要修改为选择部署gialab的节点的name，通过 kubectl get no 命令获取到的 name
	NODE_IP="1.1.1.1"            ###这个ip为gitlab的访问地址，需要修改为部署集群中任意master节点一个节点的实际ip
	potal_path="/root/alauda/gitlab/portal"         ###potal的数据目录，一般不需要修改，若有规划，可修改为别的目录
	database_path="/root/alauda/gitlab/database"    ###database的目录，一般不需要修改，若有规划，可修改为别的目录
	redis_path="/root/alauda/gitlab/redis"          ###redis的目录，一般不需要修改，若有规划，可修改为别的目录
	helm install stable/gitlab-ce --name gitlab-ce --namespace default \
	    --set global.registry.address=${REGISTRY} \
	    --set portal.debug=true \
	    --set gitlabHost=${NODE_IP} \
	    --set gitlabRootPassword=Gitlab12345 \
	    --set service.type=NodePort \
	    --set service.ports.http.nodePort=31101 \
	    --set service.ports.ssh.nodePort=31102 \
	    --set service.ports.https.nodePort=31103 \
	    --set portal.persistence.enabled=false \
	    --set portal.persistence.host.nodeName=${NODE_NAME} \
	    --set portal.persistence.host.path="$potal_path" \
	    --set portal.persistence.host.nodeName="${NODE_NAME}" \
	    --set database.persistence.enabled=false \
	    --set database.persistence.host.nodeName=${NODE_NAME} \
	    --set database.persistence.host.path="$database_path" \
	    --set database.persistence.host.nodeName="${NODE_NAME}" \
	    --set redis.persistence.enabled=false \
	    --set redis.persistence.host.nodeName=${NODE_NAME} \
	    --set redis.persistence.host.path="$redis_path" \
	    --set redis.persistence.host.nodeName="${NODE_NAME}" \
	```

* PVC 的方式部署 gitlab

	```
	NODE_IP=1.1.1.1   ###此参数为部署时指定的访问地址，写当前集群中任意一个master节点的ip即可
	portal_pvc="portalpvc"          ###默认pvc的名字为portalpvc，需要事先在default命名空间下准备好这个pvc，可更改，但也需要事先创建好对应的pvc。
	database_pvc="databasepvc"      ###默认pvc的名字为databasepvc，需要事先在default命名空间下准备好这个pvc，可更改，但也需要事先创建好对应的pvc。
	redis_pvc="redispvc"           ###默认pvc的名字为redispvc，需要事先在default命名空间下准备好这个pvc，可更改，但也需要事先创建好对应的pvc。
	REGISTRY=$(docker info |grep 60080  |tr -d ' ')
	helm install stable/gitlab-ce --name gitlab-ce --namespace default \
	    --set global.registry.address=${REGISTRY} \
	    --set portal.debug=true \
	    --set gitlabHost=${NODE_IP} \
	    --set gitlabRootPassword=Gitlab12345 \
	    --set service.type=NodePort \
	    --set service.ports.http.nodePort=31101 \
	    --set service.ports.ssh.nodePort=31102 \
	    --set service.ports.https.nodePort=31103 \
	    --set portal.persistence.enabled=true \
	    --set portal.persistence.existingClaim=$portal_pvc \
	    --set database.persistence.enabled=true \
	    --set database.persistence.existingClaim=$database_pvc \
	    --set redis.persistence.enabled=true \
	    --set redis.persistence.existingClaim=$redis_pvc \
	```

	部署完成之后，通过 `NODE_IP+31101` 端口访问 gitlab，默认用户名为：root，密码为：Gitlab12345。

###### 部署 harbor 镜像仓库

部署 harbor 有以下两种部署方式，推荐部署在业务集群，且以 PVC 的方式部署。

* 本地目录的方式部署 harbor。

	```
	REGISTRY=$(docker info |grep 60080  |tr -d ' ')
	NODE_IP="1.1.1.1"          ###此参数为部署时指定的访问地址，写当前集群中任意一个master节点的ip即可
	NODE_NAME="acp-master-1"   ###需要修改为选择部署harbor节点的name，通过 kubectl get no 命令获取到的 name
	HOST_PATH=/alauda/harbor   ###这个目录为harbor的数据目录路径，一般不需要修改，若有别的规划，可修改。
	harbor_password="Harbor12345"  ####harbor的密码，默认不需要修改，若有规划，可改
	db_password="Harbor4567"       ####harbor数据库的密码，默认不需要修改，若有规划，可改
	redis_password="Redis789"      ###harbor的redis的密码，默认不需要修改，若有规划，可改
	helm install --name harbor --namespace default stable/harbor \
	    --set global.registry.address=${REGISTRY} \
	    --set externalURL=http://${NODE_IP}:31104 \
	    --set harborAdminPassword=$harbor_password \
	    --set ingress.enabled=false \
	    --set service.type=NodePort \
	    --set service.ports.http.nodePort=31104 \
	    --set service.ports.ssh.nodePort=31105 \
	    --set service.ports.https.nodePort=31106 \
	    --set database.password=$db_password \
	    --set redis.usePassword=true \
	    --set redis.password=$redis_password \
	    --set database.persistence.enabled=false \
	    --set database.persistence.host.nodeName=${NODE_NAME} \
	    --set database.persistence.host.path=${HOST_PATH}/database \
	    --set redis.persistence.enabled=false \
	    --set redis.persistence.host.nodeName=${NODE_NAME} \
	    --set redis.persistence.host.path=${HOST_PATH}/redis \
	    --set chartmuseum.persistence.enabled=false \
	    --set chartmuseum.persistence.host.nodeName=${NODE_NAME} \
	    --set chartmuseum.persistence.host.path=${HOST_PATH}/chartmuseum \
	    --set registry.persistence.enabled=false \
	    --set registry.persistence.host.nodeName=${NODE_NAME} \
	    --set registry.persistence.host.path=${HOST_PATH}/registry \
	    --set jobservice.persistence.enabled=false \
	    --set jobservice.persistence.host.nodeName=${NODE_NAME} \
	    --set jobservice.persistence.host.path=${HOST_PATH}/jobservice \
	    --set AlaudaACP.Enabled=false \
	```

* PVC 的方式部署 harbor。

	```
	REGISTRY=$(docker info |grep 60080  |tr -d ' ')
	NODE_IP="1.1.1.1"             ######此参数为部署时指定的访问地址，写当前集群中任意一个master节点的ip即可
	database_pvc=habordatabase   ###harbor数据库使用的pvc，需要事先在default下创建这个pvc
	redis_pvc=harborredis        ###harbor的redis使用的pvc，需要事先在default下创建这个pvc
	chartmuseum_pvc=harbormuseum   ###harbor使用的pvc，需要事先在default下创建这个pvc
	registry_pvc=harborregistry     ###harbor的registry使用的pvc，需要事先在default下创建这个pvc
	jobservice_pvc=harborjob        ###harbor使用的pvc，需要事先在default下创建这个pvc
	harbor_password="Harbor12345"  ####harbor的密码，默认不需要修改，若有规划，可改
	db_password="Harbor4567"       ####harbor数据库的密码，默认不需要修改，若有规划，可改
	redis_password="Redis789"      ###harbor的redis的密码，默认不需要修改，若有规划，可改
	helm install --name harbor --namespace default stable/harbor \
	    --set global.registry.address=${REGISTRY} \
	    --set externalURL=http://${NODE_IP}:31104 \
	    --set harborAdminPassword=$harbor_password \
	    --set ingress.enabled=false \
	    --set service.type=NodePort \
	    --set service.ports.http.nodePort=31104 \
	    --set service.ports.ssh.nodePort=31105 \
	    --set service.ports.https.nodePort=31106 \
	    --set database.password=$db_password \
	    --set redis.usePassword=true \
	    --set redis.password=$redis_password \
	    --set database.persistence.enabled=true \
	    --set database.persistence.existingClaim=${database_pvc} \
	    --set redis.persistence.enabled=true \
	    --set redis.persistence.existingClaim=${redis_pvc} \
	    --set chartmuseum.persistence.enabled=true \
	    --set chartmuseum.persistence.existingClaim=${chartmuseum_pvc} \
	    --set registry.persistence.enabled=true \
	    --set registry.persistence.existingClaim=${registry_pvc} \
	    --set jobservice.persistence.enabled=true \
	    --set jobservice.persistence.existingClaim=${jobservice_pvc} \
	    --set AlaudaACP.Enabled=false \
	```

	部署完成后通过传入的部署 ip+31104 端口访问 harbor。默认用户名为：admin，密码为：Harbor12345。

###### 部署 nexus

存储方式：host path  或  PVC（推荐）。

* 以 hostPath 方式部署。

	```
	  NODE_IP=<要集群的 master 的 ip，nexus 暴露的端口是 nodePorts，集群内所有服务器都可以，但 master 节点最好，因为不用担心 master 节点被删除>
	  NODE_NAME=node1  ## 集群中实际存放 nexus 数据的某个节点的name，也就是定点部署 nexus 的节点的 name，通过 kubectl get no 命令获取到的 name
	  HOST_PATH=/alauda/nexus
	  REGISTRY=$(docker info |grep 60080  |tr -d ' ')
	  helm install stable/nexus --name nexus \
	      --set global.registry.address=${REGISTRY} \
	      --set nexus.service.nodePort=32010 \
	      --set nexusProxy.env.nexusHttpHost=${NODE_IP} \
	      --set nexusProxy.env.nexusDockerHost=${NODE_IP} \
	      --set persistence.host.nodeName=${NODE_NAME} \
	      --set persistence.host.path=${HOST_PATH}
	```

* 以 StorageClass 方式，自动创建 PVC 部署, 需要提前创建好对应 STORAGE_CLASS 的 PV。

	```
	  NODE_IP=<要集群的 master 的 ip，nexus 暴露的端口是 nodePorts，集群内所有服务器都可以，但 master 节点最好，因为不用担心 master 节点被删除>
	  NEXUS_SC=nexus-sc  ## 给 nexus 使用的 pvc 的名字
	  REGISTRY=$(docker info |grep 60080  |tr -d ' ')
	  helm install stable/nexus --name nexus \
	      --set global.registry.address=${REGISTRY} \
	      --set nexus.service.nodePort=32010 \
	      --set nexusProxy.env.nexusHttpHost=${NODE_IP} \
	      --set nexusProxy.env.nexusDockerHost=${NODE_IP} \
	      --set persistence.enabled=true \
	      --set persistence.storageClass=${NEXUS_SC}
	```

* 已经存在 PVC 时，可以直接用 PVC 部署，需要提前创建好对应 PVC。

	```
	 NODE_IP=<要集群的 master 的 ip，nexus 暴露的端口是 nodePorts，集群内所有服务器都可以，但 master 节点最好，因为不用担心 master 节点被删除>
	  NEXUS_PVC=nexus-pvc
	  REGISTRY=$(docker info |grep 60080  |tr -d ' ')
	  helm install . --name nexus \
	      --set global.registry.address=${REGISTRY} \
	      --set nexus.service.nodePort=32010 \
	      --set nexusProxy.env.nexusHttpHost=${NODE_IP} \
	      --set nexusProxy.env.nexusDockerHost=${NODE_IP} \
	      --set persistence.enabled=true \
	      --set persistence.existingClaim=${NEXUS_PVC}
	```
	
	**说明**：如需开启 nexusBackup，需要``` --set nexusBackup.enabled=true ```，以及 nexusBackup 对应的 persistence，同上方式，具体参数见 chart 中 `README.md`。

默认帐号 admin 的密码由 nexus 随机生成，需要到 pod 中 `cat /nexus-data/admin.password`，第一次登录后，会要求更改密码。

###### 部署 sonarqube

sonarqube 版本：7.9.1-community

在安装 chart 前，请先确认将用哪种方式来存储数据：

* Persistent Volume Claim (建议)

* Host path

如果 Kubernetes 集群已经有可用的 StorageClass 和 provisioner，在安装 chart 过程中会自动创建 PVC 来存储数据。 想了解更多关于 StorageClass 和 PVC 的内容，可以参考 Kubernetes 官方文档。


* **参考命令（使用 PVC）：**

	```
	registry=$(docker info |grep 60080  |tr -d ' ')
	ACP_NAMESPACE=<cpaas-system>     ## 改成部署时， --acp2-namespaces 参数指定的值，默认是cpaas-system
	helm install stable/sonarqube \
	        --name sonarqube \
	        --set plugins.useDefaultPluginsPackage=true \
	        --set global.registry.address=$registry \
	        --namespace=${ACP_NAMESPACE} \
	        --set global.namespace=${ACP_NAMESPACE} \
	        --set service.type=NodePort \
	        --set service.nodePort=<node port 端口号，默认31342> \
	        --set postgresql.database.persistence.enabled=true \
	        --set postgresql.database.persistence.existingClaim=<pvc name>  ## 给 sonarqube 使用的 pvc 的名字
	```

* **参考命令（使用 storageClass）：**

	```
	registry=$(docker info |grep 60080  |tr -d ' ')
	ACP_NAMESPACE=<cpaas-system>     ## 改成部署时， --acp2-namespaces 参数指定的值，默认是cpaas-system
	helm install stable/sonarqube \
	        --name sonarqube \
	        --set plugins.useDefaultPluginsPackage=true \
	        --set global.registry.address=$registry \
	        --namespace=${ACP_NAMESPACE} \
	        --set global.namespace=${ACP_NAMESPACE} \
	        --set service.type=NodePort \
	        --set service.nodePort=<node port 端口号，默认31342> \
	        --set postgresql.database.persistence.enabled=true \
	        --set postgresql.database.persistence.storageClass=<storageclass name>  ## 给 sonarqube 使用的 storageclass 的名字
	```

* **参考命令（使用 hostpath）：**

	```
	registry=$(docker info |grep 60080  |tr -d ' ')
	ACP_NAMESPACE=<cpaas-system>     ## 改成部署时， --acp2-namespaces 参数指定的值，默认是cpaas-system
	helm install stable/sonarqube \
	        --name sonarqube \
	        --set plugins.useDefaultPluginsPackage=true \
	        --set global.registry.address=$registry \
	        --namespace=${ACP_NAMESPACE} \
	        --set global.namespace=${ACP_NAMESPACE} \
	        --set service.type=NodePort \
	        --set service.nodePort=<node port 端口号，默认31342> \
	        --set postgresql.database.persistence.enabled=false \
	        --set postgresql.database.persistence.host.nodeName=<node name> \
	        --set postgresql.database.persistence.host.path=<path >  ## 本地路径，例如：/cpaas/data/sonarqube
	```



#### Service Mesh （微服务治理平台）部署

**说明**：

* 微服务治理平台有单独的安装包，需要下载之后解压缩到 init 节点 `/tmp` 下，并运行解压缩之后的安装目录内的 upload-images-chart.sh 脚本上传安装包和 chart。

* 部署 Service Mesh 业务集群组件的时候，至少要有一个 slave 节点，且这个节点上不能部署 alb 和 ingress，即：不能有 `alb2=ture` 和 `ingress=true` 的标签。

* Service Mesh 依赖 ingress、alauda-cluster-base。

* `<>` 代表要根据环境情况替换尖括号内的值，替换完毕之后，不包括尖括号。

* 如果在部署 global 的时候，`--enabled-features` 这个参数没有加上  asm ，需要手动部署微服务治理平台的 globla 组件。

1. 在 global 集群的第一个 master 节点上	执行以下命令：

	```
	ACP_NAMESPACE=<cpaas-system>  ## 改成部署平台时， --acp2-namespaces 参数指定的值，默认是cpaas-system
	GLOBAL_HOST=<平台访问地址>      ## 部署时 --domain-name 参数指定的值，没有 http 或 https 的前缀
	
	https_or_http=<https>         ## 默认是 https，如果在部署时，使用了 --use-http 这个参数，那么需要改成 http
	kubectl captain create \
	        --version $(helm search | grep '^stable/global-asm ' | awk '{print $2}') \
	        --set global.host=${GLOBAL_HOST} \
	        --set global.scheme='${https_or_http}' \
	        --configmap=acp-config \
	        --namespace=${ACP_NAMESPACE} \
	        --chart=stable/global-asm \
	        global-asm
	        
	kubectl captain create \
	        --version $(helm search | grep '^stable/asm-init ' | awk '{print $2}') \
	        --set install.mode="global" \
	        --set global.host=${GLOBAL_HOST} \
	        --namespace=${ACP_NAMESPACE} \
	        --chart=stable/asm-init \
	        asm-init
	
	```

2. 在业务集群的 master 节点上执行如下操作：

	```
	kubectl label node <选定的 slave 的名字> istio=true
	```

部署成功之后，在界面上单击左上角产品菜单，能看到 Service Mesh 。单击进入，选择管理视图，开始部署  Service Mesh 的业务集群组件，步骤如下：

1. 提前收集以下几个信息：

	* ES（部署在 global 上的） ，例如：

		地址：http://10.0.0.17:9200
		
		用户：alaudaes                ### 默认值
		
		密码：es_password_1qaz2wsx    ### 随机生成的值

		可以通过在 global 集群上的一个 master 节点上执行如下命令，获取密码

		```
		kubectl get deployments -n ${ACP_NAMESPACE} cpaas-elasticsearch -o yaml | grep ALAUDA_ES_PASSWORD -A 1 | grep value | awk '{print $NF}' 
		```

	* prometheus（部署在对应集群上的） ，例如：

		地址：http://10.0.0.12:30900

	* ingress 地址（部署在对应集群上的） ，获取方式：

		``` 
		cat /etc/kubernetes/kubelet.conf | grep server: | grep -oE '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' 
		```

	* IP 范围（对应集群的） ，获取方式：

		```
		cat /etc/kubeadm/kubeadmcfg.yaml | grep podSubnet |awk -F : '{print $5 $4}'|tr -d 'a-z,A-Z}'|sed 's+ +\\,+g'|sed 's/^..//g'|sed 's/..$//g' 
		```

2. 在微服务治理平台页面的“管理视图”里创建“服务网格”，如下图：

	<img src="images/asm 部署-ui.png" style="zoom:50%;" />

	这里的“组件高可用”代表用多副本方式启动微服务治理平台组件。

	<img src="images/asm 部署，多副本选择.png" style="zoom:50%;" />

3. 在“管理视图”的“项目”里，启动 ns 的服务网格。

	<img src="images/asm 部署，启动 ns 的服务网格.png" style="zoom:50%;" />

	在“管理视图”的“健康检查”里可以看到各组件的状态即可，组件启动需要一段时间，请耐心等待。

	<img src="images/asm 部署-健康检查.png" style="zoom:50%;" />


#### Machine Learning（机器学习平台）

**说明**：

* 机器学习平台有单独的安装包，需要下载之后解压缩到 init 节点 /tmp 下，并运行解压缩之后的安装目录内的 upload-images-chart.sh 脚本上传安装包和 chart 。aml 有附加的镜像包，同样解压缩并执行upload-images-chart.sh 脚本上传。

* `<>` 代表要根据环境情况替换尖括号内的值，替换完毕之后，不包括尖括号。

* 如果在部署 global 的时候，`--enabled-features` 这个参数没有加上 aml ，需要手动部署机器学习平台的 globla 组件。

##### global 下部署 global-aml

**说明**：如果在部署 global 的时候，`--enabled-features` 这个参数没有加上 aml ，需要手动部署机器学习平台的 globla 组件。

在 global 集群的第一个 master 节点上执行以下命令：

```
ACP_NAMESPACE=<cpaas-system>  ## 改成部署平台时， --acp2-namespaces 参数指定的值，默认是cpaas-system
kubectl captain create \
        --version $(helm search | grep '^stable/global-aml ' | awk '{print $2}') \
        --configmap=acp-config \
        --namespace=${ACP_NAMESPACE} \
        --chart=stable/global-aml \
        global-aml
```

##### 业务集群下部署 aml-volcano

在 global 集群的第一个 master 节点上执行以下命令：

```
REGISTRY_ENDPOINT=$(docker info |grep 60080  |tr -d ' ')
REGION_NAME=<name>                  ## 想要部署 aml-volcano 的集群的名字
ACP_NAMESPACE=<cpaas-system>        ## 改成部署平台时， --acp2-namespaces 参数指定的值，默认是cpaas-system

mkdir /tmp/region_helmrequest
cat << EOF >/tmp/region_helmrequest/${REGION_NAME}-volcano.yaml
apiVersion: app.alauda.io/v1alpha1
kind: HelmRequest
metadata:
  finalizers:
  - captain.alauda.io
  generation: 1
  name: ${REGION_NAME}-volcano
  namespace: ${ACP_NAMESPACE}
spec:
  chart: stable/volcano
  namespace: ${ACP_NAMESPACE}
  releaseName: ${REGION_NAME}-volcano
  clusterName: ${REGION_NAME}
  values:
    global:
      namespace: ${ACP_NAMESPACE}
      registry:
        address: ${REGISTRY_ENDPOINT}
  version: $(helm search | grep '^stable/volcano ' | awk '{print $2}')

EOF
kubectl create -f /tmp/region_helmrequest/${REGION_NAME}-volcano.yaml
```

##### 业务集群下部署 aml-core

**说明**：部署 aml-core 依赖业务集群有 ingress，请按照本文档部署 ingress 的内容，部署 ingress。

在 global 集群的第一个 master 节点上执行以下命令：

```
REGISTRY_ENDPOINT=$(docker info |grep 60080  |tr -d ' ')
REGION_NAME=<name>                  ## 想要部署 aml-core 的集群的名字
ACP_NAMESPACE=<cpaas-system>        ## 改成部署平台时， --acp2-namespaces 参数指定的值，默认是cpaas-system
ingress_address=<1.1.1.1>           ## master 的地址，如果是多个 master，这就是 ingress vip 的地址

mkdir /tmp/region_helmrequest
cat << EOF >/tmp/region_helmrequest/${REGION_NAME}-aml-core.yaml
apiVersion: app.alauda.io/v1alpha1
kind: HelmRequest
metadata:
  finalizers:
  - captain.alauda.io
  generation: 1
  name: ${REGION_NAME}-aml-core
  namespace: ${ACP_NAMESPACE}
spec:
  chart: stable/aml-core
  namespace: ${ACP_NAMESPACE}
  releaseName: ${REGION_NAME}-aml-core
  clusterName: ${REGION_NAME}
  values:
    global:
      labelBaseDomain: cpaas.io
      namespace: ${ACP_NAMESPACE}
      registry:
        address: ${REGISTRY_ENDPOINT}
      host: ${ingress_address}
  version: $(helm search | grep '^stable/aml-core ' | awk '{print $2}')

EOF
kubectl create -f /tmp/region_helmrequest/${REGION_NAME}-aml-core.yaml
```

##### 业务集群下部署 aml-ambassador

在 global 集群的第一个 master 节点上执行以下命令：

```
REGISTRY_ENDPOINT=$(docker info |grep 60080  |tr -d ' ')
REGION_NAME=<name>                  ## 想要部署 aml-ambassador 的集群的名字
ACP_NAMESPACE=<cpaas-system>        ## 改成部署平台时， --acp2-namespaces 参数指定的值，默认是cpaas-system
ingress_address=<1.1.1.1>           ## master 的地址，如果是多个 master，这就是 ingress vip 的地址

mkdir /tmp/region_helmrequest
cat << EOF >/tmp/region_helmrequest/${REGION_NAME}-aml-ambassador.yaml
apiVersion: app.alauda.io/v1alpha1
kind: HelmRequest
metadata:
  finalizers:
  - captain.alauda.io
  generation: 1
  name: ${REGION_NAME}-aml-ambassador
  namespace: ${ACP_NAMESPACE}
spec:
  chart: stable/aml-ambassador
  namespace: ${ACP_NAMESPACE}
  releaseName: ${REGION_NAME}-aml-ambassador
  clusterName: ${REGION_NAME}
  values:
    global:
      namespace: ${ACP_NAMESPACE}
      registry:
        address: ${REGISTRY_ENDPOINT}
  version: $(helm search | grep '^stable/aml-ambassador ' | awk '{print $2}')

EOF
kubectl create -f /tmp/region_helmrequest/${REGION_NAME}-aml-ambassador.yaml
```

#### API Management Platform（API 管理平台）

**说明**：

* API 管理平台有单独的安装包，需要下载之后解压缩到 init 节点 /tmp 下，并运行解压缩之后的安装目录内的 upload-images-chart.sh 脚本上传安装包和 chart 
* 需要客户提供postgresql 数据库，并创建 kong kongswagger 和 ampfile 这三个数据库提供给 amp 使用，或者在部署平台的时候，使用 --make-db 在 init 节点创建一个 poc 使用的数据库
*  部署结束后，如果出现跳转错误，请按照本文档 《和 v2.5 、 v2.6.0 相比部署差异》 这一节中，amp 相关的内容进行操作
* 在部署 global 的时候，`--enabled-features` 参数填 amp 即可以自动部署（非高可用的 amp）。如果部署的时候没有加，平台部署成功后再想要添加这个产品，请按如下内容操作

在 global 集群的第一个 master 节点上，顺序执行以下命令：

```
REGISTRY_ENDPOINT=$(docker info |grep 60080  |tr -d ' ')
ACP_NAMESPACE=<cpaas-system>      ## 改成部署平台时， --acp2-namespaces 参数指定的值，默认是cpaas-system
DEFAULT_ROOT_USER=<alauda>        ## 预设根账户名称，默认为 alauda
DOMAIN_NAME=<vip or domainname>   ## 需要修改为 global 界面的访问地址，也就是部署 global 的时候，--domain-name 参数的值
https_or_http=<https>             ## 默认值是 https，如果部署的时候使用了--use-http 参数，就需要改成 http
ES_HOST=<DD>                      ## elasticsearch的地址
ES_PORT=<DD>                      ## elasticsearch的端口
ES_USER=<DD>                      ## elasticsearch的用户名
ES_PASSWORD=<DD>                  ## elasticsearch的密码
KONG_REPLICAS=<2>                 ## Kong集群副本数目，高可用必须大于2，建议设置成和打了 kong=true 的服务器数量一致
DB_HOST=<DD>                      ## 数据库地址
DB_PORT=<DD>                      ## 数据库端口
DB_USER=<DD>                      ## 数据库用户名
DB_PASSWORD=<DD>                  ## 数据库密码

kubectl captain create \
           --version $(helm search | grep '^stable/amp ' | awk '{print $2}') \
           --namespace=${ACP_NAMESPACE} \
           --chart=stable/amp \
           --set amp.deploy=false \
           --set kong.deploy=true \
           --set kong.replicas=${KONG_REPLICAS} \
           --set kong.exposeAsService=false \
           --set kong.database.deploy=false \
           --set kong.database.exposeAsService=false \
           --set kong.database.external.host=${DB_HOST} \
           --set kong.database.external.port=${DB_PORT} \
           --set kong.database.external.postgresUser=${DB_USER} \
           --set kong.database.external.postgresPassword=${DB_PASSWORD} \
           --set kong.database.external.database=postgres \
           --set kong.env.db_update_frequency=5 \
           --set kong.runMigrations=true \ 
           --set env.elasticHost="${ES_HOST}" \
           --set env.elasticPort="${ES_PORT}" \
           --set env.elasticUsername="${ES_USER}" \
           --set env.elasticPassword="${ES_PASSWORD}" \
           --set global.namespaces=${ACP_NAMESPACE} \
           --set global.registry.address=${REGISTRY_ENDPOINT} \
           --set logstash.serviceAccount.create=false \
           kong-cluster --timeout 3000
```

上述命令执行完成后，继续执行以下命令：

```
REGISTRY_ENDPOINT=$(docker info |grep 60080  |tr -d ' ')
ACP_NAMESPACE=<cpaas-system>  ## 改成部署平台时， --acp2-namespaces 参数指定的值，默认是cpaas-system
DB_HOST=<DD>                  ## 数据库地址
DB_PORT=<DD>                  ## 数据库端口
DB_USER=<DD>                  ## 数据库用户名
DB_PASSWORD=<DD>              ## 数据库密码

kubectl captain create \
        --version $(helm search | grep '^stable/amp-kong ' | awk '{print $2}') \
        --namespace=${ACP_NAMESPACE} \
        --chart=stable/amp-kong \
        --set global.registry.address=${REGISTRY_ENDPOINT} \
        --set postgresql.enabled=false \
        --set env.pg_host=${DB_HOST} \
        --set env.pg_database=kongswagger \
        --set env.pg_port=${DB_PORT} \
        --set env.pg_user=${DB_USER} \
        --set env.pg_password=${DB_PASSWORD} \
        amp-kong
```

上述命令执行完成后，继续执行以下命令：

```
REGISTRY_ENDPOINT=$(docker info |grep 60080  |tr -d ' ')
ACP_NAMESPACE=<cpaas-system>  ## 改成部署平台时， --acp2-namespaces 参数指定的值，默认是cpaas-system
DEFAULT_ROOT_USER=<alauda>    ## 预设根账户名称，默认为 alauda
DOMAIN_NAME=<domainname>      ## 需要修改为 global 界面的访问地址，也就是部署 global 的时候，--domain-name 参数的值
https_or_http=<https>         ## 默认值是 https，如果部署的时候使用了--use-http 参数，就需要改成 http
ES_HOST=<DD>                  ## elasticsearch的地址
ES_PORT=<DD>                  ## elasticsearch的端口
ES_USER=<DD>                  ## elasticsearch的用户名
ES_PASSWORD=<DD>              ## elasticsearch的密码
DB_HOST=<DD>                              ## 数据库地址
DB_PORT=<DD>                              ## 数据库端口
DB_USER=<DD>                              ## 数据库用户名
DB_PASSWORD=<DD>                     ## 数据库密码

kubectl captain create \
        --version $(helm search | grep '^stable/amp ' | awk '{print $2}') \
        --namespace=${ACP_NAMESPACE} \
        --chart=stable/amp \
        --set amp.deploy=true \
        --set ampDashboard.consoleOrg=${DEFAULT_ROOT_USER} \
        --set ampDashboard.consoleURL=${https_or_http}://${DOMAIN_NAME} \
        --set kong.deploy=false \
        --set kong.exposeAsService=false \
        --set kong.external.gatewayDomain=http://${DOMAIN_NAME}:30000 \
        --set kong.external.gatewaySecureDomain=https://${DOMAIN_NAME}:30443 \
        --set kong.external.gatewayAdmin=http://amp-apiproject-kong:8001 \
        --set kong.external.gatewaySecureAdmin=https://amp-apiproject-kong:8444 \
        --set kong.database.deploy=false \
        --set env.authConfigMapNamespace=${ACP_NAMESPACE} \
        --set env.configMapNamespace=${ACP_NAMESPACE} \
        --set env.elasticHost="${ES_HOST}" \
        --set env.elasticPort="${ES_PORT}" \
        --set env.elasticUsername="${ES_USER}" \
        --set env.elasticPassword="${ES_PASSWORD}" \
        --set global.namespaces=${ACP_NAMESPACE} \
        --set global.registry.address=${REGISTRY_ENDPOINT} \
        --set masterHost=${DOMAIN_NAME} \
        --set env.filedb.dbHost="${DB_HOST}" \
        --set env.filedb.dbPort="${DB_PORT}" \
        --set env.filedb.dbName="ampfile" \
        --set env.filedb.dbUser=${DB_USER} \
        --set env.filedb.dbPassword="${DB_PASSWORD}" \
        --set env.filedb.dbUseSsl=false \
        --set env.filedb.dbGormDebugMode=true \
        amp
```

部署完毕后，需要登录数据库建立索引，方式如下：

```
#1 首先执行下面的命令登录数据库
psql -h $DB_HOST -p $DB_PORT -U $DB_USER -d ampfile

#2 登录数据库成功后，执行下面的命令建立索引
CREATE INDEX file_name_index ON files (file_name);

#3 执行下面的命令查看索引是否成功创建
\d files
```


#### TKE

**说明**：部署平台的时候，`--enabled-features` 参数填 tke 即可以自动部署，不支持平台部署成功后，再添加这个产品。

#### 部署 CSP （ceph）

CSP 可以支持三种存储模式：对象存储、ceph rbd（块存储）、cephfs（文件存储），一个主机选做了三种存储模式中一种，则无法复用其余角色。

**说明**：默认部署会修改 hostname 为 ip，由于 cephrbd 不支持多 Pod 挂载读写，cephfs 支持，所以容器平台默认支持 cephfs 模式。

* **主机需求**

	* 对象存储：需要 4 个 4C8G 的主机，1 个 installer 节点，3 个数据节点（暂不支持在 init 节点和 global 节点部署 ceph，需要额外的机器）。 
	
	* ceph rbd（块存储） ：需要 4 个 4C8G 的主机，1 个 installer 节点，3 个数据节点（暂不支持在 init 节点和 global 节点部署 ceph，需要额外的机器）。
	
	* cephfs（文件存储）：需要 4 个 4C8G 的主机，1 个 installer 节点，3 个数据节点（暂不支持在 init 节点和 global 节点部署 ceph，需要额外的机器）。

* **磁盘需求**

每个数据节点必须要有单独的外挂盘做 ceph 的数据存储，磁盘的大小根据资源使用规划即可（最低50G）。


**部署步骤**：

1. 准备安装包，ceph 的安装包在我们的压缩包里，具体路径为解压后的安装包路径下的 `/other/ceph` 下。

	```
	[root@init ceph]# pwd
	/<安装目录>/other/ceph
	[root@init ceph]# ls
	cephfs.yaml ceph-rbd.yaml CSP-V2.4.10.288.tar.gz
	cephfs.zip ceph-storageclass.yaml rbd
	```

2. 通过 `scp` 命令将 `CSP-V2.4.10.288.tar.gz` 拷贝到准备好的 installer 节点上。

3. 解压安装包，并部署。

	```
	tar zxvf CSP-V2.4.10.288.tar.gz
	cd csp-pkg-2.4.10.288/
	systemctl stop ntpd  && systemctl disable ntpd 
	./cspcmd install --controller 10.0.128.135             ####需要将10.0.128.135这个ip换为本机的实际ip
	```

​	等待部署完成之后，会显示访问地址以及端口，一般为 `IP+8089 端口` 。

4. 修改配置文件，支持页面创建 cephfs 存储池。

	```
	sed -i 's/FILE_STORAGE: false/FILE_STORAGE: true/g' /data/csp-ins/current/csp-console/server/config/index.js
	systemctl restart csp-console.service
	```

5. 通过刚才部署时候的命令传入的 `ip+8089` 访问页面，用户名和密码默认为：admin/admin。

6. 创建集群。

	输入集群名称，输入集群名称单击（下一步），NTP Server IP 默认 `127.0.0.1 `如下图：

	<img src="images/ceph 4.1 创建集群.png" style="zoom:50%;" />

7. 添加主机。

	* 添加主机 ip。
	
	* 填写节点序列号，按顺序填 SN1、SN2、SN3 填即可。
	
	* 数据中心统一填 dc1。
	
	* 机架位置统一填 rack1。
	
	* 输入存储服务器 root 密码。
	
	* 输入ssh 端口   如下图：

		<img src="images/ceph 4.2 添加主机.png" style="zoom:50%;" />
		
	* 单击（提交）按钮，等待添加主机完成操作，需等待 3 分钟，如下图：
	
		<img src="images/ceph 4.2 提交.png" style="zoom:50%;" />
	
8. 主机分配。

	分配主机：勾选你要配置的主机 ip ，有且仅需要 3 台，单击（完成）后创建集群，存储集群主机初始化完成，如下图：
	
	<img src="images/ceph 5 分配主机.png" style="zoom:50%;" />


9. 创建 cephfs 存储池，单击 **文件存储->元数据节点->创建存储池**。

	* 填写存储池数据。

	* 选择存储节点 ip。
  	
	* 本次部署选择的为极简模式，需自己分配数据盘（高级模式）。

	* 选择 **3 副本** 冗余策略。<br>**说明**：默认是 3 副本冗余策略。

	* 选择数据安全级别节点。<br>**注意**：存储节点分配，全选即可（操作系统盘不能用作对象存储池，系统做了屏蔽，因此在该页面中，看不到机器的系统盘），如下图所示。

		<img src="images/ceph 6 创建存储池.png" style="zoom:50%;" />

	* 单击 **确定** 完成对象存储池的创建。<br>预计等待 2 分钟,等待创建完成，创建完成之后对接即可。


### 对接 ceph 集群

1. 在准备对接的集群所有节点安装 ceph 客户端。

	```
	yum install  ceph-common ceph-fuse  -y
	```
  
2. 腾讯的 ceph 集群默认为 none 模式，没有开启认证，通过配置文件获得 monitor 节点的配置。

	**说明**：需要去腾讯的 ceph 集群的某个 data 节点操作。
	
	```
	conf=$(ps -fe | grep CLUSTERMON.*.conf | grep ceph-mon | awk '{print $(NF-2)}')
	cat $conf | grep mon_addr           ##获得monitor节点的ip以及端口
	mon_addr = 10.0.129.175:6789
	mon_addr = 10.0.129.8:6791
	mon_addr = 10.0.129.73:6790
	```

3. 准备好cephfs 压缩包，将压缩包拷贝到某台 master 机器的 `/root` 下，并解压。

	这个压缩包在init节点的安装包路径下的 other 目录，`/<安装目录>/other/ceph/cephfs.zip`。
	
	```
	scp cephfs.zip  root@1.1.1.1:/root/   ##需要将1.1.1.1换为业务集群的某台master机器的ip地址。
	```
  
4. 去刚传完文件的 master 的 root 下执行以下命令行：

	  ```
	  mv cephfs /tmp        ###为了防止当前目录下存在同名目录
	  unzip cephfs.zip
	  cd cephfs/
	```

5. 将 `deployment.yaml` 中的 image 地址更换为自己环境的 image 地址。<br>例如：`10.0.128.173:60080/claas/cephfs-provisioner:latest`。

6. 创建 ns：`kubectl create ns cephfs`

	**说明**：若为 ocp 集群，则需要使用如下 yaml 创建 namespace。

	  ```
	  cat <<EOF >/root/cephfs/ns.yaml
	  apiVersion: v1
	  kind: Namespace
	  metadata:
	    annotations:
	      openshift.io/sa.scc.supplemental-groups: 1000000000/0
	      openshift.io/sa.scc.uid-range: 1000000000/0
	    name: cephfs
	  spec:
	    finalizers:
	    - kubernetes
	  status:
	    phase: Active
	  EOF
	  kubectl create -f /root/cephfs/ns.yaml
	  ```

7. 创建相关资源。

	  ``` 
	  NAMESPACE=cephfs
	  sed -r -i "s/namespace: [^ ]+/namespace: $NAMESPACE/g" /root/cephfs/*.yaml
	  sed -r -i "N;s/(name: PROVISIONER_SECRET_NAMESPACE.*\n[[:space:]]*)value:.*/\1value: $NAMESPACE/" /root/cephfs/deployment.yaml
	  kubectl apply -f /root/cephfs -n $NAMESPACE
	```

8. 创建 storageclass。

	```
	cat <<EOF > cephfs-storageclass.yaml
	kind: StorageClass
	apiVersion: storage.k8s.io/v1
	metadata:
	  name: cephfs
	  selfLink: /apis/storage.k8s.io/v1/storageclasses/cephfs
	  uid: 198bcd73-a1ff-11e8-aafa-5254004c757f
	  resourceVersion: '11786274'
	  annotations:
	    ceph.type: cephfs
	provisioner: ceph.com/cephfs                          
	parameters:
	  adminId: admin
	  adminSecretName: cephfs-admin-secret
	  adminSecretNamespace: kube-system
	  claimRoot: /volumes/kubernetes
	  monitors: '10.0.129.175:6789,10.0.129.8:6791,10.0.129.73:6790'         ## 需要修改为第2步获得ceph集群的monitors的IP地址和端口
	  reclaimPolicy: Delete
	volumeBindingMode: Immediate
	  EOF
	    
	    
	kubectl create -f cephfs-storageclass.yaml
	```

9. 定义管理员密码。
	
	```
	echo "AQCYfAxdbXTwCRAAnz9MvJgO3KselABH2OoKOA==" > /tmp/secret 
	kubectl create secret generic cephfs-admin-secret --from-file=/tmp/secret --namespace=kube-system
	kubectl create secret generic cephfs-secret-user --from-file=/tmp/secret
	```

10. 创建 PVC 验证。

	```
	cat <<EOF > cephfs.yaml
	    
	kind: PersistentVolumeClaim
	apiVersion: v1
	metadata:
	  name: claim1
	  namespace: default
	  annotations:
	    volume.beta.kubernetes.io/storage-class: cephfs
	    volume.beta.kubernetes.io/storage-provisioner: ceph.com/cephfs
	  finalizers:
	    - kubernetes.io/pvc-protection
	spec:
	  accessModes:
	    - ReadWriteMany
	  resources:
	    requests:
	      storage: 1Gi
	  volumeMode: Filesystem
	EOF
	    
	kubectl create -f cephfs.yaml
	```


11. 检查对接是否成功。

    ```  
    kubectl get pvc      ###查看刚创建的pvc为bound状态即成功
    NAME     STATUS   VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS   AGE
    claim1   Bound    pvc-b480aa29-a142-11e9-9d43-525400d63265   1Gi        RWX            cephfs         19m
    ```

### 安装 cpaas-fit 与 tsf 对接

在 global 集群的第一个 master 节点上执行以下命令：

```
REGISTRY_ENDPOINT=$(docker info |grep 60080  |tr -d ' ')
ACP_NAMESPACE=<cpaas-system>      ## 改成部署平台时， --acp2-namespaces 参数指定的值，默认是cpaas-system
DOMAIN_NAME=<vip or domainname>   ## 需要修改为 global 界面的访问地址，也就是部署 global 的时候，--domain-name 参数的值
https_or_http=<https>                          ## 默认值是 https，如果部署的时候使用了--use-http 参数，就需要改成 http
TSF_ENDPOINT=<tsf 的访问地址>          ## 部署完 tsf 后访问他的地方，例如http://10.0.129.145

kubectl captain create \
           --version $(helm search | grep '^stable/cpaas-fit ' | awk '{print $2}') \
           --namespace=${ACP_NAMESPACE} \
           --chart=stable/cpaas-fit \
           --set global.namespaces=${ACP_NAMESPACE} \
           --set global.registry.address=${REGISTRY_ENDPOINT} \
           --set envs.tsf_endpoint=${TSF_ENDPOINT} \
           --set envs.dex_issuer=${https_or_http}://${DOMAIN_NAME}/dex \
           --set envs.redirect_uri=${https_or_http}://${DOMAIN_NAME}/fit/tsf
           cpaas-fit
```

## global 组件配置

### 开启计量功能后，若需要使用报表导出功能，morgans 配置存储的方法

**说明**：计量功能导出报表，需要 morgans 挂卷，方法如下：

1. 执行操作的位置：global 集群的第一个 master 上
2. 所需资源：global 需要一个 pvc 卷，请参考运维手册，对接存储，并手动创建 pvc
3. 给 morgans 配置存储的方法：

```
ACP_NAMESPACE=<cpaas-system>     ## 改成部署时， --acp2-namespaces 参数指定的值，默认是cpaas-system
kubectl edit hr -n ${ACP_NAMESPACE} alauda-container-platform
```

修改 alauda-container-platform 这个 hr ，添加如下值：

```
# 找到 valuesFrom: ，在这一行上面添加如下四行内容
  values:
    morgans:
      meterReports:
        persistentVolumeClaim: "<pvc name>"  ## 将<pvc name> 改成上面第二步创建的 pvc 的 name
```

如下图：

![](images/morgans-pvc.png)

稍等片刻，等 captain 更新 alauda-container-platform 这个 chart 即可

 <div STYLE="page-break-after: always;"></div>
# 第五部分 平台配置及测试

## 检查部署是否成功

###  检查 global 平台是否部署成功

**说明**：global 组件通过 helm 和 captain 来部署，生成的资源全部都存放在运行 global 组件的 Kubernetes 集群内。

在运行 global 组件的 Kubernetes 集群的 master 节点上执行以下命令：

```
## 检查 helm 部署的 chart 是否成功，执行如下命令查找部署失败的 chart 
helm list --failed  

## 检查 captain 部署的 chart 是否成功，执行如下命令查找部署失败的 chart
kubectl get helmrequest --all-namespaces | awk '{if ($NF != "Synced")print}'

## 检查 catpain 部署的 chart release 是否成功，执行如下命令查找失败的 release
kubectl get rel --all-namespaces | awk '{if ($3 != "deployed")print}'

## 检查 所有 pod 是否正常，执行如下命令查找失败的 pod
kubectl get pod --all-namespaces | awk '{if ($4 != "Running" && $4 != "Completed")print}' | awk -F'[/ ]+' '{if ($3 != $4)print}'
```

###  检查业务服务集群是否部署成功

**说明**：业务服务集群的组件通过 helm 和 captain 来部署，生成的 helmrequest 资源存放在运行 global 组件的 Kubernetes 集群内，captain 部署的 chart 产生的 release 资源存放在业务服务集群内。

在运行 global 组件的 Kubernetes 集群的 master 节点上和业务服务集群的 master 节点上执行以下命令：

```
## 检查 helm 部署的 chart 是否成功，执行如下命令查找部署失败的 chart ，在业务服务集群的 master 节点上操作
helm list --failed  

## 检查 captain 部署的 chart 是否成功，执行如下命令查找部署失败的 chart，在运行 glboal 组件的 k8s 集群的 master 节点上操作
kubectl get helmrequest --all-namespaces | awk '{if ($NF != "Synced")print}'

## 检查 catpain 部署的 chart release 是否成功，执行如下命令查找失败的 release，在业务服务集群的 master 节点上操作
kubectl get rel --all-namespaces | awk '{if ($3 != "deployed")print}'

## 检查 所有 pod 是否正常，执行如下命令查找失败的 pod，在业务服务集群的 master 节点上操作
kubectl get pod --all-namespaces | awk '{if ($4 != "Running" && $4 != "Completed")print}' | awk -F'[/ ]+' '{if ($3 != $4)print}'
```


## 平台访问地址及登录用户名、密码，以及兼容的浏览器

### 平台访问地址

**all in one 部署方案** 和客户没有提供域名的 **poc 部署方案** 的访问地址：

* 没有使用 `--use-http` 参数的平台访问地址：`https://<init 节点 ip>` 

* 使用了 `--use-http` 参数的平台访问地址：`http://<init 节点 ip>`

**正式生产环境部署方案** 和客户提供域名的 **poc 部署方案** 的访问地址：

* 没有使用 `--use-http` 参数的平台访问地址：`https://<--domain-name 参数指定的地址>` 

* 使用了 `--use-http` 参数的平台访问地址：`http://<--domain-name 参数指定的地址>`

### 平台登录权限

用户名：`--root-username` 参数指定的值，默认为 admin@cpaas.io。

密码：password

### 兼容的浏览器版本

**仅兼容 Chrome 浏览器，且是最近的 3 个版本之一**。


## 部署成功后操作

### 备份 etcd 数据

1. 拷贝安装目录下的 backup_recovery.sh 和 function.sh 脚本到每一个 master 服务器的 `/tmp` 下。

2. 在每一台 master 节点上执行以下命令：

	```
	cd /tmp
	bash backup_recovery.sh run_function=back_k8s_file
	```




**注意**： 上述命令会将原来的 crontab 全部删掉，建议使用它 `crontab -e` 命令添加。

## 平台功能测试

请参考《TKE for Alauda Container Platfrom POC 测试用例.docx》。

## 添加监控、告警

<div STYLE="page-break-after: always;"></div>
# 第六部分 FAQ

## 平台部署错误

### 安装 chart 失败

**提示信息**： install chart error，安装日志里会显示出错的 chart 名字。

**出错原因**： 脚本通过 helm install 命令部署 chart 出错，详细日志会保存到第一台 master 节点的/cpaas/chart_install.log 文件内。

**错误处理流程**：首先登录到第一台 master 节点上，执行 `helm list -a` 命令检查 chart ，发现错误的 chart 后，执行`helm delete --purge <chart name>` 删掉 chart ，然后执行`bash /tmp/install_chart.sh` 继续安装 chart，如果还是不成功，建议清空环境重新部署。依旧出错，请反馈给运维。
    

### 对接 global 集群失败

**提示信息**：get global token Repeat 10 times to get global token failed，之后对接 global 集群也会失败，提示 `access global region error`。

**出错原因**：这种情况一般是chart 安装失败，global 的 chart 没起来，或者因为资源不够、global 集群的服务器速度太慢，获取 token 的时候 global 组件还未正常运行，所以获取token 失败了，没有正确的 token，global 没起来调用 api 自然失败。

**错误处理流程**：chart 安装失败，请参照上面的方法处理，等待所有 global 组件的 pod 状态都正常后，去 init 节点上，执行以下命令行后对接成功：
	
```
cat /tmp/add_global_region.sh | sed 's/Bearer "/Bearer '$(bash /tmp/get_token.sh )'"/g' >/tmp/dd.sh ; bash /tmp/dd.sh
```



## 平台功能错误

请参考《TKE for Alauda Container Platfrom 运维手册》。







