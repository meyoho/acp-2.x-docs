# 第一部分  前言

本文档介绍了2.6 版本的平台小版本之间的升级，比如从 2.6.0 升级到 2.6.1 或者从 2.6.1 升级至2.6.3 。 **注意**： 如果想要跨越版本升级，比如从 2.6.0 直接到 2.6.3 ，就需要将 2.6.1 和 2.6.2 的安装包下载，并且在 init 上解压缩，并执行 `bash ./upload-images-chart.sh` ，因为需要这些镜像。也可选择只上传 2.6.3 的镜像，当升级完毕后执行 `kubectl get pod --all-namespaces` ，找到所有因为镜像拉不下来，无法启动的 pod，记录这些 pod 所需的镜像，再从以前版本的安装包中提取这些镜像并上传。

## 读者对象

本文档适用于具备基本的 linux、容器、Kubernetes 知识，想要安装和配置平台的用户。包括：

* 实施顾问和平台管理员

* 对平台进行维护的运维人员

* 负责整个项目生命周期的项目经理

## 文档目的

平台运维工程师依据本文档升级平台版本。


## 修订记录

| 文档版本 | 发布日期 | 修订内容 |
| ----- | ----- | ---- |
| v1.0 | 2020-04-26| 第一版|

## **警告**

在升级过程中，严禁任何对 chart 删除的操作，包括 `helm delete`  和 `kubectl delete hr` 这两种直接删除 chart 资源的操作，也禁止 通过 `kubectl delete -f xxx.yaml` 删除 yaml 文件中定义的 hr 资源的操作。平台所有的数据都存储在 chart 定义的 crd 中，删除 chart 之后数据就丢失了，且无法找回，只能回滚。


<div STYLE="page-break-after: always;"></div>

# 第二部分  升级过程

## 升级前准备

### 备份数据

请参考 2.5 或 2.3 升级至 2.6 的文档中，升级部分

## 平台升级

### 下载 v2.6 的安装包，安装包下载链接在部署文档相关章节中

**执行命令位置**：init 节点

**操作步骤**：下载安装包并解压缩

### 升级到2.6.3 的特殊操作

**如果平台经历过上传 2.6.2 的 chart 和镜像的过程，需要执行这一步的操作，删掉 alauda-base-v2.6.11 的 chart**

**执行命令位置**：global 的第一个 master 节点

**操作步骤**：执行如下命令：

```
CHART_ENDPOINT=$(helm repo list | awk '/^stable/{print $2}')
curl -I -X DELETE ${CHART_ENDPOINT%/}/api/charts/alauda-base/v2.6.11
```


### 上传 v2.6 的镜像和 chart 到 init 节点的私有仓库和 chart repo 内

**执行命令位置**：init 节点

**操作步骤**：进入 v2.6 的安装目录，执行命令 `bash ./upload-images-chart.sh`。

**注意**：执行命令过程需要一个小时左右，视 init 服务器 cpu 和硬盘速度不同，会缩短或延迟，注意安装目录所在分区的空间，上传之前至少要保证 10G 的空余空间。

### 更新 kaldr

**执行命令位置**：init 节点

**操作步骤**：执行如下命令：

```
mkdir /cpaas
REGISTRY_ENDPOINT=$(docker info |grep 60080  |tr -d ' ')
	
	
for i in $(curl ${REGISTRY_ENDPOINT}/v2/alaudaorg/kaldr/tags/list 2>/dev/null | jq '.tags' | sed -e '1d' -e '$d' -e 's/"//g' -e 's/,//g')
do
  docker pull ${REGISTRY_ENDPOINT}/alaudaorg/kaldr:$i >/dev/null 2>&1
  echo $i $(date -d $(docker inspect ${REGISTRY_ENDPOINT}/alaudaorg/kaldr:$i -f '{{.Created}}') +%s)
done | sort -k2n,2 | awk 'END{print $1}' > /tmp/dd
	
	
​	
KALDR_IMAGE=${REGISTRY_ENDPOINT}/alaudaorg/kaldr:$(cat /tmp/dd)
docker rm -f yum
mv -f /cpaas/.run_kaldr.sh /cpaas/.run_kaldr.sh.old
cat /cpaas/.run_kaldr.sh.old | awk '{for(i=1;i<NF;i++){printf $i" "}print "'$KALDR_IMAGE'"}' >/cpaas/.run_kaldr.sh
bash /cpaas/.run_kaldr.sh
```

执行命令完毕后，yum 容器会被重建，请检查是否重建。


### 刷新 repo 

* 目的：刷新 chart 仓库
* 命令：如下

  
```
helm repo update
```

* 执行位置：global 集群的第一个 master 节点上
* 执行完毕检查： 执行 `helm search` 会发现 chart 的 version 已经更新

### 更新平台 chart

* 目的：升级平台所有的 chart 到最新的版本
* 命令：如下

```
for i in $(helm list | sed 1d | awk '{print $1}') ; do helm upgrade $i stable/$i --timeout=3000 --wait --force >>/cpaas/upgrade_chart.log && echo "upgrade $i success" || echo "upgrade $i error" ; sleep 5 ; done

kubectl edit hr -n <cpaas-system> XXX 来升级通过 captain 安装的 chart，只需要将 version 字段改成最新的2.6.x 的版本（具体版本，通过 helm search 命令来查看）就可以了
```

* 执行位置：global 集群的第一个 master 节点上
* 执行完毕检查： 执行 `helm list ` 执行 `kubectl get hr -n <cpaas-system> -o yaml`  确认 chart 的 version 是否已经更新。


### 更新业务集群 chart

* 目的：升级业务所有的 chart 到最新的版本
* 命令：如下

```
for i in $(helm list | sed 1d | awk '{print $1}') ; do helm upgrade $i stable/$i --timeout=3000 --wait --force >>/cpaas/upgrade_chart.log && echo "upgrade $i success" || echo "upgrade $i error" ; sleep 5 ; done

kubectl edit hr -n <cpaas-system> XXX 来升级通过 captain 安装的 chart，只需要将 version 字段改成最新的2.6.x 的版本（具体版本，通过 helm search 命令来查看）就可以了

```

* 执行位置：业务集群的第一个 master 节点上
* 执行完毕检查： 执行 `helm list ` 执行 `kubectl get hr -n <cpaas-system> -o yaml`  确认 chart 的 version 是否已经更新。


## 升级结束之后的检查

### 在所有的集群的第一个 master 节点上执行如下命令：，检查升级版本是否正确

```
kubectl get hr --all-namespaces | awk '{if ($NF != "Synced")print}'   #检查captain 部署的所有 chart 是否部署成功
helm list --failed ; helm list --pending               # 检查captain 部署的所有 chart 是否部署成功

kubectl get hr --all-namespaces ; helm list -a    # 检查部署的 chart 的版本
```

### 按照测试用例，测试升级之后功能是否正常


## 回滚

**注意：**恢复的顺序是 global 集群 、 业务集群。如果只有业务集群升级失败，可以选择在 global 集群上，删除升级失败的这个集群相关的所有 hr 资源，再回滚升级失败的业务集群，严禁直接回滚业务集群。

1. 获取 etcd 地址，并停掉所有(业务集群和 global 集群） master 节点上的 kubelet 服务。

    **执行命令的环境：** 业务集群和 global 集群的所有 master 节点上
    
    **执行命令的命令：** 
    
    ```
    ETCD_SERVER=($(kubectl get pod -n kube-system $(kubectl get pod -n kube-system | grep etcd | awk 'NR==1 {print $1}') -o yaml | awk '/--initial-cluster=/{print}' | sed -e 's/,/ /g' -e's/^.*cluster=//' | sed -e 's#[0-9\.]*=https://##g' -e 's/:2380//g')) ； systemctl stop kubelet
    ```

2. 删掉 kube-apiserver 容器，目的是恢复过程和恢复之后，global 不要通过调用 kubeapi 写数据到业务集群的 etcd 内。

    **执行命令的环境：** 要恢复 Kubernetes 集群的所有 master 节点上
    
    **执行命令的命令：** `docker rm -f $(docker ps -a | awk '/_kube-api/{print $NF}')`
    
    **命令的结果：** 所有 kube-api 的容器都被删除。


3.  判断 etcdctl 命令是否存在第一台 master 节点上，一般执行命令 `backup_recovery.sh` 这个备份 etcd 的脚本，会自动将 etcdctl 拷贝到 `/usr/bin/etcdctl`，如果不存在，需要自行手动拷贝出来。

    **执行命令的环境：** 要恢复 Kubernetes 集群的第一台 master 节点上
    
    **执行命令的命令：** `whereis etcdctl`
    
    **命令的结果：** 应该打印处 etcdctl 的路径，如果没有就表明是错的。

4. 通过备份的快照恢复 etcd。

    **执行命令的环境：** 要恢复 Kubernetes 集群的第一台 master 节点上
    
    **执行命令的命令：**
    
    ```
    mkdir /tmp/dd
    echo ${ETCD_SERVER[@]}  ## 检查地址是否获取成功
    snapshot_db=<备份时，导出的 etcd 快照文件名，必须是绝对路径>
    for i in ${ETCD_SERVER[@]}
    do
        export ETCDCTL_API=3
        etcdctl snapshot restore ${snapshot_db} \
        --cert=/etc/kubernetes/pki/etcd/server.crt \
        --key=/etc/kubernetes/pki/etcd/server.key \
        --cacert=/etc/kubernetes/pki/etcd/ca.crt \
        --data-dir=/tmp/dd/etcd \
        --name ${i} \
        --initial-cluster ${ETCD_SERVER[0]}=https://${ETCD_SERVER[0]}:2380,${ETCD_SERVER[1]}=https://${ETCD_SERVER[1]}:2380,${ETCD_SERVER[2]}=https://${ETCD_SERVER[2]}:2380 \
        --initial-advertise-peer-urls https://$i:2380 && \
    mv /tmp/dd/etcd etcd_$i
    done
    ```
    
    **命令的结果：** 会生成 `etcd_<ip 地址>` 这样的三个目录，将这三个目录拷贝到对应 ip 的服务器的 `/root` 内。

5. 删掉 etcd 容器。

    **执行命令的环境：** 要恢复 Kubernetes 集群的所有 master 节点上
    
    **执行命令的命令：** `docker rm -f $(docker ps -a | awk '/_etcd/{print $NF}')`
    
    **命令的结果：** 所有 etcd 的容器都被删除。

6. 迁移恢复的数据。

    **执行命令的环境：** 要恢复 Kubernetes 集群的所有 master 节点上
    
    **执行命令的命令：**
    
    ```
    docker ps -a | awk '/_etcd/{print $NF}' ##确保没有 etcd 容器
    mv /var/lib/etcd/member /cpaas/backup
    mv /root/var/lib/etcd/member  /var/lib/etcd
    ```
    
    **命令的结果：** 会把 etcd 的数据挪到备份目录下，然后将上一步生成的目录拷贝到 `/var/lib/etcd` 里。

7.  启动 etcd 和 kube-api。

    **执行命令的环境：** 要恢复 Kubernetes 集群的所有 master 节点上
    
    **执行命令的命令：** `systemctl start kubelet`
    
    **命令的结果：** kubelet 服务启动后，会自动创建 etcd 的 pod，这个时候执行命令 `docker ps -a | grep -E 'etcd|kube-api'` 会找到 etcd 和 kube-api 容器。

8.  回滚之后，如果出现在kubelet和页面查看所有资源都存在，但是在业务节点没有资源的情况，重启k8s 集群内所有节点的 kubelet 和 docker 服务，也可采用重启集群内所有服务器的方式来解决。
