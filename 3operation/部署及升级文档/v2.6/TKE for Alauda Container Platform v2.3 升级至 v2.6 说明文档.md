# 第一部分  前言

本文档介绍了如何将平台版本从 v2.3 升级到 v2.6。

## 读者对象

本文档适用于具备基本的 linux、容器、Kubernetes 知识，想要安装和配置平台的用户。包括：

* 实施顾问和平台管理员

* 对平台进行维护的运维人员

* 负责整个项目生命周期的项目经理

## 文档目的

平台运维工程师依据本文档升级平台版本。

## 修订记录

| 文档版本 | 发布日期 | 修订内容 |
| ----- | ----- | ---- |
| v1.0 | 2020-01-08| 第一版，适用于 TKE for Alauda Container Platform 私有部署版本的 v2.3 升级至 v2.6 |
| v1.1 | 2020-01-20| 修改脚本错误|
| v1.2 | 2020-02-21| 第三版，增加注释，完善描述内容|
| v1.3 | 2020-03-03| 第四版，根据反馈修改|
| v1.4 | 2020-04-17| 第五版，修改容易出错的步骤，增加警示|

## **警告**

在升级过程中，严禁任何对 chart 删除的操作，包括 `helm delete`  和 `kubectl delete hr` 这两种直接删除 chart 资源的操作，也禁止 通过 `kubectl delete -f xxx.yaml` 删除 yaml 文件中定义的 hr 资源的操作。平台所有的数据都存储在 chart 定义的 crd 中，删除 chart 之后数据就丢失了，且无法找回，只能回滚。


<div STYLE="page-break-after: always;"></div>

# 第二部分  升级过程

## 升级前准备

### 备份数据

**执行位置**：global 集群的 master 和业务服务集群的 master 节点。

**操作步骤**：首先将 2.6 安装目录下的 “backup_recovery.sh” 脚本复制到 master 节点上。

执行的命令： 
	
```
mkdir -p /cpaas/backup && \
cp -r /var/lib/etcd/ /cpaas/backup && \
cp -r /etc/kubernetes/ /cpaas/backup
	
bash ./backup_recovery.sh run_function=back_k8s_file
```
命令作用：备份集群的 etcd。

备份 jenkins 挂载的目录，无论是 hostpath 还是挂载的 pvc ，都需要将数据都拷贝出来，备份到一个目录中

## 平台升级

### 下载 v2.6 的安装包

**执行位置**：init 节点

**操作步骤**：下载安装包并解压缩，安装包下载位置见部署文档

### 上传 v2.6 的镜像和 chart 到 init 节点的私有仓库和 chart repo 内

**执行位置**：init 节点

**操作步骤**：进入 v2.6 的安装目录，执行 `bash ./upload-images-chart.sh`

**注意**：执行过程需要一个小时左右，视 init 服务器 cpu 和硬盘速度不同，会缩短或延迟，注意安装目录所在分区的空间，上传之前至少要保证 10G 的空余空间。

### 更新 kaldr

**执行位置**：init 节点

**操作步骤**：执行如下命令：

```
mkdir /cpaas
REGISTRY_ENDPOINT=$(docker info |grep 60080  |tr -d ' ')
	
for i in $(curl ${REGISTRY_ENDPOINT}/v2/alaudaorg/kaldr/tags/list 2>/dev/null | jq '.tags' | sed -e '1d' -e '$d' -e 's/"//g' -e 's/,//g')
do
  docker pull ${REGISTRY_ENDPOINT}/alaudaorg/kaldr:$i >/dev/null 2>&1
  echo $i $(date -d $(docker inspect ${REGISTRY_ENDPOINT}/alaudaorg/kaldr:$i -f '{{.Created}}') +%s)
done | sort -k2n,2 | awk 'END{print $1}' > /tmp/dd
	
	
KALDR_IMAGE=${REGISTRY_ENDPOINT}/alaudaorg/kaldr:$(cat /tmp/dd)
docker rm -f yum
mv -f /cpaas/.run_kaldr.sh /cpaas/.run_kaldr.sh.old
cat /cpaas/.run_kaldr.sh.old | awk '{for(i=1;i<NF;i++){printf $i" "}print "'$KALDR_IMAGE'"}' >/cpaas/.run_kaldr.sh
bash /cpaas/.run_kaldr.sh
```

执行完毕后，yum 容器会被重建，请检查是否重建。


### 停掉认证组件的 pod

**执行位置**：global 集群的第一个 master 上

**操作步骤**：执行如下命令：
	
```
ACP_NAMESPACE=<alauda-system>     # ns
mkdir /cpaas/backup
kubectl get cm -n ${ACP_NAMESPACE} dex-configmap -o yaml > /cpaas/backup/dex-configmap.yaml.new
kubectl scale deployment -n ${ACP_NAMESPACE} auth-controller2 dex --replicas=0
```

### 修改 acp-config 

**注意：**如果这个 2.3 的环境是从更低版本升级过来的，global 集群会不存在 acp-config 这个 cm，请在 global 集群的第一个 master 上，执行如下命令创建这个 cm：

```
ACP_NAMESPACE=<global 组件所在的 ns >
rm -rf /tmp/acp-config ; mkdir /tmp/acp-config ; helm get values dex >/tmp/acp-config/values.yaml
kubectl create configmap acp-config -n ${ACP_NAMESPACE} --from-file /tmp/acp-config/values.yaml
```

**执行位置**：global 集群的第一个 master 上

**操作步骤**：执行如下命令：
	
```
ACP_NAMESPACE=<alauda-system>     # ns
kubectl edit cm -n ${ACP_NAMESPACE} acp-config
```

修改如下内容：
	
```
kafka:
  ha_switch: true                                # 这个不动
  zk_host: 10.0.128.132,10.0.129.50,10.0.129.31  # 不动
  zk_connect: cpaas-zookeeper                    # 新增
  retention_hours: 2                             # 新增
  roll_hours: 1                                  # 新增
```


找到 `consoledevops:` 及之后的两行，将这三行复制下来，添加到其之后，如图：

<img src="images/up_add_aml.png" style="zoom:50%;" />

检查 kafkaHost: cpaas-kafka:9092 # 这个 key 的值是不是 `cpaas-kafka:9092`。

并在 furion: 上面增加以下内容：

```
    clusterRegistry:
      supportedVersions: v1.13,v1.14

```

如图：

<img src="images/add_devops.png" style="zoom:50%;" />

新增如下内容：
	
```
    consoleamp:                         #在consoleace 后面插入，将consoleace 三行复制，修改consoleace 为consoleamp
      scheme: https                    
      host: xxxxxxx                     #和consoleace 的 host 一样
    resources:                          #之下的7行都是新增的
      requests:
        cpu: 50m
        memory: 100Mi
      limits:
        cpu: "2"
        memory: 4Gi
```

在 `mars:` 前面插入以下内容：

**注意**：如果对接了客户的 es 和 kafka ，esHost 和 kafkaHost 的值请参考 lanaya 的值修改。

```
morgans:
  esHost: http://cpaas-elasticsearch:9200
  kafkaHost: cpaas-kafka:9092
```


**注意**： acp-config 这个 cm ，最终必须包括如下键值，请仔细检查。

```
alaudaConsole:
  apiAddress:
  erebusApiAddress:
  esAuditTtl:
  esEventTtl:
  esLogTtl:
  oidcIssuerUrl:
  oidcProtocolOverride:
  oidcRedirectUrl:
  redirect:
clusterRegistry:
  supportedVersions:
configmap:
  default_admin:
consoleace:
  host:
  scheme:
consoleacp:
  host:
  scheme:
consoleaml:
  host:
  scheme:
consoleamp:
  callback_path:
  instsll_amp:
  scheme:
consoleasm:
  host:
  scheme:
consoledevops:
  host:
  scheme:
consoleplatform:
  host:
  scheme:
dashboard:
  host:
  scheme:
devops:
  host:
  scheme:
devopsApiServer:
  etcdServer:
dex:
  host:
diabloFrontend:
  args:
    apiAddress:
    oidcIssuerUrl:
    oidcRedirectUrl:
elasticsearch:
  es_passwd:
  es_passwd_64:
  es_user_64:
  node_ip1:
  node_ip2:
  node_ip3:
  singlenode:
furion:
  apiGateway:
  chartmuseumUrl:
  downloadServerUrl:
  globalResourceNamespace:
  kaldrUrl:
  registryUrl:
global:
  auth:
    default_admin:
    default_admin_md5:
    default_userbinding_name:
    is_install_auth:
  host:
  labelBaseDomain: # 执行 helm get values dex | grep labelBaseDomain 如果没有找到，就不需要这个key
  namespace:
  registry:
    address:
  replicas:
  tke:
icarus:
  gateway:
kafka:
  ha_switch:
  retention_hours:
  roll_hours:
  zk_host:
lanaya:
  esHost:
  kafkaHost:
mars:
  esHost:
morgans:
  esHost:
  kafkaHost:
nortrom:
  esHost:
  kafkaHost:
  token:
portal:
  host:
  scheme:
resources:
  limits:
    cpu:
    memory:
  requests:
    cpu:
    memory:
secret:
  tlsCrt:
  tlsKey:
tiny:
  esHost:
  kafkaHost:
```

**注2：** 执行`kubectl get cm -n ${ACP_NAMESPACE} acp-config -o yaml` ，data 下，必须是 values.yaml，如下图所示。

<img src="images/acp-config.png" style="zoom:50%;" />

### 刷新 repo 并更新平台 chart

**执行位置**：global 集群的第一个 master 上

**操作步骤**：执行如下命令：

```
helm repo update
	
	
for i in dex cert-manager captain
do
  helm upgrade $i stable/$i >>/cpaas/upgrade_chart.log && echo "upgrade $i success" || echo "upgrade $i error"
  sleep 10
done	
	
	
for i in kube-prometheus prometheus-operator
do
  helm upgrade $i stable/$i --timeout=3000 --wait --force >>/cpaas/upgrade_chart.log && echo "upgrade $i success" || echo "upgrade $i error"
  sleep 10
done
```

**注意**：剩下的组件改成 captain 部署，首先将 2.6 安装目录下的 `other/kubectl-captain` 拷贝到 global 集群的 master 的节点的 `/usr/local/sbin` 下，并增加可以执行权限。如果 `/usr/local/sbin` 不在 PATH 内，可以拷贝到 `/usr/sbin`。

* kafka-zookeeper

    安装，执行如下命令：

	```
	ACP_NAMESPACE=<alauda-system>     # ns
	kubectl captain create \
	        --version $(helm search | grep '^stable/kafka-zookeeper ' | awk '{print $2}') \
	        --configmap=acp-config \
	        --namespace=${ACP_NAMESPACE} \
	        --chart=stable/kafka-zookeeper \
	        kafka-zookeeper
	```
	
	检查，执行如下命令：
	
	`kubectl get hr -n ${ACP_NAMESPACE} kafka-zookeeper ; kubectl get pod --all-namespaces | grep -E 'kafk|zoo' `
	
	如果 PHASE 的状态是Synced 即说明 captain 安装成功，如果 pod 的 create 时间是刚刚操作的时间，说明更新 pod 成功。
	
	删除helm 内 release，执行如下命令：
	
	`kubectl delete cm -n kube-system $(kubectl get cm -n kube-system | awk '/^kafka-zookeeper/{print $1}') `
	
	这样就删掉了kafka-zookeeper 在 helm 中的 chart list，再执行：
	
	`helm list -a | grep kafka-zoo`
	
	**说明**：找不到 kafka-zookeeper 就表明升级成功了。
	
* elasticsearch

    安装，执行如下命令：

	```
	ACP_NAMESPACE=<alauda-system>     # ns
	kubectl captain create \
	        --version $(helm search | grep '^stable/elasticsearch ' | awk '{print $2}') \
	        --configmap=acp-config \
	        --namespace=${ACP_NAMESPACE} \
	        --chart=stable/elasticsearch \
	        elasticsearch
	```
	
	检查，执行如下命令：
	
	`kubectl get hr -n ${ACP_NAMESPACE} elasticsearch ; kubectl get pod --all-namespaces | grep -E 'elasticsearch' `
	
	如果 PHASE 的状态是 Synced 即说明 captain 安装成功，如果 pod 的 create 时间是刚刚操作的时间，说明更新 pod 成功。
	
	删除helm 内 release，执行如下命令：
	
	`kubectl delete cm -n kube-system $(kubectl get cm -n kube-system | awk '/^elasticsearch/{print $1}') `
		
	这样就删掉了elasticsearch 在 helm 中的 chart list，再执行：
	
	`helm list -a | grep elasticsearch `
	
	**说明**：找不到 elasticsearch 就就表明升级成功了。

* nginx-ingress

    安装，执行如下命令：

	```
	ACP_NAMESPACE=<alauda-system>     # ns
	kubectl captain create \
	        --version $(helm search | grep '^stable/nginx-ingress ' | awk '{print $2}') \
	        --configmap=acp-config \
	        --namespace=${ACP_NAMESPACE} \
	        --chart=stable/nginx-ingress \
	        nginx-ingress
	```
	
	检查，执行如下命令：
	
	`kubectl get hr -n ${ACP_NAMESPACE} nginx-ingress ; kubectl get pod --all-namespaces | grep -E 'ingress' `
	
	如果 PHASE 的状态是 Synced 即说明 captain 安装成功，如果 pod 的 create 时间是刚刚操作的时间，说明更新 pod 成功。如果发现新增了 pod ，但是老的 ingress 的 pod 还存在，这是更新过程中，label 问题造成的，alb、ingress 和 dashboard 这三个组件有一定几率出现这个问题，选择删掉这三个组件的老的 deploy ，然后继续按文档操作即可。
	
	删除helm 内 release，执行如下命令：
	
	`kubectl delete cm -n kube-system $(kubectl get cm -n kube-system | awk '/^nginx-ingress/{print $1}') `
	
	这样就删掉了nginx-ingress 在 helm 中的 chart list，再执行：
	
	`helm list -a | grep ingress `
	
	**说明**：找不到 ingress 就表明升级成功了。

* alauda-base

    安装，执行如下命令：

	```
	ACP_NAMESPACE=<alauda-system>     # ns
	kubectl captain create \
	        --version $(helm search | grep '^stable/alauda-base ' | awk '{print $2}') \
	        --configmap=acp-config \
	        --set meepo.kubefedVersion=$(helm search | grep '^stable/kubefed ' | awk '{print $2}') \
	        --namespace=${ACP_NAMESPACE} alauda-base \
	        --chart=stable/alauda-base \
	        alauda-base
	```
	
	检查，执行如下命令：
	
	`kubectl get hr -n ${ACP_NAMESPACE} alauda-base ; kubectl get pod --all-namespaces | grep -E 'lanay' `
	
	如果 PHASE 的状态是Synced 即说明 captain 安装成功，如果 pod 的 create 时间是刚刚操作的时间，说明更新 pod 成功。
	
	删除helm 内 release，执行如下命令：
	
	`kubectl delete cm -n kube-system $(kubectl get cm -n kube-system | awk '/^alauda-base/{print $1}') `
	
	这样就删掉了alauda-base 在 helm 中的 chart list，再执行：
	
	`helm list -a | grep alauda-base `
	
	**说明**：找不到 alauda-base 就表明升级成功了。

* download-server

    安装，执行如下命令：

	```
	ACP_NAMESPACE=<alauda-system>     # ns
	kubectl captain create \
	        --version $(helm search | grep '^stable/download-server ' | awk '{print $2}') \
	        --configmap=acp-config \
	        --namespace=${ACP_NAMESPACE} \
	        --chart=stable/download-server \
	        download-server
	```
	
	检查，执行如下命令：
	
	`kubectl get hr -n ${ACP_NAMESPACE} download-server  ; kubectl get pod --all-namespaces | grep -E 'down' `
	
	如果 PHASE 的状态是 Synced 即说明 captain 安装成功，如果 pod 的 create 时间是刚刚操作的时间，说明更新 pod 成功。
	
	删除 helm 内 release，执行如下命令：
	
	`kubectl delete cm -n kube-system $(kubectl get cm -n kube-system | awk '/^download-server/{print $1}') `
	
	这样就删掉了download-server 在 helm 中的 chart list，再执行：
	
	`helm list -a | grep download-server `
	
	**说明**：找不到 download-server 就表明升级成功了。

* alauda-cluster-base

    安装，执行如下命令：

	```
	ACP_NAMESPACE=<alauda-system>     # ns
	REGISTRY_ENDPOINT=$(docker info |grep 60080  |tr -d ' ')
	ROOT_USERNAME=<admin@cpaas.io>   # 管理员用户名，默认admin@cpaas.io
	kubectl captain create \
	        --version $(helm search | grep '^stable/alauda-cluster-base ' | awk '{print $2}') \
	        --set global.namespace=${ACP_NAMESPACE} \
	        --set global.registry.address=${REGISTRY_ENDPOINT} \
	        --set global.auth.default_admin=${ROOT_USERNAME} \
	        --namespace=${ACP_NAMESPACE} \
	        --chart=stable/alauda-cluster-base \
	        alauda-cluster-base
	```
	
	检查，执行如下命令：
	
	`kubectl get hr -n ${ACP_NAMESPACE} alauda-cluster-base  ; kubectl get pod --all-namespaces | grep -E 'charon' `
	
	如果 PHASE 的状态是 Synced 即说明 captain 安装成功，如果 pod 的 create 时间是刚刚操作的时间，说明更新 pod 成功。
	
	删除 helm 内 release，执行如下命令：
	
	`kubectl delete cm -n kube-system $(kubectl get cm -n kube-system | awk '/^alauda-cluster-base/{print $1}') `
	
	这样就删掉了alauda-cluster-base 在 helm 中的 chart list，再执行：
	
	`helm list -a | grep alauda-cluster-base `
	
	**说明**：找不到 alauda-cluster-base 就表明升级成功了。

* alauda-log-agent

    安装，执行如下命令：

	```
	ACP_NAMESPACE=<alauda-system>     # ns
	REGISTRY_ENDPOINT=$(docker info |grep 60080  |tr -d ' ')
	token=$(kubectl describe secrets  $(kubectl get secret -n kube-system | awk '/^clusterrole-aggregation-controller-token/{print $1}') -n kube-system|grep ^token|awk '{print $2}')
	DOMAIN_NAME=<域名>   # 部署时，--domain-name 参数的值
       
	kubectl captain create \
	        --version $(helm search | grep '^stable/alauda-log-agent ' | awk '{print $2}') \
	        --namespace=${ACP_NAMESPACE} \
	        --set global.namespace=${ACP_NAMESPACE} \
	        --set nevermore.region=global \
	        --set nevermore.apiGatewayHost=https://${DOMAIN_NAME} \
	        --set nevermore.token="${token}" \
	        --set global.registry.address=${REGISTRY_ENDPOINT} \
	        --chart=stable/alauda-log-agent \
	        alauda-log-agent
	```
	
	检查，执行如下命令：
	
	`kubectl get hr -n ${ACP_NAMESPACE} alauda-log-agent  ; kubectl get pod --all-namespaces | grep -E 'nevermore' `
	
	如果 PHASE 的状态是 Synced 即说明 captain 安装成功，如果 pod 的 create 时间是刚刚操作的时间，说明更新 pod 成功。
	
	删除 helm 内 release，执行如下命令：
	
	`kubectl delete cm -n kube-system $(kubectl get cm -n kube-system | awk '/^alauda-log-agent/{print $1}') `
	
	这样就删掉了alauda-log-agent 在 helm 中的 chart list，再执行：
	
	`helm list -a | grep alauda-log-agent `
	
	**说明**：找不到 alauda-log-agent 就表明升级成功了。

* dashboard

    安装，执行如下命令：

	```
	ACP_NAMESPACE=<alauda-system>     # ns
	REGISTRY_ENDPOINT=$(docker info |grep 60080  |tr -d ' ')
	DOMAIN_NAME=<域名>   # 部署时，--domain-name 参数的值
       
	kubectl captain create \
	        --version $(helm search | grep '^stable/dashboard ' | awk '{print $2}') \
	        --namespace=${ACP_NAMESPACE} \
	        --set namespace=${ACP_NAMESPACE} \
	        --set global.registry.address=${REGISTRY_ENDPOINT} \
	        --set ingress.host=${DOMAIN_NAME} \
	        --chart=stable/dashboard \
	        dashboard
	```
	
	检查，执行如下命令：
	
	`kubectl get hr -n ${ACP_NAMESPACE} dashboard ; kubectl get pod --all-namespaces | grep -E 'dashboard' `
	
	如果 PHASE 的状态是 Synced 即说明 captain 安装成功，如果 pod 的 create 时间是刚刚操作的时间，说明更新 pod 成功。如果发现新增了 pod ，但是老的 ingress 的 pod 还存在，这是更新过程中，label 问题造成的，alb、ingress 和 dashboard 这三个组件有一定几率出现这个问题，选择删掉这三个组件的老的 deploy ，然后继续按文档操作即可。
	
	删除 helm 内 release，执行如下命令：
	
	`kubectl delete cm -n kube-system $(kubectl get cm -n kube-system | awk '/^dashboard/{print $1}') `
	
	这样就删掉了 dashboard 在 helm 中的 chart list，再执行：
	
	`helm list -a | grep dashboard `
	
	**说明**：找不到 dashboard 就表明升级成功了。

* cpaas-monitor

    安装，执行如下命令：

	```
	ACP_NAMESPACE=<alauda-system>     # ns
	REGISTRY_ENDPOINT=$(docker info |grep 60080  |tr -d ' ')       
	kubectl captain create \
	        --version $(helm search | grep '^stable/cpaas-monitor ' | awk '{print $2}') \
	        --namespace=${ACP_NAMESPACE} \
	        --set global.namespace=${ACP_NAMESPACE} \
	        --set global.registry.address=${REGISTRY_ENDPOINT} \
	        --set cluster.isGlobal='true' \
	        --set cluster.name=global \
	        --set cluster.prometheusName=kube-prometheus \
	        --chart=stable/cpaas-monitor \
	        cpaas-monitor
	```
	
	检查，执行如下命令：
	
	`kubectl get hr -n ${ACP_NAMESPACE} cpaas-monitor `
	
	如果 PHASE 的状态是 Synced 即说明 captain 安装成功。
	
	删除 helm 内 release，执行如下命令：
	
	`kubectl delete cm -n kube-system $(kubectl get cm -n kube-system | awk '/^cpaas-monitor/{print $1}') `
	
	这样就删掉了cpaas-monitor 在 helm 中的 chart list，再执行
	
	`helm list -a | grep cpaas-monitor `
	
	找不到 cpaas-monitor 就是升级成功了
	
* alauda-container-platform

    安装，执行如下命令：

	```
	ACP_NAMESPACE=<alauda-system>     # ns
	kubectl captain create \
	        --version $(helm search | grep '^stable/alauda-container-platform' | awk '{print $2}') \
	        --configmap=acp-config \
	        --namespace=${ACP_NAMESPACE} \
	        --chart=stable/alauda-container-platform \
	        alauda-container-platform
	```
	
	检查，执行如下命令：
	
	`kubectl get hr -n ${ACP_NAMESPACE} alauda-container-platform ; kubectl get pod -n ${ACP_NAMESPACE} | grep icarus `
	
	如果 PHASE 的状态是 Synced 即说明 captain 安装成功。
	
	删除 helm 内 release，执行如下命令：
	
	`kubectl delete cm -n kube-system $(kubectl get cm -n kube-system | awk '/^alauda-container-platform/{print $1}') `
	
	这样就删掉了alauda-container-platform 在 helm 中的 chart list，再执行：
	
	`helm list -a | grep alauda-container-platform `
	
	**说明**：找不到 alauda-container-platform 就表明升级成功了。

* public-chart-repo

    安装，执行如下命令：

	```
	ACP_NAMESPACE=<alauda-system>     # ns
	REGISTRY_ENDPOINT=$(docker info |grep 60080  |tr -d ' ')       
	kubectl captain create \
	        --version $(helm search | grep '^stable/public-chart-repo' | awk '{print $2}') \
	        --set global.registry.address=${REGISTRY_ENDPOINT} \
	        --set global.namespace=${ACP_NAMESPACE} \
	        --namespace=${ACP_NAMESPACE} \
	        --chart=stable/public-chart-repo \
	        public-chart-repo
	```
	
	检查，执行如下命令：
	
	`kubectl get hr -n ${ACP_NAMESPACE} public-chart-repo ; kubectl get pod -n ${ACP_NAMESPACE} | grep public-chart-repo `
	
	如果 PHASE 的状态是 Synced 即说明 captain 安装成功。
	
* alauda-devops

    安装，执行如下命令：

	```
	ACP_NAMESPACE=<alauda-system>     # ns
	kubectl captain create \
	        --version $(helm search | grep '^stable/alauda-devops ' | awk '{print $2}') \
	        --configmap=acp-config \
	        --namespace=${ACP_NAMESPACE} \
	        --chart=stable/alauda-devops \
	        alauda-devops
	```
	
	检查，执行如下命令：
	
	`kubectl get hr -n ${ACP_NAMESPACE}  alauda-devops ; kubectl get pod -n ${ACP_NAMESPACE} | grep devops-api `
	
	如果 PHASE 的状态是 Synced 即说明 captain 安装成功。
	
	删除 helm 内 release，执行如下命令：
	
	`kubectl delete cm -n kube-system $(kubectl get cm -n kube-system | awk '/^alauda-devops/{print $1}') `
	
	这样就删掉了 alauda-devops 在 helm 中的 chart list，再执行：
	
	`helm list -a | grep alauda-devops `
	
	**说明**：找不到 alauda-devops 就表明升级成功了。
	
* devops 业务组件升级，请看集群升级部分

### tke 升级

1. 安装，执行如下命令：

	```
	rm -rf /tmp/dd ; mkdir /tmp/dd ; helm get values tke >/tmp/dd/values.yaml
	kubectl create configmap tke-config -n ${ACP_NAMESPACE} --from-file /tmp/dd/values.yaml
	ACP_NAMESPACE=<alauda-system>     # ns
	kubectl captain create \
	        --version $(helm search | grep '^stable/tke ' | awk '{print $2}') \
	        --configmap=tke-config \
	        --namespace=${ACP_NAMESPACE} \
	        --chart=stable/tke \
	        tke
	```

2. 检查，执行如下命令：
	

	`kubectl get hr -n ${ACP_NAMESPACE} tke ; kubectl get pod -n ${ACP_NAMESPACE} | grep tke-platform-api`
	
	如果 PHASE 的状态是 Synced 即说明 captain 安装成功。
	
3. 删除 helm 内 release，执行如下命令：
   	
   `kubectl delete cm -n kube-system $(kubectl get cm -n kube-system | awk '/^tke/{print $1}') `
   	
   这样就删掉了 tke 在 helm 中的 chart list。
   
4. 再执行查找命令：
   	
   `helm list -a | grep tke `
   	
   **说明**：找不到 tke 就表明升级成功了。


### 启动认证组件

**执行位置**：global 集群的第一个 master 上

**操作步骤**：执行如下命令：

```
ACP_NAMESPACE=<alauda-system>     # ns
kubectl scale deployment -n ${ACP_NAMESPACE} auth-controller2 dex --replicas=2
```


### 数据初始化

**执行位置**：global 集群的第一个 master 上

**操作步骤**：执行如下命令：

```
kubectl exec -it -n $(kubectl get po --all-namespaces | grep courier | awk '{print $1" "$2}' | head -n 1) python /migration/migrate.py   #初始化 courier 数据
kubectl exec -it -n $(kubectl get po --all-namespaces | grep morgans | awk '{print $1" "$2}' | head -n 1) python /morgans/migration/api/migrate.py   # 初始化 morgans 数据
```


### 升级之后验证

**执行位置**：global 集群的第一个 master 上

**操作步骤**：执行命令 `helm list -a ; kubectl get hr -n ${ACP_NAMESPACE} ` ，除了publick-chart-repo之外，其他 chart 版本都应该是 2.6。


## 业务集群及集群组件升级

### 使用 calico 网络的集群升级raven的镜像

**执行位置**：calico 集群的第一个 master 上

**操作步骤**：执行如下命令：

```
kubectl -n kube-system edit deploy raven   #如果找不到raven 这个 deploy，说明 集群是2.2 之前部署的，需要迁移，因为极其复杂，建议重新创建集群
修改raven的镜像tag为b0b6142fca3113e3cacb981ae2565e92ba2233d6
```

### 使用 ovn 网络的集群升级 ovn 版本到0.10.0

**执行位置**：ovn 集群的第一个 master 上

**操作步骤**：执行如下命令：

**注意：**升级过程中容器网络会中断，需要提前申请停机时间。

```
## 删除 metis webhook
kubectl delete mutatingwebhookconfigurations.admissionregistration.k8s.io metis

## 备份每个 master 节点 /etc/origin/openvswitch 下的文件
mkdir -p /cpaas/backup/ovn ; cp -Ra /etc/origin/openvswitch /cpaas/backup/ovn

## 更新 ovn 相关组件 yaml
cat <<EOF >ovn.yaml
apiVersion: v1
kind: Namespace
metadata:
  name: kube-ovn

---
apiVersion: v1
kind: ConfigMap
metadata:
  name: ovn-config
  namespace: kube-ovn

---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: ovn
  namespace: kube-ovn

---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  annotations:
    rbac.authorization.k8s.io/system-only: "true"
  name: system:ovn
rules:
  - apiGroups:
      - "kubeovn.io"
    resources:
      - subnets
      - subnets/status
      - ips
    verbs:
      - "*"
  - apiGroups:
      - ""
    resources:
      - pods
      - namespaces
      - nodes
      - configmaps
    verbs:
      - create
      - get
      - list
      - watch
      - patch
      - update
  - apiGroups:
      - ""
      - networking.k8s.io
      - apps
    resources:
      - networkpolicies
      - services
      - endpoints
      - statefulsets
      - daemonsets
    verbs:
      - get
      - list
      - watch
  - apiGroups:
      - ""
    resources:
      - events
    verbs:
      - create
      - patch
      - update

---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: ovn
roleRef:
  name: system:ovn
  kind: ClusterRole
  apiGroup: rbac.authorization.k8s.io
subjects:
  - kind: ServiceAccount
    name: ovn
    namespace: kube-ovn

---
kind: Service
apiVersion: v1
metadata:
  name: ovn-nb
  namespace: kube-ovn
spec:
  ports:
    - name: ovn-nb
      protocol: TCP
      port: 6641
      targetPort: 6641
  type: ClusterIP
  selector:
    app: ovn-central
    ovn-nb-leader: "true"
  sessionAffinity: None

---
kind: Service
apiVersion: v1
metadata:
  name: ovn-sb
  namespace: kube-ovn
spec:
  ports:
    - name: ovn-sb
      protocol: TCP
      port: 6642
      targetPort: 6642
  type: ClusterIP
  selector:
    app: ovn-central
    ovn-sb-leader: "true"
  sessionAffinity: None

---
kind: Deployment
apiVersion: apps/v1
metadata:
  name: ovn-central
  namespace: kube-ovn
  annotations:
    kubernetes.io/description: |
      OVN components: northd, nb and sb.
spec:
  replicas: 1
  strategy:
    rollingUpdate:
      maxSurge: 0%
      maxUnavailable: 100%
    type: RollingUpdate
  selector:
    matchLabels:
      app: ovn-central
  template:
    metadata:
      labels:
        app: ovn-central
        component: network
        type: infra
    spec:
      tolerations:
      - operator: Exists
        effect: NoSchedule
      affinity:
        podAntiAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            - labelSelector:
                matchLabels:
                  app: ovn-central
              topologyKey: kubernetes.io/hostname
      serviceAccountName: ovn
      hostNetwork: true
      containers:
        - name: ovn-central
          image: "index.alauda.cn/alaudak8s/kube-ovn-db:v0.10.0"
          imagePullPolicy: IfNotPresent
          securityContext:
            capabilities:
              add: ["SYS_NICE"]
          env:
            - name: POD_IP
              valueFrom:
                fieldRef:
                  fieldPath: status.podIP
            - name: POD_NAME
              valueFrom:
                fieldRef:
                  fieldPath: metadata.name
            - name: POD_NAMESPACE
              valueFrom:
                fieldRef:
                  fieldPath: metadata.namespace
          resources:
            requests:
              cpu: 500m
              memory: 300Mi
          volumeMounts:
            - mountPath: /run/openvswitch
              name: host-run-ovs
            - mountPath: /var/run/openvswitch
              name: host-run-ovs
            - mountPath: /sys
              name: host-sys
              readOnly: true
            - mountPath: /etc/openvswitch
              name: host-config-openvswitch
            - mountPath: /var/log/openvswitch
              name: host-log
          readinessProbe:
            exec:
              command:
                - sh
                - /root/ovn-is-leader.sh
            periodSeconds: 3
          livenessProbe:
            exec:
              command:
              - sh
              - /root/ovn-healthcheck.sh
            initialDelaySeconds: 30
            periodSeconds: 7
            failureThreshold: 5
      nodeSelector:
        beta.kubernetes.io/os: "linux"
        kube-ovn/role: "master"
      volumes:
        - name: host-run-ovs
          hostPath:
            path: /run/openvswitch
        - name: host-sys
          hostPath:
            path: /sys
        - name: host-config-openvswitch
          hostPath:
            path: /etc/origin/openvswitch
        - name: host-log
          hostPath:
            path: /var/log/openvswitch

---
kind: DaemonSet
apiVersion: apps/v1
metadata:
  name: ovs-ovn
  namespace: kube-ovn
  annotations:
    kubernetes.io/description: |
      This daemon set launches the openvswitch daemon.
spec:
  selector:
    matchLabels:
      app: ovs
  updateStrategy:
    type: OnDelete
  template:
    metadata:
      labels:
        app: ovs
        component: network
        type: infra
    spec:
      tolerations:
      - operator: Exists
        effect: NoSchedule
      serviceAccountName: ovn
      hostNetwork: true
      hostPID: true
      containers:
        - name: openvswitch
          image: "index.alauda.cn/alaudak8s/kube-ovn-node:v0.10.0"
          imagePullPolicy: IfNotPresent
          securityContext:
            runAsUser: 0
            privileged: true
          env:
            - name: POD_IP
              valueFrom:
                fieldRef:
                  fieldPath: status.podIP
          volumeMounts:
            - mountPath: /lib/modules
              name: host-modules
              readOnly: true
            - mountPath: /run/openvswitch
              name: host-run-ovs
            - mountPath: /var/run/openvswitch
              name: host-run-ovs
            - mountPath: /sys
              name: host-sys
              readOnly: true
            - mountPath: /etc/openvswitch
              name: host-config-openvswitch
            - mountPath: /var/log/openvswitch
              name: host-log
          readinessProbe:
            exec:
              command:
              - sh
              - /root/ovs-healthcheck.sh
            periodSeconds: 5
          livenessProbe:
            exec:
              command:
              - sh
              - /root/ovs-healthcheck.sh
            initialDelaySeconds: 10
            periodSeconds: 5
            failureThreshold: 5
          resources:
            requests:
              cpu: 200m
              memory: 300Mi
            limits:
              cpu: 1000m
              memory: 800Mi
      nodeSelector:
        beta.kubernetes.io/os: "linux"
      volumes:
        - name: host-modules
          hostPath:
            path: /lib/modules
        - name: host-run-ovs
          hostPath:
            path: /run/openvswitch
        - name: host-sys
          hostPath:
            path: /sys
        - name: host-config-openvswitch
          hostPath:
            path: /etc/origin/openvswitch
        - name: host-log
          hostPath:
            path: /var/log/openvswitch
EOF

## 修改 ovn.yaml  有如下改动：
    ## 1、 查看当前 ovn-central deployment 下的环境变量 NODE_IPS 将其更新到下载的 yaml 中，并将 replica 数量改为和之前一致
    ## 2、 将 yaml 中镜像地址换成私有地址
    ## 3、 执行下面的命令
kubectl apply -f ovn.yaml

## 等待 ovn-central pod ready， 手动杀掉 ovs-ovn pod 进行重建

## 更新 kube-ovn 控制平面组件
cat <<EOF >kube-ovn.yaml
---
kind: Deployment
apiVersion: apps/v1
metadata:
  name: kube-ovn-controller
  namespace: kube-ovn
  annotations:
    kubernetes.io/description: |
      kube-ovn controller
spec:
  replicas: 2
  selector:
    matchLabels:
      app: kube-ovn-controller
  strategy:
    rollingUpdate:
      maxSurge: 0%
      maxUnavailable: 100%
    type: RollingUpdate
  template:
    metadata:
      labels:
        app: kube-ovn-controller
        component: network
        type: infra
    spec:
      tolerations:
      - operator: Exists
        effect: NoSchedule
      affinity:
        podAntiAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            - labelSelector:
                matchLabels:
                  app: kube-ovn-controller
              topologyKey: kubernetes.io/hostname
      serviceAccountName: ovn
      hostNetwork: true
      containers:
        - name: kube-ovn-controller
          image: "index.alauda.cn/alaudak8s/kube-ovn-controller:v0.10.0"
          imagePullPolicy: IfNotPresent
          command:
          - /kube-ovn/start-controller.sh
          args:
          - --default-cidr=10.16.0.0/16
          - --default-gateway=10.16.0.1
          - --node-switch-cidr=100.64.0.0/16
          env:
            - name: POD_NAME
              valueFrom:
                fieldRef:
                  fieldPath: metadata.name
            - name: KUBE_NAMESPACE
              valueFrom:
                fieldRef:
                  fieldPath: metadata.namespace
            - name: KUBE_NODE_NAME
              valueFrom:
                fieldRef:
                  fieldPath: spec.nodeName
          readinessProbe:
            exec:
              command:
                - nc
                - -z
                - -w3
                - 127.0.0.1
                - "10660"
            periodSeconds: 3
          livenessProbe:
            exec:
              command:
                - nc
                - -z
                - -w3
                - 127.0.0.1
                - "10660"
            initialDelaySeconds: 30
            periodSeconds: 7
            failureThreshold: 5
      nodeSelector:
        beta.kubernetes.io/os: "linux"

---
kind: DaemonSet
apiVersion: apps/v1
metadata:
  name: kube-ovn-cni
  namespace: kube-ovn
  annotations:
    kubernetes.io/description: |
      This daemon set launches the kube-ovn cni daemon.
spec:
  selector:
    matchLabels:
      app: kube-ovn-cni
  updateStrategy:
    type: OnDelete
  template:
    metadata:
      labels:
        app: kube-ovn-cni
        component: network
        type: infra
    spec:
      tolerations:
      - operator: Exists
        effect: NoSchedule
      serviceAccountName: ovn
      hostNetwork: true
      hostPID: true
      initContainers:
      - name: install-cni
        image: "index.alauda.cn/alaudak8s/kube-ovn-cni:v0.10.0"
        imagePullPolicy: IfNotPresent
        command: ["/kube-ovn/install-cni.sh"]
        securityContext:
          runAsUser: 0
          privileged: true
        volumeMounts:
          - mountPath: /etc/cni/net.d
            name: cni-conf
          - mountPath: /opt/cni/bin
            name: cni-bin
      containers:
      - name: cni-server
        image: "index.alauda.cn/alaudak8s/kube-ovn-cni:v0.10.0"
        imagePullPolicy: IfNotPresent
        command:
          - sh
          - /kube-ovn/start-cniserver.sh
        args:
          - --enable-mirror=true
        securityContext:
          capabilities:
            add: ["NET_ADMIN", "SYS_ADMIN", "SYS_PTRACE"]
        env:
          - name: POD_IP
            valueFrom:
              fieldRef:
                fieldPath: status.podIP
          - name: KUBE_NODE_NAME
            valueFrom:
              fieldRef:
                fieldPath: spec.nodeName
        volumeMounts:
          - mountPath: /run/openvswitch
            name: host-run-ovs
          - mountPath: /var/run/netns
            name: host-ns
            mountPropagation: HostToContainer
        readinessProbe:
          exec:
            command:
              - nc
              - -z
              - -w3
              - 127.0.0.1
              - "10665"
          periodSeconds: 3
        livenessProbe:
          exec:
            command:
              - nc
              - -z
              - -w3
              - 127.0.0.1
              - "10665"
          initialDelaySeconds: 30
          periodSeconds: 7
          failureThreshold: 5
      nodeSelector:
        beta.kubernetes.io/os: "linux"
      volumes:
        - name: host-run-ovs
          hostPath:
            path: /run/openvswitch
        - name: cni-conf
          hostPath:
            path: /etc/cni/net.d
        - name: cni-bin
          hostPath:
            path: /opt/cni/bin
        - name: host-ns
          hostPath:
            path: /var/run/netns

---
kind: DaemonSet
apiVersion: apps/v1
metadata:
  name: kube-ovn-pinger
  namespace: kube-ovn
  annotations:
    kubernetes.io/description: |
      This daemon set launches the openvswitch daemon.
spec:
  selector:
    matchLabels:
      app: kube-ovn-pinger
  updateStrategy:
    type: RollingUpdate
  template:
    metadata:
      labels:
        app: kube-ovn-pinger
        component: network
        type: infra
    spec:
      tolerations:
        - operator: Exists
          effect: NoSchedule
      serviceAccountName: ovn
      hostPID: true
      containers:
        - name: pinger
          image: "index.alauda.cn/alaudak8s/kube-ovn-pinger:v0.10.0"
          imagePullPolicy: IfNotPresent
          securityContext:
            runAsUser: 0
            privileged: false
          env:
            - name: POD_IP
              valueFrom:
                fieldRef:
                  fieldPath: status.podIP
            - name: HOST_IP
              valueFrom:
                fieldRef:
                  fieldPath: status.hostIP
            - name: POD_NAME
              valueFrom:
                fieldRef:
                  fieldPath: metadata.name
            - name: NODE_NAME
              valueFrom:
                fieldRef:
                  fieldPath: spec.nodeName
          volumeMounts:
            - mountPath: /lib/modules
              name: host-modules
              readOnly: true
            - mountPath: /run/openvswitch
              name: host-run-ovs
            - mountPath: /var/run/openvswitch
              name: host-run-ovs
            - mountPath: /sys
              name: host-sys
              readOnly: true
            - mountPath: /etc/openvswitch
              name: host-config-openvswitch
            - mountPath: /var/log/openvswitch
              name: host-log
          resources:
            requests:
              cpu: 100m
              memory: 300Mi
            limits:
              cpu: 200m
              memory: 400Mi
      nodeSelector:
        beta.kubernetes.io/os: "linux"
      volumes:
        - name: host-modules
          hostPath:
            path: /lib/modules
        - name: host-run-ovs
          hostPath:
            path: /run/openvswitch
        - name: host-sys
          hostPath:
            path: /sys
        - name: host-config-openvswitch
          hostPath:
            path: /etc/origin/openvswitch
        - name: host-log
          hostPath:
            path: /var/log/openvswitch
---
kind: Service
apiVersion: v1
metadata:
  name: kube-ovn-pinger
  namespace: kube-ovn
  labels:
    app: kube-ovn-pinger
spec:
  selector:
    app: kube-ovn-pinger
  ports:
    - port: 8080
      name: metrics
---
kind: Service
apiVersion: v1
metadata:
  name: kube-ovn-controller
  namespace: kube-ovn
  labels:
    app: kube-ovn-controller
spec:
  selector:
    app: kube-ovn-controller
  ports:
    - port: 10660
      name: metrics
EOF

## 修改 kube-ovn.yaml 将 yaml 中镜像地址换成私有地址，并执行下面的命令
kubectl apply -f kube-ovn.yaml

## 等待 kube-ovn-controller pod ready，手动杀掉 kube-ovn-cni pod 进行重建

##重启所有非 host 模式的 pod，可以选择重启集群所有节点的 docker
```


### 升级 cert-manager

**执行位置**：每一个集群的第一个 master 上

**操作步骤**：执行如下命令：

```
helm up
helm upgrade cert-manager stable/cert-manager
```

### 升级 alauda-cluster-base

**执行位置**：每一个集群的第一个 master 上和 global 的 master 上

**操作步骤**：执行如下命令：


1. 在 global 的第一个 master 上执行如下命令：

	```
	REGION_NAME=<要升级的业务集群的名字>
	REGISTRY_ENDPOINT=$(docker info |grep 60080  |tr -d ' ')
	ACP_NAMESPACE=<alauda-system>     # ns
	ROOT_USERNAME=<admin@cpaas.io>    # 管理员用户名，默认admin@cpaas.io
	mkdir /tmp/region_helmrequest
	cat << EOF >/tmp/region_helmrequest/${REGION_NAME}-alauda-cluster-base.yaml
	apiVersion: app.alauda.io/v1alpha1
	kind: HelmRequest
	metadata:
	  finalizers:
	  - captain.alauda.io
	  generation: 1
	  name: ${REGION_NAME}-alauda-cluster-base
	  namespace: ${ACP_NAMESPACE}
	spec:
	  chart: stable/alauda-cluster-base
	  namespace: ${ACP_NAMESPACE}
	  releaseName: ${REGION_NAME}-alauda-cluster-base
	  clusterName: ${REGION_NAME}
	  values:
	    global:
	      auth:
	        default_admin: ${ROOT_USERNAME}
	      namespace: ${ACP_NAMESPACE}
	      registry:
	        address: ${REGISTRY_ENDPOINT}
	version: $(helm search | grep '^stable/alauda-cluster-base ' | awk '{print $2}')
	EOF
	kubectl create -f /tmp/region_helmrequest/${REGION_NAME}-alauda-cluster-base.yaml
	```
	
2. 检查，在 global 集群的第一台 master 上执行如下命令：
	
	`kubectl get hr -n ${ACP_NAMESPACE} ${REGION_NAME}-alauda-cluster-base `
	
	**说明**：如果 PHASE 的状态是 Synced 即说明 captain 安装成功。
	
3. 在业务集群的第一个 master 上执行如下命令：
	
	`kubectl get pod -n ${ACP_NAMESPACE} | grep charon`
	
	**说明**：如果 pod 的 create 时间是刚刚操作的时间，说明更新 pod 成功。
	
4. 删除 helm 内 release，在业务集群的第一个 master 上执行如下命令：
	
	`kubectl delete cm -n kube-system $(kubectl get cm -n kube-system | awk '/^alauda-cluster-base/{print $1}') `
	
	这样就删掉了 alauda-cluster-base 在 helm 中的 chart list，再执行：
	
	`helm list -a | grep alauda-cluster-base `
	
	**说明**：找不到 alauda-cluster-base 就表明升级成功了。
	


### nevermore 升级

**执行位置**：每一个集群的第一个 master 上和 global 的 master 上

**操作步骤**：执行如下命令：


1. 在 global 的第一个 master 上执行如下命令：

	```
	REGION_NAME=<要升级的业务集群的名字>
	REGISTRY_ENDPOINT=$(docker info |grep 60080  |tr -d ' ')
	ACP_NAMESPACE=<alauda-system>     # ns
	DOMAIN_NAME=<域名>                 # 部署时，--domain-name 参数的值
	https_or_http=<https>             # 部署时选择用那种方式访问平台
	token=                            # 在业务集群执行 kubectl describe secrets  $(kubectl get secret -n kube-system | awk '/^clusterrole-aggregation-controller-token/{print $1}') -n kube-system|grep ^token|awk '{print $2}'  获取的值
	mkdir /tmp/region_helmrequest
	cat << EOF >/tmp/region_helmrequest/${REGION_NAME}-alauda-log-agent.yaml
	apiVersion: app.alauda.io/v1alpha1
	kind: HelmRequest
	metadata:
	  finalizers:
	  - captain.alauda.io
	  generation: 1
	  name: ${REGION_NAME}-alauda-log-agent
	  namespace: ${ACP_NAMESPACE}
	spec:
	  chart: stable/alauda-log-agent
	  namespace: ${ACP_NAMESPACE}
	  releaseName: ${REGION_NAME}-alauda-log-agent
	  clusterName: ${REGION_NAME}
	  values:
	    global:
	      namespace: ${ACP_NAMESPACE}
	      registry:
	        address: ${REGISTRY_ENDPOINT}
	    nevermore:
	      apiGatewayHost: ${https_or_http}://${DOMAIN_NAME}
	      region: ${REGION_NAME}
	      token: ${token}
	version: $(helm search | grep '^stable/alauda-log-agent ' | awk '{print $2}')
	
	EOF
	kubectl create -f /tmp/region_helmrequest/${REGION_NAME}-alauda-log-agent.yaml
	```
	
2. 检查，在 global 上执行如下命令：
	
	`kubectl get hr -n ${ACP_NAMESPACE} ${REGION_NAME}-alauda-log-agent `
	
3. 在业务集群的 master 上执行 `kubectl get pod --all-namespaces | grep -E 'nevermore' `
	
	**说明**：如果 PHASE 的状态是 Synced 即说明 captain 安装成功，如果 pod 的 create 时间是刚刚操作的时间，说明更新 pod 成功。
	
4. 删除 helm 内 release，在业务集群的 master 上执行如下命令：
	
	`kubectl delete cm -n kube-system $(kubectl get cm -n kube-system | awk '/^alauda-log-agent/{print $1}') `
	
	这样就删掉了alauda-log-agent 在 helm 中的 chart list，再执行：
	
	` helm list -a | grep alauda-log-agent `
	
	**说明**：找不到 alauda-log-agent 就表明升级成功了。
	
### dashboard 升级

**执行位置**：每一个集群的第一个 master 上和 global 的 master 上

**操作步骤**：执行如下命令：


1. 在 global 的第一个 master 上执行如下命令：

	```
	REGION_NAME=<要升级的业务集群的名字>
	REGISTRY_ENDPOINT=$(docker info |grep 60080  |tr -d ' ')
	ACP_NAMESPACE=<alauda-system>     # ns
	ingress=<业务集群 ingress 的地址>   # 部署时，--domain-name 参数的值
	mkdir /tmp/region_helmrequest
	
	cat << EOF >/tmp/region_helmrequest/${REGION_NAME}-dashboard.yaml
	apiVersion: app.alauda.io/v1alpha1
	kind: HelmRequest
	metadata:
	  finalizers:
	  - captain.alauda.io
	  generation: 1
	  name: ${REGION_NAME}-dashboard
	  namespace: ${ACP_NAMESPACE}
	spec:
	  chart: stable/dashboard
	  namespace: ${ACP_NAMESPACE}
	  releaseName: ${REGION_NAME}-dashboard
	  clusterName: ${REGION_NAME}
	  values:
	    global:
	      namespace: ${ACP_NAMESPACE}
	      registry:
	        address: ${REGISTRY_ENDPOINT}
	    homepage: ${ingress}
	    ingress:
	      host: ${ingress}
	    registry:
	      address: ${REGISTRY_ENDPOINT}
	version: $(helm search | grep '^stable/dashboard ' | awk '{print $2}')
	
	EOF
	kubectl create -f /tmp/region_helmrequest/${REGION_NAME}-dashboard.yaml
	```
	
2. 检查，在 global 上执行如下命令：
	
	`kubectl get hr -n ${ACP_NAMESPACE} ${REGION_NAME}-dashboard `
	
3. 在业务集群的 master 上执行： `kubectl get pod --all-namespaces | grep -E 'dashboard-dashboard' `
	
	**说明**：如果 PHASE 的状态是 Synced 即说明 captain 安装成功，如果 pod 的 create 时间是刚刚操作的时间，说明更新 pod 成功。如果发现新增了 pod ，但是老的 ingress 的 pod 还存在，这是更新过程中，label 问题造成的，alb、ingress 和 dashboard 这三个组件有一定几率出现这个问题，选择删掉这三个组件的老的 deploy ，然后继续按文档操作即可。

4. 删除 helm 内 release，在业务集群的 master 上执行如下命令：
	
	`kubectl delete cm -n kube-system $(kubectl get cm -n kube-system | awk '/dashboard/{print $1}') `
	
	这样就删掉了 dashboard 在 helm 中的 chart list，再执行
	
	`helm list -a | grep dashboard `
	
	**说明**：找不到 dashboard 就表明升级成功了。

### ingress 升级

**执行位置**：每一个集群的第一个 master 上和 global 的 master 上

**操作步骤**：执行如下命令：


1. 在 global 的第一个 master 上执行如下命令：

	```
	REGION_NAME=<要升级的业务集群的名字>
	REGISTRY_ENDPOINT=$(docker info |grep 60080  |tr -d ' ')
	ACP_NAMESPACE=<alauda-system>     # ns
	mkdir /tmp/region_helmrequest
	cat << EOF >/tmp/region_helmrequest/${REGION_NAME}-nginx-ingress.yaml
	apiVersion: app.alauda.io/v1alpha1
	kind: HelmRequest
	metadata:
	  finalizers:
	  - captain.alauda.io
	  generation: 1
	  name: ${REGION_NAME}-nginx-ingress
	  namespace: ${ACP_NAMESPACE}
	spec:
	  chart: stable/nginx-ingress
	  namespace: ${ACP_NAMESPACE}
	  releaseName: ${REGION_NAME}-nginx-ingress
	  clusterName: ${REGION_NAME}
	  values:
	    global:
	      namespace: ${ACP_NAMESPACE}
	      registry:
	        address: ${REGISTRY_ENDPOINT}
	    registry:
	      address: ${REGISTRY_ENDPOINT}
	version: $(helm search | grep '^stable/nginx-ingress ' | awk '{print $2}')
	
	EOF
	kubectl create -f /tmp/region_helmrequest/${REGION_NAME}-nginx-ingress.yaml
	```
	
2. 检查，在 global 上执行如下命令：
	
	`kubectl get hr -n ${ACP_NAMESPACE} ${REGION_NAME}-nginx-ingress `
	
3. 在业务集群的 master 上执行 `kubectl get pod --all-namespaces | grep -E 'nginx-ingress' `
	
	**说明**：如果 PHASE 的状态是 Synced 即说明 captain 安装成功，如果 pod 的 create 时间是刚刚操作的时间，说明更新 pod 成功。如果发现新增了 pod ，但是老的 ingress 的 pod 还存在，这是更新过程中，label 问题造成的，alb、ingress 和 dashboard 这三个组件有一定几率出现这个问题，选择删掉这三个组件的老的 deploy ，然后继续按文档操作即可。

4. 删除 helm 内 release，在业务集群的 master 上执行如下命令：
	
	`kubectl delete cm -n kube-system $(kubectl get cm -n kube-system | awk '/nginx-ingress/{print $1}') `
	
	这样就删掉了ingress 在 helm 中的 chart list，再执行
	
	`helm list -a | grep nginx-ingress `
	
	**说明**：找不到 ingress 就表明升级成功了。
	
	
### 普罗米修斯升级

**执行位置**：每一个集群的第一个 master 上

**操作步骤**：执行如下命令：

```
helm upgrade prometheus-operator stable/prometheus-operator
sleep 30
helm upgrade kube-prometheus stable/kube-prometheus
```


### alb升级

**执行位置**：平台 UI

* **操作步骤（UI）**：如果 alb 是通过 ui 页面生成一个 helmrequest 资源部署的，通过如下方式升级：

	1. 用管理员身份登录平台，单击左上角的图标切换产品，选择 Container Platform 。然后单击右上角的视图开关进入管理视图，如下图所示。
		
		<img src="images/升级 alb 1.png" style="zoom:50%;" />
		
	2. 单击左侧导航栏的资源管理，选择正确的 ns 和集群，找到 helmrequest 这个资源，单击更新，如下图所示。
		
		<img src="images/升级 alb 2.png" style="zoom:50%;" />
		
	3. 在 spec 下修改 version 这个 key，如果没有，添加 version。这个 key 的值就是你希望升级 alb 的目标版本，在集群的 master 下，执行 `helm search | grep alb` 就能找到最新的 alb 的版本，如下图所示。
		
		<img src="images/升级 alb 3.png" style="zoom:50%;" />
		
	4. 修改完毕后，点击右下角的更新，稍等片刻 alb 就开始更新了。
		
	5. 在集群上，执行 `kubectl get deploy -n <ns> <alb 名称> -o wide` 检查 alb 的 deploy 的状态，看看版本是否变化确认升级是否成功，如果一直卡在0/n状态，执行`kubectl get rs -n <ns> | grep <alb名称>` 检查下
  如果发现新增了 pod ，但是老的 ingress 的 pod 还存在，这是更新过程中，label 问题造成的，alb、ingress 和 dashboard 这三个组件有一定几率出现这个问题，选择删掉这三个组件的老的 deploy ，然后继续按文档操作即可。
*** 更详细的操作，请参考用户手册***
  


* **操作步骤（helm install）**：如果 alb 是通过 helm install 的方式部署的 chart ，通过如下命令升级：

	```
	## 在部署了 alb 的业务集群上的 master 上执行：
	REGION_NAME=<name>                  ## 想要部署 alb2 的集群的名字
	ACP_NAMESPACE=<cpaas-system>        ## 改成部署平台时， --acp2-namespaces 参数指定的值，默认是cpaas-system
	helm get values alauda-alb2 >/tmp/values.yaml
	
	## 将创建的/tmp/values.yaml 文件拷贝到 global 集群的第一台 master 节点的 /tmp 目录下，然后在这个 master 上执行：
	kubectl create configmap alb2-config -n <ACP_NAMESPACE> --from-file /tmp/values.yaml
	
	
	mkdir /tmp/region_helmrequest
	cat << EOF >/tmp/region_helmrequest/${REGION_NAME}-alauda-alb2.yaml
	apiVersion: app.alauda.io/v1alpha1
	kind: HelmRequest
	metadata:
	  finalizers:
	  - captain.alauda.io
	  generation: 1
	  name: ${REGION_NAME}-alauda-alb2
	  namespace: ${ACP_NAMESPACE}
	spec:
	  chart: stable/alauda-alb2
	  namespace: ${ACP_NAMESPACE}
	  releaseName: ${REGION_NAME}-alauda-alb2
	  clusterName: ${REGION_NAME}
	  valuesFrom:
	  - configMapKeyRef:
	      key: values.yaml
	      name: alb2-config
	      optional: false
	version: $(helm search | grep '^stable/alauda-alb2 ' | awk '{print $2}')
	
	EOF
	kubectl create -f /tmp/region_helmrequest/${REGION_NAME}-alauda-alb2.yaml
	```
	
	1. 检查，在 global 集群的第一台 master 上执行如下命令：
	
		`kubectl get hr -n ${ACP_NAMESPACE} ${REGION_NAME}-alauda-alb2 `
	
		**说明**：如果 PHASE 的状态是 Synced 即说明 captain 安装成功。
	
	2. 在业务集群的第一个 master 上执行如下命令：
	
		`kubectl get pod -n ${ACP_NAMESPACE} | grep alb`
	
		**说明**：如果 pod 的 create 时间是刚刚操作的时间，说明更新 pod 成功。
	
	3. 删除helm 内 release，在业务集群的第一个 master 上执行如下命令：
	
		`kubectl delete cm -n kube-system $(kubectl get cm -n kube-system | awk '/^alauda-alb2 /{print $1}') `
	
		这样就删掉了alauda-alb2 在 helm 中的 chart list，再执行：
	
		`helm list -a | grep alb `
	
		**说明**：找不到 alauda-alb2 就表明升级成功了。 
	

**注意：**升级完毕候，需要修改 alb 的 yaml ，alb 的 yaml 的 nodeselector 要手动的重新加上 `alb2:true` 标签。




###  Tapp 升级

**执行位置**：每一个集群的第一个 master 上

**操作步骤**：执行如下命令：

```
helm upgrade tapp-controller stable/tapp-controller
```

### gitlab-ce 升级

**执行位置**：每一个集群的第一个 master 上

**操作步骤**：执行如下命令：

```
helm upgrade gitlab-ce  stable/gitlab-ce
```

### harbor 升级

**执行位置**：每一个集群的第一个 master 上

**操作步骤**：执行如下命令：

```
helm upgrade harbor stable/harbor
```

### sonarqube 升级

**执行位置**：每一个安装了 sonarqube 的集群的第一个 master 上

**操作步骤**：执行如下命令：

```
## 升级前先将sonarqube 的 deploy 改成0，一般名字叫 sonarqube-sonarqube
kubectl scale deployment -n <ns> <sonarqube name> --replicas=0

## 进到sonarqube-postgresql的容器将所有的数据库转储
pg_dumpall > db.out

## 升级 sonarqube 的 chart 
helm upgrade <sonar name> stable/sonarqube

## 手动修改 sonarqube-postgresql 的 deploy 中存储数据 volumes，如果是用的pvc就新建一个pvc替换，如果是hostpath就从新换一个path
## 将db.out 拷贝到sonarqube-postgresql容器，执行
psql -f db.out postgres

## 待sonarqube-sonarqube起来后验证（建议手动修改sonarqube-sonarqube的健康检查时间）
```

### jenkins 升级

**执行位置**：每一个集群的第一个 master 上

**操作步骤**：执行如下命令：

```
helm upgrade  jenkins stable/jenkins
```
jenkins组件升级后，需要根据最新版本的包，手工更新 jenkins 系统配置里面的镜像， 例如：更新 golang 的镜像。

1. `helm inspect stable/jenkins` 找到 golang12 镜像版本，有以下两个：

 golang12ubuntu:
  repository: alaudaorg/builder-go
  tag: 1.12-ubuntu-v2.6.0
golang12Alpine:
  repository: alaudaorg/builder-go
  tag: 1.12-alpine-v2.6.0

2. 登陆 **jenkins->manage jenkins->configure system** 搜索找到模版名称 golang1.12 然后根据原来镜像 tag 是含有 ubuntu 还是 alpine 更新为相应的镜像 tag。

3. apply 并 save 即可。
	

如果要使用平台多分支流水线支持 gitlab，则需要在 jenkins 配置页面配置 gitlab 相关信息。

1. 登陆 **jenkins->manage jenkins->configure system** 找到 gitlab，点击增加 gitlab 服务。**显示名称** 填写平台集成 gitlab 的集成名称；**服务URL** 填写 gitlab 的地址。<br>凭据单击添加；类型选择 gitlab 个人令牌，令牌输入 gitlab token，id 随便输入；然后选择刚才的 id 凭据，勾选 Web Hook，最后单击测试链接，则会显示链接成功。

2. 搜索 Jenkins Location ，将 jenkins url 由原来的服务发现的地址：`http://jenkins:8080` 改成 jenkins 的访问地址。

4. 单击保存。
	
5. 登录 **jenkins->manage jenkins->configure system**  搜索 java，查看 java 工作空间卷（Workspace Volume）的配置，如果不是 Empty Dir Workspace Volume ，修改成这个。

### 注意事项

#### devops-controller 中的 toolx 下的 yaml 更新问题

如果 Kubernetes 集群中已经存在相应的 tooltype 的资源，toolx 下的 yaml 更新可能不会更新 Kubernetes 集群中已经存在的 resource。

在 v2.3 中，更新的解决方案是：在 Kubernetes 集群中删除想要更新的 tooltype 的 resource 后重启 devops-controller。

示例：更新 name 为 jira 的 tooltype。

1. 执行命令：`kubectl delete tooltype jira`

2. 执行命令重启 devops-controller。

	```
	DC=`kubectl -n ${ACP_NAMESPACE} get pod |grep devops-controller|wc -l` && kubectl -n ${ACP_NAMESPACE} scale deploy/devops-controller --replicas=0 && kubectl -n ${ACP_NAMESPACE} scale deploy/devops-controller --replicas=$DC
	```

## 升级结束之后的检查

### 在所有的集群的第一个 master 上执行如下命令，检查升级版本是否争取：

```
kubectl get hr --all-namespaces | awk '{if ($NF != "Synced")print}'   #检查captain 部署的所有 chart 是否部署成功
helm list --failed ; helm list --pending               # 检查captain 部署的所有 chart 是否部署成功

kubectl get hr --all-namespaces ; helm list -a    # 检查部署的 chart 的版本
```

### 按照测试用例，测试升级之后功能是否正常


## 回滚


**注意：**恢复的顺序是 global 集群 、 业务集群。如果只有业务集群升级失败，可以选择在 global 集群上，删除升级失败的这个集群相关的所有 hr 资源，再回滚升级失败的业务集群，严禁直接回滚业务集群。

1. 获取 etcd 地址，并停掉所有(业务集群和 global 集群） master 节点上的 kubelet 服务。

    **执行命令的环境：** 业务集群和 global 集群的所有 master 节点上。
    
    **执行的命令：** ```ETCD_SERVER=($(kubectl get pod -n kube-system $(kubectl get pod -n kube-system | grep etcd | awk 'NR==1 {print $1}') -o yaml | awk '/--initial-cluster=/{print}' | sed -e 's/,/ /g' -e's/^.*cluster=//' | sed -e 's#[0-9\.]*=https://##g' -e 's/:2380//g')) ； systemctl stop kubelet```

2. 删掉 kube-apiserver 容器，目的是恢复过程和恢复之后，global 不要通过调用 kubeapi 写数据到业务集群的 etcd 内。

    **执行命令的环境：** 要恢复 Kubernetes 集群的所有 master 节点上。
    
    **执行的命令：** ```docker rm -f $(docker ps -a | awk '/_kube-api/{print $NF}')```
    
    **命令的结果：** 所有 kube-api 的容器都被删除。


3.  判断 etcdctl 命令是否存在第一台 master 节点上，一般执行 `backup_recovery.sh` 这个备份 etcd 的脚本，会自动将 etcdctl 拷贝到 `/usr/bin/etcdctl`，如果不存在，需要自行手动拷贝出来。

    **执行命令的环境：** 要恢复 Kubernetes 集群的第一台 master 节点上。
    
    **执行的命令：** ```whereis etcdctl```
    
    **命令的结果：** 应该打印处 etcdctl 的路径，如果没有就表明是错的。

4. 通过备份的快照恢复 etcd。

    **执行命令的环境：** 要恢复 Kubernetes 集群的第一台 master 节点上。
    
    **执行的命令：**
    
    ```
    mkdir /tmp/dd
    echo ${ETCD_SERVER[@]}  ## 检查地址是否获取成功
    snapshot_db=<备份时，导出的 etcd 快照文件名，必须是绝对路径>
    for i in ${ETCD_SERVER[@]}
    do
        export ETCDCTL_API=3
        etcdctl snapshot restore ${snapshot_db} \
        --cert=/etc/kubernetes/pki/etcd/server.crt \
        --key=/etc/kubernetes/pki/etcd/server.key \
        --cacert=/etc/kubernetes/pki/etcd/ca.crt \
        --data-dir=/tmp/dd/etcd \
        --name ${i} \
        --initial-cluster ${ETCD_SERVER[0]}=https://${ETCD_SERVER[0]}:2380,${ETCD_SERVER[1]}=https://${ETCD_SERVER[1]}:2380,${ETCD_SERVER[2]}=https://${ETCD_SERVER[2]}:2380 \
        --initial-advertise-peer-urls https://$i:2380 && \
    mv /tmp/dd/etcd etcd_$i
    done
    ```
    
    **命令的结果：** 会生成 `etcd_<ip 地址>` 这样的三个目录，将这三个目录拷贝到对应ip的服务器的 `/root` 内。

5. 删掉 etcd 容器。

    **执行命令的环境：** 要恢复 Kubernetes 集群的所有 master 节点上。
    
    **执行的命令：** ```docker rm -f $(docker ps -a | awk '/_etcd/{print $NF}')```
    
    **命令的结果：** 所有 etcd 的容器都被删除。

6. 迁移恢复的数据。

    **执行命令的环境：** 要恢复 Kubernetes 集群的所有 master 节点上。
    
    **执行的命令：**
    
    ```
    docker ps -a | awk '/_etcd/{print $NF}' ##确保没有 etcd 容器
    mv /var/lib/etcd/member /cpaas/backup
    mv /root/var/lib/etcd/member  /var/lib/etcd
    ```
    
    **命令的结果：** 会把 etcd 的数据挪到备份目录下，然后将上一步生成的目录拷贝到 `/var/lib/etcd` 里。

7.  启动 etcd 和 kube-api。

    **执行命令的环境：** 要恢复 Kubernetes 集群的所有 master 节点上。
    
    **执行的命令：** `systemctl start kubelet`
    
    **命令的结果：** kubelet 服务启动后，会自动创建 etcd 的 pod，这个时候执行 `docker ps -a | grep -E 'etcd|kube-api'` 会找到 etcd 和 kube-api 容器。

8.  回滚之后，如果出现在kubelet和页面查看所有资源都存在，但是在业务节点没有资源的情况，重启k8s 集群内所有节点的 kubelet 和 docker 服务，也可采用重启集群内所有服务器的方式来解决。