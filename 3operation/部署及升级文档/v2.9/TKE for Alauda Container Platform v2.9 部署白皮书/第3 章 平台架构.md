# 架构图

## 平台部署架构图 - all in one

![部署方法](images/部署架构-allinone.png)

## 平台部署架构图 - poc（不提供 LB）

![部署方法](images/部署架构-poc-nolb.png)

## 平台部署架构图 - poc（提供 LB）

![部署方法](images/部署架构-poc-lb.png)

## 平台部署架构图 - 正式生产环境（6节点）

![部署方法](images/正式生产环境部署方案-6.png)

## 平台部署架构图 - 正式生产环境（9节点）

![部署方法](images/正式生产环境部署方案-9.png)

# 平台架构（内部网络文档，不阅读不影响部署）

访问地址：http://confluence.alauda.cn/pages/viewpage.action?pageId=61902720

**说明**：仅公司内网可访问。