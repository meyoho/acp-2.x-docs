# 第一部分  前言

本文档介绍了2.9 版本的平台小版本之间的升级，比如从2.9.0 升级到2.9.1 或者从2.9.1 升级至2.9.3

## 读者对象

本文档适用于具备基本的 linux、容器、Kubernetes 知识，想要安装和配置平台的用户。包括：

* 实施顾问和平台管理员
* 对平台进行维护的运维人员
* 负责整个项目生命周期的项目经理

## 文档目的

平台运维工程师依据本文档升级平台版本。


## 修订记录

| 文档版本 | 发布日期 | 修订内容 |
| ----- | ----- | ---- |
| v1.0 | 2020-01-06| 第一版|

## **警告**

在升级过程中，严禁任何对 chart 删除的操作，包括 `helm delete`  和 `kubectl delete hr` 这两种直接删除 chart 资源的操作，也禁止 通过 `kubectl delete -f xxx.yaml` 删除 yaml 文件中定义的 hr 资源的操作。平台所有的数据都存储在 chart 定义的 crd 中，删除 chart 之后数据就丢失了，且无法找回，只能回滚。


<div STYLE="page-break-after: always;"></div>

# 第二部分  升级过程

## 升级前准备

### 备份数据

请参考 2.8 升级至 2.9 的文档中，升级部分

## 平台升级

### 下载最新的 v2.9 的安装包

* 目的： 下载安装包
* 执行位置：init 节点
* 操作步骤：参考最新的部署文档第三章，下载安装包并解压缩

### 上传 v2.9 的镜像和 chart 到 init 节点的私有仓库和 chart repo 内

* 目的：上传最新的 2.9 的镜像和 chart 到 init 节点的镜像仓库和 chart repo
*  执行位置：init 节点
* 操作步骤：进入最新的 v2.9 的安装目录，执行命令 `bash ./upload-images-chart.sh`
* **注意**：执行命令过程需要一个小时左右，视 init 服务器 cpu 和硬盘速度不同，会缩短或延迟，注意安装目录所在分区的空间，上传之前至少要保证 10G 的空余空间
*  执行完毕检查：执行 ` helm up ; helm searcn` 检查 chart 版本，执行 `docker images` 查看镜像，并尝试选择一个镜像 pull 一下


### 升级 captain 

* 目的： 升级 captain 到最新的版本
* 执行位置：init 节点
* 操作步骤：进入最新的 v2.9 的安装目录，执行命令 ：

```
ACP_NAMESPACE=<cpaas-system>   # 平台的 ns ，默认是 cpaas-system

. /cpaas/install_info
rm -rf /tmp/captain-deploy.yaml ; cp ./captain-deploy.yaml  /tmp/captain-deploy.yaml
sed -i -e "s/captain-system/${ACP_NAMESPACE}/g" -e "s/index.alauda.cn/${REGISTRY_ENDPOINT}/g" /tmp/captain-deploy.yaml
kubectl apply --validate=false -n ${ACP_NAMESPACE} -f /tmp/captain-deploy.yaml
```

*  执行完毕检查： 执行 `kubectl get pod --all-namespaces | grep capta` 检查 pod 创建时间，检查 pod 使用的镜像版本


### 升级 sentry 

* 目的： 升级 sentry 到最新的版本
* 执行位置：init 节点
* 操作步骤：执行命令 `helm upgrade sentry stable/sentry`
* 执行完毕检查： 执行 `helm list -a ` 检查 sentry 版本和状态


### 更新平台 chart

* 目的：升级平台所有的 chart 到最新的版本
*  执行位置：init 节点上
* 命令：如下

```
for i in $(helm list | sed 1d | awk '{print $1}') ; do helm upgrade $i stable/$i --timeout=3000 --force >>/cpaas/upgrade_chart.log && echo "upgrade $i success" || echo "upgrade $i error" ; sleep 5 ; done

kubectl edit cm -n <cpaas-system> sentry-cm # 将这个 configmap 中 ，appreleases.config 字段下的 chart 的版本改成最新的2.9.x 的版本（具体版本，通过 helm search 命令来查看）就可以了
```

* 执行完毕检查： 执行 `helm list `  执行 `kubectl describe apprelease -n <cpaas-system> -o yaml`  确认 chart 的 version 是否已经更新。

### 更新业务集群组件的 chart

* 目的：升级业务集群所有的 chart 到最新的版本
*  执行位置：在业务集群的第一个 master 节点上 和 init 节点上分别执行
* 命令：**在业务集群的第一台 master 上执行**

```
helm up

for i in $(helm list | sed 1d | awk '{print $1}') ; do helm upgrade $i stable/$i --timeout=3000 --force >>/cpaas/upgrade_chart.log && echo "upgrade $i success" || echo "upgrade $i error" ; sleep 5 ; done

kubectl edit cm -n <cpaas-system> sentry-cm # 将这个 configmap 中 ，appreleases.config 字段下的 chart 的版本改成最新的2.9.x 的版本（具体版本，通过 helm search 命令来查看）就可以了
```

* 执行完毕检查： 执行 `helm list ` 执行 `kubectl describe apprelease -n <cpaas-system> -o yaml` 确认 chart 的 version 是否已经更新
* 命令：**在 init 节点上执行**

```
helm up
kubectl get hr -n <cpaas-system>                  ## 获取所有 helmrequest 资源，然后执行
kubectl edit hr -n <cpaas-system> <hr-name>       ## 编辑 hr 资源，修改资源中，chart version 的值为最新的 2.9.x 的版本（具体版本，通过 helm search 命令来查看）就可以了
```

* 执行完毕检查： 执行 `kubectl get hr -n <cpaas-system>`  确认 chart 的 version 是否已经更新

## 升级结束之后的检查

### 按照测试用例，测试升级之后功能是否正常


## 回滚

**注意：**恢复的顺序是 global 集群 、 业务集群。如果只有业务集群升级失败，可以选择在 global 集群上，删除升级失败的这个集群相关的所有 hr 资源，再回滚升级失败的业务集群，严禁直接回滚业务集群。

1. 获取 etcd 地址，并停掉所有(业务集群和 global 集群） master 节点上的 kubelet 服务。

    **执行命令的环境：** 业务集群和 global 集群的所有 master 节点上
    
    **执行命令的命令：** 
    
```
ETCD_SERVER=($(kubectl get pod -n kube-system $(kubectl get pod -n kube-system | grep etcd | awk 'NR==1 {print $1}') -o yaml | awk '/--initial-cluster=/{print}' | sed -e 's/,/ /g' -e's/^.*cluster=//' | sed -e 's#[0-9\.]*=https://##g' -e 's/:2380//g')) ； systemctl stop kubelet
```

2. 删掉 kube-apiserver 容器，目的是恢复过程和恢复之后，global 不要通过调用 kubeapi 写数据到业务集群的 etcd 内。

    **执行命令的环境：** 要恢复 Kubernetes 集群的所有 master 节点上
    
    **执行命令的命令：** `docker rm -f $(docker ps -a | awk '/_kube-api/{print $NF}')`
    
    **命令的结果：** 所有 kube-api 的容器都被删除。


3.  判断 etcdctl 命令是否存在第一台 master 节点上，一般执行命令 `backup_recovery.sh` 这个备份 etcd 的脚本，会自动将 etcdctl 拷贝到 `/usr/bin/etcdctl`，如果不存在，需要自行手动拷贝出来。

    **执行命令的环境：** 要恢复 Kubernetes 集群的第一台 master 节点上
    
    **执行命令的命令：** `whereis etcdctl`
    
    **命令的结果：** 应该打印处 etcdctl 的路径，如果没有就表明是错的。

4. 通过备份的快照恢复 etcd。

    **执行命令的环境：** 要恢复 Kubernetes 集群的第一台 master 节点上
    
    **执行命令的命令：**
    
```
mkdir /tmp/dd
echo ${ETCD_SERVER[@]}  ## 检查地址是否获取成功
snapshot_db=<备份时，导出的 etcd 快照文件名，必须是绝对路径>
for i in ${ETCD_SERVER[@]}
do
    export ETCDCTL_API=3
    etcdctl snapshot restore ${snapshot_db} \
    --cert=/etc/kubernetes/pki/etcd/server.crt \
    --key=/etc/kubernetes/pki/etcd/server.key \
    --cacert=/etc/kubernetes/pki/etcd/ca.crt \
    --data-dir=/tmp/dd/etcd \
    --name ${i} \
    --initial-cluster ${ETCD_SERVER[0]}=https://${ETCD_SERVER[0]}:2380,${ETCD_SERVER[1]}=https://${ETCD_SERVER[1]}:2380,${ETCD_SERVER[2]}=https://${ETCD_SERVER[2]}:2380 \
    --initial-advertise-peer-urls https://$i:2380 && \
mv /tmp/dd/etcd etcd_$i
done
```
    
    **命令的结果：** 会生成 `etcd_<ip 地址>` 这样的三个目录，将这三个目录拷贝到对应 ip 的服务器的 `/root` 内。

5. 删掉 etcd 容器。

    **执行命令的环境：** 要恢复 Kubernetes 集群的所有 master 节点上
    
    **执行命令的命令：** `docker rm -f $(docker ps -a | awk '/_etcd/{print $NF}')`
    
    **命令的结果：** 所有 etcd 的容器都被删除。

6. 迁移恢复的数据。

    **执行命令的环境：** 要恢复 Kubernetes 集群的所有 master 节点上
    
    **执行命令的命令：**
    
```
docker ps -a | awk '/_etcd/{print $NF}' ##确保没有 etcd 容器
mv /var/lib/etcd/member /cpaas/backup
mv /root/var/lib/etcd/member  /var/lib/etcd
```
    
    **命令的结果：** 会把 etcd 的数据挪到备份目录下，然后将上一步生成的目录拷贝到 `/var/lib/etcd` 里。

7.  启动 etcd 和 kube-api。

    **执行命令的环境：** 要恢复 Kubernetes 集群的所有 master 节点上
    
    **执行命令的命令：** `systemctl start kubelet`
    
    **命令的结果：** kubelet 服务启动后，会自动创建 etcd 的 pod，这个时候执行命令 `docker ps -a | grep -E 'etcd|kube-api'` 会找到 etcd 和 kube-api 容器。

8.  回滚之后，如果出现在kubelet和页面查看所有资源都存在，但是在业务节点没有资源的情况，重启k8s 集群内所有节点的 kubelet 和 docker 服务，也可采用重启集群内所有服务器的方式来解决。
