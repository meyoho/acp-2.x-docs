# 说明

*  容器平台的 gpu 节点和 ai 、 TI-matrix 的 gpu 驱动的安装，请参考本文档
*  下面相关操作涉及的 rpm 文件、压缩包等，都在 安装目录下的 other/gpu 目录里
*  暂时只支持 centos 7.6 下测试通过，centos 7.x 的其他版本未经充分测试，只有理论上安装的可能

# 安装驱动

1.  以 p40 这个型号的 GPU 为例，其他型号的 gpu 驱动，请参考本文档最下面的附记。安装目录下的 other/gpu/NVIDIA-Linux-x86_64-440.64.00.run  即是 p40 的驱动，拷贝到需要安装 gpu 驱动的服务器上执行：

**注意**：安装 *.run 类型的驱动依赖：gcc、kernel-devel 请自行解决

```
chmod a+x NVIDIA-Linux-x86_64-440.64.00.run
./NVIDIA-Linux-x86_64-440.64.00.run
```

一路回车安装

2. 验证驱动是否安装成功，执行 `nvidia-smi `  会返回如下信息：

```
[root@VM_0_4_centos gpu]# nvidia-smi
Wed Jan  8 16:53:07 2020      
+-----------------------------------------------------------------------------+
| NVIDIA-SMI 440.33.01    Driver Version: 440.33.01    CUDA Version: 10.2     |
|-------------------------------+----------------------+----------------------+
| GPU  Name        Persistence-M| Bus-Id        Disp.A | Volatile Uncorr. ECC |
| Fan  Temp  Perf  Pwr:Usage/Cap|         Memory-Usage | GPU-Util  Compute M. |
|===============================+======================+======================|
|   0  Tesla V100-SXM2...  Off  | 00000000:00:06.0 Off |                    0 |
| N/A   33C    P0    38W / 300W |  31014MiB / 32510MiB |      1%      Default |
+-------------------------------+----------------------+----------------------+
                                                                                
+-----------------------------------------------------------------------------+
| Processes:                                                       GPU Memory |
|  GPU       PID   Type   Process name                             Usage      |
|=============================================================================||
+-----------------------------------------------------------------------------+
```


3. 创建集群，详细参考用户手册，并保证勾选 gpu 和 gpu 主机，如图

![](images/gpu-创建集群.png)

**注意**：非 2.9 版本的平台需要 手动安装 nvidia 的 docker runtime，将 init 节点上，安装目录里 `other/gpu/nvidia-container` 下的四个 rpm 包拷贝到 gpu 节点上，执行 `yum localinstall -y ./*.rpm`

4.  验证 docker 配置，在 2.9 版本的平台上，按照用户手册创建 gup 集群，默认会更改 docker 配置，请检查 gpu 集群的所有节点的 /etc/docker/daemon.json 配置，确保如下：

```
{ 
   ......
  "default-runtime": "nvidia",
  "runtimes": {
      "nvidia": {
            "path": "/usr/bin/nvidia-container-runtime",
            "runtimeArgs": []
          }
   }
   ......
}
```



# 在 gpu 集群创建 cm 等资源

## vgpu 集群

**注意**： 如果创建集群的时候，在 UI 页面上选择的是 vgpu


1.  安装 gpu-manager ，在 UI 上操作，如图：

![](images/gpu-安装gpumanager.png)

2. 然后创建两个 cm 
  - 描述 tencent.com/vcuda-core

```
base_label=<cpaas.io>  # 2.3 之后默认都是 cpaas.io 了，如果平台是从以前版本升级上来的，这个值可能是 alauda.io。具体可以看一下 alauda-base 这个 chart 的变量，和 global.labelBaseDomain 变量的值保持一致
apiVersion: v1
kind: ConfigMap
data:
  key: "tencent.com/vcuda-core"  #指定键的名称
  dataType: "integer"            #指定Value类型
  defaultValue: "20"             #默认值, 如果不存在或者为空，则没有默认值
  descriptionZh: "虚拟 GPU 核心数，100个虚拟核心等于 1 个 GPU 物理核心"     #中文描述信息
  descriptionEn: "GPU vcore count，100 vcore equals 1 GPU core "      #英文描述信息
  group: "gpu-manager"           #组别
  limits: "optional"             #这个资源是否出现在limits里, 可能的值: disabled 禁用/required 必须输入/optional 可选输入
  requests: "disabled"           #这个资源是否出现在requests里，可能的值: disabled 禁用/required 必须输入/optional 可选输入/fromLimits 跟limits相同
  
metadata:
  name: cf-crl-gpu-manager-vcuda-core
  labels:
    features.${base_label}/type: CustomResourceLimitation
    features.${base_label}/group: gpu-manager
    features.${base_label}/enabled: "true"
  namespace: kube-public
```

  - 描述 tencent.com/vcuda-memory

```
apiVersion: v1
kind: ConfigMap
data:
  key: "tencent.com/vcuda-memory"  #指定键的名称
  dataType: "integer"            #指定Value类型
  defaultValue: "20"             #默认值, 如果不存在或者为空，则没有默认值
  descriptionZh: "虚拟 GPU 显存，每单位等于 256Mi 显存"     #中文描述信息
  descriptionEn: "GPU vmemory, 256 Mi physical memory per Unit"      #英文描述信息
  group: "gpu-manager"           #组别
  limits: "optional"             #这个资源是否出现在limits里, 可能的值: disabled 禁用/required 必须输入/optional 可选输入
  requests: "disabled"           #这个资源是否出现在requests里，可能的值: disabled 禁用/required 必须输入/optional 可选输入/fromLimits 跟limits相同
  
metadata:
  name: cf-crl-gpu-manager-vcuda-memory
  labels:
    features.${base_label}/type: CustomResourceLimitation
    features.${base_label}/group: gpu-manager
    features.${base_label}/enabled: "true"
  namespace: kube-public
```


## pgpu 集群

**注意**： 如果创建集群的时候，在 UI 页面上选择的是 pgpu

1. 如果有，删除 GPU-manager 的 configmap

`kubectl -n kube-public delete  cm  cf-crl-gpu-manager-vcuda-core cf-crl-gpu-manager-vcuda-memory`

2. 创建另外的 cm

```
base_label=<cpaas.io>  # 2.3 之后默认都是 cpaas.io 了，如果平台是从以前版本升级上来的，这个值可能是 alauda.io。具体可以看一下 alauda-base 这个 chart 的变量，和 global.labelBaseDomain 变量的值保持一致

apiVersion: v1
kind: ConfigMap
data:
  key: "nvidia.com/gpu"                 #指定键的名称
  dataType: "integer"                   #指定Value类型
  defaultValue: "1"                     #默认值, 如果不存在或者为空，则没有默认值
  descriptionZh: "GPU 数量"              #中文描述信息
  descriptionEn: "GPU quantity，"       #英文描述信息
  group: "nvidia-device-plugin"         #组别
  limits: "optional"                    #这个资源是否出现在limits里, 可能的值: disabled 禁用/required 必须输入/optional 可选输入
  requests: "disabled"                  #这个资源是否出现在requests里，可能的值: disabled 禁用/required 必须输入/optional 可选输入/fromLimits 跟limits相同
   
metadata:
  name: cf-crl-nvidia-device-plugin
  labels:
    features.${base_label}/type: CustomResourceLimitation
    features.${base_label}/group: nvidia-device-plugin
    features.${base_label}/enabled: "true"
  namespace: kube-public
```

3. 安装 nvidia-device-plugin

  - 3.1. 创建 nvidia-device-plugin.yml 文件

```
REGISTRY_ENDPOINT=$(docker info |grep 60080  |tr -d ' ')

apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: nvidia-device-plugin-daemonset
  namespace: kube-system
spec:
  updateStrategy:
    type: RollingUpdate
  template:
    metadata:
      # Mark this pod as a critical add-on; when enabled, the critical add-on scheduler
      # reserves resources for critical add-on pods so that they can be rescheduled after
      # a failure.  This annotation works in tandem with the toleration below.
      annotations:
        scheduler.alpha.kubernetes.io/critical-pod: ""
      labels:
        name: nvidia-device-plugin-ds
    spec:
      tolerations:
      # Allow this pod to be rescheduled while the node is in "critical add-ons only" mode.
      # This, along with the annotation above marks this pod as a critical add-on.
      - key: CriticalAddonsOnly
        operator: Exists
      - key: nvidia.com/gpu
        operator: Exists
        effect: NoSchedule
      containers:
      - image: ${REGISTRY_ENDPOINT}/alaudaorg/k8s-device-plugin:1.0.0-beta
        name: nvidia-device-plugin-ctr
        securityContext:
          allowPrivilegeEscalation: false
          capabilities:
            drop: ["ALL"]
        volumeMounts:
          - name: device-plugin
            mountPath: /var/lib/kubelet/device-plugins
      volumes:
        - name: device-plugin
          hostPath:
            path: /var/lib/kubelet/device-plugins
```

  - 3.2. 创建的 nvidia-device-plugin daemonset 到kube-system 下 

```
kubectl create -f nvidia-device-plugin.yml
# 查看ds 状态
kubectl -n kube-system get pods   |grep nvidia
nvidia-device-plugin-daemonset-5wdcx   1/1     Running   0          75m
nvidia-device-plugin-daemonset-fd6jd   1/1     Running   0          75m
```

# 测试

## 创建测试 deployment

```
REGISTRY_ENDPOINT=$(docker info |grep 60080  |tr -d ' ')

apiVersion: apps/v1
kind: Deployment
metadata:
  name: gpu-test-nvidia
spec:
  replicas: 2
  selector:
    matchLabels:
      app: gpu-test-nvidia
      version: v1
  template:
    metadata:
      labels:
        app: gpu-test-nvidia
        version: v1
    spec:
      containers:
        - image: '${REGISTRY_ENDPOINT}/alaudaorg/gpu-test:latest'
          imagePullPolicy: IfNotPresent
          name: gpu
          resources:
            limits:
              cpu: '2'
              memory: 4Gi
              nvidia.com/gpu: 1
```

## Pod 启动后会有大量日志输出，且在 GPU Node 上运行 nvidia-smi 会有python 进程大量占用 GPU

```
[root@VM_0_4_centos gpu]# nvidia-smi
Wed Jan  8 17:48:20 2020      
+-----------------------------------------------------------------------------+
| NVIDIA-SMI 440.33.01    Driver Version: 440.33.01    CUDA Version: 10.2     |
|-------------------------------+----------------------+----------------------+
| GPU  Name        Persistence-M| Bus-Id        Disp.A | Volatile Uncorr. ECC |
| Fan  Temp  Perf  Pwr:Usage/Cap|         Memory-Usage | GPU-Util  Compute M. |
|===============================+======================+======================|
|   0  Tesla V100-SXM2...  Off  | 00000000:00:06.0 Off |                    0 |
| N/A   33C    P0    38W / 300W |  31014MiB / 32510MiB |      1%      Default |
+-------------------------------+----------------------+----------------------+
                                                                                
+-----------------------------------------------------------------------------+
| Processes:                                                       GPU Memory |
|  GPU       PID   Type   Process name                             Usage      |
|=============================================================================|
|    0     25465      C   python                                     31003MiB |
+-----------------------------------------------------------------------------+

```

# 附记，其他型号的 GPU 驱动下载方式

1.  查看GPU型号，登录GPU Node查看 GPU型号，如图GPU型号为：Tesla V100 SXM2

```
[root@VM_0_4_centos ~]# lspci |grep -i NVIDIA
00:06.0 3D controller: NVIDIA Corporation GV100GL [Tesla V100 SXM2 32GB] (rev a1)
```

2. 下载驱动：前往：nvidia官网 

  - 填写GPU型号信息：

![](images/gpu-download1.png)

 - 点击搜索后：

![](images/gpu-download2.png)

  - 点击DOWNLOAD后 Copy下载链接：

![](images/gpu-download3.png)

  - 得到下载链接，在GPU Node上创建 /home/gpu 目录并使用wget下载驱动文件保存到 /home/gpu

```
[root@VM_0_4_centos ~]# mkdir -p /home/gpu
[root@VM_0_4_centos ~]# cd /home/gpu/
[root@VM_0_4_centos ~]# wget http://us.download.nvidia.com/tesla/440.33.01/NVIDIA-Linux-x86_64-440.33.01.run
[root@VM_0_4_centos ~]# ls NVIDIA-Linux-x86_64-440.33.01.run
NVIDIA-Linux-x86_64-440.33.01.run
```
