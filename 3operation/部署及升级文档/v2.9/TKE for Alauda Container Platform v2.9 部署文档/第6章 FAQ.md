# FAQ

## 平台部署错误

### 安装 chart 失败

**错误处理流程**：参考运维手册处理 helm 和 sentry 部署 chart 失败的情况。

## 平台功能错误

请参考《TKE 企业版 Container Platfrom 运维手册》。