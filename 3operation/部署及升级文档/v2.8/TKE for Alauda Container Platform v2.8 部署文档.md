# 第一部分  前言

本文档介绍了如何安装和配置平台。

## 读者对象

本文档适用于具备基本的 linux、网络、存储、容器、Kubernetes 知识，想要安装和配置平台的用户，包括：

* 实施顾问和平台管理员

* 规划平台架构的售前工程师

* 负责整个项目生命周期的项目经理

## 文档目的

* 实施工程师依据本文档部署平台。

* 实施工程师依据本文档，配置平台参数。

* 平台管理员依据本文档测试平台功能。

## 修订记录

| 文档版本 | 发布日期   | 修订内容                                                     |
| -------- | ---------- | ------------------------------------------------------------ |
| v1.0| 2020-02-21 | 第一版，适用于 TKE for Alauda Container Platform 私有部署版本 v2.8。 |
| v1.1 | 2020-03-05 | 第二版，fix 错误，增加了无法安装 docker 的补救方法。|
| v1.2 | 2020-03-05 | 第三版，更新安装包信息。|
| v1.4 | 2020-03-05 | 更新 amp 安装包信息。|





<div STYLE="page-break-after: always;"></div>
# 第二部分  部署和配置简介

平台有多种部署方案，在规划平台架构之前，请参考《TKE for Alauda Container Platform v2.8 部署白皮书》中部署方案部分。

平台由 global 、global 插件、业务服务集群和集群组件构成，global 是平台的核心，管理、运维功能由 global 提供。global 提供了丰富的插件来扩展 global 的功能，详细内容请参考《TKE for Alauda Container Platform v2.8 部署白皮书》。

业务服务集群是 global 管理的运行客户业务服务的 Kubernetes 集群，集群组件是运行在客户业务服务集群之上的功能模块，提供平台功能、搜集相关数据。

平台可以部署在物理机和虚拟机上，服务器的数量、配置要求等信息请参考《TKE for Alauda Container Platform v2.8 部署白皮书》中硬件需求部分。

平台的部署需要域名、vip、ip 范围、lb 等网络资源，网络资源的具体需求请参考《TKE for Alauda Container Platform v2.8 部署白皮书》中网络需求部分。

平台支持在 centos  操作系统上部署，操作系统版本和配置请参考本文档中软件需求部分。

平台中的 global 、global 插件、业务服务器集群、业务服务集群插件的部署请看本文第四部分。

<div STYLE="page-break-after: always;"></div>
# 第三部分  部署过程概述

平台是一款复杂的产品，具有多个需要安装和配置的插件、组件，为确保部署成功，请了解部署架构图和部署步骤的流程顺序图。
## 安装包信息及下载链接

### 安装包下载信息

|下载链接 |安装包大小| | md5| 备注|
|:--| :---:| :---:| :---:| ---  |
|ftp://ftp.alauda.cn:2121/tke-for-alauda/2.8/cpaas-20200305.tgz |23618222459 |22G |2f72e1eb18ca920f3a127d017704bf59 |主安装包，包括 container-platform 和 devops |
|ftp://ftp.alauda.cn:2121/tke-for-alauda/2.8/cpaas-aml-20200305.tgz | 3947008599 |3.5G |c7a83be1bfa3b8c756f1eeb074caba53| 机器学习平台Machine Learning 的安装包|
|ftp://ftp.alauda.cn:2121/tke-for-alauda/2.8/cpaas-amp-20200315.tgz |751227396| 700M|5ec8d85bdbc5416d80f0dc2466818589| API管理平台 API Management Platform 的安装包|
|ftp://ftp.alauda.cn:2121/tke-for-alauda/2.8/cpaas-asm-20200305.tgz |1379655357| 1G| 09157bc8411a761a4583692a7eaffb8c| 微服务治理平台Service Mesh的安装包|
|ftp://ftp.alauda.cn:2121/alauda-acp/aml-images-package-201911241244/cpaas-aml-demo_images-20191124.tgz|388610646|300M|c4d29780b65ff1b641b4da9d7413ec34|机器学习 demo 的镜像|
|ftp://ftp.alauda.cn:2121/alauda-acp/aml-images-package-201911241244/cpaas-aml-full_images-20191124.tgz|17214941481|17G|fa7be19f3495edb3a027e173e328fd71|更多的机器学习镜像|

### 主安装包之外的独立产品安装包或附加镜像包使用方法

* 在平台部署的时候，部署参数 `--enabled-features` 中添加  asm（微服务治理平台）、aml（机器学习平台）、amp（API 管理平台 等独立产品，可以正常部署这些独立产品，但是pod 会因为找不到镜像而无法启动。等平台部署完毕后，按照下面的方法上传独立产品（例如 amp 的安装包 <cpaas-amp-20200109.tgz> ）的镜像到 init 节点的镜像仓库，再稍等片刻，amp 的 pod 就可以自动 pull 下镜像启动成功了。

* 如果在平台部署成功之后，想要部署独立产品，需要下载相应的安装包到平台的 init 节点，并按照下面的方法 push 镜像到 init 节点的镜像仓库之后，按照部署文档部署即可。

**上传独立产品镜像的方法**：

```
rm -rf /tmp/cpaas-package-tmp ; mkdir /tmp/cpaas-package-tmp
tar zxvf <独立产品或附加镜像包的名字，例如cpaas-amp-20191204.tgz> -C /tmp/cpaas-package-tmp
cd /tmp/cpaas-package-tmp/cpaas ; bash ./upload-images-chart.sh
```
上传成功后，`/tmp/cpaas-package-tmp`目录可以删掉。

**注意：** 因为某些独立产品或单独镜像包比较大，push 过程还要将所有独立产品的镜像 pull 到 init 节点再 push，会占用大量空间，请保证平台安装目录、docker 使用的存储空间有足够容量。



## 平台逻辑架构图

请参考《TKE for Alauda Container Platform v2.8 部署白皮书》。

## 平台部署架构图

请参考《TKE for Alauda Container Platform v2.8 部署白皮书》。

## 平台的部署流程图

<img src="images/部署步骤.png" style="zoom:50%;" />

<div STYLE="page-break-after: always;"></div>
# 第四部分 开始部署

## 检查部署需求是否满足

### 软件需求

请参考《TKE for Alauda Container Platform v2.8 部署白皮书》。

### 硬件需求

请参考《TKE for Alauda Container Platform v2.8 部署白皮书》。

### 网络需求

请参考《TKE for Alauda Container Platform v2.8 部署白皮书》。

## 解决不满足的需求


**注意**：因环境差异，kaldre 目录做本地源不会 100% 安装软件成功，此时，就需要自行配置软件源去安装。

### 软件需求

|需求项|具体要求|不满足后的解决办法|
| ------------- | --------------- | ------------- |
| 操作系统| centos 7.7 |7.x 的其他版本也可以安装部署，但需要升级 kernel (ovn 要求) 和手动安装 docker|
| kernel 版本  | 内核大于等于3.10.0-1062.el7.x86_64|必须满足|
| 操作系统安装要求|最小安装|如果不满足，可能出现配置不同造成部署失败|
| 工具软件  |curl tar ip ssh sshpass jq netstat timedatectl ntpdate nslookup base64 tr head openssl md5sum|手动添加源，并安装软件，请看表格后面的操作方式|
|用户权限|root|具备sudo 权限的用户也可部署，但有一定几率部署失败|
|swap|关闭|必须关闭|
|防火墙|关闭|必须关闭|
|selinux|关闭|必须关闭|
|时间同步|所有服务器要求时间必须同步，误差不得超过 2 秒|必须同步|
|时区|所有服务器时区必须统一|必须统一|
|/etc/sysctl.conf| `vm.max_map_count=262144   net.ipv4.ip_forward = 1` |必须满足|
|hostname 格式|字母开头，只能是字母、数字和短横线-组成，不能用短横线结尾，长度在 4-23 之间。|必须满足|
|/etc/hosts|所有服务器可以通过hostname 解析成 ip，可以将localhost解析成127.0.0.1，hosts 文件内，不能有重复的 hostname|必须满足|
|/tmp/ 权限| 要求 `/tmp` 目录的权限是 `777`|必须满足|

#### 解决无法通过 init 节点启动的软件源安装 docker 的步骤

1.  解压缩安装包。

2. 将安装包内的 /other/docker.tgz 压缩包解压缩到/usr/bin 下，例如：
`tar xvaf "<安装目录>/other/docker.tgz" -C /usr/bin --strip-components=1`。

3. 拷贝包内的 `other/docker.service` 到 `/etc/systemd/system`，并执行 `systemctl daemon-reload`。

4. 创建 `/etc/docker/daemon.json` 文件 ，内容如下：

	```
	mkdir /etc/docker/
	cat <<EOF >/etc/docker/daemon.json
	{
	    "insecure-registries": [
	        "$<init 节点的 ip>:60080"
	    ],
	    "log-opts": {
	      "max-size": "100m",
	      "max-file": "1"
	    },
	    "storage-driver": "overlay2"
	}
	```

5. 启动 docker daemon ，执行 `systemctl enable docker ; systemctl start docker`。


#### 解决工具软件不满足的步骤

1. 解压缩安装包

2. 将安装包内的 kaldr 目录，拷贝到每一台服务器的 `/tmp` 目录下。

	在每一台服务器上执行如下命令，添加软件源。

	* 适用 于 Centos ：

		```
		mkdir /etc/yum.repos.d/old
		mv /etc/yum.repos.d/*.repo /etc/yum.repos.d/old
		cat <<EOF >/etc/yum.repos.d/alauda.repo 
		[alauda]
		name=Alauda
		baseurl=file:///tmp/kaldr/yum/
		enabled=1
		gpgcheck=0
		EOF
		yum clean all && yum makecache
		```

	* 适用于 Ubuntu：

		```
		mv /etc/apt/sources.list /etc/apt/sources.list.old
		echo "deb [arch=amd64] file:///tmp/kaldr/apt/ ubuntu-xenial main" >/etc/apt/sources.list
		add-apt-repository "deb [arch=amd64] file:///tmp/kaldr/apt/ ubuntu-xenial main"
		cat /tmp/kaldr/apt/alauda.key | apt-key add -
		apt-get update
		```
	
	**补充说明**：可通过以下命令行，确认某个软件属于哪个包。
	
	* 适用 于 Centos：`yum provides <程序名>`
	
	* 适用于 Ubuntu ：`apt-file search <程序名>`
	
3. 安装软件包，参考以下命令行。

	* 适用 于 Centos：`yum install -y <上一步获取到的软件包的名字>`
  
	* 适用于 Ubuntu ：`apt-get install -y <上一步获取到的软件包的名字>`

### 硬件需求

**注意**：这是实施部署的前提条件，必须满足硬件需求，不满足硬件需求就无法保证正常部署，或部署成功的平台无法长时间稳定运行。

### 网络需求

**注意**：这是实施部署的前提条件，必须满足网络需求，不满足网络需求就无法保证正常部署，或部署成功的平台无法长时间稳定运行。

## 和 v2.6 相比部署差异

### 改变的参数

`--enabled-features`：去掉了 tke，只要部署 Container Platform，默认就必须用 tke 管理集群。去掉了 AMP（API 管理平台），sentry 不支持部署 AMP.


### 其他改变

* 除了 sentry 用 helm 安装外 ，剩下所有组件都是通过 sentry 部署的，sentry 使用及运维，请参考 v2.8 运维手册。

* init 节点安装 docker 的方式改变。


## global 平台部署

### server_list.json 文件格式说明

```
"server_role": {         #服务器角色
  "master":true,         #有这个键值，代表这台服务器角色是 k8s 的 master
  "global":true,         #有这个键值，代表这是运行 global 组件的服务器
  "log":true             #有这个键值，代表这是运行日志组件的服务器
},
"ip_addr": "1.1.1.1",    #服务器 ip 地址
"ssh_port": "22",        #ssh 端口
"ssh_user": "root",      #ssh 用户名
"ssh_passwd": "",        #ssh 密码
"ssh_key_file": "/root/.ssh/id_rsa" #ssh 密钥，如果这个值不为空，那么就优先通过密钥登录，就是出现错误也不会选择 password 认证登录。
```

### all in one 方案

部署命令执行的位置：all in one 服务器上

**部署过程**：

1. 解压缩安装包。

2. 进入到解压缩后的目录。

3. 执行以下命令进行部署：

   **部署方式 1**： 部署脚本配置如下参数，部署一个通过 `https://<本机 ip>` 访问的环境：

	```
	bash ./up-cpaas.sh \
	     --network-interface=<网卡名，默认eth0> \
	     --enabled-features=<需要安装那些产品，默认acp,devops,asm，支持acp,devops,asm,aml,ace3> \
	     --single-mode
	```
  
   **部署方式 2**： 部署脚本配置如下参数，部署一个通过 `http://<本机 ip>` 访问的环境：

	```
	bash ./up-cpaas.sh \
	     --network-interface=<网卡名，默认eth0> \
	     --enabled-features=<需要安装那些产品，默认acp,devops,asm，支持acp,devops,asm,aml,ace3> \
	     --use-http \
	     --single-mode
	```
   
   **部署方式 3**： 部署脚本配置如下参数，部署一个通过 `https://<域名>` 访问的环境：

	```
	bash ./up-cpaas.sh \
	     --network-interface=<网卡名，默认eth0> \
	     --enabled-features=<需要安装那些产品，默认acp,devops,asm，支持acp,devops,asm,aml,ace3> \
	     --domain-name=<域名> \
	     --key-file=<域名证书私钥文件> \
	     --cert-file=<域名证书公钥文件> \
	     --single-mode
	```

**注 1**：只有上述例子中的参数经过测试，改变参数的做法很可能造成部署不成功。
**注 2**：所有有默认值的参数，都是可选的，如果部署时不添加这个参数，就使用默认值。
**注 3**：all in one 不支持部署 API 管理平台。

### poc 部署方案

部署命令执行的位置： init 服务器上

**部署过程**：

1. 解压缩安装包。

2. 进入到解压缩后的目录。

3. 在 up-cpaas.sh 脚本所在的目录下创建 server_list.json 文件，示例如下：

	``` 
	 [
	  {
	    "server_role": {
	      "master":true,
	      "global":true,
	      "log":true
	    },
	    "ip_addr": "1.1.1.1",
	    "ssh_port": "22",
	    "ssh_user": "root",
	    "ssh_passwd": "",
	    "ssh_key_file": "/root/.ssh/id_rsa"
	  },
	  {
	    "server_role": {
	      "master":true,
	      "global":true,
	      "log":true
	    },
	    "ip_addr": "2.2.2.2",
	    "ssh_port": "22",
	    "ssh_user": "root",
	    "ssh_passwd": "",
	    "ssh_key_file": "/root/.ssh/id_rsa"
	  },
	  {
	    "server_role": {
	      "master":true,
	      "global":true,
	      "log":true
	    },
	    "ip_addr": "3.3.3.3",
	    "ssh_port": "22",
	    "ssh_user": "root",
	    "ssh_passwd": "",
	    "ssh_key_file": "/root/.ssh/id_rsa"
	  }
	]
	```

4. 执行以下命令进行部署：

	**部署方式 1**：部署一个通过 `https://<域名>` 访问的高可用环境：

	```
	bash ./up-cpaas.sh \
	     --network-interface=<网卡名，默认是eth0> \
	     --cpaas-namespaces=<ns 名，默认是cpaas-system> \
	     --enabled-features=<需要安装那些产品，默认acp,devops,asm，支持acp,devops,asm,aml,ace3> \
	     --global-network-mode=<选择 global 集群使用那种网络，默认是 ovn，可选 flannel 和 calico> \
	     --kube-cluster-subnet<global 集群的cluster ip 范围，默认是10.96.0.0/12，掩码建议不要小于16位> \
	     --domain-name=<域名> \
	     --kube_controlplane_endpoint=<k8s vip> \
	     --key-file=<域名证书私钥文件> \
	     --cert-file=<域名证书公钥文件> \
	     --root-username=<管理员用户名，必须是邮箱格式，默认是 admin@cpaas.io>
	     
	```
	**部署方式 2**：如果客户没有提供 lb 和 vip ，也可以用下面的命令进行部署，但不再是高可用的环境了。

	```
	bash ./up-cpaas.sh \
	     --network-interface=<网卡名，默认是eth0> \
	     --make-k8s-lb \
	     --domain-name=<域名> \
	     --key-file=<域名证书私钥文件> \
	     --cert-file=<域名证书公钥文件> \
	     --enabled-features=<需要安装那些产品，默认acp,devops,asm，支持acp,devops,asm,aml,ace3> 
	```
	**部署方式 3**：如果客户连域名也不提供，也可以用下面的命令进行部署，但不再是高可用的环境了。

	```
	bash ./up-cpaas.sh \
	     --network-interface=<网卡名，默认是eth0> \
	     --make-k8s-lb \
	     --enabled-features=<需要安装那些产品，默认acp,devops,asm，支持acp,devops,asm,aml,ace3> \
	     --use-http
	```
	

**注 1**： 只支持上面给出的部署例子，部署参数组合在上述例子之外的都未经测试，不能保证支持。

**注 2**： 所有有默认值的参数，都是可选的，如果部署时不添加这个参数，就使用默认值。

**注 3**： 不提供域名证书，也不使用 `--use-http` 参数，脚本会自动签发证书，但是 chrome 浏览器会判断出这个证书不是合法机构签发的，会报错禁止浏览，需要更改 chrome 浏览器的的启动参数，容忍不合法的证书，修改方法请自行搜索。


### 正式生产环境部署方案

部署命令执行的位置： init 服务器上

**部署过程**：

1. 解压缩安装包。

2. 进入到解压缩后的目录。

3. 在 up-cpaas.sh 脚本所在的目录下创建 server_list.json 文件，示例如下：

	* 6 节点 server_list.json 文件详情：

		```
		[
		  {
		    "server_role": {
		      "master":true,
		      "global":true
		    },
		    "ip_addr": "1.1.1.1",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  },
		  {
		    "server_role": {
		      "master":true,
		      "global":true
		    },
		    "ip_addr": "2.2.2.2",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  },
		  {
		    "server_role": {
		      "master":true,
		      "global":true
		    },
		    "ip_addr": "3.3.3.3",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  },
		  {
		    "server_role": {
		      "log":true
		    },
		    "ip_addr": "4.4.4.4",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  },
		  {
		    "server_role": {
		      "log":true
		    },
		    "ip_addr": "5.5.5.5",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  },
		  {
		    "server_role": {
		      "log":true
		    },
		    "ip_addr": "6.6.6.6",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  }
		]
		```

	* 9 节点 server_list.json 文件详情：

		```
		[
		  {
		    "server_role": {
		      "master":true
		    },
		    "ip_addr": "1.1.1.1",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  },
		  {
		    "server_role": {
		      "master":true
		    },
		    "ip_addr": "2.2.2.2",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  },
		  {
		    "server_role": {
		      "master":true
		    },
		    "ip_addr": "3.3.3.3",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  },
		  {
		    "server_role": {
		      "log":true
		    },
		    "ip_addr": "4.4.4.4",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  },
		  {
		    "server_role": {
		      "log":true
		    },
		    "ip_addr": "5.5.5.5",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  },
		  {
		    "server_role": {
		      "log":true
		    },
		    "ip_addr": "6.6.6.6",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  },
		  {
		    "server_role": {
		      "global":true
		    },
		    "ip_addr": "7.7.7.7",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  },
		  {
		    "server_role": {
		      "global":true
		    },
		    "ip_addr": "8.8.8.8",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  },
		  {
		    "server_role": {
		      "global":true
		    },
		    "ip_addr": "9.9.9.9",
		    "ssh_port": "22",
		    "ssh_user": "root",
		    "ssh_passwd": "",
		    "ssh_key_file": "/root/.ssh/id_rsa"
		  }
		]
		```

4. 执行如下命令部署：

	**部署方式 1**：部署一个通过 https://<域名> 访问的高可用环境：

	```
	bash ./up-cpaas.sh \
	     --network-interface=<网卡名，默认是eth0> \
	     --cpaas-namespaces=<ns 名，默认是cpaas-system> \
	     --enabled-features=<需要安装那些产品，默认acp,devops,asm，支持acp,devops,asm,aml,ace3> \
	     --global-network-mode=<选择 global 集群使用那种网络，默认是 kube-ovn，可选 flannel 和 calico> \
	     --kube-cluster-subnet<global 集群的cluster ip 范围，默认是10.96.0.0/12，掩码建议不要小于16位> \
	     --domain-name=<域名> \
	     --kube_controlplane_endpoint=<k8s vip> \
	     --cert-sans=<可选参数，如果没有容灾需求可以不加，两地 global 集群 vip 的 vip> \
	     --key-file=<域名证书私钥文件> \
	     --cert-file=<域名证书公钥文件> \
	     --root-username=<管理员用户名，必须是邮箱格式，默认是 admin@cpaas.io>
	```
	
	**部署方式 2**：如果客户没有提供证书，也可以用下面的命令进行部署，脚本会自动创建一个证书，但是浏览器访问 ui 的时候会提示证书不是正规机构颁发的，有安全隐患。
	
	```
	bash ./up-cpaas.sh \
	     --network-interface=<网卡名，默认是eth0> \
	     --cpaas-namespaces=<ns 名，默认是cpaas-system> \
	     --enabled-features=<需要安装那些产品，默认acp,devops,asm，支持acp,devops,asm,aml,ace3> \
	     --global-network-mode=<选择 global 集群使用那种网络，默认是 kube-ovn，可选 flannel 和 calico> \
	     --kube-cluster-subnet<global 集群的cluster ip 范围，默认是10.96.0.0/12，掩码建议不要小于16位> \
	     --domain-name=<域名> \
	     --kube_controlplane_endpoint=<k8s vip> \
	     --cert-sans=<可选参数，如果没有容灾需求可以不加，两地 global 集群 vip 的 vip> \
	     --root-username=<管理员用户名，必须是邮箱格式，默认是 admin@cpaas.io>
	```
	
	**部署方式 3**：如果客户没有提供证书，也不希望浏览器提示证书不是正规机构颁发，可以选择使用 http 协议访问，执行如下命令部署。
	
	```
	bash ./up-cpaas.sh \
	     --use-http \
	     --network-interface=<网卡名，默认是eth0> \
	     --cpaas-namespaces=<ns 名，默认是cpaas-system> \
	     --enabled-features=<需要安装那些产品，默认acp,devops,asm，支持acp,devops,asm,aml,ace3> \
	     --global-network-mode=<选择 global 集群使用那种网络，默认是 kube-ovn，可选 flannel 和 calico> \
	     --kube-cluster-subnet<global 集群的cluster ip 范围，默认是10.96.0.0/12，掩码建议不要小于16位> \
	     --domain-name=<域名> \
	     --kube_controlplane_endpoint=<k8s vip> \
	     --cert-sans=<可选参数，如果没有容灾需求可以不加，两地 global 集群 vip 的 vip> \
	     --root-username=<管理员用户名，必须是邮箱格式，默认是 admin@cpaas.io>
	```

**注 1**： 只支持上面给出的部署例子，部署参数组合在上述例子之外的都未经测试，不保证能够支持。

**注 2**： 所有有默认值的参数，都是可选的，如果部署时不添加这个参数，就使用默认值。

**注 3**： 不提供域名证书，也不使用 `--use-http` 参数，脚本会自动签发证书，但是 chrome 浏览器会判断出这个证书不是合法机构签发的，会报错禁止浏览，需要更改 chrome 浏览器的启动参数，容忍不合法的证书，修改方法请自行搜索。

**注 4 **： 部署 global 完毕之后，需要手动部署public-chart-repo，在 init 节点上执行如下命令：

```
ACP_NAMESPACE=<cpaas-system>        ## 改成部署 global 时， --acp2-namespaces 参数指定的值，默认是cpaas-system
REGISTRY=$(docker info |grep 60080  |tr -d ' ')

helm install --name=public-chart-repo \
                 --version $(helm search | grep '^stable/public-chart-repo' | awk '{print $2}') \
                 --set global.registry.address=${REGISTRY_ENDPOINT} \
                 --set global.namespace=${ACP_NAMESPACE} \
                 --namespace=${ACP_NAMESPACE}  \
                 stable/public-chart-repo --wait --timeout 3000 --debug
```

## 业务服务集群部署

登录 global 的 UI 后，请参考用户手册，部署业务服务集群或对接业务服务集群。

**注意 **：因为重构了审计功能，且这个版本暂时不支持部署之后自动修改 kube-api 配置，所以部署业务集群成功之后，需要如下操作：

1. 拷贝 global 集群的 master 节点的 `/etc/kubernetes/pki/policy.yaml` 文件到业务服务集群的每一台 master 节点上。

2. 修改业务服务集群的 master 节点上的 `/etc/kubernetes/manifests/kube-apiserver.yaml`  配置文件，增加如下内容：

	<img src="images/kube-api 增加内容.png" style="zoom:50%;" />

	```
	    - --feature-gates=AdvancedAuditing=true
	    - --audit-policy-file=/etc/kubernetes/pki/policy.yaml
	    - --audit-log-format=json
	    - --audit-log-path=/etc/kubernetes/audit/audit.log
	    - --audit-log-mode=batch
	    - --audit-log-maxsize=20
	
	    - mountPath: /etc/kubernetes/audit
	      name: audit
	      
	  - hostPath:
	      path: /etc/kubernetes/audit
	      type: DirectoryOrCreate
	    name: audit
	    
	```
	

**注意**: `--audit` 开头的参数，只有上述五个，请仔细检查。

## 业务服务集群组件、global 产品和第三方插件部署

### 在业务服务集群上，通过 sentry 部署 cert-manager、alauda-cluster-base、nevermore、nginx-ingress和 普罗米修斯的方法

**注意1：**：如果 prometheus 报错，提示找不到 etcd-ca，在集群的 master 上执行下面的命令创建：

```
	kubectl create secret tls etcd-ca --cert=/etc/kubernetes/pki/etcd/ca.crt --key=/etc/kubernetes/pki/etcd/ca.key -n <改成部署时配置的 ns 的值>

kubectl create secret tls etcd-peer --cert=/etc/kubernetes/pki/etcd/peer.crt --key=/etc/kubernetes/pki/etcd/peer.key -n <改成部署时配置的 ns 的值>
```

**注意2：**：要手动在要部署 prometheus 的机器上增加 `monitoring=enabled` 的label，如果采用 localVolumePath 的方式部署，确保打了 `monitoring=enabled` label 的机器上 `/cpaas/monitoring/` 目录下存在如下 3 个文件夹: alertmanager、prometheus、grafana。

1. 首先准备 sentry 需要的环境变量，在 init 节点上执行 `helm get values sentry > /tmp/sentry-values.yaml`，将 `/tmp/sentry-values.yaml` 拷贝到业务服务集群的第一台 master 节点的 `/tmp` 目录下，并修改这个文件，保证只有如下 key，且 key 的值符合备注的要求。

	```
	global:
	  host:                                 # 不修改
	  region:                               # 要安装的业务集群的名字
	  labelBaseDomain:                      # 不修改，如果没有，也不添加这个 key
	  scheme:                               # 不修改
	  auth:
	    defaultAdmin:                       # 不修改
	  sentryVersion:                        # 不修改
	  sentryInstall:                        # 不修改
	  namespace:                            # 不修改
	  registry:
	    address:                            # 不修改
	charts:
	  repository:                           # 不修改
	  versions:
	    cpaasAppreleases:                   # 不修改
	    nginxIngress:                       # 不修改
	    certManager:                        # 不修改
	    clusterBase:                        # 不修改
	    kubePrometheus:                     # 不修改
	    prometheusOperator:                 # 不修改
	    logAgent:                           # 不修改
	    cpaasMonitor:                       # 不修改
	platform:
	  install: false                        # 改成false
	clusterPlatform:
	  install: true                         # 必须改成 true ，才能安装业务集群需要的 ingress 和 cert-manager 
	  logAgent:
	    install:                            # 如果要安装 nevermore ，改成 true
	    region:                             # 改成要安装的业务集群的名字
	    token:                              # 改成在业务集群上执行 $(kubectl describe secret -n kube-system $(kubectl get secret -n kube-system | awk '/clusterrole-aggregation-controller-token/ {print $1}') | awk '/^token:/{print $2}') 获取的值
	    isOCP: false                        # 如果是 ocp 集群，改成 true
	    containerEngine: docker             # 如果底层使用的是crio管理容器，这个值需要改成 crio
	  kubePrometheus:
	    install:                            # 如果要安装 kube-prometheus ，改成 true
	    serviceType:                        # 不修改
	    localVolumePath:                    # 如果本地目录方式部署，保留不修改，如果用 pvc 的方式，删掉这个变量
	    storageClassName:                   # 如果本地目录方式部署，保留值为 null，如果用 pvc 的方式，改成替换为当前集群的 StorageClass 的名字
	    alertmanagerReceiverName:           # 不修改
	    alertmanagerReceiverWebhookSendResolved: #不修改
	  prometheusOperator:
	    install: true                       # 修改成和 kubePrometheus.install 的值相同的值
	  cpaasMonitor:
	    install: true                       # 改成false
	```


2. 在业务服务集群的第一台 master 节点上执行以下命令：

	```
	ACP_NAMESPACE=<cpaas-system>        ## 改成部署 global 时， --acp2-namespaces 参数指定的值，默认是cpaas-system
	helm install --name acp-business --debug --wait --timeout 3000 --namespace=${ACP_NAMESPACE} -f /tmp/sentry-values.yaml stable/sentry 
	
	```

3. 如果 kubePrometheus.install 的值是 true，需要集群对接监控，在部署普罗米修斯的集群的 master 节点上操作。
	
	```
	ip=2.2.2.2                          ## 需要修改为业务集群任意一个 master 节点的外网 ip，若没有外网地址，使用默认的实际ip
	ACP_NAMESPACE=<cpaas-system>        ## 改成部署时， --acp2-namespaces 参数指定的值，默认是cpaas-system
	cat << eof > /tmp/prometheus-feature.yaml
	apiVersion: infrastructure.alauda.io/v1alpha1
	kind: Feature
	metadata:
	  labels:
	    instanceType: prometheus
	    type: monitoring
	  name: prometheus
	spec:
	  accessInfo:
	    grafanaAdminPassword: admin                # grafana 默认密码
	    grafanaAdminUser: admin                    # grafana 默认用户
	    grafanaUrl: http://$ip:30902               # grafana 地址
	    name: kube-prometheus                      # 安装 kube-prometheus chart 时指定的名称
	    namespace: ${ACP_NAMESPACE}                # kube-prometheus chart 所在的命名空间
	    prometheusTimeout: 10                      # prometheus 请求超时时间
	    prometheusUrl: http://$ip:30900            # prometheus 地址
	  instanceType: prometheus
	  type: monitoring
	  version: "1.0"
	eof
	 
	kubectl apply -f /tmp/prometheus-feature.yaml
	```

### sentry 不可部署的业务集群可选组件

#### 业务集群部署 alb2

**说明**：在 UI 页面部署，详情参考用户手册。


#### cron-hpa-controller【定时扩缩容的组件，可不部署】

在 global 的第一台 master 节点上执行以下命令：

```
REGISTRY_ENDPOINT=$(docker info |grep 60080  |tr -d ' ')
REGION_NAME=<name>                  ## 想要部署 cron-hpa-controller 的集群的名字
ACP_NAMESPACE=<cpaas-system>        ## 改成部署时， --acp2-namespaces 参数指定的值，默认是cpaas-system

mkdir /tmp/region_helmrequest
cat << EOF >/tmp/region_helmrequest/${REGION_NAME}-cron-hpa-controller.yaml
apiVersion: app.alauda.io/v1alpha1
kind: HelmRequest
metadata:
  finalizers:
  - captain.alauda.io
  generation: 1
  name: ${REGION_NAME}-cron-hpa-controller
  namespace: ${ACP_NAMESPACE}
spec:
  chart: stable/cron-hpa-controller
  namespace: ${ACP_NAMESPACE}
  releaseName: ${REGION_NAME}-cron-hpa-controller
  clusterName: ${REGION_NAME}
  values:
    global:
      namespace: ${ACP_NAMESPACE}
      registry:
        address: ${REGISTRY_ENDPOINT}
  version: $(helm search | grep '^stable/cron-hpa-controller ' | awk '{print $2}')

EOF
kubectl create -f /tmp/region_helmrequest/${REGION_NAME}-cron-hpa-controller.yaml
```

#### tapp 【若客户没有 tapp 需求，可不部署】

**说明**：不支持在 ake 集群部署，在 tke 集群是通过页面扩展组件一键部署，详情请参考用户手册。

#### metrics-server 【devops 自动扩缩容需要用到这个组件】dashboard 包括这个组件，如果已经部署了dashboard，就无需再部署

在待部署 metrics-server 的集群的第一台 master 节点上执行以下命令：

```
REGISTRY_ENDPOINT=$(docker info |grep 60080  |tr -d ' ')
ACP_NAMESPACE=<cpaas-system>        ## 改成部署时， --acp2-namespaces 参数指定的值，默认是cpaas-system

cat <<EOF>/tmp/metrics-server.yaml
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: metrics-server
  namespace: ${ACP_NAMESPACE}
  labels:
    k8s-app: metrics-server
spec:
  selector:
    matchLabels:
      k8s-app: metrics-server
  template:
    metadata:
      name: metrics-server
      labels:
        k8s-app: metrics-server
    spec:
      containers:
      - name: metrics-server
        image: ${REGISTRY_ENDPOINT}/alaudak8s/metrics-server-amd64:v0.3.1
        imagePullPolicy: IfNotPresent
        command:
        - /metrics-server
        - --kubelet-preferred-address-types=InternalIP
        - --kubelet-insecure-tls
        volumeMounts:
        - name: tmp-dir
          mountPath: /tmp
      volumes:
      # mount in tmp so we can safely use from-scratch images and/or read-only containers
      - name: tmp-dir
        emptyDir: {}
 
---
apiVersion: v1
kind: Service
metadata:
  name: metrics-server
  namespace: ${ACP_NAMESPACE}
  labels:
    kubernetes.io/name: "Metrics-server"
spec:
  selector:
    k8s-app: metrics-server
  ports:
  - port: 443
    protocol: TCP
    targetPort: 443
---
apiVersion: apiregistration.k8s.io/v1beta1
kind: APIService
metadata:
  name: v1beta1.metrics.k8s.io
spec:
  service:
    name: metrics-server
    namespace: ${ACP_NAMESPACE}
  group: metrics.k8s.io
  version: v1beta1
  insecureSkipTLSVerify: true
  groupPriorityMinimum: 100
  versionPriority: 100
EOF
kubectl create -f /tmp/metrics-server.yaml

```

### global 产品和第三方插件部署

#### ACE 3.3

**说明**：

* `<>` 代表要根据环境情况替换尖括号内的值，替换完毕之后，不包括尖括号。

* 如果在部署 global 的时候，`--enabled-features` 这个参数没有加上 ace3 ，需要手动部署容器管理平台的 globla 组件。

在 global 集群的第一个 master 节点上，执行如下操作：

```
ACP_NAMESPACE=<cpaas-system>  ## 改成部署平台时， --acp2-namespaces 参数指定的值，默认是cpaas-system
DOMAIN_NAME=<域名>       ## 部署时，--domain-name 参数传入的值
LABELBASEDOMAIN='cpaas.io'
OIDC_PROTOCOL=https         ## 如果部署时，添加了--use-http 参数，将这个值改成 http
cat <<EOF >/tmp/apprelease-ace3.yaml
apiVersion: operator.alauda.io/v1alpha1
kind: AppRelease
metadata:
  name: alauda-cloud-enterprise
  namespace: ${ACP_NAMESPACE}
spec:
  chart:
    name: alauda-cloud-enterprise
    repository: ${CHART_ENDPOINT}
    version: $(helm search | grep '^stable/alauda-cloud-enterprise ' | awk '{print $2}')
  values:
    global:
      tke: true
      namespace: ${ACP_NAMESPACE}
      host: "${DOMAIN_NAME}"
      registry:
        address: ${REGISTRY_ENDPOINT}
      labelBaseDomain: ${LABELBASEDOMAIN}
    alaudaConsole:
      apiAddress: ${OIDC_PROTOCOL}://${DOMAIN_NAME}
      oidcIssuerUrl: ${OIDC_PROTOCOL}://${DOMAIN_NAME}/dex
      oidcRedirectUrl: https://${DOMAIN_NAME}
  version: 0.0.9
EOF
```

#### Container-platform（容器管理平台）

**说明**：

* `<>` 代表要根据环境情况替换尖括号内的值，替换完毕之后，不包括尖括号。

* 如果在部署 global 的时候，`--enabled-features` 这个参数没有加上 acp ，需要手动部署容器管理平台的 globla 组件。

在 global 集群的第一个 master 节点上执行以下命令：

```
helm get values sentry >/tmp/sentry_values.yaml
## 修改 /tmp/sentry_values.yaml ，将 platform.acp.install 的值改成 'true'
helm upgrade sentry -f /tmp/sentry_values.yaml stable/sentry
```

#### devops

**说明**：

* `<>` 代表要根据环境情况替换尖括号内的值，替换完毕之后，不包括尖括号。

* 如果在部署 global 的时候，`--enabled-features` 这个参数没有加上 devops ，需要手动部署 devops 的 globla 组件。

在 global 集群的第一个 master 节点上执行以下命令：

```
helm get values sentry >/tmp/sentry_values.yaml
## 修改 /tmp/sentry_values.yaml ，将 platform.devops.install 的值改成 'true'
helm upgrade sentry -f /tmp/sentry_values.yaml stable/sentry
```

##### devops 所需的 harbor、jenkins、gitlab、nexus、sonarqube 第三方插件部署

**说明**：若使用 PVC 方式部署以下插件。需要事先准备好对应的 PVC，PVC 默认我们提供的是对接 cephfs，因此需要先确保集群对接了 ceph，然后创建对应的 PVC。

```
cat <<EOF > cephfs.yaml
   
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: claim1                 ###  对应 pvc 的名字
  namespace: default
  annotations:
    volume.beta.kubernetes.io/storage-class: cephfs
    volume.beta.kubernetes.io/storage-provisioner: ceph.com/cephfs
  finalizers:
    - kubernetes.io/pvc-protection
spec:
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 1Gi              ###对应 pvc 的大小
  volumeMode: Filesystem
EOF
   
kubectl create -f cephfs.yaml
```

###### 部署 jenkins

jenkins 的部署方式有以下四种，我们推荐在业务集群部署 jenkins，且最好以 PVC 的方式部署。***建议将 jenkins部署到单独的机器上，以免devops的构建pod调度到jenkins所在所在主机执行构建任务从而影响jenkins的稳定性***，可以采用设置污点的方式。

**相关指令：**

* 设置污点：`kubectl taint nodes node1 key1=value1:NoSchedule`

* 去除污点：`kubectl taint nodes node1 key1:NoSchedule-`

**操作步骤：**

* 在 global 集群以本地目录的方式部署 jenkins，（对接了GitLab）。

	```
	NODE_NAME="acp2-master-1"            ###需要修改为集群中实际存放jenkins数据的某个节点的name，通过 kubectl get no 命令获取到的 name
	path="/root/alauda/jenkins"          ###默认数据目录为/root/alauda/jenkins，若有需要可更改。
	password="Jenkins12345"              ###默认密码为Jenkins12345，若有需要可更改
	REGISTRY=$(docker info |grep 60080  |tr -d ' ')
	ACP_NAMESPACE=<cpaas-system>     ## 改成部署时， --acp2-namespaces 参数指定的值，默认是cpaas-system
	 
	cat <<EOF > values.yaml
	 
	global:
	  registry:
	    address: ${REGISTRY}
	Master:
	  ServiceType: NodePort
	  NodePort: 32001
	  AdminPassword: "$password"
	  gitlabConfigs:                      ###可选参数，用来配置GitLab服务器，如没有，这块可以删除，或者安装之后通过Jenkins的页面进行操作
	    - name: <GitLab服务器名字>          ###GitLab服务器名字，是DevOps集成平台上面对应的显示名称。DevOps管理视图->DevOps工具链 对应的GitLab服务名字
	      manageHooks: true               ###自动添加GitLab的Webhook
	      serverUrl: <GitLab服务器地址>     ###GitLab服务器的地址，访问地址
	      token: <GitLab凭据token>         ###GitLab的Personal access tokens，需要在GitLab上生成
	  Location:
	    url: <Jenkins Location URL>       ###Jenkins的访问地址，用来做GitLab的webhook回调使用
	Persistence:
	  Enabled: false
	  Host:
	    NodeName: ${NODE_NAME}
	    Path: $path
	AlaudaACP:
	  Enabled: true
	alaudapipelineplugin:
	  consoleURL: ""
	  apiEndpoint: ""
	  apiToken: ""
	  account: ""
	  spaceName: ""
	  clusterName: ""
	  namespace: ""
	AlaudaDevOpsCredentialsProvider:
	  globalNamespaces: "${ACP_NAMESPACE}-global-credentials,${ACP_NAMESPACE}"
	Erebus:
	  Namespace: "${ACP_NAMESPACE}"
	  URL: "https://erebus.${ACP_NAMESPACE}.svc.cluster.local:443/kubernetes"
	 
	EOF
	
	helm install stable/jenkins --name jenkins --namespace default -f values.yaml
	```

* 在 global 以 pvc 的方式部署 jenkins，（对接了GitLab）。

	```
	password="Jenkins12345"              ###默认密码为Jenkins12345，若有需要可更改
	pvc_name="jenkinspvc"                ###默认pvc的名字为jenkinspvc，需要事先在default命名空间下准备好这个pvc，可更改，但也需要事先创建好对应的pvc。
	REGISTRY=$(docker info |grep 60080  |tr -d ' ')
	ACP_NAMESPACE=<cpaas-system>     ## 改成部署时， --acp2-namespaces 参数指定的值，默认是cpaas-system

	cat <<EOF > values.yaml
	 
	global:
	  registry:
	    address: ${REGISTRY}
	Master:
	  ServiceType: NodePort
	  NodePort: 32001
	  AdminPassword: "$password"
	  gitlabConfigs:                      ###可选参数，用来配置GitLab服务器，如没有，这块可以删除，或者安装之后通过Jenkins的页面进行操作
	    - name: <GitLab服务器名字>          ###GitLab服务器名字，是DevOps集成平台上面对应的显示名称。DevOps管理视图->DevOps工具链 对应的GitLab服务名字
	      manageHooks: true               ###自动添加GitLab的Webhook
	      serverUrl: <GitLab服务器地址>     ###GitLab服务器的地址，访问地址
	      token: <GitLab凭据token>         ###GitLab的Personal access tokens，需要在GitLab上生成
	  Location:
	    url: <Jenkins Location URL>       ###Jenkins的访问地址，用来做GitLab的webhook回调使用
	Persistence:
	  Enabled: true
	  ExistingClaim "$pvc_name"
	AlaudaACP:
	  Enabled: true
	alaudapipelineplugin:
	  consoleURL: ""
	  apiEndpoint: ""
	  apiToken: ""
	  account: ""
	  spaceName: ""
	  clusterName: ""
	  namespace: ""
	AlaudaDevOpsCredentialsProvider:
	  globalNamespaces: "${ACP_NAMESPACE}-global-credentials,${ACP_NAMESPACE}"
	Erebus:
	  Namespace: "${ACP_NAMESPACE}"
	  URL: "https://erebus.${ACP_NAMESPACE}.svc.cluster.local:443/kubernetes"
	 
	EOF
	 
	helm install stable/jenkins --name jenkins --namespace default -f values.yaml
	
	```

* 在业务集群以本地路径的方式部署 jenkins，（对接了GitLab）。

	```
	NODE_NAME="acp2-master-1"            ###需要修改为集群中实际存放jenkins数据的某个节点的name，通过 kubectl get no 命令获取到的 name
	global_vip="1.1.1.1"                 ###需要修改为平台的访问地址，如果访问地址是域名，就必须配置成域名，因为 jenkins 需要访问 global 平台的 erebus，如果平台是域名访问的话，erebus 的 ingress 策略会配置成只能域名访问。
	path="/root/alauda/jenkins"          ###默认数据目录为/root/alauda/jenkins，若有需要可更改。
	password="Jenkins12345"              ###默认密码为Jenkins12345，若有需要可更改
	REGISTRY=$(docker info |grep 60080  |tr -d ' ')
	TOKEN=       ### 如何获取token，到devops-apiserver所在集群（一般为global集群）执行：echo $(kubectl get secret -n ${ACP_NAMESPACE} $(kubectl get secret -n ${ACP_NAMESPACE} | grep devops-apiserver-token |awk '{print $1}') -o jsonpath={.data.token} |base64 --d)
	ACP_NAMESPACE=<cpaas-system>     ## 改成部署时， --acp2-namespaces 参数指定的值，默认是cpaas-system

	cat <<EOF > values.yaml
	  
	global:
	  registry:
	    address: ${REGISTRY}
	Master:
	  ServiceType: NodePort
	  NodePort: 32001
	  AdminPassword: "$password"
	  gitlabConfigs:                      ###可选参数，用来配置GitLab服务器，如没有，这块可以删除，或者安装之后通过Jenkins的页面进行操作
	    - name: <GitLab服务器名字>          ###GitLab服务器名字，是DevOps集成平台上面对应的显示名称。DevOps管理视图->DevOps工具链 对应的GitLab服务名字
	      manageHooks: true               ###自动添加GitLab的Webhook
	      serverUrl: <GitLab服务器地址>     ###GitLab服务器的地址，访问地址
	      token: <GitLab凭据token>         ###GitLab的Personal access tokens，需要在GitLab上生成
	  Location:
	    url: <Jenkins Location URL>       ###Jenkins的访问地址，用来做GitLab的webhook回调使用
	Persistence:
	  Enabled: false
	  Host:
	    NodeName: ${NODE_NAME}
	    Path: $path
	AlaudaACP:
	  Enabled: false
	alaudapipelineplugin:
	  consoleURL: ""
	  apiEndpoint: ""
	  apiToken: ""
	  account: ""
	  spaceName: ""
	  clusterName: ""
	  namespace: ""
	AlaudaDevOpsCredentialsProvider:
	  globalNamespaces: "${ACP_NAMESPACE}-global-credentials,${ACP_NAMESPACE},kube-system"
	AlaudaDevOpsCluster:
	  Cluster:
	    masterUrl: "https://$global_vip:6443"
	    token: ${TOKEN}
	Erebus:
	  Namespace: "${ACP_NAMESPACE}"
	  URL: "https://$global_vip:443/kubernetes"
	 
	EOF
	 
	helm install stable/jenkins --name jenkins --namespace default -f values.yaml
	
	```

* 在业务集群以 PVC 的方式部署 jenkins，（对接了GitLab）。

	```
	global_vip="1.1.1.1"                 ###需要修改为平台的访问地址，如果访问地址是域名，就必须配置成域名，因为 jenkins 需要访问 global 平台的 erebus，如果平台是域名访问的话，erebus 的 ingress 策略会配置成只能域名访问。
	password="Jenkins12345"              ###默认密码为Jenkins12345，若有需要可更改
	pvc_name="jenkinspvc"                ###默认pvc的名字为jenkinspvc，需要事先在default命名空间下准备好这个pvc，可更改，但也需要事先创建好对应的pvc。
	REGISTRY=$(docker info |grep 60080  |tr -d ' ')
	TOKEN=       ### 如何获取token，到devops-apiserver所在集群（一般为global集群）执行：echo $(kubectl get secret -n ${ACP_NAMESPACE} $(kubectl get secret -n ${ACP_NAMESPACE} | grep devops-apiserver-token |awk '{print $1}') -o jsonpath={.data.token} |base64 -d)
	ACP_NAMESPACE=<cpaas-system>     ## 改成部署时， --acp2-namespaces 参数指定的值，默认是cpaas-system

	cat <<EOF > values.yaml
	  
	global:
	  registry:
	    address: ${REGISTRY}
	Master:
	  ServiceType: NodePort
	  NodePort: 32001
	  AdminPassword: "$password"
	  gitlabConfigs:                      ###可选参数，用来配置GitLab服务器，如没有，这块可以删除，或者安装之后通过Jenkins的页面进行操作
	    - name: <GitLab服务器名字>          ###GitLab服务器名字，是DevOps集成平台上面对应的显示名称。DevOps管理视图->DevOps工具链 对应的GitLab服务名字
	      manageHooks: true               ###自动添加GitLab的Webhook
	      serverUrl: <GitLab服务器地址>     ###GitLab服务器的地址，访问地址
	      token: <GitLab凭据token>         ###GitLab的Personal access tokens，需要在GitLab上生成
	  Location:
	    url: <Jenkins Location URL>       ###Jenkins的访问地址，用来做GitLab的webhook回调使用
	Persistence:
	  Enabled: true
	  ExistingClaim: "$pvc_name"
	AlaudaACP:
	  Enabled: false
	alaudapipelineplugin:
	  consoleURL: ""
	  apiEndpoint: ""
	  apiToken: ""
	  account: ""
	  spaceName: ""
	  clusterName: ""
	  namespace: ""
	AlaudaDevOpsCredentialsProvider:
	  globalNamespaces: "${ACP_NAMESPACE}-global-credentials,${ACP_NAMESPACE},kube-system"
	AlaudaDevOpsCluster:
	  Cluster:
	    masterUrl: "https://$global_vip:6443"
	    token: ${TOKEN}
	Erebus:
	  Namespace: "${ACP_NAMESPACE}"
	  URL: "https://$global_vip:443/kubernetes"
	 
	EOF
	 
	helm install stable/jenkins --name jenkins --namespace default -f values.yaml
	```

	部署成功后到 Jenkins 修改配置访问集群任意一个节点 ip+32001 端口，访问 jenkins 页面，用户名：admin   密码：Jenkins12345。

	单击 **系统管理->系统设置**：
	
	a. Alauda Jenkins Sync 里勾选 **启用** 按钮，添加 ***Jenkins 服务名称***，例如：jenkinstest，名称不能和别的已经部署的 jekins 服务名称一样（例如：global 里的 jenkins），界面集成的时候必须与这个名字一致。
  
	b. 到 Kubernetes Cluster Configuration 下的 Credentials，单击添加，添加一个 serviceaccount token （类型选择 secret text， Secret 位置输入token， ID 输入任意凭据名称（例如：test-token））。<br>如何获取 token？到 devops-apiserver 所在集群（一般为 global 集群）执行命令行：<br>`echo $(kubectl get secret -n ${ACP_NAMESPACE} $(kubectl get secret -n ${ACP_NAMESPACE}  | grep devops-apiserver-token |awk '{print $1}') -o jsonpath={.data.token} |base64 -d)`。<br>添加之后。Credentials 选择刚才创建的 secret。
  
	c. 都配置成功后，在 Kubernetes Cluster Configuration 下单击 [Test Connection] 测试连接，提示 `Connect to  succeed` 后，单击保存。


###### 部署 gitlab


部署 gitlab 有以下两种方式，推荐部署在业务集群，且以 PVC 的方式部署。

* 以本地路径的方式部署。

	```
	REGISTRY=$(docker info |grep 60080  |tr -d ' ')
	NODE_NAME=acp2-master-1      ###需要修改为选择部署gialab的节点的name，通过 kubectl get no 命令获取到的 name
	NODE_IP="1.1.1.1"            ###这个ip为gitlab的访问地址，需要修改为部署集群中任意master节点一个节点的实际ip
	potal_path="/root/alauda/gitlab/portal"         ###potal的数据目录，一般不需要修改，若有规划，可修改为别的目录
	database_path="/root/alauda/gitlab/database"    ###database的目录，一般不需要修改，若有规划，可修改为别的目录
	redis_path="/root/alauda/gitlab/redis"          ###redis的目录，一般不需要修改，若有规划，可修改为别的目录
	helm install stable/gitlab-ce --name gitlab-ce --namespace default \
	    --set global.registry.address=${REGISTRY} \
	    --set portal.debug=true \
	    --set gitlabHost=${NODE_IP} \
	    --set gitlabRootPassword=Gitlab12345 \
	    --set service.type=NodePort \
	    --set service.ports.http.nodePort=31101 \
	    --set service.ports.ssh.nodePort=31102 \
	    --set service.ports.https.nodePort=31103 \
	    --set portal.persistence.enabled=false \
	    --set portal.persistence.host.nodeName=${NODE_NAME} \
	    --set portal.persistence.host.path="$potal_path" \
	    --set portal.persistence.host.nodeName="${NODE_NAME}" \
	    --set database.persistence.enabled=false \
	    --set database.persistence.host.nodeName=${NODE_NAME} \
	    --set database.persistence.host.path="$database_path" \
	    --set database.persistence.host.nodeName="${NODE_NAME}" \
	    --set redis.persistence.enabled=false \
	    --set redis.persistence.host.nodeName=${NODE_NAME} \
	    --set redis.persistence.host.path="$redis_path" \
	    --set redis.persistence.host.nodeName="${NODE_NAME}" \
	```

* PVC 的方式部署 gitlab。

	```
	NODE_IP=1.1.1.1   ###此参数为部署时指定的访问地址，写当前集群中任意一个master节点的ip即可
	portal_pvc="portalpvc"          ###默认pvc的名字为portalpvc，需要事先在default命名空间下准备好这个pvc，可更改，但也需要事先创建好对应的pvc。
	database_pvc="databasepvc"      ###默认pvc的名字为databasepvc，需要事先在default命名空间下准备好这个pvc，可更改，但也需要事先创建好对应的pvc。
	redis_pvc="redispvc"           ###默认pvc的名字为redispvc，需要事先在default命名空间下准备好这个pvc，可更改，但也需要事先创建好对应的pvc。
	REGISTRY=$(docker info |grep 60080  |tr -d ' ')
	helm install stable/gitlab-ce --name gitlab-ce --namespace default \
	    --set global.registry.address=${REGISTRY} \
	    --set portal.debug=true \
	    --set gitlabHost=${NODE_IP} \
	    --set gitlabRootPassword=Gitlab12345 \
	    --set service.type=NodePort \
	    --set service.ports.http.nodePort=31101 \
	    --set service.ports.ssh.nodePort=31102 \
	    --set service.ports.https.nodePort=31103 \
	    --set portal.persistence.enabled=true \
	    --set portal.persistence.existingClaim=$portal_pvc \
	    --set database.persistence.enabled=true \
	    --set database.persistence.existingClaim=$database_pvc \
	    --set redis.persistence.enabled=true \
	    --set redis.persistence.existingClaim=$redis_pvc \
	```

	部署完成之后，通过 `NODE_IP+31101` 端口访问 gitlab，默认用户名为：root，密码为：Gitlab12345。

###### 部署 harbor 镜像仓库

部署 harbor 有以下两种部署方式，推荐部署在业务集群，且以 PVC 的方式部署。

* 本地目录的方式部署 harbor。

	```
	REGISTRY=$(docker info |grep 60080  |tr -d ' ')
	NODE_IP="1.1.1.1"          ###此参数为部署时指定的访问地址，写当前集群中任意一个master节点的ip即可
	NODE_NAME="acp-master-1"   ###需要修改为选择部署harbor节点的name，通过 kubectl get no 命令获取到的 name
	HOST_PATH=/alauda/harbor   ###这个目录为harbor的数据目录路径，一般不需要修改，若有别的规划，可修改。
	harbor_password="Harbor12345"  ####harbor的密码，默认不需要修改，若有规划，可改
	db_password="Harbor4567"       ####harbor数据库的密码，默认不需要修改，若有规划，可改
	redis_password="Redis789"      ###harbor的redis的密码，默认不需要修改，若有规划，可改
	helm install --name harbor --namespace default stable/harbor \
	    --set global.registry.address=${REGISTRY} \
	    --set externalURL=http://${NODE_IP}:31104 \
	    --set harborAdminPassword=$harbor_password \
	    --set ingress.enabled=false \
	    --set service.type=NodePort \
	    --set service.ports.http.nodePort=31104 \
	    --set service.ports.ssh.nodePort=31105 \
	    --set service.ports.https.nodePort=31106 \
	    --set database.password=$db_password \
	    --set redis.usePassword=true \
	    --set redis.password=$redis_password \
	    --set database.persistence.enabled=false \
	    --set database.persistence.host.nodeName=${NODE_NAME} \
	    --set database.persistence.host.path=${HOST_PATH}/database \
	    --set redis.persistence.enabled=false \
	    --set redis.persistence.host.nodeName=${NODE_NAME} \
	    --set redis.persistence.host.path=${HOST_PATH}/redis \
	    --set chartmuseum.persistence.enabled=false \
	    --set chartmuseum.persistence.host.nodeName=${NODE_NAME} \
	    --set chartmuseum.persistence.host.path=${HOST_PATH}/chartmuseum \
	    --set registry.persistence.enabled=false \
	    --set registry.persistence.host.nodeName=${NODE_NAME} \
	    --set registry.persistence.host.path=${HOST_PATH}/registry \
	    --set jobservice.persistence.enabled=false \
	    --set jobservice.persistence.host.nodeName=${NODE_NAME} \
	    --set jobservice.persistence.host.path=${HOST_PATH}/jobservice \
	    --set AlaudaACP.Enabled=false \
	```

* PVC 的方式部署 harbor。

	```
	REGISTRY=$(docker info |grep 60080  |tr -d ' ')
	NODE_IP="1.1.1.1"             ######此参数为部署时指定的访问地址，写当前集群中任意一个master节点的ip即可
	database_pvc=habordatabase   ###harbor数据库使用的pvc，需要事先在default下创建这个pvc
	redis_pvc=harborredis        ###harbor的redis使用的pvc，需要事先在default下创建这个pvc
	chartmuseum_pvc=harbormuseum   ###harbor使用的pvc，需要事先在default下创建这个pvc
	registry_pvc=harborregistry     ###harbor的registry使用的pvc，需要事先在default下创建这个pvc
	jobservice_pvc=harborjob        ###harbor使用的pvc，需要事先在default下创建这个pvc
	harbor_password="Harbor12345"  ####harbor的密码，默认不需要修改，若有规划，可改
	db_password="Harbor4567"       ####harbor数据库的密码，默认不需要修改，若有规划，可改
	redis_password="Redis789"      ###harbor的redis的密码，默认不需要修改，若有规划，可改
	helm install --name harbor --namespace default stable/harbor \
	    --set global.registry.address=${REGISTRY} \
	    --set externalURL=http://${NODE_IP}:31104 \
	    --set harborAdminPassword=$harbor_password \
	    --set ingress.enabled=false \
	    --set service.type=NodePort \
	    --set service.ports.http.nodePort=31104 \
	    --set service.ports.ssh.nodePort=31105 \
	    --set service.ports.https.nodePort=31106 \
	    --set database.password=$db_password \
	    --set redis.usePassword=true \
	    --set redis.password=$redis_password \
	    --set database.persistence.enabled=true \
	    --set database.persistence.existingClaim=${database_pvc} \
	    --set redis.persistence.enabled=true \
	    --set redis.persistence.existingClaim=${redis_pvc} \
	    --set chartmuseum.persistence.enabled=true \
	    --set chartmuseum.persistence.existingClaim=${chartmuseum_pvc} \
	    --set registry.persistence.enabled=true \
	    --set registry.persistence.existingClaim=${registry_pvc} \
	    --set jobservice.persistence.enabled=true \
	    --set jobservice.persistence.existingClaim=${jobservice_pvc} \
	    --set AlaudaACP.Enabled=false \
	```

	部署完成后通过传入的部署 ip+31104 端口访问 harbor。默认用户名为：admin，密码为：Harbor12345。

###### 部署 nexus

存储方式：host path  或  PVC（推荐）。

* 以 hostPath 方式部署。

	```
	  NODE_IP=<要集群的 master 的 ip，nexus 暴露的端口是 nodePorts，集群内所有服务器都可以，但 master 节点最好，因为不用担心 master 节点被删除>
	  NODE_NAME=node1  ## 集群中实际存放 nexus 数据的某个节点的name，也就是定点部署 nexus 的节点的 name，通过 kubectl get no 命令获取到的 name
	  HOST_PATH=/alauda/nexus
	  REGISTRY=$(docker info |grep 60080  |tr -d ' ')
	  helm install stable/nexus --name nexus \
	      --set global.registry.address=${REGISTRY} \
	      --set nexus.service.nodePort=32010 \
	      --set nexusProxy.env.nexusHttpHost=${NODE_IP} \
	      --set nexusProxy.env.nexusDockerHost=${NODE_IP} \
	      --set persistence.host.nodeName=${NODE_NAME} \
	      --set persistence.host.path=${HOST_PATH}
	```

* 以 StorageClass 方式，自动创建 PVC 部署, 需要提前创建好对应 STORAGE_CLASS 的 PV。

	```
	  NODE_IP=<要集群的 master 的 ip，nexus 暴露的端口是 nodePorts，集群内所有服务器都可以，但 master 节点最好，因为不用担心 master 节点被删除>
	  NEXUS_SC=nexus-sc  ## 给 nexus 使用的 pvc 的名字
	  REGISTRY=$(docker info |grep 60080  |tr -d ' ')
	  helm install stable/nexus --name nexus \
	      --set global.registry.address=${REGISTRY} \
	      --set nexus.service.nodePort=32010 \
	      --set nexusProxy.env.nexusHttpHost=${NODE_IP} \
	      --set nexusProxy.env.nexusDockerHost=${NODE_IP} \
	      --set persistence.enabled=true \
	      --set persistence.storageClass=${NEXUS_SC}
	```

* 已经存在 PVC 时，可以直接用 PVC 部署，需要提前创建好对应 PVC。

	```
	 NODE_IP=<要集群的 master 的 ip，nexus 暴露的端口是 nodePorts，集群内所有服务器都可以，但 master 节点最好，因为不用担心 master 节点被删除>
	  NEXUS_PVC=nexus-pvc
	  REGISTRY=$(docker info |grep 60080  |tr -d ' ')
	  helm install . --name nexus \
	      --set global.registry.address=${REGISTRY} \
	      --set nexus.service.nodePort=32010 \
	      --set nexusProxy.env.nexusHttpHost=${NODE_IP} \
	      --set nexusProxy.env.nexusDockerHost=${NODE_IP} \
	      --set persistence.enabled=true \
	      --set persistence.existingClaim=${NEXUS_PVC}
	```

	**说明**：如需开启 nexusBackup，需要``` --set nexusBackup.enabled=true ```，以及 nexusBackup 对应的 persistence，同上方式，具体参数见 chart 中 `README.md`。

默认帐号 admin 的密码由 nexus 随机生成，需要到 pod 中 `cat /nexus-data/admin.password`，第一次登录后，会要求更改密码。

###### 部署 sonarqube

sonarqube 版本：7.9.1-community

在安装 chart 前，请先确认将用哪种方式来存储数据：

* Persistent Volume Claim (建议)

* Host path

如果 Kubernetes 集群已经有可用的 StorageClass 和 provisioner，在安装 chart 过程中会自动创建 PVC 来存储数据。 想了解更多关于 StorageClass 和 PVC 的内容，可以参考 Kubernetes 官方文档。


* **参考命令（使用 PVC）：**

	```
	registry=$(docker info |grep 60080  |tr -d ' ')
	ACP_NAMESPACE=<cpaas-system>     ## 改成部署时， --acp2-namespaces 参数指定的值，默认是cpaas-system
	helm install stable/sonarqube \
	        --name sonarqube \
	        --set plugins.useDefaultPluginsPackage=true \
	        --set global.registry.address=$registry \
	        --namespace=${ACP_NAMESPACE} \
	        --set global.namespace=${ACP_NAMESPACE} \
	        --set service.type=NodePort \
	        --set service.nodePort=<node port 端口号，默认31342> \
	        --set postgresql.database.persistence.enabled=true \
	        --set postgresql.database.persistence.existingClaim=<pvc name>  ## 给 sonarqube 使用的 pvc 的名字
	```

* **参考命令（使用 storageClass）：**

	```
	registry=$(docker info |grep 60080  |tr -d ' ')
	ACP_NAMESPACE=<cpaas-system>     ## 改成部署时， --acp2-namespaces 参数指定的值，默认是cpaas-system
	helm install stable/sonarqube \
	        --name sonarqube \
	        --set plugins.useDefaultPluginsPackage=true \
	        --set global.registry.address=$registry \
	        --namespace=${ACP_NAMESPACE} \
	        --set global.namespace=${ACP_NAMESPACE} \
	        --set service.type=NodePort \
	        --set service.nodePort=<node port 端口号，默认31342> \
	        --set postgresql.database.persistence.enabled=true \
	        --set postgresql.database.persistence.storageClass=<storageclass name>  ## 给 sonarqube 使用的 storageclass 的名字
	```

* **参考命令（使用 hostpath）：**

	```
	registry=$(docker info |grep 60080  |tr -d ' ')
	ACP_NAMESPACE=<cpaas-system>     ## 改成部署时， --acp2-namespaces 参数指定的值，默认是cpaas-system
	helm install stable/sonarqube \
	        --name sonarqube \
	        --set plugins.useDefaultPluginsPackage=true \
	        --set global.registry.address=$registry \
	        --namespace=${ACP_NAMESPACE} \
	        --set global.namespace=${ACP_NAMESPACE} \
	        --set service.type=NodePort \
	        --set service.nodePort=<node port 端口号，默认31342> \
	        --set postgresql.database.persistence.enabled=false \
	        --set postgresql.database.persistence.host.nodeName=<node name> \
	        --set postgresql.database.persistence.host.path=<path >  ## 本地路径，例如：/cpaas/data/sonarqube
	```



#### Service Mesh （微服务治理平台）部署

**说明**：

* 微服务治理平台有单独的安装包，需要下载之后解压缩到 init 节点 /tmp 下，并运行解压缩之后的安装目录内的 upload-images-chart.sh 脚本上传安装包和 chart。

* 部署 Service Mesh 业务集群组件的时候，至少要有一个 slave 节点，且这个节点上不能部署 alb 和 ingress，即：不能有 `alb2=ture` 和 `ingress=true` 的标签。

* Service Mesh 依赖 ingress、alauda-cluster-base。

* `<>` 代表要根据环境情况替换尖括号内的值，替换完毕之后，不包括尖括号。

* 如果在部署 global 的时候，`--enabled-features` 这个参数没有加上  asm ，需要手动部署微服务治理平台的 globla 组件。


1. 在 global 集群的第一个 master 节点上	执行以下命令：

	```
	helm get values sentry >/tmp/sentry_values.yaml
	## 修改 /tmp/sentry_values.yaml ，将 platform.asm.install 的值改成 'true'
	helm upgrade sentry -f /tmp/sentry_values.yaml stable/sentry
	```

2. 在业务集群的 master 节点上执行如下操作：

	```
	kubectl label node <选定的 slave 的名字> istio=true
	```

部署成功之后，在界面上单击左上角产品菜单，能看到 Service Mesh 。单击进入，选择管理视图，开始部署  Service Mesh 的业务集群组件，步骤如下：

1. 提前收集以下几个信息：

	* ES【部署在 global 上的】 ，例如：

		地址：http://10.0.0.17:9200
		
		用户：alaudaes                ### 默认值
		
		密码：es_password_1qaz2wsx    ### 随机生成的值

		可以通过在 global 集群上的一个 master 节点上执行如下命令，获取密码

		```
		kubectl get deployments -n ${ACP_NAMESPACE} cpaas-elasticsearch -o yaml | grep ALAUDA_ES_PASSWORD -A 1 | grep value | awk '{print $NF}' 
		```

	* prometheus【部署在对应集群上的】 ，例如：

		地址：http://10.0.0.12:30900

	* ingress 地址【部署在对应集群上的】 ，获取方式：

		``` 
		cat /etc/kubernetes/kubelet.conf | grep server: | grep -oE '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' 
		```

	* IP 范围【对应集群的】 ，获取方式：

		```
		cat /etc/kubeadm/kubeadmcfg.yaml | grep podSubnet |awk -F : '{print $5 $4}'|tr -d 'a-z,A-Z}'|sed 's+ +\\,+g'|sed 's/^..//g'|sed 's/..$//g' 
		```

2. 在微服务治理平台页面的“管理视图”里创建“服务网格”，如下图：

	<img src="images/asm 部署-ui.png" style="zoom:50%;" />

	这里的“组件高可用”代表用多副本方式启动微服务治理平台组件。

	<img src="images/asm 部署，多副本选择.png" style="zoom:50%;" />

3. 在“管理视图”的“项目”里，启动 ns 的服务网格。

	<img src="images/asm 部署，启动 ns 的服务网格.png" style="zoom:50%;" />

	在“管理视图”的“健康检查”里可以看到各组件的状态即可，组件启动需要一段时间，请耐心等待。

	<img src="images/asm 部署-健康检查.png" style="zoom:50%;" />


#### Machine Learning（机器学习平台）

**说明**：

* 机器学习平台有单独的安装包，需要下载之后解压缩到 init 节点 `/tmp` 下，并运行解压缩之后的安装目录内的 upload-images-chart.sh 脚本上传安装包和 chart 。aml 有附加的镜像包，同样解压缩并执行upload-images-chart.sh 脚本上传。

* `<>` 代表要根据环境情况替换尖括号内的值，替换完毕之后，不包括尖括号。

##### global 下部署 global-aml

**说明**：如果在部署 global 的时候，`--enabled-features` 这个参数没有加上 aml ，需要手动部署机器学习平台的 globla 组件。

在 global 集群的第一个 master 节点上执行以下命令：

```
ACP_NAMESPACE=<cpaas-system>  ## 改成部署平台时， --acp2-namespaces 参数指定的值，默认是cpaas-system
DOMAIN_NAME=<域名>             ## 部署时，--domain-name 参数传入的值
LABELBASEDOMAIN='cpaas.io'
OIDC_PROTOCOL=https           ## 如果部署时，添加了--use-http 参数，将这个值改成 http
CHART_ENDPOINT=               ## 参考 init 节点的/cpaas/install_info 文件，定义这个变量的值


cat <<EOF >/tmp/apprelease-global-aml.yaml
apiVersion: operator.alauda.io/v1alpha1
kind: AppRelease
metadata:
  name: global-aml
  namespace: ${ACP_NAMESPACE}
spec:
  chart:
    name: global-aml
    repository: ${CHART_ENDPOINT}
    version: $(helm search | grep '^stable/global-aml ' | awk '{print $2}')
  values:
    global:
      tke: true
      namespace: ${ACP_NAMESPACE}
      host: "${DOMAIN_NAME}"
      registry:
        address: ${REGISTRY_ENDPOINT}
      labelBaseDomain: ${LABELBASEDOMAIN}
    alaudaConsole:
      apiAddress: ${OIDC_PROTOCOL}://${DOMAIN_NAME}
      oidcIssuerUrl: ${OIDC_PROTOCOL}://${DOMAIN_NAME}/dex
      oidcRedirectUrl: https://${DOMAIN_NAME}
  version: 0.0.9
EOF
kuberctl create -f /tmp/apprelease-global-aml.yaml
```

##### 业务集群下部署 aml-volcano

在 global 集群的第一个 master 节点上执行以下命令：

```
REGISTRY_ENDPOINT=$(docker info |grep 60080  |tr -d ' ')
REGION_NAME=<name>                  ## 想要部署 aml-volcano 的集群的名字
ACP_NAMESPACE=<cpaas-system>        ## 改成部署平台时， --acp2-namespaces 参数指定的值，默认是cpaas-system

mkdir /tmp/region_helmrequest
cat << EOF >/tmp/region_helmrequest/${REGION_NAME}-volcano.yaml
apiVersion: app.alauda.io/v1alpha1
kind: HelmRequest
metadata:
  finalizers:
  - captain.alauda.io
  generation: 1
  name: ${REGION_NAME}-volcano
  namespace: ${ACP_NAMESPACE}
spec:
  chart: stable/volcano
  namespace: ${ACP_NAMESPACE}
  releaseName: ${REGION_NAME}-volcano
  clusterName: ${REGION_NAME}
  values:
    global:
      namespace: ${ACP_NAMESPACE}
      registry:
        address: ${REGISTRY_ENDPOINT}
  version: $(helm search | grep '^stable/volcano ' | awk '{print $2}')

EOF
kubectl create -f /tmp/region_helmrequest/${REGION_NAME}-volcano.yaml
```

##### 业务集群下部署 aml-core

**说明**：部署 aml-core 依赖业务集群有 ingress，请按照本文档部署 ingress 的内容，部署 ingress。

在 global 集群的第一个 master 节点上执行以下命令：

```
REGISTRY_ENDPOINT=$(docker info |grep 60080  |tr -d ' ')
REGION_NAME=<name>                  ## 想要部署 aml-core 的集群的名字
ACP_NAMESPACE=<cpaas-system>        ## 改成部署平台时， --acp2-namespaces 参数指定的值，默认是cpaas-system
ingress_address=<1.1.1.1>           ## master 的地址，如果是多个 master，这就是 ingress vip 的地址

mkdir /tmp/region_helmrequest
cat << EOF >/tmp/region_helmrequest/${REGION_NAME}-aml-core.yaml
apiVersion: app.alauda.io/v1alpha1
kind: HelmRequest
metadata:
  finalizers:
  - captain.alauda.io
  generation: 1
  name: ${REGION_NAME}-aml-core
  namespace: ${ACP_NAMESPACE}
spec:
  chart: stable/aml-core
  namespace: ${ACP_NAMESPACE}
  releaseName: ${REGION_NAME}-aml-core
  clusterName: ${REGION_NAME}
  values:
    global:
      labelBaseDomain: cpaas.io
      namespace: ${ACP_NAMESPACE}
      registry:
        address: ${REGISTRY_ENDPOINT}
      host: ${ingress_address}
  version: $(helm search | grep '^stable/aml-core ' | awk '{print $2}')

EOF
kubectl create -f /tmp/region_helmrequest/${REGION_NAME}-aml-core.yaml
```

##### 业务集群下部署 aml-ambassador

在 global 集群的第一个 master 节点上执行以下命令：

```
REGISTRY_ENDPOINT=$(docker info |grep 60080  |tr -d ' ')
REGION_NAME=<name>                  ## 想要部署 aml-ambassador 的集群的名字
ACP_NAMESPACE=<cpaas-system>        ## 改成部署平台时， --acp2-namespaces 参数指定的值，默认是cpaas-system
ingress_address=<1.1.1.1>           ## master 的地址，如果是多个 master，这就是 ingress vip 的地址

mkdir /tmp/region_helmrequest
cat << EOF >/tmp/region_helmrequest/${REGION_NAME}-aml-ambassador.yaml
apiVersion: app.alauda.io/v1alpha1
kind: HelmRequest
metadata:
  finalizers:
  - captain.alauda.io
  generation: 1
  name: ${REGION_NAME}-aml-ambassador
  namespace: ${ACP_NAMESPACE}
spec:
  chart: stable/aml-ambassador
  namespace: ${ACP_NAMESPACE}
  releaseName: ${REGION_NAME}-aml-ambassador
  clusterName: ${REGION_NAME}
  values:
    global:
      namespace: ${ACP_NAMESPACE}
      registry:
        address: ${REGISTRY_ENDPOINT}
  version: $(helm search | grep '^stable/aml-ambassador ' | awk '{print $2}')

EOF
kubectl create -f /tmp/region_helmrequest/${REGION_NAME}-aml-ambassador.yaml
```

#### API Management Platform（API 管理平台）

**说明**：v2.8 支持的 amp 1.5 独立发版，部署方式稍后提供。

### 部署 CSP （ceph）

CSP 可以支持三种存储模式：对象存储、ceph rbd（块存储）、cephfs（文件存储），一个主机选做了三种存储模式中一种，则无法复用其余角色。

**说明**：默认部署会修改 hostname 为 ip，由于 cephrbd 不支持多 Pod 挂载读写，cephfs 支持，所以容器平台默认支持 cephfs 模式。

**主机需求**

* 对象存储：需要 4 个 4C8G 的主机，1 个 installer 节点，3 个数据节点（暂不支持在 init 节点和 global 节点部署 ceph，需要额外的机器）。 
	
* ceph rbd（块存储） ：需要 4 个 4C8G 的主机，1 个 installer 节点，3 个数据节点（暂不支持在 init 节点和 global 节点部署 ceph，需要额外的机器）。
	
* cephfs（文件存储）：需要 4 个 4C8G 的主机，1 个 installer 节点，3 个数据节点（暂不支持在 init 节点和 global 节点部署 ceph，需要额外的机器）。

**磁盘需求**

每个数据节点必须要有单独的外挂盘做 ceph 的数据存储，磁盘的大小根据资源使用规划即可（最低50G）。


**部署步骤**：

1. 准备安装包，ceph 的安装包在我们的压缩包里，具体路径为解压后的安装包路径下的 `/other/ceph` 下。

	```
	[root@init ceph]# pwd
	/<安装目录>/other/ceph
	[root@init ceph]# ls
	cephfs.yaml ceph-rbd.yaml CSP-V2.4.10.288.tar.gz
	cephfs.zip ceph-storageclass.yaml rbd
	```

2. 通过 `scp` 命令将 `CSP-V2.4.10.288.tar.gz` 拷贝到准备好的 installer 节点上。

3. 解压安装包，并部署。

	```
	tar zxvf CSP-V2.4.10.288.tar.gz
	cd csp-pkg-2.4.10.288/
	systemctl stop ntpd  && systemctl disable ntpd 
	./cspcmd install --controller 10.0.128.135             ####需要将10.0.128.135这个ip换为本机的实际ip
	```

​	等待部署完成之后，会显示访问地址以及端口，一般为 `IP+8089 端口` 。

4. 修改配置文件，支持页面创建 cephfs 存储池。

	```
	sed -i 's/FILE_STORAGE: false/FILE_STORAGE: true/g' /data/csp-ins/current/csp-console/server/config/index.js
	systemctl restart csp-console.service
	```

5. 通过刚才部署时候的命令传入的 `ip+8089` 访问页面，用户名和密码默认为：admin/admin。

6. 创建集群。

	输入集群名称，输入集群名称单击【下一步】，NTP Server IP 默认 `127.0.0.1 `如下图：

	<img src="images/ceph 4.1 创建集群.png" style="zoom:50%;" />

7. 添加主机。

	* 添加主机 ip。
	
	* 填写节点序列号，按顺序填 SN1、SN2、SN3 填即可。
	
	* 数据中心统一填 dc1。
	
	* 机架位置统一填 rack1。
	
	* 输入存储服务器 root 密码。
	
	* 输入ssh 端口   如下图：

		<img src="images/ceph 4.2 添加主机.png" style="zoom:50%;" />
		
	* 单击【提交】按钮，等待添加主机完成操作，需等待 3 分钟，如下图：
	
		<img src="images/ceph 4.2 提交.png" style="zoom:50%;" />
	
8. 主机分配。

	分配主机：勾选你要配置的主机 ip ，有且仅需要 3 台，单击【完成】后创建集群，存储集群主机初始化完成，如下图：
	
	<img src="images/ceph 5 分配主机.png" style="zoom:50%;" />


9. 创建 cephfs 存储池，单击 **文件存储->元数据节点->创建存储池**。

	* 填写存储池数据。

	* 选择存储节点 ip。
  	
	* 本次部署选择的为极简模式，需自己分配数据盘（高级模式）。

	* 选择 **3 副本** 冗余策略。<br>**说明**：默认是 3 副本冗余策略。

	* 选择数据安全级别节点。<br>**注意**：存储节点分配，全选即可（操作系统盘不能用作对象存储池，系统做了屏蔽，因此在该页面中，看不到机器的系统盘），如下图所示。

		<img src="images/ceph 6 创建存储池.png" style="zoom:50%;" />

	* 单击 **确定** 完成对象存储池的创建。<br>预计等待 2 分钟,等待创建完成，创建完成之后对接即可。


### 对接 ceph 集群

1. 在准备对接的集群所有节点安装 ceph 客户端。

	```
	yum install  ceph-common ceph-fuse  -y
	```
  
2. 腾讯的 ceph 集群默认为 none 模式，没有开启认证，通过配置文件获得 monitor 节点的配置。

	**说明**：需要去腾讯的 ceph 集群的某个 data 节点操作。
	
	```
	conf=$(ps -fe | grep CLUSTERMON.*.conf | grep ceph-mon | awk '{print $(NF-2)}')
	cat $conf | grep mon_addr           ##获得monitor节点的ip以及端口
	mon_addr = 10.0.129.175:6789
	mon_addr = 10.0.129.8:6791
	mon_addr = 10.0.129.73:6790
	```

3. 准备好cephfs 压缩包，将压缩包拷贝到某台 master 机器的 `/root` 下，并解压。

	这个压缩包在init节点的安装包路径下的 other 目录，`/<安装目录>/other/ceph/cephfs.zip`。
	
	```scp cephfs.zip  root@1.1.1.1:/root/   ##需要将1.1.1.1换为业务集群的某台master机器的ip地址。```
  
4. 去刚传完文件的 master 的 root 下执行以下命令行：

	  ```
	  mv cephfs /tmp        ###为了防止当前目录下存在同名目录
	  unzip cephfs.zip
	  cd cephfs/
	```

5. 将 `deployment.yaml` 中的 image 地址更换为自己环境的 image 地址。<br>例如：`10.0.128.173:60080/claas/cephfs-provisioner:latest`。

6. 创建 ns：```kubectl create ns cephfs```

	**说明**：若为 ocp 集群，则需要使用如下 yaml 创建 namespace。

	  ```
	  cat <<EOF >/root/cephfs/ns.yaml
	  apiVersion: v1
	  kind: Namespace
	  metadata:
	    annotations:
	      openshift.io/sa.scc.supplemental-groups: 1000000000/0
	      openshift.io/sa.scc.uid-range: 1000000000/0
	    name: cephfs
	  spec:
	    finalizers:
	    - kubernetes
	  status:
	    phase: Active
	  EOF
	  kubectl create -f /root/cephfs/ns.yaml
	  ```

7. 创建相关资源。

	  ``` 
	  NAMESPACE=cephfs
	  sed -r -i "s/namespace: [^ ]+/namespace: $NAMESPACE/g" /root/cephfs/*.yaml
	  sed -r -i "N;s/(name: PROVISIONER_SECRET_NAMESPACE.*\n[[:space:]]*)value:.*/\1value: $NAMESPACE/" /root/cephfs/deployment.yaml
	  kubectl apply -f /root/cephfs -n $NAMESPACE
	```

8. 创建 storageclass。

	```
	cat <<EOF > cephfs-storageclass.yaml
	kind: StorageClass
	apiVersion: storage.k8s.io/v1
	metadata:
	  name: cephfs
	  selfLink: /apis/storage.k8s.io/v1/storageclasses/cephfs
	  uid: 198bcd73-a1ff-11e8-aafa-5254004c757f
	  resourceVersion: '11786274'
	  annotations:
	    ceph.type: cephfs
	provisioner: ceph.com/cephfs                          
	parameters:
	  adminId: admin
	  adminSecretName: cephfs-admin-secret
	  adminSecretNamespace: kube-system
	  claimRoot: /volumes/kubernetes
	  monitors: '10.0.129.175:6789,10.0.129.8:6791,10.0.129.73:6790'         ## 需要修改为第2步获得ceph集群的monitors的IP地址和端口
	  reclaimPolicy: Delete
	  volumeBindingMode: Immediate
	  EOF
	    
	    
	kubectl create -f cephfs-storageclass.yaml
	```

9. 定义管理员密码。

	  ```
	echo "AQCYfAxdbXTwCRAAnz9MvJgO3KselABH2OoKOA==" > /tmp/secret 
	kubectl create secret generic cephfs-admin-secret --from-file=/tmp/secret --namespace=kube-system
	kubectl create secret generic cephfs-secret-user --from-file=/tmp/secret
	```

10. 创建 PVC 验证。

	  ```
	cat <<EOF > cephfs.yaml
	    
	kind: PersistentVolumeClaim
	apiVersion: v1
	metadata:
	  name: claim1
	  namespace: default
	  annotations:
	    volume.beta.kubernetes.io/storage-class: cephfs
	    volume.beta.kubernetes.io/storage-provisioner: ceph.com/cephfs
	  finalizers:
	    - kubernetes.io/pvc-protection
	spec:
	  accessModes:
	    - ReadWriteMany
	  resources:
	    requests:
	      storage: 1Gi
	  volumeMode: Filesystem
	EOF
	    
	kubectl create -f cephfs.yaml
	```

11. 检查对接是否成功。

    ```  
    kubectl get pvc      ###查看刚创建的pvc为bound状态即成功
    NAME     STATUS   VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS   AGE
    claim1   Bound    pvc-b480aa29-a142-11e9-9d43-525400d63265   1Gi        RWX            cephfs         19m
    ```

### 安装 cpaas-fit 与 tsf 对接

在 global 集群的第一个 master 节点上执行以下命令：

```
REGISTRY_ENDPOINT=$(docker info |grep 60080  |tr -d ' ')
ACP_NAMESPACE=<cpaas-system>      ## 改成部署平台时， --acp2-namespaces 参数指定的值，默认是cpaas-system
DOMAIN_NAME=<vip or domainname>   ## 需要修改为 global 界面的访问地址，也就是部署 global 的时候，--domain-name 参数的值
https_or_http=<https>                          ## 默认值是 https，如果部署的时候使用了--use-http 参数，就需要改成 http
TSF_ENDPOINT=<tsf 的访问地址>          ## 部署完 tsf 后访问他的地方，例如http://10.0.129.145

kubectl captain create \
           --version $(helm search | grep '^stable/cpaas-fit ' | awk '{print $2}') \
           --namespace=${ACP_NAMESPACE} \
           --chart=stable/cpaas-fit \
           --set global.namespaces=${ACP_NAMESPACE} \
           --set global.registry.address=${REGISTRY_ENDPOINT} \
           --set envs.tsf_endpoint=${TSF_ENDPOINT} \
           --set envs.dex_issuer=${https_or_http}://${DOMAIN_NAME}/dex \
           --set envs.redirect_uri=${https_or_http}://${DOMAIN_NAME}/fit/tsf
           cpaas-fit
```


 <div STYLE="page-break-after: always;"></div>
# 第五部分 平台配置及测试

## 检查部署是否成功

###  检查 global 平台是否部署成功

**说明**：global 组件通过 helm 和 captain 来部署，生成的资源全部都存放在运行 global 组件的 Kubernetes 集群内。

在运行 global 组件的 Kubernetes 集群的 master 节点上执行以下命令：

```
## 检查 helm 部署的 chart 是否成功，执行如下命令查找部署失败的 chart 
helm list --failed  

## 检查 sentry 部署的 chart 是否成功，执行如下命令查找部署失败的 chart
kubectl get apprelease --all-namespaces '


## 检查 所有 pod 是否正常，执行如下命令查找失败的 pod
kubectl get pod --all-namespaces | awk '{if ($4 != "Running" && $4 != "Completed")print}' | awk -F'[/ ]+' '{if ($3 != $4)print}'
```

###  检查业务服务集群是否部署成功

**说明**：参考运维手册中 sentry 运维部分，检查 chart 是否部署成功。


## 平台访问地址及登录用户名、密码，以及兼容的浏览器

### 平台访问地址

**all in one 部署方案** 和 **poc 部署方案** 中没有使用 --domain-name 参数的平台的访问地址：

* 没有使用 `--use-http` 参数的平台访问地址：`https://<init 节点 ip>` 
* 使用了 `--use-http` 参数的平台访问地址：`http://<init 节点 ip>`

**正式生产环境部署方案** 和 **poc 部署方案** 中使用了 --domain-name 参数的平台的访问地址：

* 没有使用 `--use-http` 参数的平台访问地址：`https://<--domain-name 参数指定的地址>` 
* 使用了 `--use-http` 参数的平台访问地址：`http://<--domain-name 参数指定的地址>`

### 访问产品路径

***部署成功之后，访问平台，登录之后点击左上角的产品下拉菜单（九宫格图标）选择访问某个产品***
***也可以在登录之后，平台地址后加路径直接访问各个产品，例如：***

* acp <平台访问地址>/console-acp
* devops <平台访问地址>/console-devops
* asm <平台访问地址>/console-asm
* aml <平台访问地址>/console-aml
* 平台成熟度设置（alpha 功能开关设置） <平台访问地址>/console-platform/#/home/feature-gate

### 平台登录权限

用户名：`--root-username` 参数指定的值，默认为 admin@cpaas.io。

默认密码：password

### 兼容的浏览器版本

**仅兼容 Chrome 浏览器，且是发版时最近的 3 个版本之一**。


## 部署成功后操作

### 备份 etcd 数据

1. 拷贝安装目录下的 backup_recovery.sh 和 function.sh 脚本到每一个 master 服务器的 `/tmp` 下。

2. 在每一台 master 节点上执行以下命令：

	```
	cd /tmp
	bash backup_recovery.sh run_function=back_k8s_file
	```

## 平台功能测试

请参考《TKE for Alauda Container Platfrom POC 测试用例.docx》。

## 添加监控、告警

<div STYLE="page-break-after: always;"></div>
# 第六部分 FAQ

## 平台部署错误

### 安装 chart 失败

**错误处理流程**：参考运维手册处理 helm 和 sentry 部署 chart 失败的情况。

## 平台功能错误

请参考《TKE for Alauda Container Platfrom 运维手册》。







 