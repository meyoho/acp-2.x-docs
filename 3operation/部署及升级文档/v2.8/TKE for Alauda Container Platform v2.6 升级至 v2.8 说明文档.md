#  目录

1. 关于本文档
	* 文档受众
	* 文档目的
	* 修订记录
2. 升级过程
	* 升级前准备
	* 平台升级
	* 集群升级
	* 回滚

<div STYLE="page-break-after: always;"></div>
# 第一部分  前言

本文档介绍了如何将平台版本从 v2.6 升级到 v2.8。

## 读者对象

本文档适用于具备基本的 linux、容器、Kubernetes 知识，想要安装和配置平台的用户。包括：

* 实施顾问和平台管理员

* 对平台进行维护的运维人员

* 负责整个项目生命周期的项目经理

## 文档目的

平台运维工程师依据本文档升级平台版本。


## 修订记录

| 文档版本 | 发布日期   | 修订内容                                      |
| -------- | ---------- | --------------------------------------------- |
| v1.0   | 2020-1-6 | 第一版，适用于 TKE for Alauda Container Platform 私有部署版本的 v2.6 升级至 v2.8。 |
| v1.1 | 2020-2-21| 第二版，增加注释，补充描述不清晰的地方|
| v1.2 | 2020-03-02 | 第三版，更新  |




<div STYLE="page-break-after: always;"></div>
# 第二部分  升级过程

## 升级前准备

### 备份数据

* 执行位置：global 集群的 master 和业务服务集群的 master 节点。
* 操作步骤：首先将安装目录下的 “backup_recovery.sh” 脚本复制到 master 节点上。
* 执行的命令： 
	
	```
	mkdir -p /cpaas/backup && \
	cp -r /var/lib/etcd/ /cpaas/backup && \
	cp -r /etc/kubernetes/ /cpaas/backup
	
	bash ./backup_recovery.sh run_function=back_k8s_file
	```
* 命令作用：备份集群的 etcd


## 平台升级

### 下载 v2.8 的安装包

* 执行位置：init 节点
* 操作步骤：下载安装包并解压缩

### 上传 v2.8 的镜像和 chart 到 init 节点的私有仓库和 chart repo 内

* 执行位置：init 节点
* 操作步骤：进入 v2.8 的安装目录，执行 `bash ./upload-images-chart.sh`

**注意**：执行过程需要一个小时左右，视 init 服务器 cpu 和硬盘速度不同，会缩短或延迟，注意安装目录所在分区的空间，上传之前至少要保证 10G 的空余空间。

### 更新 kaldr

* 执行位置：init 节点
* 操作步骤：执行如下命令

	```
	mkdir /cpaas
	REGISTRY_ENDPOINT=$(docker info |grep 60080  |tr -d ' ')


	for i in $(curl ${REGISTRY_ENDPOINT}/v2/alaudaorg/kaldr/tags/list 2>/dev/null | jq '.tags' | sed -e '1d' -e '$d' -e 's/"//g' -e 's/,//g')
	do
	  docker pull ${REGISTRY_ENDPOINT}/alaudaorg/kaldr:$i >/dev/null 2>&1
	  echo $i $(date -d $(docker inspect ${REGISTRY_ENDPOINT}/alaudaorg/kaldr:$i -f '{{.Created}}') +%s)
	done | sort -k2n,2 | awk 'END{print $1}' > /tmp/dd
	
	
	KALDR_IMAGE=${REGISTRY_ENDPOINT}/alaudaorg/kaldr:$(cat /tmp/dd)
	docker rm -f yum
	mv -f /cpaas/.run_kaldr.sh /cpaas/.run_kaldr.sh.old
	cat /cpaas/.run_kaldr.sh.old | awk '{for(i=1;i<NF;i++){printf $i" "}print "'$KALDR_IMAGE'"}' >/cpaas/.run_kaldr.sh
	bash /cpaas/.run_kaldr.sh
	```

	执行完毕后，yum 容器会被重建，请检查是否重建。


### 停掉认证组件的 pod

* 执行位置：global 集群的第一个 master 上
* 操作步骤：执行如下命令
	```
	mkdir /cpaas/backup
	ACP_NAMESPACE=<alauda-system>     # ns
	kubectl get cm -n ${ACP_NAMESPACE} dex-configmap -o yaml > /cpaas/backup/dex-configmap.yaml.new
	kubectl scale deployment -n ${ACP_NAMESPACE} auth-controller2 dex --replicas=0
	```


### 创建 sentry_values.yaml 及 cm

* 执行位置：global 集群的第一个 master 上
* 操作步骤：执行如下命令
	
```
#######  更新 helm 获取变量

helm up
helm get values dex --output json >/tmp/values.json

########   准备变量
es_install=<true>                # 如果不安装 es ，对接客户的，改成false
kafka_install=<true>             # 如果不安装 es ，对接客户的，改成false
GLOBAL_REGION_NAME=<global>      # global集群的命令，默认是 global
LABELBASEDOMAIN=$(cat /tmp/values.json | jq .global.labelBaseDomain | sed 's/"//g') ##注意，要检查这个变量是否为空，如果是空那就是从2.3 甚至更早的版本升级上来的，这个变量的值可以执行  kubectl get cm -n $(cat /tmp/values.json | jq .global.namespace | sed 's/"//g') ui-config -o json | jq .data.LABEL_BASE_DOMAIN 命令来查看
NORTROM_TOKEN=$(kubectl describe secret -n kube-system $(kubectl get secret -n kube-system | awk '/clusterrole-aggregation-controller-token/ {print $1}') | awk '/^token:/{print $2}')

############## 生成 yaml

cat <<EOF >/cpaas/sentry_values.yaml
global:
  host: $(cat /tmp/values.json | jq .alaudaConsole.apiAddress | sed -e 's#^.*://##' -e 's/"//g')
  region: ${GLOBAL_REGION_NAME}
  scheme: $(cat /tmp/values.json | jq .alaudaConsole.oidcProtocolOverride | sed 's/"//g')
  auth:
    defaultAdmin: $(cat /tmp/values.json | jq .global.auth.default_admin | sed 's/"//g')
  namespace: $(cat /tmp/values.json | jq .global.namespace | sed 's/"//g')
  replicas: $(cat /tmp/values.json | jq .global.replicas | sed 's/"//g')
  registry:
    address: $(cat /tmp/values.json | jq .global.registry.address | sed 's/"//g')
  labelBaseDomain: ${LABELBASEDOMAIN}
args:
  workers: 2
charts:
  repository: $(cat /tmp/values.json | jq .furion.chartmuseumUrl | sed 's/"//g')
  versions:
    cpaasAppreleases: $(helm search | grep '^stable/cpaas-appreleases ' | awk '{print $2}')
    dex: $(helm search | grep '^stable/dex ' | awk '{print $2}')
    alaudaBase: $(helm search | grep '^stable/alauda-base ' | awk '{print $2}')
    nginxIngress: $(helm search | grep '^stable/nginx-ingress ' | awk '{print $2}')
    acp: $(helm search | grep '^stable/alauda-container-platform ' | awk '{print $2}')
    certManager: $(helm search | grep '^stable/cert-manager ' | awk '{print $2}')
    tke: $(helm search | grep '^stable/tke ' | awk '{print $2}')
    kafkaZookeeper: $(helm search | grep '^stable/kafka-zookeeper ' | awk '{print $2}')
    elasticsearch: $(helm search | grep '^stable/elasticsearch ' | awk '{print $2}')
    devops: $(helm search | grep '^stable/alauda-devops ' | awk '{print $2}')
    asm: $(helm search | grep '^stable/global-asm ' | awk '{print $2}')
    clusterBase: $(helm search | grep '^stable/alauda-cluster-base ' | awk '{print $2}')
    kubePrometheus: $(helm search | grep '^stable/kube-prometheus ' | awk '{print $2}')
    prometheusOperator: $(helm search | grep '^stable/prometheus-operator ' | awk '{print $2}')
    logAgent: $(helm search | grep '^stable/alauda-log-agent ' | awk '{print $2}')
    asmInit: $(helm search | grep '^stable/asm-init ' | awk '{print $2}')
    cpaasMonitor: $(helm search | grep '^stable/cpaas-monitor ' | awk '{print $2}')
    
platform:
  protocolOverride: $(cat /tmp/values.json | jq .alaudaConsole.oidcProtocolOverride | sed 's/"//g')
  install: true
  kubefedVersion: $(helm search | grep '^stable/kubefed ' | awk '{print $2}')
  secret:
    customerCrt: "$(cat /etc/kubernetes/pki/cert.pem | base64 -w 0)"
    customerKey: "$(cat /etc/kubernetes/pki/key.pem | base64 -w 0)"
    apiserverCrt: "$(cat /etc/kubernetes/pki/apiserver.crt | base64 -w 0)"
    apiserverKey: "$(cat /etc/kubernetes/pki/apiserver.key | base64 -w 0)"
    etcdCaCrt: "$(cat /etc/kubernetes/pki/etcd/ca.crt | base64 -w 0)"
    etcdCaKey: "$(cat /etc/kubernetes/pki/etcd/ca.key | base64 -w 0)"
    etcdPeerCrt: "$(cat /etc/kubernetes/pki/etcd/peer.crt | base64 -w 0)"
    etcdPeerKey: "$(cat /etc/kubernetes/pki/etcd/peer.key | base64 -w 0)"
    etcdServerCrt: "$(cat /etc/kubernetes/pki/etcd/server.crt | base64 -w 0)"
    etcdServerKey: "$(cat /etc/kubernetes/pki/etcd/server.key | base64 -w 0)"
    frontProxyClientCrt: "$(cat /etc/kubernetes/pki/front-proxy-client.crt | base64 -w 0)"
    frontProxyClientKey: "$(cat /etc/kubernetes/pki/front-proxy-client.key | base64 -w 0)"
    frontProxyCaCrt: "$(cat /etc/kubernetes/pki/front-proxy-ca.crt | base64 -w 0)"
    frontProxyCaKey: "$(cat /etc/kubernetes/pki/front-proxy-ca.key | base64 -w 0)"

  supportedK8sVersions: v1.13,v1.14,v1.16

  log:
    nodes: $(cat /tmp/values.json | jq .kafka.zk_host | sed 's/"//g')

  elasticsearch:
    install: ${es_install}
    host: $(cat /tmp/values.json | jq .nortrom.esHost | sed 's/"//g')
    username: $(cat /tmp/values.json | jq .elasticsearch.es_user_64 | sed 's/"//g' | base64 -d)
    singleNode: $(cat /tmp/values.json | jq .elasticsearch.singlenode)


  kafka:
    install: ${kafka_install}
    host: $(cat /tmp/values.json | jq .nortrom.kafkaHost | sed 's/"//g')
    haSwitch: $(cat /tmp/values.json | jq .kafka.ha_switch)


  acp:
    install: false

  devops:
    install: false
    etcdServer: https://etcd.kube-system:2379,https://etcd.kube-system:2379,https://etcd.kube-system:2379
# 如果是单点部署，改成  etcdServer: https://etcd.kube-system:2379

  asm:
    install: false
  asmInit:
    mode: global

  captain:
    install: false

clusterPlatform:
  install: true

  logAgent:
    install: true
    region: ${GLOBAL_REGION_NAME}
    apiGatewayUrl: /v4/callback/logs
    token: ${NORTROM_TOKEN}
    isOCP: false
    containerEngine: docker

  kubePrometheus:
    install: true
    serviceType: NodePort
    localVolumePath: /cpaas/monitoring
    storageClassName:
    alertmanagerReceiverName: default-receiver
    alertmanagerReceiverWebhookSendResolved: false
    platform: ACP

  prometheusOperator:
    install: true

  cpaasMonitor:
    install: true
    prometheusName: kube-prometheus
    adminNotification:
EOF
```

***注：*** 执行完毕之后，检查 sentry_values.yaml 确保没有key 的值未更新，如果平台是通过 ip 而不是域名访问，global.host 的值可能为空，把这个值改成部署时，--domain-name 参数指定的值，然后执行如下命令创建 cm

```
cp /cpaas/sentry_values.yaml /tmp/values.yaml
kubectl delete configmap acp-config -n $(cat /tmp/values.json | jq .global.namespace | sed 's/"//g')
kubectl create configmap acp-config -n $(cat /tmp/values.json | jq .global.namespace | sed 's/"//g') --from-file /tmp/values.yaml
```

### 创建 acp-secret.yaml 及 secret

* 执行位置：global 集群的第一个 master 上
* 操作步骤：执行如下命令


```
### 生成 zk kafka 的变量
## 定义及生成 kafka zookeeper 密码
    ZK_USER=$(</dev/urandom tr -dc a-z | head -c10 | sed 's/[^a-z]//g')
    ZK_USER_64=$(echo -n ${ZK_USER} | base64)
    ZK_PASSWORD=$(</dev/urandom tr -dc A-Z-a-z | head -c15 | sed 's/[^a-zA-Z]//g')
    ZK_PASSWORD_64=$(echo -n ${ZK_PASSWORD} | base64)
    
### 生成 yaml
cat <<EOO >/cpaas/acp-secret.yaml
apiVersion: v1
kind: Secret
metadata:
  labels:
    name: acp-config-secret
  name: acp-config-secret
  namespace: $(cat /tmp/values.json | jq .global.namespace | sed 's/"//g')
type: Opaque
data:
  ZK_USER: ${ZK_USER_64}
  ZK_PASSWORD: ${ZK_PASSWORD_64}
  KAFKA_USER: ${ZK_USER_64}
  KAFKA_PASSWORD: ${ZK_PASSWORD_64}
  DB_PORT: ${DB_PORT_64}
  DB_PASSWORD: ${DB_PASSWORD_64}
  DB_USER: ${DB_USER_64}
  DB_HOST: ${DB_HOST_64}
  DB_ENGINE: ${DB_ENGINE_64}
  ES_USER: $(cat /tmp/values.json | jq .elasticsearch.es_user_64 | sed 's/"//g')
  ES_PASSWORD: $(cat /tmp/values.json | jq .elasticsearch.es_passwd_64 | sed 's/"//g')
EOO
kubectl delete -f /cpaas/acp-secret.yaml
kubectl create -f /cpaas/acp-secret.yaml --validate=false
```

### 升级 captain

**注：** 在 cpaas 2.8 发版时，Captain 去除了 cert-manager 的依赖，改用直接用 yaml 部署。已经用 helm 部署过的 Captain , 如果直接删除的话，会导致相关CRD及CR被删。所以需要手工清理掉相关资源并且用最新的方式安装 Captain 的文档。

* 执行位置：global 集群的第一个 master 上
* 操作步骤：执行如下命令清理

```
ACP_NAMESPACE=$(cat /tmp/values.json | jq .global.namespace | sed 's/"//g')
kubectl delete mutatingwebhookconfigurations.admissionregistration.k8s.io captain
kubectl delete validatingwebhookconfigurations.admissionregistration.k8s.io captain
kubectl delete svc -n ${ACP_NAMESPACE} captain
kubectl delete issuer captain-selfsigned-issuer -n ${ACP_NAMESPACE}
kubectl delete cert captain-serving-cert -n ${ACP_NAMESPACE}
kubectl delete deploy captain -n ${ACP_NAMESPACE}
kubectl delete secret captain-webhook-cert -n ${ACP_NAMESPACE}
# (可能没有，出错可以忽略)
kubectl get cm -n kube-system |grep ^captain | awk '{print $1}' | xargs kubectl delete cm -n kube-system
```

**注：** 通过 yaml 部署新的 captain 

* 首先，从2.8 的安装目录中，将captain-deploy.yaml 文件拷贝到 global 第一个 master 的/tmp 目录下，然后执行如下命令：

```
sed -i -e "s/captain-system/${ACP_NAMESPACE}/g" -e "s/index.alauda.cn/$(cat /tmp/values.json | jq .global.registry.address | sed 's/"//g')/g" /tmp/captain-deploy.yaml

kubectl apply --validate=false -n ${ACP_NAMESPACE} -f /tmp/captain-deploy.yaml
```
* 检查，在 global 集群的第一个 master 上，执行如下命令，如果发现 captain 的deploy 是刚刚创建不就，且状态正常，就说明升级成功

`kubectl get deploy -n ${ACP_NAMESPACE} | grep capta`


### 升级 global 及业务集群 **注：必须按照下面的步骤，顺序执行**

#### 第一步 停掉 captain 

* 执行位置：global 集群的第一个 master 上
* 操作步骤：执行如下命令

```
kubectl scale deploy -n $(cat /tmp/values.json | jq .global.namespace | sed 's/"//g') captain-controller-manager --replicas=0

### 执行 如下命令检查 deploy 的副本数是否改成0 ，captain 的 pod 是否全部停掉
kubectl get deploy -n $(cat /tmp/values.json | jq .global.namespace | sed 's/"//g')  | grep captain
kubectl get pod -n $(cat /tmp/values.json | jq .global.namespace | sed 's/"//g')  | grep captain
```

#### 第二步  删除 cert-manager 资源

* 执行位置：global 集群的第一个 master 上
* 操作步骤：执行如下命令

```
kubectl delete deploy cert-manager cert-manager-cainjector cert-manager-webhook -n cert-manager 
kubectl delete svc cert-manager cert-manager-webhook -n cert-manager

kubectl delete MutatingWebhookConfiguration cert-manager-webhook
kubectl delete ValidatingWebhookConfiguration cert-manager-webhook

kubectl delete apiservices v1beta1.admission.certmanager.k8s.io
```

#### 第三步 安装 sentry ，global 组件中的 alauda-base、dex、cpaas-monitor、cert-manager、elasticsearch、kafka-zookeeper ，升级业务集群组件中的cluster-base、nevermore、nginx-ingress、prometheus-operator、kube-prometheus 通过安装 sentry 会更新成新版本

* 执行位置：global 集群的第一个 master 上
* 操作步骤：执行如下命令

```
ACP_NAMESPACE=$(cat /tmp/values.json | jq .global.namespace | sed 's/"//g')

kubectl scale deployment -n ${ACP_NAMESPACE} cpaas-kafka --replicas=0
## 然后去 kafka pod 运行的节点（即有log=true 标签的节点）删掉kafka 的目录，执行 rm -rf /cpaas/data/kafka*
cat <<EOF > /cpaas/.run_install_sentry.sh
helm install --name sentry \\
     -f /cpaas/sentry_values.yaml \\
     --debug --wait --timeout 3000 \\
     --namespace=${ACP_NAMESPACE} \\
     stable/sentry

EOF

bash /cpaas/.run_install_sentry.sh
```
然后删掉 nortrom 的 ingress，**如果不删掉会造成审计数据流入错误的地址，审计无法使用**

* 检查

执行`kubectl get appreleases -n ${ACP_NAMESPACE}` 查看上述 chart 名字的 apprelease 资源状态是否是deployed ，版本是否是 v2.8.x

#### 第四步，删掉 helm 的 cm

* 执行位置：global 集群的第一个 master 上
* 操作步骤：执行如下命令

```
kubectl delete cm -n kube-system $(kubectl get cm -n kube-system | grep -E '^cert-manager.v|^dex.v|^kube-prometheus.v|^prometheus-operator.v' | awk '{print $1}')
```
	
* 检查，执行如下命令：
	
	`helm list -a | grep -E '^cert-manager|^dex|^kube-prometheus|^prometheus-operator' `
	
	找不到上述 chart 就是成功了
	
#### 第五步，保存并删除 mutating webhook

* 执行位置：global 集群的第一个 master 上
* 操作步骤：执行如下命令

```
kubectl get mutatingwebhookconfigurations captain-mutating-webhook-configuration -o yaml > /cpaas/m.yaml
kubectl get validatingwebhookconfigurations captain-validating-webhook-configuration -o yaml > /cpaas/v.yaml
kubectl delete mutatingwebhookconfigurations captain-mutating-webhook-configuration
kubectl delete validatingwebhookconfigurations captain-validating-webhook-configuration
```

#### 第六步，删掉 alauda-base、cpaas-monitor、elasticsearch、kafka-zookeeper、alauda-cluster-base、alauda-log-agent、nginx-ingress 和 tke的 helmrequest 和 release 资源

* 执行位置：global 集群的第一个 master 上
* 操作步骤：执行如下命令

```
for i in $(kubectl get hr -n ${ACP_NAMESPACE} | grep -E '^alauda-base|^cpaas-monitor|^elasticsearch|^kafka-zookeeper|^alauda-cluster-base|^alauda-log-agent|^nginx-ingress|^tke' | awk '{print $1}')
do
    kubectl patch hr $i -n ${ACP_NAMESPACE} -p '{"metadata":{"finalizers": []}}' --type=merge
    sleep 3
    kubectl get rel -n ${ACP_NAMESPACE} | grep ^$(kubectl get hr -n ${ACP_NAMESPACE} $i -o jsonpath='{.spec.releaseName}').v | awk '{print $1}' | xargs kubectl delete rel -n ${ACP_NAMESPACE}
    kubectl delete hr -n ${ACP_NAMESPACE} $i
done
```

* 检查，执行如下命令：
	
	`kubectl get hr -n ${ACP_NAMESPACE} | grep -E '^alauda-base|^cpaas-monitor|^elasticsearch|^kafka-zookeeper|^alauda-cluster-base|^alauda-log-agent|^nginx-ingress|^tke'`

如果找不到上述 chart 的 hr 资源，就证明删除 hr 资源成功


#### 第七步，升级 acp、 devops 、asm 的 global 组件

* 执行位置：global 集群的第一个 master 上
* 操作步骤：执行如下命令

```
## 获取变量
helm get values sentry >/cpaas/sentry_values.yaml
## 编辑变量文件
vim /cpaas/sentry_values.yaml
## platform.acp.install 的值是 true，代表安装 acp，以此类推，asm devops 等 chart 的安装就是把相应变量的值改成 true
platform:
  acp:
    install: true
  asm:
    install: true
  asmInit:
    mode: global
  devops:
    install: true
## 更新 sentry
helm upgrade -f /cpaas/sentry_values.yaml sentry stable/sentry
```
	
* 检查：执行`kubectl get appreleases -n ${ACP_NAMESPACE}` 检查是否存在名为alauda-container-platform、alauda-devops、global-asm 和 asm-init 这几个 apprelease 资源，资源状态是否是deployed ，版本是否是 v2.8.x

#### 第八步，删掉 alauda-container-platform、alauda-devops、global-asm 和 asm-init 的 helmrequest 和 release 资源

* 执行位置：global 集群的第一个 master 上
* 操作步骤：执行如下命令

***注：*** 一定保证上一步已经升级了这四个 chart ，如果上一步没升级，这一步直接复制粘贴执行命令，等到恢复 captain 之后，没升级的 chart 就会被删掉，造成数据丢失

```
for i in $(kubectl get hr -n ${ACP_NAMESPACE} | grep -E '^alauda-container-platform|^alauda-devops|^global-asm|^asm-init' | awk '{print $1}')
do
    kubectl patch hr $i -n ${ACP_NAMESPACE} -p '{"metadata":{"finalizers": []}}' --type=merge
    sleep 3
    kubectl get rel -n ${ACP_NAMESPACE} | grep ^$(kubectl get hr -n ${ACP_NAMESPACE} $i -o jsonpath='{.spec.releaseName}').v | awk '{print $1}' | xargs kubectl delete rel -n ${ACP_NAMESPACE}
    kubectl delete hr -n ${ACP_NAMESPACE} $i
done
```

* 检查，执行如下命令：
	
	`kubectl get hr -n ${ACP_NAMESPACE} | grep -E '^alauda-container-platform|^alauda-devops|^global-asm|^asm-init' | awk '{print $1}'`

如果找不到上述 chart 的 hr 资源，就证明删除 hr 资源成功

#### 第九步，升级alauda-cloud-enterprise

* 执行位置：global 集群的第一个 master 上
* 操作步骤：执行如下命令
	
```
kubectl get hr -n ${ACP_NAMESPACE} alauda-cloud-enterprise -o json >/tmp/alauda-cloud-enterprise.json
helm get values sentry --output json | jq . > /tmp/sentry_values.json


cat <<EOF >/tmp/apprelease-ace3.yaml
apiVersion: operator.alauda.io/v1alpha1
kind: AppRelease
metadata:
  name: alauda-cloud-enterprise
  namespace: $(cat /tmp/sentry_values.json | jq .global.namespace | sed 's/"//g')
spec:
  chart:
    name: alauda-cloud-enterprise
    repository: $(cat /tmp/sentry_values.json | jq .charts.repository | sed 's/"//g')
    version: $(helm search | grep '^stable/alauda-cloud-enterprise ' | awk '{print $2}')
  values:
    global:
      tke: true
      namespace: $(cat /tmp/sentry_values.json | jq .global.namespace | sed 's/"//g')
      host: $(cat /tmp/sentry_values.json | jq .global.host | sed 's/"//g' | grep -v -E '([^0-9]|\b)((1[0-9]{2}|2[0-4][0-9]|25[0-5]|[1-9][0-9]|[0-9])\.){3}(1[0-9][0-9]|2[0-4][0-9]|25[0-5]|[1-9][0-9]|[0-9])([^0-9]|\b)')
      registry:
        address: $(cat /tmp/sentry_values.json | jq .global.registry.address | sed 's/"//g')
      labelBaseDomain: $(cat /tmp/sentry_values.json | jq .global.labelBaseDomain | sed 's/"//g')
    alaudaConsole:
      apiAddress: $(cat /tmp/sentry_values.json | jq .global.scheme | sed 's/"//g')://$(cat /tmp/sentry_values.json | jq .global.host | sed 's/"//g')
      oidcIssuerUrl: $(cat /tmp/sentry_values.json | jq .global.scheme | sed 's/"//g')://$(cat /tmp/sentry_values.json | jq .global.host | sed 's/"//g')/dex
      oidcRedirectUrl: https://$(cat /tmp/sentry_values.json | jq .global.host | sed 's/"//g')
  version: 0.0.9
EOF
kubectl apply -f /tmp/apprelease-ace3.yaml
```

* 检查，执行如下命令：
	
	`kubectl get apprelease -n ${ACP_NAMESPACE} | grep -E '^alauda-cloud-enterprise'`
如果alauda-cloud-enterprise 的状态是deployed 就证明安装成功，版本是 v2.8.x 就证明升级成功

#### 第十步，删掉 alauda-cloud-enterprise 的 helmrequest 和 release 资源

* 执行位置：global 集群的第一个 master 上
* 操作步骤：执行如下命令

***注：*** 一定保证上一步已经升级了这四个 chart ，如果上一步没升级，这一步直接复制粘贴执行命令，等到恢复 captain 之后，没升级的 chart 就会被删掉，造成数据丢失

```
for i in $(kubectl get hr -n ${ACP_NAMESPACE} | grep -E '^alauda-cloud-enterprise' | awk '{print $1}')
do
    kubectl patch hr $i -n ${ACP_NAMESPACE} -p '{"metadata":{"finalizers": []}}' --type=merge
    sleep 3
    kubectl get rel -n ${ACP_NAMESPACE} | grep ^$(kubectl get hr -n ${ACP_NAMESPACE} $i -o jsonpath='{.spec.releaseName}').v | awk '{print $1}' | xargs kubectl delete rel -n ${ACP_NAMESPACE}
    kubectl delete hr -n ${ACP_NAMESPACE} $i
done
```

* 检查，执行如下命令：
	
	`kubectl get hr -n ${ACP_NAMESPACE} | grep -E '^alauda-cloud-enterprise' `

如果找不到上述 chart 的 hr 资源，就证明删除 hr 资源成功

#### 第十一步，升级 global-aml

* 执行位置：global 集群的第一个 master 上
* 操作步骤：执行如下命令
	
```
kubectl get hr -n ${ACP_NAMESPACE} alauda-cloud-enterprise -o json >/tmp/alauda-cloud-enterprise.json
helm get values sentry --output json | jq . > /tmp/sentry_values.json


cat <<EOF >/tmp/apprelease-global-aml.yaml
apiVersion: operator.alauda.io/v1alpha1
kind: AppRelease
metadata:
  name: global-aml
  namespace: $(cat /tmp/sentry_values.json | jq .global.namespace | sed 's/"//g')
spec:
  chart:
    name: global-aml
    repository: $(cat /tmp/sentry_values.json | jq .charts.repository | sed 's/"//g')
    version: $(helm search | grep '^stable/global-aml ' | awk '{print $2}')
  values:
    global:
      tke: true
      namespace: $(cat /tmp/sentry_values.json | jq .global.namespace | sed 's/"//g')
      host: $(cat /tmp/sentry_values.json | jq .global.host | sed 's/"//g' | grep -v -E '([^0-9]|\b)((1[0-9]{2}|2[0-4][0-9]|25[0-5]|[1-9][0-9]|[0-9])\.){3}(1[0-9][0-9]|2[0-4][0-9]|25[0-5]|[1-9][0-9]|[0-9])([^0-9]|\b)')
      registry:
        address: $(cat /tmp/sentry_values.json | jq .global.registry.address | sed 's/"//g')
      labelBaseDomain: $(cat /tmp/sentry_values.json | jq .global.labelBaseDomain | sed 's/"//g')
    alaudaConsole:
      apiAddress: $(cat /tmp/sentry_values.json | jq .global.scheme | sed 's/"//g')://$(cat /tmp/sentry_values.json | jq .global.host | sed 's/"//g')
      oidcIssuerUrl: $(cat /tmp/sentry_values.json | jq .global.scheme | sed 's/"//g')://$(cat /tmp/sentry_values.json | jq .global.host | sed 's/"//g')/dex
      oidcRedirectUrl: https://$(cat /tmp/sentry_values.json | jq .global.host | sed 's/"//g')
  version: 0.0.9
EOF

kubectl apply -f /tmp/apprelease-global-aml.yaml
```

* 检查，执行如下命令：
	
	`kubectl get apprelease -n ${ACP_NAMESPACE} | grep -E '^global-aml'`
如果 global-aml 的状态是deployed 就证明安装成功，版本是 v2.8.x 就证明升级成功


#### 第十二步，删掉 global-aml 的 helmrequest 和 release 资源

* 执行位置：global 集群的第一个 master 上
* 操作步骤：执行如下命令

***注：*** 一定保证上一步已经升级了这个 chart ，如果上一步没升级，这一步直接复制粘贴执行命令，等到恢复 captain 之后，没升级的 chart 就会被删掉，造成数据丢失

```
for i in $(kubectl get hr -n ${ACP_NAMESPACE} | grep -E '^global-aml' | awk '{print $1}')
do
    kubectl patch hr $i -n ${ACP_NAMESPACE} -p '{"metadata":{"finalizers": []}}' --type=merge
    sleep 3
    kubectl get rel -n ${ACP_NAMESPACE} | grep ^$(kubectl get hr -n ${ACP_NAMESPACE} $i -o jsonpath='{.spec.releaseName}').v | awk '{print $1}' | xargs kubectl delete rel -n ${ACP_NAMESPACE}
    kubectl delete hr -n ${ACP_NAMESPACE} $i
done
```

* 检查，执行如下命令：
	
	`kubectl get hr -n ${ACP_NAMESPACE} | grep -E '^global-aml' `

如果找不到上述 chart 的 hr 资源，就证明删除 hr 资源成功

#### 第十三步， global 其他 chart 
##### 其他 chart

*  public-chart-repo 不支持升级
*  download-server 不支持升级
*  dashboard 不支持升级
*  amp 相关的 amp、amp-kong、amp-minio、kong-cluster 不支持升级

	
##### devops 升级	

* alauda-devops 在上面的升级中，alauda-devops 已经升级完毕
* devops 业务组件升级，请看集群升级部分
		
##### tke 升级

* tke 在2.8 已经合并到 aluada-base 内，无需操作

##### asm 升级

* global-asm 在上面的升级中，global-asm 已经升级完毕
* asm-init 在上面的升级中，asm-init 已经升级完毕
* asm 业务集群组件升级，请看下面集群组件升级


##### 启动认证组件

* 执行位置：global 集群的第一个 master 上
* 操作步骤：执行如下命令

	```
	ACP_NAMESPACE=<cpaas-system>     ## 改成部署时， --acp2-namespaces 参数指定的值，默认是cpaas-system
	kubectl scale deployment -n ${ACP_NAMESPACE} auth-controller2 dex --replicas=2
	```


##### 数据初始化

* 执行位置：global 集群的第一个 master 上
* 操作步骤：执行如下命令

	```
	kubectl exec -it -n $(kubectl get po --all-namespaces | grep courier | awk '{print $1" "$2}' | head -n 1) python /migration/migrate.py   #初始化 courier 数据
	kubectl exec -it -n $(kubectl get po --all-namespaces | grep morgans | awk '{print $1" "$2}' | head -n 1) python /morgans/migration/api/migrate.py   # 初始化 morgans 数据
	```

至此，global 组件升级完毕，可以正常访问了


#### 第十四步，业务服务集群升级，通过 sentry 进行 alauda-cluster-base、cert-manager、kube-prometheus、alauda-log-agent、nginx-ingress、prometheus-operator、cpaas-monitor 集群组件的 chart 升级

* 执行位置：业务集群的第一个 master 上
* 操作步骤：执行如下命令
	
```
#######  更新 helm 获取变量 
第一步，将升级最开始在global 的第一个 master 节点上执行命令获取的/tmp/values.json 文件拷贝到业务集群的第一个 master 的/tmp 目录

helm get values kube-prometheus --output json > /tmp/kube-prometheus.json
helm up

########   准备变量
isOCP=false                    # 业务集群是否是 ocp
containerEngine=docker         # 是否用的 docker
REGION_NAME=<cls-j2zvrr9t>     # 业务集群的名字
LABELBASEDOMAIN=$(cat /tmp/values.json | jq .global.labelBaseDomain | sed 's/"//g') ##注意，要检查这个变量是否为空，如果是空那就是从2.3 甚至更早的版本升级上来的，这个变量的值可以执行  kubectl get cm -n $(cat /tmp/values.json | jq .global.namespace | sed 's/"//g') ui-config -o json | jq .data.LABEL_BASE_DOMAIN 命令来查看
NORTROM_TOKEN=$(kubectl describe secret -n kube-system $(kubectl get secret -n kube-system | awk '/clusterrole-aggregation-controller-token/ {print $1}') | awk '/^token:/{print $2}')

############## 生成 yaml
mkdir /cpaas
cat <<EOF >/cpaas/sentry_values.yaml
global:
  host: $(cat /tmp/values.json | jq .global.host | sed 's/"//g')
  region: ${REGION_NAME}
  auth:
    defaultAdmin: $(cat /tmp/values.json | jq .global.auth.default_admin | sed 's/"//g')
  namespace: $(cat /tmp/values.json | jq .global.namespace | sed 's/"//g')
  registry:
    address: $(cat /tmp/values.json | jq .global.registry.address | sed 's/"//g')
  labelBaseDomain: ${LABELBASEDOMAIN}
args:
  workers: 2
charts:
  repository: $(cat /tmp/values.json | jq .furion.chartmuseumUrl | sed 's/"//g')
  versions:
    cpaasAppreleases: $(helm search | grep '^stable/cpaas-appreleases ' | awk '{print $2}')
    nginxIngress: $(helm search | grep '^stable/nginx-ingress ' | awk '{print $2}')
    certManager: $(helm search | grep '^stable/cert-manager ' | awk '{print $2}')
    clusterBase: $(helm search | grep '^stable/alauda-cluster-base ' | awk '{print $2}')
    kubePrometheus: $(helm search | grep '^stable/kube-prometheus ' | awk '{print $2}')
    prometheusOperator: $(helm search | grep '^stable/prometheus-operator ' | awk '{print $2}')
    logAgent: $(helm search | grep '^stable/alauda-log-agent ' | awk '{print $2}')
    cpaasMonitor: $(helm search | grep '^stable/cpaas-monitor ' | awk '{print $2}')
    
platform:
  install: false

clusterPlatform:
  install: true

  logAgent:
    install: true
    region: ${REGION_NAME}
    token: ${NORTROM_TOKEN}
    isOCP: ${isOCP}
    containerEngine: ${containerEngine}

  kubePrometheus:
    install: true
    serviceType: $(cat /tmp/kube-prometheus.json | jq .grafana.service.type | sed -e 's/"//g')
    localVolumePath: $(cat /tmp/kube-prometheus.json | jq .grafana.storageSpec.persistentVolumeSpec.local.path | sed -e 's/"//g' -e 's#/[a-z]*$##' -e 's/null//g')
    storageClassName: $(cat /tmp/kube-prometheus.json | jq .grafana.storageSpec.volumeClaimTemplate.spec.storageClassName | sed -e 's/"//g')
    platform: ACP

  prometheusOperator:
    install: true

EOF
```

***注：*** 执行完毕之后，检查 sentry_values.yaml 确保没有key 的值未更新，如果平台是通过 ip 而不是域名访问，global.host 的值可能为空，把这个值改成部署时，--domain-name 参数指定的值，然后执行如下命令创建 cm

```
cat <<EOF > /cpaas/.run_install_sentry.sh
helm install --name acp-business \\
     -f /cpaas/sentry_values.yaml \\
     --debug --wait --timeout 3000 \\
     --namespace=$(cat /tmp/values.json | jq .global.namespace | sed 's/"//g') \\
     stable/sentry

EOF
##  检查一下  /cpaas/.run_install_sentry.sh 脚本中的参数，没有空，即可执行这个脚本，升级 global

bash /cpaas/.run_install_sentry.sh
```


* 删除 cert-manager 资源
* 执行位置：业务集群的第一个 master 上
* 操作步骤：执行如下命令

```
kubectl delete deploy cert-manager cert-manager-cainjector cert-manager-webhook -n cert-manager 
kubectl delete svc cert-manager cert-manager-webhook -n cert-manager

kubectl delete MutatingWebhookConfiguration cert-manager-webhook
kubectl delete ValidatingWebhookConfiguration cert-manager-webhook

kubectl delete apiservices v1beta1.admission.certmanager.k8s.io
```

* 安装 sentry
* 执行位置：业务集群的第一个 master 上
* 操作步骤：执行如下命令

`bash /cpaas/.run_install_sentry.sh`

* 检查，执行`kubectl get apprelease -n cpaas-system` 查看 apprelease 资源状态是否正常

#### 第十五步，清理 helm 和 captain 安装的 alauda-cluster-base、cert-manager、kube-prometheus、alauda-log-agent、nginx-ingress、prometheus-operator 和cpaas-monitor chart 的记录

* 如果使用 helm 安装业务集群的 chart
* 执行位置：业务集群的第一个 master 上
* 操作步骤：执行如下命令

```
kubectl delete cm -n kube-system $(kubectl get cm -n kube-system | grep -E '^alauda-cluster-base.v|^cert-manager.v|^kube-prometheus.v|^alauda-log-agent.v|^nginx-ingress.v|^prometheus-operator.v|^cpaas-monitor.v' | awk '{print $1}')
```
	
* 检查，执行如下命令：
	
	`helm list -a | grep -E '^alauda-cluster-base|^cert-manager|^kube-prometheus|^alauda-log-agent|^nginx-ingress|^prometheus-operator|^cpaas-monitor' `
	
	找不到上述 chart 就是成功了
	
* 如果使用 captain 安装业务集群的 chart ，且创建的 helmrequest 资源在 global 集群上
* 执行位置： global 集群的第一个 master 上
* 操作步骤：执行如下命令

***注：*** 一定保证上一步已经升级了这四个 chart ，如果上一步没升级，这一步直接复制粘贴执行命令，等到恢复 captain 之后，没升级的 chart 就会被删掉，造成数据丢失

```
REGION_NAME=<cls-j2zvrr9t>     # 业务集群的名字
ACP_NAMESPACE=<cpaas-system>     ## 改成部署时， --acp2-namespaces 参数指定的值，默认是cpaas-system

cat <<EOF >/tmp/region_chart_name
stable/alauda-cluster-base
stable/cert-manager
stable/kube-prometheus
stable/alauda-log-agent
stable/nginx-ingress
stable/prometheus-operator
stable/cpaas-monitor
EOF

>/tmp/region_hr_name
for i in $(kubectl get hr -n ${ACP_NAMESPACE} | sed 1d | awk '{print $1}' )
do
    if [ "$(kubectl get hr -n ${ACP_NAMESPACE} $i -o jsonpath='{.spec.clusterName}')z" == "${REGION_NAME}z" ]
    then
        grep $(kubectl get hr -n ${ACP_NAMESPACE} $i -o jsonpath='{.spec.chart}') /tmp/region_chart_name && echo $i >>/tmp/region_hr_name
    fi
done

for i in $(cat /tmp/region_hr_name)
do
    kubectl patch hr $i -n ${ACP_NAMESPACE} -p '{"metadata":{"finalizers": []}}' --type=merge
    sleep 3
    kubectl get rel -n ${ACP_NAMESPACE} | grep ^$(kubectl get hr -n ${ACP_NAMESPACE} $i -o jsonpath='{.spec.releaseName}').v | awk '{print $1}' | xargs kubectl delete rel -n ${ACP_NAMESPACE}
    kubectl delete hr -n ${ACP_NAMESPACE} $i
done
```

* 检查，执行如下命令：
	
	`kubectl get hr -n ${ACP_NAMESPACE} | grep -E '^alauda-container-platform|^alauda-devops|^global-asm|^asm-init' | awk '{print $1}'`

如果找不到上述 chart 的 hr 资源，就证明删除 hr 资源成功

*** 删除业务集群的 release 资源*** 

* 执行位置：业务集群的第一个 master 上
* 拷贝 global 集群的第一个 master 上的/tmp/region_hr_name 文件到业务集群的第一个 master 的/tmp 目录下
* 操作步骤：执行如下命令

```
ACP_NAMESPACE=<cpaas-system>     ## 改成部署时， --acp2-namespaces 参数指定的值，默认是cpaas-system

for i in $(cat /tmp/region_hr_name)
do
    kubectl patch hr $i -n ${ACP_NAMESPACE} -p '{"metadata":{"finalizers": []}}' --type=merge
    sleep 3
    kubectl get rel -n ${ACP_NAMESPACE} | grep ^$(kubectl get hr -n ${ACP_NAMESPACE} $i -o jsonpath='{.spec.releaseName}').v | awk '{print $1}' | xargs kubectl delete rel -n ${ACP_NAMESPACE}
    kubectl delete hr -n ${ACP_NAMESPACE} $i
done
```
#### 第十六步 ake环境升级后对接业务集群

**注1 **： 因为从2.8 开始，集群管理统一更换成 tke。之前用 ake 管理集群的平台升级之后，需要按下面的步骤操作。2.6 在升级前就是通过 tke 管理集群的，可以跳过这一步，但建议执行第一步进行检查，确保没有遗漏的集群。
**注2 **： 本操作步骤的目的是将老的ake环境的集群资源需要迁移一份到tke的定义上，下面的内容描述了如何迁移以及需要迁移那些集群。

1. 确定那些集群需要迁移操作，在 global 集群的第一个 master 上执行如下命令：

```
ACP_NAMESPACE=<cpaas-system>         ## 改成部署时， --acp2-namespaces 参数指定的值，默认是cpaas-system

kubectl get clusters.clusterregistry.k8s.io -n ${ACP_NAMESPACE}    # 获取到所有的ake集群名字
kubectl get cluster.platform.tke.cloud.tencent.com                             # 获取所有tke集群名字
```
所有名字在ake集群中存在的，但是tke集群中不存在的都需要迁移

2. 开始迁移，下面的例子假设要迁移的集群的名字是 calico ，首先在 global 集群的第一个 master 上执行下面的命令获取cluster资源信息

```
ACP_NAMESPACE=<cpaas-system>         ## 改成部署时， --acp2-namespaces 参数指定的值，默认是cpaas-system
REGION_NAME=<calico>                         ## 上一步获取到的要执行迁移操作的集群的名字

apiserveraddr=($(kubectl get clusters.clusterregistry.k8s.io -n ${ACP_NAMESPACE} calico -o jsonpath='{.spec.kubernetesApiEndpoints.serverEndpoints}' | sed -e 's#.*//##' -e 's/]//g' -e 's/:/ /g' ))
cat <<EOF >/tmp/cluster.yaml
{
  "kind": "Cluster",
  "apiVersion": "platform.tke.cloud.tencent.com/v1",
  "metadata": {
    "name": "${REGION_NAME}"
  },
  "spec": {
    "displayName": "${REGION_NAME}",
    "tenantID": "",
    "type": "Imported",
    "version": ""
  },
  "status": {
    "addresses": [
      {
        "host": "${apiserveraddr[0]}",
        "type": "Advertise",
        "port": ${apiserveraddr[1]}
      }
    ]
  }
}
EOF
```

3.  在要迁移的业务集群的第一个 master 上执行下面的命令，获取token和CA证书，准备clustercredential资源

```
REGION_NAME=<calico>                         ## 上一步获取到的要执行迁移操作的集群的名字

clustertoke=$(kubectl get secret -n kube-system | awk '/clusterrole-aggregation-controller-token/{print $1}')
kubectl get secret -n kube-system ${clustertoke} -o json | jq .data >/tmp/secret.data
k8s_token=$(echo $(jq .token /tmp/secret.data | sed 's/"//g') | base64 -d )
cat <<EOF > /tmp/clustercredential.yaml
{
  "kind": "ClusterCredential",
  "apiVersion": "platform.tke.cloud.tencent.com/v1",
  "clusterName": "${REGION_NAME}",
  "tenantID": "",
  "metadata": {
    "name": "${REGION_NAME}",
    "generateName": "clustercredential"
  },
  "caCert": "$(awk '/ca.crt/{print $2}' /tmp/secret.data | sed -e 's/"//g' -e 's/,//g')",
  "token": "$k8s_token"
}
EOF
```

4. 在global创建cluster资源，将上一步创建的 `/tmp/clustercredential.yaml` 文件拷贝到 global 集群的第一台 master 的/tmp 目录下，然后去 global 集群的第一个 master 上执行如下命令：

```
kubectl create -f /tmp/cluster.yaml
kubectl create -f /tmp/clustercredential.yaml
```

#### 第十七步，恢复Mutatingwebhook，恢复 cpatain 

* 执行位置：global 集群的第一个 master 上
* 操作步骤：执行如下命令

```
ACP_NAMESPACE=<cpaas-system>     ## 改成部署时， --acp2-namespaces 参数指定的值，默认是cpaas-system

kubectl apply -f /cpaas/m.yaml
kubectl apply -f /cpaas/v.yaml
kubectl scale deploy -n ${ACP_NAMESPACE} captain-controller-manager --replicas=1
```

至此，通过 sentry 升级 glboal 集群和业务服务集群的 chart 结束，下面还有不用 sentry 管理的 chart 的升级工作。



### 升级 asm 业务集群组件

1. 在业务集群删除asm-controller-webhook的service

*  执行位置：安装了 asm 的业务集群的第一个 master 上，执行如下命令:


例如此服务在cpaas-system下 `kubectl -n cpaas-system delete service asm-controller-webhook`

2. 升级helmrequest的版本信息进入global集群操作（helmrequest是在global集群的)查询asm-init的helmrequest，将其版本信息，改成新版本的chart的版本信，注意 ${region_name} 变量是对应业务集群名字。

*  执行位置：global集群的第一个 master 上，执行如下命令:

```
ACP_NAMESPACE=<cpaas-system>     ## 改成部署时， --acp2-namespaces 参数指定的值，默认是cpaas-system
region_name=<集群名字>
kubectl -n ${ACP_NAMESPACE} edit  hr asm-${region_name}-asm-init
kubectl -n ${ACP_NAMESPACE} edit  hr asm-${region_name}-istio-init
kubectl -n ${ACP_NAMESPACE} edit  hr asm-${region_name}-jaeger-operator
kubectl -n ${ACP_NAMESPACE} edit  hr asm-${region_name}-alauda-service-mesh
kubectl -n ${ACP_NAMESPACE} edit  hr asm-${region_name}-istio
```

例如：修改asm-asm-asm-init的版本号信息

```
apiVersion: app.alauda.io/v1alpha1
kind: HelmRequest
metadata:
  annotations:
    kubectl.kubernetes.io/last-applied-configuration: |
      {"apiVersion":"app.alauda.io/v1alpha1","kind":"HelmRequest","metadata":{"annotations":{},"name":"asm-asm-asm-init","namespace":"alauda-system"},"spec":{"chart":"release/asm-init","namespace":"alauda-system","releaseName":"asm-asm-asm-init","values":{"install":{"mode":"global"}},"version":"v2.2.9"}}
  creationTimestamp: "2019-10-17T07:37:28Z"
  finalizers:
  - captain.alauda.io
  generation: 1
  name: asm-asm-asm-init
  namespace: alauda-system
  resourceVersion: "94086381"
  selfLink: /apis/app.alauda.io/v1alpha1/namespaces/alauda-system/helmrequests/asm-asm-asm-init
  uid: f8a6e22e-f0b0-11e9-b758-ea3e455b3c6e
spec:
  chart: release/asm-init
  namespace: alauda-system
  releaseName: asm-asm-asm-init
  values:
    install:
      mode: global
  version: v2.2.9                     # 换成新版本的chart的版本信息
```

3. 升级完成后，删除controller证书

*  执行位置：global集群的第一个 master 上，执行如下命令:

```
ACP_NAMESPACE=<cpaas-system>     ## 改成部署时， --acp2-namespaces 参数指定的值，默认是cpaas-system

a. 查到这个secret

     kubectl get secrets -n ${ACP_NAMESPACE} --show-labels |grep asm-controller-webhook-serving-cert

b. 删除这个secret

     kubectl delete secrets -n ${ACP_NAMESPACE} asm-controller-webhook-serving-cert

c.  重启asm-controller的pod，即删除asm-controller的pod

     kubectl -n ${ACP_NAMESPACE} delete po -l app=asm-controller
```
	
### dashboard 升级

* 2.8 和2.6 相比，dashboard 没有变化，无需升级



### alb升级

* 执行位置：平台 UI
* 操作步骤：执行如下命令

	1. 用管理员身份登录平台，单击左上角的图标切换产品，选择 Container Platform 。然后单击右上角的视图开关进入管理视图，如下图所示。
	
		<img src="images/升级 alb 1.png" style="zoom:50%;" />
	
	2. 单击左侧导航栏的资源管理，选择正确的 ns 和集群，找到 helmrequest 这个资源，单击更新，如下图所示。
	
		<img src="images/升级 alb 2.png" style="zoom:50%;" />
	
	3. 在 spec 下修改 version 这个 key，如果没有，添加 version。这个 key 的值就是你希望升级 alb 的目标版本，在集群的 master 下，执行 `helm search | grep alb` 就能找到最新的 alb 的版本，如下图所示。
	
		<img src="images/升级 alb 3.png" style="zoom:50%;" />
	
	4. 修改完毕后，点击右下角的更新，稍等片刻 alb 就开始更新了。
	
	5. 在集群上，执行 `kubectl get deploy -n <ns> <alb 名称> -o wide` 检查 alb 的 deploy 的状态，看看版本是否变化确认升级是否成功，如果一直卡在0/n状态，执行`kubectl get rs -n <ns> | grep <alb名称>` 检查下。如果发现新增了 pod ，但是老的 ingress 的 pod 还存在，这是更新过程中，label 问题造成的，alb、ingress 和 dashboard 这三个组件有一定几率出现这个问题，选择删掉这三个组件的老的 deploy ，然后继续按文档操作即可。
	6.  migration，进入 alb 的 pod 内，执行 `./alb/migrate_v26tov28 --dry-run=false`

**注意**：升级之后会造成 alb 的一些规则不兼容，需要重建
  

###  Tapp 升级

* 执行位置：每一个集群的第一个 master 上
* 操作步骤：执行如下命令

	```
	helm upgrade tapp-controller stable/tapp-controller
	```

### gitlab-ce 升级

* 执行位置：每一个集群的第一个 master 上
* 操作步骤：执行如下命令

	```
	helm upgrade --recreate-pods gitlab-ce stable/gitlab-ce
	```

### harbor 升级

* 执行位置：每一个集群的第一个 master 上
* 操作步骤：执行如下命令

	```
	helm upgrade --recreate-pods harbor stable/harbor
	```

### nexus 升级

* 执行位置：每一个集群的第一个 master 上
* 操作步骤：执行如下命令

	```
	helm upgrade --recreate-pods nexus stable/nexus
	```
	
### sonarqube 升级

* 执行位置：每一个集群的第一个 master 上
* 操作步骤：执行如下命令

	```
	helm upgrade --recreate-pods sonarqube stable/sonarqube
	```


### jenkins 升级

* 执行位置：每一个集群的第一个 master 上
* 操作步骤：执行如下命令

	```
	helm upgrade --recreate-pods jenkins stable/jenkins
	```
    jenkins组件升级后，需要根据最新版本的包，手工更新jenkins系统配置里面的镜像， 比如更新golang的镜像
	1. helm inspect stable/jenkins 找到golang12镜像版本，有如下两个

```
	 golang12ubuntu:
      repository: alaudaorg/builder-go
      tag: 1.12-ubuntu-v2.6.0
	golang12Alpine:
      repository: alaudaorg/builder-go
      tag: 1.12-alpine-v2.6.0
```

	2.登陆jenkins->manage jenkins->configure system 搜索找到模版名称golang1.12 然后根据原来镜像tag是含有ubuntu还是alpine更新为相应的镜像tag
	3.apply并save即可

### 注意事项

#### 2.8 审计重构，需要修改 kube-api 的设置

想要获取审计数据，请参考 2.8 的部署文档中审计相关的内容，去配置 kube-api 。**注意**： 如果在2.6 已经使用了审计功能，修改 kube-api 的时候，不仅仅要增加2.8所需要的 audit 的配置，还需要将2.6 所带的 audit 的配置删掉，具体请看部署文档

#### devops-controller 中的 toolx 下的 yaml 更新问题

如果 Kubernetes 集群中已经存在相应的 tooltype 的资源，toolx 下的 yaml 更新可能不会更新 Kubernetes 集群中已经存在的 resource。

在 v2.6 中，更新的解决方案是：在 Kubernetes 集群中删除想要更新的 tooltype 的 resource 后重启 devops-controller。

示例：更新 name 为 jira 的 tooltype

1. 执行命令：`kubectl delete tooltype jira`

2. 执行命令重启 devops-controller。

	```
	ACP_NAMESPACE=<alauda-system>     # ns
	DC=`kubectl -n ${ACP_NAMESPACE} get pod |grep devops-controller|wc -l` && kubectl -n ${ACP_NAMESPACE} scale deploy/devops-controller --replicas=0 && kubectl -n ${ACP_NAMESPACE} scale deploy/devops-controller --replicas=$DC
	```

## 升级结束之后的检查


### 按照测试用例，测试升级之后功能是否正常


## 回滚

***注意：***恢复的顺序是 global 集群 、 业务集群。如果只有业务集群升级失败，不支持只回滚业务集群，必须都要回滚，因为 global 集群上，存有业务集群的 hr 资源。

1. 获取 etcd 地址，并停掉所有(业务集群和 global 集群） master 节点上的 kubelet 服务。

        **执行命令的环境：** 业务集群和 global 集群的所有 master 节点上

        **执行的命令：** ```ETCD_SERVER=($(kubectl get pod -n kube-system $(kubectl get pod -n kube-system | grep etcd | awk 'NR==1 {print $1}') -o yaml | awk '/--initial-cluster=/{print}' | sed -e 's/,/ /g' -e's/^.*cluster=//' | sed -e 's#[0-9\.]*=https://##g' -e 's/:2380//g')) ； systemctl stop kubelet```

2. 删掉 kube-apiserver 容器，目的是恢复过程和恢复之后，global 不要通过调用 kubeapi 写数据到业务集群的 etcd 内。

        **执行命令的环境：** 要恢复 Kubernetes 集群的所有 master 节点上

        **执行的命令：** ```docker rm -f $(docker ps -a | awk '/_kube-api/{print $NF}')```

        **命令的结果：** 所有 kube-api 的容器都被删除。


3.  判断 etcdctl 命令是否存在第一台 master 节点上，一般执行 `backup_recovery.sh` 这个备份 etcd 的脚本，会自动将 etcdctl 拷贝到 `/usr/bin/etcdctl`，如果不存在，需要自行手动拷贝出来。

        **执行命令的环境：** 要恢复 Kubernetes 集群的第一台 master 节点上

        **执行的命令：** ```whereis etcdctl```

        **命令的结果：** 应该打印处 etcdctl 的路径，如果没有就表明是错的。

4. 通过备份的快照恢复 etcd。

        **执行命令的环境：** 要恢复 Kubernetes 集群的第一台 master 节点上

        **执行的命令：**

        ```
        mkdir /tmp/dd
        ETCD_SERVER=($(kubectl get pod -n kube-system $(kubectl get pod -n kube-system | grep etcd | awk 'NR==1 {print $1}') -o yaml | awk '/--initial-cluster=/{print}' | sed -e 's/,/ /g' -e's/^.*cluster=//' | sed -e 's#[0-9\.]*=https://##g' -e 's/:2380//g'))  ## 这个地址在回滚的第一步已经获取了，执行回滚之前，echo 这个变量检查地址是否获取成功
        snapshot_db=<备份时，导出的 etcd 快照文件名，必须是绝对路径>
        for i in ${ETCD_SERVER[@]}
        do
            export ETCDCTL_API=3
            etcdctl snapshot restore ${snapshot_db} \
            --cert=/etc/kubernetes/pki/etcd/server.crt \
            --key=/etc/kubernetes/pki/etcd/server.key \
            --cacert=/etc/kubernetes/pki/etcd/ca.crt \
            --data-dir=/tmp/dd/etcd \
            --name ${i} \
            --initial-cluster ${ETCD_SERVER[0]}=https://${ETCD_SERVER[0]}:2380,${ETCD_SERVER[1]}=https://${ETCD_SERVER[1]}:2380,${ETCD_SERVER[2]}=https://${ETCD_SERVER[2]}:2380 \
            --initial-advertise-peer-urls https://$i:2380 && \
        mv /tmp/dd/etcd etcd_$i
        done
        ```

        **命令的结果：** 会生成 `etcd_<ip 地址>` 这样的三个目录，将这三个目录拷贝到对应ip的服务器的 `/root` 内。

5. 删掉 etcd 容器。

        **执行命令的环境：** 要恢复 Kubernetes 集群的所有 master 节点上

        **执行的命令：** ```docker rm -f $(docker ps -a | awk '/_etcd/{print $NF}')```

        **命令的结果：** 所有 etcd 的容器都被删除。

6. 迁移恢复的数据。

        **执行命令的环境：** 要恢复 Kubernetes 集群的所有 master 节点上

        **执行的命令：**

        ```
        docker ps -a | awk '/_etcd/{print $NF}' ##确保没有 etcd 容器
        mv /var/lib/etcd/member /cpaas/backup
        mv /root/var/lib/etcd/member  /var/lib/etcd
        ```

        **命令的结果：** 会把 etcd 的数据挪到备份目录下，然后将上一步生成的目录拷贝到 `/var/lib/etcd` 里。

7.  启动 etcd 和 kube-api。

        **执行命令的环境：** 要恢复 Kubernetes 集群的所有 master 节点上

        **执行的命令：** `systemctl start kubelet`

        **命令的结果：** kubelet 服务启动后，会自动创建 etcd 的 pod，这个时候执行 `docker ps -a | grep -E 'etcd|kube-api'` 会找到 etcd 和 kube-api 容器。

8.  回滚之后，如果出现在kubelet和页面查看所有资源都存在，但是在业务节点没有资源的情况，重启k8s 集群内所有节点的 kubelet 和 docker 服务，也可采用重启集群内所有服务器的方式来解决
